package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.framework.web.service.TokenService;
import com.ruoyi.system.domain.ProjectAuthorize;
import com.ruoyi.system.domain.ValidationGroups.*;
import com.ruoyi.system.service.IProductPropertyValueService;
import com.ruoyi.system.service.IProjectAuthorizeService;
import com.ruoyi.system.service.ISysDeptService;
import com.ruoyi.system.service.ISysUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/system/project/authorize")
public class ProjectAuthorizeController extends BaseController {
    @Autowired
    private IProjectAuthorizeService projectAuthorizeService;
    @Autowired
    private TokenService tokenService;
    @Autowired
    private ISysDeptService sysDeptService;
    @Autowired
    private ISysUserService sysUserService;

    @PreAuthorize("@ss.hasPermi('system:project:authorize:query_user_info')")
    @GetMapping("/query_user_info")
    public TableDataInfo queryUserInfo(@Validated(Select.class) ProjectAuthorize projectAuthorize){
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        // 设置companyId
        Long companyId = sysDeptService.selectCompanyIdByUser(loginUser.getUser());
        //验证是否授权
        boolean commonRole = sysUserService.checkUserIsCommonRole(loginUser.getUser());
        if (!commonRole){
            projectAuthorizeService.checkProjectIsAuthorizeWithProjectId(loginUser.getUser(),projectAuthorize.getProjectId());
        }
        projectAuthorize.setCompanyId(companyId);
        //startPage();
        List<ProjectAuthorize> projectAuthorizes = projectAuthorizeService.queryAllUserInfo(projectAuthorize);
        return getDataTable(projectAuthorizes);
    }
    @PreAuthorize("@ss.hasPermi('system:project:authorize:authorize_user')")
    @GetMapping("/query_authorize_user_info")
    public TableDataInfo queryAuthorizeUserInfo(@Validated(Select.class) ProjectAuthorize projectAuthorize){
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        // 设置companyId
        Long companyId = sysDeptService.selectCompanyIdByUser(loginUser.getUser());
        //验证是否授权
        boolean commonRole = sysUserService.checkUserIsCommonRole(loginUser.getUser());
        if (!commonRole){
            projectAuthorizeService.checkProjectIsAuthorizeWithProjectId(loginUser.getUser(),projectAuthorize.getProjectId());
        }
        startPage();
        List<ProjectAuthorize> projectAuthorizes = projectAuthorizeService.queryAuthorizeUserInfo(projectAuthorize);
        return getDataTable(projectAuthorizes);
    }
    @PreAuthorize("@ss.hasPermi('system:project:authorize:enum')")
    @GetMapping("/enum")
    public List<ProjectAuthorize> enumAuthorized(@Validated(Select.class) ProjectAuthorize projectAuthorize){
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        // 设置companyId
        Long companyId = sysDeptService.selectCompanyIdByUser(loginUser.getUser());
        boolean commonRoleFlag = sysUserService.checkUserIsCommonRole(loginUser.getUser());
        if (!commonRoleFlag){
            //验证是否授权
            projectAuthorizeService.checkProjectIsAuthorizeWithProjectId(loginUser.getUser(),projectAuthorize.getProjectId());
        }
        List<ProjectAuthorize> projectAuthorizes = projectAuthorizeService.queryAuthorizeUserInfo(projectAuthorize);
        return projectAuthorizes;
    }
    @PreAuthorize("@ss.hasPermi('system:project:authorize:add')")
    @PostMapping("/add")
    public AjaxResult add(@RequestBody @Validated(Create.class)List<ProjectAuthorize> projectAuthorizes){
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        // 设置companyId
        Long companyId = sysDeptService.selectCompanyIdByUser(loginUser.getUser());
        projectAuthorizes.stream().forEach(a ->{
            projectAuthorizeService.checkProjectIsAuthorizeWithProjectId(loginUser.getUser(),a.getProjectId());
            a.setCompanyId(companyId);
            a.setCreateBy(loginUser.getUsername());
            a.setCreateTime(new Date());
        });
        if (projectAuthorizeService.addBatch(projectAuthorizes)){
            return AjaxResult.success("授权成功");
        }
        return AjaxResult.error("授权失败");
    }

    /**
     * 解除项目授权信息
     */
    @PreAuthorize("@ss.hasPermi('system:project:authorize:remove')")
    @Log(title = "解除项目授权信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult delete(@PathVariable @NotNull(message = "id不能为null") String ids){
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        if (projectAuthorizeService.delete(ids,loginUser.getUser())){
            return AjaxResult.success("解除成功！");
        }
        return AjaxResult.error("解除失败！");
    }
}
