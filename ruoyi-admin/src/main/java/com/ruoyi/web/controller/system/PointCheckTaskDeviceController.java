package com.ruoyi.web.controller.system;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.SysDept;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.exception.CustomException;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.framework.web.service.TokenService;
import com.ruoyi.system.domain.PointCheckTaskDeal;
import com.ruoyi.system.domain.PointCheckTaskDevice;
import com.ruoyi.system.domain.ValidationGroups.*;
import com.ruoyi.system.domain.vo.PointCheckCreatReportVO;
import com.ruoyi.system.domain.vo.PointCheckDeviceDetailDTO;
import com.ruoyi.system.domain.vo.PointCheckPCReportVO;
import com.ruoyi.system.domain.vo.QueryVo;
import com.ruoyi.system.service.IPointCheckTaskDealService;
import com.ruoyi.system.service.IPointCheckTaskDeviceService;
import com.ruoyi.system.service.ISysDeptService;
import io.swagger.models.auth.In;
import lombok.extern.slf4j.Slf4j;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.awt.desktop.AboutHandler;
import java.util.Date;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/system/point_check_task_device")
public class PointCheckTaskDeviceController extends BaseController {

    @Autowired
    private TokenService tokenService;
    @Autowired
    private ISysDeptService sysDeptService;
    @Autowired
    private IPointCheckTaskDeviceService pointCheckTaskDeviceService;

    @PreAuthorize("@ss.hasPermi('system:point:check:task:device:list')")
    @GetMapping("/list")
    public TableDataInfo list(PointCheckTaskDevice pointCheckTaskDevice) {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        // 设置companyId
        Long companyId = sysDeptService.selectCompanyIdByUser(loginUser.getUser());
        pointCheckTaskDevice.setCompanyId(companyId);
        startPage();
        List<PointCheckTaskDevice> pointCheckTaskDeviceList = pointCheckTaskDeviceService.queryList(pointCheckTaskDevice);
        return getDataTable(pointCheckTaskDeviceList);
    }



    /**
     * 详情
     *
     * @param id
     * @return
     */
    @GetMapping("/get_info")
    public AjaxResult getInfo(Integer id) {
        PointCheckTaskDevice pointCheckTaskDevice = new PointCheckTaskDevice();
        pointCheckTaskDevice.setId(id);
        PointCheckTaskDevice result = pointCheckTaskDeviceService.getOne(pointCheckTaskDevice);
        return AjaxResult.success(result);
    }

//    @PreAuthorize("@ss.hasPermi('system:point:check:task:device:add')")
//    @PostMapping("/add")
//    public AjaxResult add(@RequestBody @Validated(Create.class) PointCheckTaskDevice pointCheckTaskDevice) {
//        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
//        // 设置companyId
//        Long companyId = sysDeptService.selectCompanyIdByUser(loginUser.getUser());
//        //验证该任务下产品是否存在
//        pointCheckTaskDevice.setCompanyId(companyId);
//        pointCheckTaskDevice.setCreateBy(loginUser.getUsername());
//        pointCheckTaskDevice.setCreateTime(new Date());
//        pointCheckTaskDeviceService.checkIsExist(pointCheckTaskDevice);
//        if (pointCheckTaskDeviceService.add(pointCheckTaskDevice)) {
//            return AjaxResult.success("添加成功");
//        }
//        return AjaxResult.error("添加失败");
//    }

//    /**
//     * 编辑
//     *
//     * @param pointCheckTaskDevice
//     * @return
//     */
//    @PreAuthorize("@ss.hasPermi('system:point:check:task:device:edit')")
//    @PostMapping("/edit")
//    public AjaxResult edit(@RequestBody @Validated(Update.class) PointCheckTaskDevice pointCheckTaskDevice) {
//        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
//        // 设置companyId
//        Long companyId = sysDeptService.selectCompanyIdByUser(loginUser.getUser());
//        //验证该任务下产品是否存在
//        pointCheckTaskDevice.setCompanyId(companyId);
//        pointCheckTaskDeviceService.checkIsExist(pointCheckTaskDevice);
//        if (pointCheckTaskDeviceService.update(pointCheckTaskDevice)) {
//            return AjaxResult.success("修改成功");
//        }
//        return AjaxResult.error("修改失败");
//    }

    /**
     * 删除
     *
     * @param id
     * @return
     */
    @PreAuthorize("@ss.hasPermi('system:point:check:task:device:delete')")
    @DeleteMapping("/{id}")
    public AjaxResult delete(@PathVariable @NotNull(message = "id不能为null") Integer id) {
        PointCheckTaskDevice param = new PointCheckTaskDevice();
        param.setId(id);
        if (pointCheckTaskDeviceService.delete(param)) {
            return AjaxResult.success("删除成功");
        }
        return AjaxResult.error("删除失败");
    }

    /**
     * 生成报告
     *
     * @param id
     * @return
     */
    @GetMapping("/create_report")
    public AjaxResult createReport(Integer id,Date startTime) {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        // 设置companyId
        Long companyId = sysDeptService.selectCompanyIdByUser(loginUser.getUser());
        QueryVo queryVo = new QueryVo();
        queryVo.filters.put("id",id);
        queryVo.filters.put("startTime",startTime);
        queryVo.filters.put("companyId",companyId);
        queryVo.filters.put("executor",loginUser.getUsername());
        List<PointCheckCreatReportVO> reports = pointCheckTaskDeviceService.createReport(queryVo);
        return AjaxResult.success(reports);
    }

    @GetMapping("/create_report_v2")
    public AjaxResult createReportV2(Integer taskId,Integer taskDeviceId) {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        // 设置companyId
        Long companyId = sysDeptService.selectCompanyIdByUser(loginUser.getUser());
        QueryVo queryVo = new QueryVo();
        queryVo.filters.put("id",taskDeviceId);
        queryVo.filters.put("companyId",companyId);
        queryVo.filters.put("executor",loginUser.getUsername());
        List<PointCheckCreatReportVO> reports = pointCheckTaskDeviceService.createReportV2(queryVo, taskId,taskDeviceId);
        return AjaxResult.success(reports);
    }


    @GetMapping("/quick_create_report")
    public AjaxResult quickCreateReport(Integer id) {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        // 设置companyId
        Long companyId = sysDeptService.selectCompanyIdByUser(loginUser.getUser());
        pointCheckTaskDeviceService.quickCreateReport(id, loginUser.getUser(), companyId);
        return AjaxResult.success("生成成功");
    }

    /**
     * PC端查询点检报告
     * @param sn  产品序列号
     * @param startTime 搜索时间
     * @return
     */
    @PreAuthorize("@ss.hasPermi('system:point:check:task:device:pc:get_report')")
    @GetMapping("/pc/get_report")
    public AjaxResult getReport(String sn, Date startTime){
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        // 设置companyId
        Long companyId = sysDeptService.selectCompanyIdByUser(loginUser.getUser());
        SysDept sysDept = sysDeptService.selectDeptById(companyId);
        QueryVo queryVo = new QueryVo();
        queryVo.filters.put("sn",sn);
        queryVo.filters.put("startTime",startTime);
        queryVo.filters.put("companyId",companyId);
        PointCheckPCReportVO report = pointCheckTaskDeviceService.getReport(queryVo);
        return AjaxResult.success(report);
    }

    @PreAuthorize("@ss.hasPermi('system:point:check:task:device:pc:get_report')")
    @GetMapping("/get_switchDevice")
    public AjaxResult getSwitchDevice(Date date, Integer type, Integer deviceId) {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        // 设置companyId
        Long companyId = sysDeptService.selectCompanyIdByUser(loginUser.getUser());
        QueryVo queryVo = new QueryVo();
        queryVo.filters.put("deviceId",deviceId);
        queryVo.filters.put("startTime",date);
        queryVo.filters.put("companyId",companyId);
        List<PointCheckTaskDevice> switchDevice = pointCheckTaskDeviceService.getSwitchDevice(queryVo, type);
        return AjaxResult.success(switchDevice);
    }


    @PreAuthorize("@ss.hasPermi('system:point:check:task:device:pc:get_report')")
    @GetMapping("/pc/get_report_v2")
    public AjaxResult getReportV2(Integer taskDeviceId,Integer taskId){
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        // 设置companyId
        Long companyId = sysDeptService.selectCompanyIdByUser(loginUser.getUser());
        SysDept sysDept = sysDeptService.selectDeptById(companyId);
        QueryVo queryVo = new QueryVo();
        queryVo.filters.put("companyId",companyId);
        PointCheckPCReportVO report = pointCheckTaskDeviceService.getReportV2(queryVo,taskDeviceId,taskId);
        return AjaxResult.success(report);
    }

    @PreAuthorize("@ss.hasPermi('system:point:check:task:device:get_device_detail')")
    @GetMapping("/get_device_detail")
    public AjaxResult getDeviceDetail(String sn){
        SysUser user = SecurityUtils.getLoginUser().getUser();
        Long companyId = sysDeptService.selectCompanyIdByUser(user);
        QueryVo queryVo = new QueryVo();
        queryVo.filters.put("executor",user.getUserName());
        queryVo.filters.put("company_id",companyId);
        queryVo.filters.put("sn",sn);
        queryVo.filters.put("now_date",new Date());
        PointCheckDeviceDetailDTO deviceDetail = pointCheckTaskDeviceService.getDeviceDetail(queryVo);
        return AjaxResult.success(deviceDetail);
    }
}
