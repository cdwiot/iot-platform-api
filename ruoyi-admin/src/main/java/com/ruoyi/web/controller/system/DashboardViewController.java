package com.ruoyi.web.controller.system;

import com.ruoyi.framework.web.service.TokenService;
import com.ruoyi.system.domain.DashboardView;
import com.ruoyi.system.domain.ValidationGroups.Update;
import com.ruoyi.system.domain.vo.QueryVo;
import com.ruoyi.system.service.ICustomPermissionService;
import com.ruoyi.system.service.IDashboardViewService;
import com.ruoyi.system.service.ISysDeptService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;

import lombok.extern.slf4j.Slf4j;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ruoyi.common.constant.HttpStatus;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.utils.MessageUtils;
import com.ruoyi.common.utils.ServletUtils;

@Slf4j
@RestController
@RequestMapping("/system/dashboard_view")
public class DashboardViewController extends BaseController {
    @Autowired
    private TokenService tokenService;

    @Autowired
    private ISysDeptService sysDeptService;

    @Autowired
    private IDashboardViewService dashboardViewService;
    @Autowired
    private ICustomPermissionService customPermissionService;


    @PreAuthorize("@ss.hasPermi('system:dashboard_view:get')")
    @GetMapping(value = "/get", produces = "application/json;charset=UTF-8")
    public AjaxResult load(@RequestParam(name = "projectId", required = false) Integer projectId) {
        if (projectId == null) {
            projectId = 0;
        }
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        Long companyId = sysDeptService.selectCompanyIdByUser(loginUser.getUser());
        QueryVo queryVo = new QueryVo();
        queryVo.filters.put("project_id", projectId);
        queryVo.filters.put("user_id", loginUser.getUser().getUserId());
        queryVo.filters.put("company_id", companyId);
        if (!projectId.equals(0)) {
            String sql = customPermissionService.getProjectAuthorizePermission(null, "project_id", loginUser.getUser());
            queryVo.setCustomPermissionSql(sql);
        }
        DashboardView dashboardView = dashboardViewService.load(queryVo);
        if (dashboardView == null) {
            return AjaxResult.error(HttpStatus.NOT_FOUND, MessageUtils.message("error.not_found"));
        }
        return AjaxResult.success(dashboardView);
    }

    @PreAuthorize("@ss.hasPermi('system:dashboard_view:set')")
    @PostMapping(value = "/set", produces = "application/json;charset=UTF-8")
    public AjaxResult save(
        @RequestParam(name = "projectId", required = false) Integer projectId,
        @RequestBody @Validated(Update.class) DashboardView dashboardView) {
        if (projectId == null) {
            projectId = 0;
        }
        dashboardView.setProjectId(projectId);
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        dashboardView.setUserId(loginUser.getUser().getUserId().intValue());
        Long companyId = sysDeptService.selectCompanyIdByUser(loginUser.getUser());
        if (!projectId.equals(0)) {
            String sql = customPermissionService.getProjectAuthorizePermission(null, "project_id", loginUser.getUser());
            dashboardView.setCustomPermissionSql(sql);
        }
        dashboardView.setCompanyId(companyId);
        if (dashboardViewService.save(dashboardView)) {
            return AjaxResult.success();
        }
        return AjaxResult.error(MessageUtils.message("error.operation_failed"));
    }
}
