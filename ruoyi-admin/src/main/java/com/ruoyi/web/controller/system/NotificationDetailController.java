package com.ruoyi.web.controller.system;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.system.domain.EmailDetail;
import com.ruoyi.system.domain.OfficialDetail;
import com.ruoyi.system.domain.SmsDetail;
import com.ruoyi.system.domain.VoiceDetail;
import com.ruoyi.system.service.IEmailDetailService;
import com.ruoyi.system.service.IOfficialDetailService;
import com.ruoyi.system.service.ISmsDetailService;
import com.ruoyi.system.service.IVoiceDetailService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/system/notification/detail")
public class NotificationDetailController extends BaseController {
    @Autowired
    private ISmsDetailService smsDetailService;
    @Autowired
    private IOfficialDetailService officialDetailService;
    @Autowired
    private IEmailDetailService emailDetailService;
    @Autowired
    private IVoiceDetailService voiceDetailService;

    @PreAuthorize("@ss.hasPermi('system:notification:detail:sms_detail:list')")
    @GetMapping("/sms_detail/list")
    public TableDataInfo smsDetailList(SmsDetail smsDetail){
        startPage();
        List<SmsDetail> list = smsDetailService.list(smsDetail);
        return getDataTable(list);
    }

    @PreAuthorize("@ss.hasPermi('system:notification:detail:sms_detail:query')")
    @GetMapping("/sms_detail/get_info")
    public AjaxResult smsDetailGetInfo(Integer id){
        SmsDetail smsDetail = new SmsDetail();
        smsDetail.setId(id);
        SmsDetail detail = smsDetailService.getOne(smsDetail);
        return AjaxResult.success(detail);
    }

    @PreAuthorize("@ss.hasPermi('system:notification:detail:email_detail:list')")
    @GetMapping("/email_detail/list")
    public TableDataInfo emailDetailList(EmailDetail emailDetail){
        startPage();
        List<EmailDetail> list = emailDetailService.list(emailDetail);
        return getDataTable(list);
    }

    @PreAuthorize("@ss.hasPermi('system:notification:detail:email_detail:query')")
    @GetMapping("/email_detail/get_info")
    public AjaxResult emailDetailGetInfo(Integer id){
        EmailDetail emailDetail = new EmailDetail();
        emailDetail.setId(id);
        EmailDetail detail = emailDetailService.getOne(emailDetail);
        return AjaxResult.success(detail);
    }

    @PreAuthorize("@ss.hasPermi('system:notification:detail:official_detail:list')")
    @GetMapping("/official_detail/list")
    public TableDataInfo officialDetailList(OfficialDetail officialDetail){
        startPage();
        List<OfficialDetail> list = officialDetailService.list(officialDetail);
        return getDataTable(list);
    }

    @PreAuthorize("@ss.hasPermi('system:notification:detail:official_detail:query')")
    @GetMapping("/official_detail/get_info")
    public AjaxResult officialDetailGetInfo(Integer id){
        OfficialDetail officialDetail = new OfficialDetail();
        officialDetail.setId(id);
        OfficialDetail detail = officialDetailService.getOne(officialDetail);
        return AjaxResult.success(detail);
    }

    @PreAuthorize("@ss.hasPermi('system:notification:detail:voice_detail:list')")
    @GetMapping("/voice_detail/list")
    public TableDataInfo voiceDetailList(VoiceDetail voiceDetail){
        startPage();
        List<VoiceDetail> list = voiceDetailService.index(voiceDetail);
        return getDataTable(list);
    }

    @PreAuthorize("@ss.hasPermi('system:notification:detail:voice_detail:query')")
    @GetMapping("/voice_detail/get_info")
    public AjaxResult voiceDetailGetInfo(Integer id){
        VoiceDetail voiceDetail = new VoiceDetail();
        voiceDetail.setId(id);
        VoiceDetail detail = voiceDetailService.retrieve(voiceDetail);
        return AjaxResult.success(detail);
    }
}
