package com.ruoyi.web.controller.system;

import cn.hutool.core.collection.CollUtil;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.exception.CustomException;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.system.domain.TerminalDetail;
import com.ruoyi.system.domain.ValidationGroups.Create;
import com.ruoyi.system.domain.vo.QueryVo;
import com.ruoyi.system.domain.vo.TerminalDetailVO;
import com.ruoyi.system.service.ISysDeptService;
import com.ruoyi.system.service.ITerminalDetailService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping("/system/terminal/detail")
public class TerminalDetailController extends BaseController {

    @Autowired
    private ISysDeptService sysDeptService;

    @Autowired
    private ITerminalDetailService terminalDetailService;

    @PreAuthorize("@ss.hasPermi('system:terminal:detail:list')")
    @GetMapping("/enum/{productId}")
    public AjaxResult getTerminalParamEnum(@PathVariable("productId") @NotNull(message = "产品型号id不能为null") Integer productId) {
        LoginUser loginUser = SecurityUtils.getLoginUser();
        Long companyId = sysDeptService.selectCompanyIdByUser(loginUser.getUser());
        QueryVo queryVo = new QueryVo();
        queryVo.filters.put("product_id", productId);
        queryVo.filters.put("company_id", companyId);
        List<Map<String, Object>> terminalParamEnum = terminalDetailService.getTerminalParamEnum(queryVo);
        return AjaxResult.success(terminalParamEnum);
    }

    @PreAuthorize("@ss.hasPermi('system:terminal:detail:list')")
    @GetMapping("/list")
    public AjaxResult queryTerminalDetail(Integer productId) {
        LoginUser loginUser = SecurityUtils.getLoginUser();
        Long companyId = sysDeptService.selectCompanyIdByUser(loginUser.getUser());
        QueryVo queryVo = new QueryVo();
        queryVo.filters.put("product_id", productId);
        queryVo.filters.put("company_id", companyId);
        List<TerminalDetailVO> terminalDetailVOS = terminalDetailService.queryTerminalDetail(queryVo);
        return AjaxResult.success(terminalDetailVOS);
    }

    @PreAuthorize("@ss.hasPermi('system:terminal:detail:add')")
    @PostMapping("/add_edit")
    public AjaxResult addOrEdit(@RequestBody List<TerminalDetail> terminalDetails) {
        LoginUser loginUser = SecurityUtils.getLoginUser();
        Long companyId = sysDeptService.selectCompanyIdByUser(loginUser.getUser());
        if (CollUtil.isEmpty(terminalDetails)){
            throw new CustomException("请输入需要操作的数据");
        }
        for (TerminalDetail terminalDetail : terminalDetails) {
            terminalDetail.setCompanyId(companyId);
            terminalDetailService.checkIsExit(terminalDetail);
            if (terminalDetail.getId() == null) {
                terminalDetail.setCreateBy(loginUser.getUsername());
                terminalDetail.setCreateTime(new Date());
                if (!terminalDetailService.add(terminalDetail)) {
                    return AjaxResult.error(terminalDetail.getItem()+"添加失败");
                }
            }
            if (!terminalDetailService.update(terminalDetail)) {
                return AjaxResult.error(terminalDetail.getItem() + "修改失败");
            }
        }
        return AjaxResult.success("操作成功");
    }

}
