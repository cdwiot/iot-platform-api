package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.framework.web.service.TokenService;
import com.ruoyi.system.domain.OfficialAccount;
import com.ruoyi.system.domain.OfficialAccountTemplate;
import com.ruoyi.system.domain.ValidationGroups.*;
import com.ruoyi.system.service.IOfficialAccountService;
import com.ruoyi.system.service.IOfficialAccountTemplateService;
import com.ruoyi.system.service.ISysDeptService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/system/official_account/template")
public class OfficialAccountTemplateController extends BaseController {

//    @Autowired
//    private TokenService tokenService;
//    @Autowired
//    private ISysDeptService sysDeptService;
//    @Autowired
//    private IOfficialAccountTemplateService officialAccountTemplateService;
//    @Autowired
//    private IOfficialAccountService officialAccountService;
//
//    /**
//     * 公众号模板配置的分页查询
//     */
//    @PreAuthorize("@ss.hasPermi('system:officialAccountTemplate:list')")
//    @GetMapping("/list")
//    public TableDataInfo list(@Validated(Select.class) OfficialAccountTemplate officialAccountTemplate) {
//        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
//        SysUser user = loginUser.getUser();
//        // 设置companyId
//        Long companyId = sysDeptService.selectCompanyIdByUser(user);
//        //不是管理员
//        OfficialAccount officialAccount = new OfficialAccount();
//        officialAccount.setCompanyId(companyId);
//        if (!user.isAdmin()){
//            //不是大账号，也没有查到配置信息，则使用大账号模板
//            OfficialAccount one = officialAccountService.getOne(officialAccount);
//            if (one == null){
//                officialAccountTemplate.setCompanyId(100L);
//            }else {
//                //不是大账号，但查到配置信息，则使用自己模板
//                officialAccountTemplate.setCompanyId(companyId);
//            }
//        }else {
//            //是大账号
//            officialAccountTemplate.setCompanyId(companyId);
//        }
//        startPage();
//        List<OfficialAccountTemplate> officialAccountTemplates = officialAccountTemplateService.queryList(officialAccountTemplate);
//        return getDataTable(officialAccountTemplates);
//    }
//
//    /**
//     * 获取公众号模板配置详细信息
//     */
//    @PreAuthorize("@ss.hasPermi('system:officialAccountTemplate:query')")
//    @GetMapping(value = "/getInfo")
//    public AjaxResult getInfo(Integer id) {
//        OfficialAccountTemplate param = new OfficialAccountTemplate();
//        param.setId(id);
//        OfficialAccountTemplate officialAccountTemplate = officialAccountTemplateService.getOne(param);
//        return AjaxResult.success(officialAccountTemplate);
//    }
//
//    /**
//     * 新增公众号模板配置
//     */
//    @PreAuthorize("@ss.hasPermi('system:officialAccountTemplate:add')")
//    @Log(title = "公众号模板配置", businessType = BusinessType.INSERT)
//    @PostMapping("/add")
//    public AjaxResult add(@RequestBody @Validated(Create.class) OfficialAccountTemplate officialAccountTemplate) {
//        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
//        // 设置companyId
//        Long companyId = sysDeptService.selectCompanyIdByUser(loginUser.getUser());
//        officialAccountTemplate.setCompanyId(companyId);
//        officialAccountTemplate.setCreateBy(loginUser.getUsername());
//        officialAccountTemplate.setCreateTime(new Date());
//        officialAccountTemplateService.checkTemplateCodeExist(officialAccountTemplate);
//        if (officialAccountTemplateService.add(officialAccountTemplate)) {
//            return AjaxResult.success("添加成功");
//        }
//        return AjaxResult.error("添加失败");
//    }
//
//    /**
//     * 修改公众号模板配置
//     */
//    @PreAuthorize("@ss.hasPermi('system:officialAccountTemplate:edit')")
//    @Log(title = "公众号模板配置", businessType = BusinessType.UPDATE)
//    @PostMapping("/edit")
//    public AjaxResult edit(@RequestBody @Validated(Update.class) OfficialAccountTemplate officialAccountTemplate) {
//        officialAccountTemplateService.checkTemplateCodeExist(officialAccountTemplate);
//        if (officialAccountTemplateService.update(officialAccountTemplate)) {
//            return AjaxResult.success("修改成功");
//        }
//        return AjaxResult.error("修改失败");
//    }
//
//    /**
//     * 删除公众号模板配置
//     */
//    @PreAuthorize("@ss.hasPermi('system:officialAccountTemplate:remove')")
//    @Log(title = "公众号模板配置", businessType = BusinessType.DELETE)
//    @DeleteMapping("/{id}")
//    public AjaxResult remove(@PathVariable @NotNull(message = "id不能为null") Integer id) {
//        OfficialAccountTemplate officialAccountTemplate = new OfficialAccountTemplate();
//        officialAccountTemplate.setId(id);
//        if (officialAccountTemplateService.delete(officialAccountTemplate)) {
//            return AjaxResult.success("删除成功");
//        }
//        return AjaxResult.error("删除成功");
//    }
}
