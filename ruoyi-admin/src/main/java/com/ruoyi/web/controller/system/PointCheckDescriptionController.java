package com.ruoyi.web.controller.system;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.exception.CustomException;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.framework.web.service.TokenService;
import com.ruoyi.system.domain.PointCheckDescription;
import com.ruoyi.system.domain.ValidationGroups.*;
import com.ruoyi.system.service.IPointCheckDescriptionService;
import com.ruoyi.system.service.ISysDeptService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/system/point_check_description")
public class PointCheckDescriptionController extends BaseController {

    @Autowired
    private TokenService tokenService;
    @Autowired
    private ISysDeptService sysDeptService;
    @Autowired
    private IPointCheckDescriptionService pointDescriptionService;

    @PreAuthorize("@ss.hasPermi('system:point:check:description:list')")
    @GetMapping("/list")
    public TableDataInfo list(PointCheckDescription pointCheckDescription) {
        startPage();
        List<PointCheckDescription> pointCheckDescriptions = pointDescriptionService.queryList(pointCheckDescription);
        return getDataTable(pointCheckDescriptions);
    }

    @PreAuthorize("@ss.hasPermi('system:point:check:description:get_info')")
    @GetMapping("/get_info")
    public AjaxResult getInfo(Integer id) {
        PointCheckDescription param = new PointCheckDescription();
        param.setId(id);
        PointCheckDescription pointCheckDescription = pointDescriptionService.getOne(param);
        if (pointCheckDescription == null){
            pointCheckDescription = new PointCheckDescription();
        }
        return AjaxResult.success(pointCheckDescription);
    }

    @PreAuthorize("@ss.hasPermi('system:point:check:description:addOrUpdate')")
    @PostMapping("/add_update")
    public AjaxResult add(@RequestBody @Validated(Create.class) PointCheckDescription pointCheckDescription) {
        PointCheckDescription result = pointDescriptionService.getOne(pointCheckDescription);
        if (result == null) {
            throw new CustomException("没有查询到自动生成的自定义字段");
        }
        pointCheckDescription.setId(result.getId());
        if (pointDescriptionService.update(pointCheckDescription)) {
            return AjaxResult.success("操作成功");
        }
        return AjaxResult.error("操作失败");
    }
    @PreAuthorize("@ss.hasPermi('system:point:check:description:addOrUpdate')")
    @PostMapping("/update_batch")
    public AjaxResult updateBatch(@RequestBody @Validated(Create.class) List<PointCheckDescription> pointCheckDescriptions){
        pointDescriptionService.updateBatch(pointCheckDescriptions);
        return AjaxResult.success("操作成功");
    }

//    @PreAuthorize("@ss.hasPermi('system:point:check:description:edit')")
//    @PostMapping("/edit")
//    public AjaxResult edit(@RequestBody @Validated(Update.class) PointCheckDescription pointCheckDescription) {
//        if (pointDescriptionService.update(pointCheckDescription)) {
//            return AjaxResult.success("修改成功");
//        }
//        return AjaxResult.error("修改失败");
//    }

//    @DeleteMapping("/{id}")
//    public AjaxResult delete(@PathVariable @NotNull(message = "id不能为null") Integer id) {
//        PointCheckDescription param = new PointCheckDescription();
//        param.setId(id);
//        if (pointDescriptionService.delete(param)) {
//            return AjaxResult.success("删除成功");
//        }
//        return AjaxResult.error("删除失败");
//    }

    /**
     * 根据产品检查Id查询文字描述
     *
     * @param detailId
     * @return
     */
    @PreAuthorize("@ss.hasPermi('system:point:check:description:query_description')")
    @GetMapping("/query_description")
    public AjaxResult queryDescription(Integer detailId) {
        PointCheckDescription param = new PointCheckDescription();
        param.setDetailId(detailId);
        List<PointCheckDescription> pointCheckDescriptions = pointDescriptionService.queryList(param);
        return AjaxResult.success(pointCheckDescriptions);
    }
}
