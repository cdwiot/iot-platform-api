package com.ruoyi.web.controller.system;

import cn.hutool.core.util.StrUtil;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.framework.web.service.TokenService;
import com.ruoyi.system.domain.PointCheckDeviceDetail;
import com.ruoyi.system.domain.PointCheckTaskDevice;
import com.ruoyi.system.domain.ValidationGroups.Create;
import com.ruoyi.system.domain.ValidationGroups.Update;
import com.ruoyi.system.domain.vo.PointCheckDeviceDetailVO;
import com.ruoyi.system.service.IPointCheckDeviceDetailService;
import com.ruoyi.system.service.IPointCheckTaskDeviceService;
import com.ruoyi.system.service.ISysDeptService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping("/system/point_check_device_detail")
public class PointCheckDeviceDetailController extends BaseController {

    @Autowired
    private TokenService tokenService;
    @Autowired
    private ISysDeptService sysDeptService;
    @Autowired
    private IPointCheckDeviceDetailService pointCheckDeviceDetailService;
    @Autowired
    private IPointCheckTaskDeviceService pointCheckTaskDeviceService;

    @PreAuthorize("@ss.hasPermi('system:point:check:device:detail:list')")
    @GetMapping("/list")
    public TableDataInfo list(PointCheckDeviceDetail pointCheckDeviceDetail) {
        startPage();
        List<PointCheckDeviceDetail> pointCheckDeviceDetailList = pointCheckDeviceDetailService.queryList(pointCheckDeviceDetail);
        return getDataTable(pointCheckDeviceDetailList);
    }

    /**
     * 获取产品检查项统计的数量
     *
     * @param taskDeviceId
     * @return
     */
    @PreAuthorize("@ss.hasPermi('system:point:check:device:detail:get_device_detailVO')")
    @GetMapping("/get_device_detailVO")
    public AjaxResult getDeviceDetailVO(Integer taskDeviceId) {
        Map<String, PointCheckDeviceDetailVO> deviceDetailVO = pointCheckDeviceDetailService.getDeviceDetailVO(taskDeviceId);
        return AjaxResult.success(deviceDetailVO);
    }

//    @GetMapping("enum/{taskDeviceId}")
//    public AjaxResult getDeviceItemEnum(@PathVariable @NotNull(message = "任务产品id不能为null") Integer taskDeviceId){
//
//    }

    /**
     * 详情
     *
     * @param id
     * @return
     */
    @PreAuthorize("@ss.hasPermi('system:point:check:device:detail:get_info')")
    @GetMapping("/get_info")
    public AjaxResult getInfo(Integer id) {
        PointCheckDeviceDetail pointCheckDeviceDetail = new PointCheckDeviceDetail();
        pointCheckDeviceDetail.setId(id);
        PointCheckDeviceDetail result = pointCheckDeviceDetailService.getOne(pointCheckDeviceDetail);
        return AjaxResult.success(result);
    }

    @PreAuthorize("@ss.hasPermi('system:point:check:device:detail:addOrUpdate')")
    @PostMapping("/add_update")
    public AjaxResult add(@RequestBody @Validated(Create.class) PointCheckDeviceDetail pointCheckDeviceDetail) {
        //验证该任务下产品是否存在
        pointCheckDeviceDetailService.checkIsExist(pointCheckDeviceDetail);
        PointCheckDeviceDetail param = new PointCheckDeviceDetail();
        param.setId(pointCheckDeviceDetail.getId());
        PointCheckDeviceDetail one = pointCheckDeviceDetailService.getOne(param);
        if (pointCheckDeviceDetailService.update(pointCheckDeviceDetail)) {
            //更新最后一次操作时间
            if (!StrUtil.equals(pointCheckDeviceDetail.getStatus(),one.getStatus())){
                Integer taskDeviceId = pointCheckDeviceDetail.getTaskDeviceId();
                PointCheckTaskDevice taskDevice = new PointCheckTaskDevice();
                taskDevice.setId(taskDeviceId);
                taskDevice.setLastUpdateTime(new Date());
                pointCheckTaskDeviceService.update(taskDevice);
            }
            return AjaxResult.success("操作成功");
        }
        return AjaxResult.error("操作失败");
    }

    /**
     * 编辑
     *
     * @param pointCheckDeviceDetail
     * @return
     */
    @PreAuthorize("@ss.hasPermi('system:point:check:device:detail:edit')")
    @PostMapping("/edit")
    public AjaxResult edit(@RequestBody @Validated(Update.class) PointCheckDeviceDetail pointCheckDeviceDetail) {
        //验证该任务下产品是否存在
        pointCheckDeviceDetailService.checkIsExist(pointCheckDeviceDetail);
        if (pointCheckDeviceDetailService.update(pointCheckDeviceDetail)) {
            return AjaxResult.success("修改成功");
        }
        return AjaxResult.error("修改失败");
    }

    /**
     * 删除
     *
     * @param id
     * @return
     */
    @PreAuthorize("@ss.hasPermi('system:point:check:device:detail:delete')")
    @DeleteMapping("/{id}")
    public AjaxResult delete(@PathVariable @NotNull(message = "id不能为null") Integer id) {
        PointCheckDeviceDetail param = new PointCheckDeviceDetail();
        param.setId(id);
        if (pointCheckDeviceDetailService.delete(param)) {
            return AjaxResult.success("删除成功");
        }
        return AjaxResult.error("删除失败");
    }
}
