package com.ruoyi.web.controller.system;

import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.framework.web.service.TokenService;
import com.ruoyi.system.domain.ProductChart;
import com.ruoyi.system.domain.vo.QueryVo;
import com.ruoyi.system.service.IProjectAuthorizeService;
import com.ruoyi.system.service.IProjectWithoutProductChartService;
import com.ruoyi.system.service.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;

import lombok.extern.slf4j.Slf4j;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotNull;

import com.alibaba.fastjson.JSONArray;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.MessageUtils;

@Slf4j
@RestController
@RequestMapping("/system/project.v2")
public class ProjectWithoutProductChartController extends BaseController
{
    @Autowired
    IProjectWithoutProductChartService projectWithoutProductChartService;
    @Autowired
    private IProjectAuthorizeService projectAuthorizeService;
    @Autowired
    private TokenService tokenService;
    @Autowired
    private ISysUserService sysUserService;

    @PreAuthorize("@ss.hasPermi('system:project:product:chart:set')")
    @Log(title = "项目", businessType = BusinessType.UPDATE)
    @PostMapping(value = "/{id}/product/chart/set", produces = "application/json;charset=UTF-8")
    public AjaxResult setProductChart(@PathVariable("id") @NotNull(message = "id不能为null") Integer id, @RequestBody JSONArray productChartIds) {
        //TODO 验证当前用户是否授权该项目
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        projectAuthorizeService.checkProjectIsAuthorizeWithProjectId(loginUser.getUser(),id);
        Iterator<Object> iterator = productChartIds.iterator();
        List<Integer> productChartIdList = new ArrayList<>();
        while (iterator.hasNext()) {
            productChartIdList.add(Integer.parseInt(iterator.next().toString()));
        }
        if (projectWithoutProductChartService.set(productChartIdList, id)) {
            return AjaxResult.success();
        }
        return AjaxResult.error(MessageUtils.message("error.operation_failed"));
    }

    @PreAuthorize("@ss.hasPermi('system:project:product:chart:get')")
    @GetMapping(value = "/{id}/product/chart/get", produces = "application/json;charset=UTF-8")
    public AjaxResult getProductChart(@PathVariable("id") @NotNull(message = "id不能为null") Integer id, @RequestParam("productId") @NotNull(message = "产品id不能为null") Integer productId) {
        //TODO 验证当前用户是否授权该项目
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        boolean commonRoleFlag = sysUserService.checkUserIsCommonRole(loginUser.getUser());
        if (!commonRoleFlag){
            projectAuthorizeService.checkProjectIsAuthorizeWithProjectId(loginUser.getUser(),id);
        }
        QueryVo queryVo = new QueryVo();
        queryVo.filters.put("project_id", id);
        queryVo.filters.put("product_id", productId);
        List<ProductChart> productCharts = projectWithoutProductChartService.get(queryVo);
        return AjaxResult.success(productCharts);
    }

    @PreAuthorize("@ss.hasPermi('system:project:product:chart:availability')")
    @GetMapping(value = "/{id}/product/chart/availability", produces = "application/json;charset=UTF-8")
    public AjaxResult productChartAvailability(@PathVariable("id") @NotNull(message = "id不能为null") Integer id, @RequestParam("productId") @NotNull(message = "产品id不能为null") Integer productId) {
        //TODO 验证当前用户是否授权该项目
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        boolean commonRoleFlag = sysUserService.checkUserIsCommonRole(loginUser.getUser());
        if (!commonRoleFlag){
            projectAuthorizeService.checkProjectIsAuthorizeWithProjectId(loginUser.getUser(),id);
        }
        QueryVo queryVo = new QueryVo();
        queryVo.filters.put("project_id", id);
        queryVo.filters.put("product_id", productId);
        List<Map<String, Object>> availability = projectWithoutProductChartService.availability(queryVo);
        return AjaxResult.success(availability);
    }
}
