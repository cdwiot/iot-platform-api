package com.ruoyi.web.controller.system;

import com.ruoyi.common.constant.HttpStatus;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.framework.web.service.TokenService;
import com.ruoyi.system.domain.RpcMsgLog;
import com.ruoyi.system.domain.ValidationGroups.Select;
import com.ruoyi.system.domain.vo.QueryVo;
import com.ruoyi.system.domain.vo.RpcMsgLogVO;
import com.ruoyi.system.service.IRpcMsgLogService;
import com.ruoyi.system.service.ISysDeptService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/system/rpcMsgLog")
public class RpcMsgLogController extends BaseController {
    @Autowired
    IRpcMsgLogService rpcMsgLogService;

    @Autowired
    private TokenService tokenService;
    @Autowired
    private ISysDeptService sysDeptService;

    @GetMapping(value = "/list", produces = "application/json;charset=UTF-8")
    public Object index(@Validated(Select.class) RpcMsgLogVO alarm) {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        Long companyId = sysDeptService.selectCompanyIdByUser(loginUser.getUser());
        QueryVo queryVo = new QueryVo();
        queryVo.filters.put("id", alarm.getId());
        queryVo.filters.put("ids", alarm.getIds());
        queryVo.filters.put("status", alarm.getStatus());
        queryVo.filters.put("companyId", alarm.getCompanyId());
        queryVo.filters.put("tbUsername", alarm.getTbUsername());
        queryVo.filters.put("company_id", companyId);

        queryVo.filters.put("sn", alarm.getSn());
        queryVo.filters.put("productSn", alarm.getProductSn());
        queryVo.filters.put("productName", alarm.getProductName());
        startPage();
        List<RpcMsgLog> alarms = rpcMsgLogService.queryPage(queryVo);
        return getDataTable(alarms);
    }

    @GetMapping(value = "retry/{id}", produces = "application/json;charset=UTF-8")
    public AjaxResult retrieve(@PathVariable("id") @NotNull(message = "id不能为null") Long id) {
        try {
            boolean f = rpcMsgLogService.exeRpcReques(id);
            if (!f) {
                return AjaxResult.error(HttpStatus.ERROR, "重试失败");
            }
        } catch (Exception e) {
            return AjaxResult.error(HttpStatus.ERROR, e.getMessage());
        }
        return AjaxResult.success();
    }

}
