package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.exception.CustomException;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.framework.web.service.TokenService;
import com.ruoyi.system.domain.SmsAccount;
import com.ruoyi.system.domain.SmsTemplate;
import com.ruoyi.system.domain.ValidationGroups.Create;
import com.ruoyi.system.domain.ValidationGroups.Select;
import com.ruoyi.system.domain.ValidationGroups.Update;
import com.ruoyi.system.service.ISmsAccountService;
import com.ruoyi.system.service.ISmsTemplateService;
import com.ruoyi.system.service.ISysDeptService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/system/sms/template")
public class SmsTemplateController extends BaseController {

//    @Autowired
//    private TokenService tokenService;
//    @Autowired
//    private ISysDeptService sysDeptService;
//    @Autowired
//    private ISmsTemplateService messageTemplateService;
//    @Autowired
//    private ISmsAccountService messageAccountService;
//
//    /**
//     * 模板配置的分页查询
//     */
//    @PreAuthorize("@ss.hasPermi('system:messageTemplate:list')")
//    @GetMapping("/list")
//    public TableDataInfo list(@Validated(Select.class) SmsTemplate smsTemplate) {
//        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
//        SysUser user = loginUser.getUser();
//        // 设置companyId
//        Long companyId = sysDeptService.selectCompanyIdByUser(user);
//        //不是管理员
//        SmsAccount smsAccount = new SmsAccount();
//        smsAccount.setCompanyId(companyId);
//        if (!user.isAdmin()){
//            //不是大账号，也没有查到配置信息，则使用大账号模板
//            SmsAccount one = messageAccountService.getOne(smsAccount);
//            if (one == null){
//                smsTemplate.setCompanyId(100L);
//            }else {
//                //不是大账号，但查到配置信息，则使用自己模板
//                smsTemplate.setCompanyId(companyId);
//            }
//        }else {
//            //是大账号
//            smsTemplate.setCompanyId(companyId);
//        }
//        startPage();
//        List<SmsTemplate> smsTemplates = messageTemplateService.queryList(smsTemplate);
//        return getDataTable(smsTemplates);
//    }
//
//    /**
//     * 获取模板配置详细信息
//     */
//    @PreAuthorize("@ss.hasPermi('system:messageTemplate:query')")
//    @GetMapping(value = "/getInfo")
//    public AjaxResult getInfo(Integer id) {
//        SmsTemplate param = new SmsTemplate();
//        param.setId(id);
//        SmsTemplate smsTemplate = messageTemplateService.getOne(param);
//        return AjaxResult.success(smsTemplate);
//    }
//
//    /**
//     * 新增模板配置
//     */
//    @PreAuthorize("@ss.hasPermi('system:messageTemplate:add')")
//    @Log(title = "模板配置", businessType = BusinessType.INSERT)
//    @PostMapping("/add")
//    public AjaxResult add(@RequestBody @Validated(Create.class) SmsTemplate smsTemplate) {
//        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
//        SysUser user = loginUser.getUser();
//        // 设置companyId
//        Long companyId = sysDeptService.selectCompanyIdByUser(loginUser.getUser());
//        //不是大账号
//        SmsAccount smsAccount = new SmsAccount();
//        smsAccount.setCompanyId(companyId);
//        if (!user.isAdmin()){
//            SmsAccount one = messageAccountService.getOne(smsAccount);
//            //没有查到
//            if(one == null){
//                throw new CustomException("添加失败，该用户没有配置特有的短信平台账号！");
//            }
//        }
//        smsTemplate.setCompanyId(companyId);
//        smsTemplate.setCreateBy(loginUser.getUsername());
//        smsTemplate.setCreateTime(new Date());
//        messageTemplateService.checkTemplateCodeExist(smsTemplate);
//        if (messageTemplateService.addMessageTemplate(smsTemplate)) {
//            return AjaxResult.success("添加成功");
//        }
//        return AjaxResult.error("添加失败");
//    }
//
//    /**
//     * 修改模板配置
//     */
//    @PreAuthorize("@ss.hasPermi('system:messageTemplate:edit')")
//    @Log(title = "模板配置", businessType = BusinessType.UPDATE)
//    @PostMapping("/edit")
//    public AjaxResult edit(@RequestBody @Validated(Update.class) SmsTemplate smsTemplate) {
//        messageTemplateService.checkTemplateCodeExist(smsTemplate);
//        if (messageTemplateService.updateMessageTemplate(smsTemplate)) {
//            return AjaxResult.success("修改成功");
//        }
//        return AjaxResult.error("修改失败");
//    }
//
//    /**
//     * 删除模板配置
//     */
//    @PreAuthorize("@ss.hasPermi('system:messageTemplate:remove')")
//    @Log(title = "模板配置", businessType = BusinessType.DELETE)
//    @DeleteMapping("/{id}")
//    public AjaxResult remove(@PathVariable @NotNull(message = "id不能为null") Integer id) {
//        SmsTemplate smsTemplate = new SmsTemplate();
//        smsTemplate.setId(id);
//        if (messageTemplateService.deleteMessageTemplate(smsTemplate)) {
//            return AjaxResult.success("删除成功");
//        }
//        return AjaxResult.error("删除成功");
//    }
}
