package com.ruoyi.web.controller.system;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.exception.CustomException;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.framework.web.service.TokenService;
import com.ruoyi.system.domain.Device;
import com.ruoyi.system.domain.DevicePropertyValue;
import com.ruoyi.system.domain.vo.DevicePropertyValueResultVO;
import com.ruoyi.system.domain.vo.DevicePropertyValueVO;
import com.ruoyi.system.domain.vo.QueryVo;
import com.ruoyi.system.service.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.List;

@SuppressWarnings("AlibabaLowerCamelCaseVariableNaming")
@Slf4j
@RestController
@RequestMapping("/system/device/property/value")
public class DevicePropertyValueController extends BaseController {

    @Autowired
    private TokenService tokenService;
    @Autowired
    private ISysDeptService sysDeptService;
    @Autowired
    private IDevicePropertyValueService devicePropertyValueService;
    @Autowired
    private IProjectAuthorizeService projectAuthorizeService;
    @Autowired
    private ISysUserService sysUserService;
    @Autowired
    private IDeviceService deviceService;

    @PreAuthorize("@ss.hasPermi('system:device:property:value:list')")
    @GetMapping("/deviceId")
    public TableDataInfo queryDevicePropertyByDeviceId(Integer deviceId,Integer propertyId,String propertyGroup) {
        //TODO 验证当前产品是否属于当前用户所建的项目
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        boolean commonRoleFlag = sysUserService.checkUserIsCommonRole(loginUser.getUser());
        if (!commonRoleFlag){
            projectAuthorizeService.checkProjectIsAuthorizeWithDeviceId(loginUser.getUser(),deviceId);
        }

        DevicePropertyValue devicePropertyValue = new DevicePropertyValue();
        devicePropertyValue.setDeviceId(deviceId);
        devicePropertyValue.setPropertyId(propertyId);
        devicePropertyValue.setPropertyGroup(propertyGroup);
        List<DevicePropertyValueResultVO> devicePropertyValueVOS = devicePropertyValueService.queryDeviceValueByDeviceId(devicePropertyValue);
        return getDataTable(devicePropertyValueVOS);
    }

    @PreAuthorize("@ss.hasPermi('system:device:property:value:saveOrUpdate')")
    @PostMapping("/saveOrUpdate")
    public AjaxResult saveOrUpdate(@RequestBody List<DevicePropertyValue> devicePropertyValues) {
        Integer deviceId = devicePropertyValues.get(0).getDeviceId();
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        projectAuthorizeService.checkProjectIsAuthorizeWithDeviceId(loginUser.getUser(),deviceId);
//        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        //验证当前用户是否是该产品创建人
//        projectAuthorizeService.checkDeviceOperationStatus(loginUser.getUser(),deviceId);
        // 设置companyId
        //设计员返回true
        boolean commonRole = sysUserService.checkUserIsCommonRole(loginUser.getUser());
        if (!commonRole){
            QueryVo queryVo = new QueryVo();
            queryVo.filters.put("id",deviceId);
            Device retrieve = deviceService.retrieve(queryVo);
            if (retrieve != null && !loginUser.getUsername().equals(retrieve.getCreateBy())){
                throw new CustomException("当前用户只能修改自己创建的产品");
            }
        }
        Long companyId = sysDeptService.selectCompanyIdByUser(loginUser.getUser());
        Boolean aBoolean = devicePropertyValueService.saveOrUpdate(devicePropertyValues, loginUser.getUsername(), companyId);
        if (aBoolean) {
            return AjaxResult.success("修改成功！");
        }
        return AjaxResult.error("修改失败！");
    }
}
