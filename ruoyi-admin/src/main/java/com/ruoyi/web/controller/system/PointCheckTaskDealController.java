package com.ruoyi.web.controller.system;

import cn.hutool.core.util.StrUtil;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.framework.web.service.TokenService;
import com.ruoyi.system.domain.PointCheckDescription;
import com.ruoyi.system.domain.PointCheckDeviceDetail;
import com.ruoyi.system.domain.PointCheckTaskDeal;
import com.ruoyi.system.domain.PointCheckTaskDevice;
import com.ruoyi.system.domain.ValidationGroups.Create;
import com.ruoyi.system.domain.ValidationGroups.Update;
import com.ruoyi.system.service.IPointCheckDeviceDetailService;
import com.ruoyi.system.service.IPointCheckTaskDealService;
import com.ruoyi.system.service.IPointCheckTaskDeviceService;
import com.ruoyi.system.service.ISysDeptService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/system/point_check_task_deal")
public class PointCheckTaskDealController extends BaseController {

    @Autowired
    private TokenService tokenService;
    @Autowired
    private ISysDeptService sysDeptService;
    @Autowired
    private IPointCheckTaskDealService pointCheckTaskDealService;
    @Autowired
    private IPointCheckTaskDeviceService pointCheckTaskDeviceService;
    @Autowired
    private IPointCheckDeviceDetailService pointCheckDeviceDetailService;

    @PreAuthorize("@ss.hasPermi('system:point:check:task:deal:list')")
    @GetMapping("/list")
    public TableDataInfo list(PointCheckTaskDeal pointCheckTaskDeal) {
        startPage();
        List<PointCheckTaskDeal> pointCheckTaskDealList = pointCheckTaskDealService.queryList(pointCheckTaskDeal);
        return getDataTable(pointCheckTaskDealList);
    }

    /**
     * 详情
     *
     * @param id
     * @return
     */
    @PreAuthorize("@ss.hasPermi('system:point:check:task:deal:get_info')")
    @GetMapping("/get_info")
    public AjaxResult getInfo(Integer id) {
        PointCheckTaskDeal pointCheckTaskDeal = new PointCheckTaskDeal();
        pointCheckTaskDeal.setId(id);
        PointCheckTaskDeal result = pointCheckTaskDealService.getOne(pointCheckTaskDeal);
        return AjaxResult.success(result);
    }

    @PreAuthorize("@ss.hasPermi('system:point:check:task:deal:addOrUpdate')")
    @PostMapping("/add_update")
    public AjaxResult add(@RequestBody @Validated(Create.class) PointCheckTaskDeal pointCheckTaskDeal) {
        PointCheckTaskDeal result = pointCheckTaskDealService.getOne(pointCheckTaskDeal);
        if (result == null) {
            LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
            // 设置companyId
            Long companyId = sysDeptService.selectCompanyIdByUser(loginUser.getUser());
            pointCheckTaskDeal.setCompanyId(companyId);
            pointCheckTaskDeal.setCreateBy(loginUser.getUsername());
            pointCheckTaskDeal.setCreateTime(new Date());
            if (pointCheckTaskDealService.add(pointCheckTaskDeal)) {
                return AjaxResult.success("操作成功");
            }
            return AjaxResult.error("操作失败");
        }
        pointCheckTaskDeal.setId(result.getId());
        if (pointCheckTaskDealService.update(pointCheckTaskDeal)) {
            //更新最后一次操作时间
            if(StrUtil.equals(pointCheckTaskDeal.getWay(),result.getWay()) || StrUtil.equals(pointCheckTaskDeal.getType(),result.getType())){
                PointCheckDeviceDetail deviceDetailParam = new PointCheckDeviceDetail();
                deviceDetailParam.setId(pointCheckTaskDeal.getDetailId());
                PointCheckDeviceDetail deviceDetail = pointCheckDeviceDetailService.getOne(deviceDetailParam);
                if (deviceDetail != null){
                    PointCheckTaskDevice taskDevice = new PointCheckTaskDevice();
                    taskDevice.setId(deviceDetail.getTaskDeviceId());
                    taskDevice.setLastUpdateTime(new Date());
                    pointCheckTaskDeviceService.update(taskDevice);
                }
            }
            return AjaxResult.success("操作成功");
        }
        return AjaxResult.error("操作失败");
    }

    /**
     * 编辑
     *
     * @param pointCheckTaskDeal
     * @return
     */
    @PreAuthorize("@ss.hasPermi('system:point:check:task:deal:edit')")
    @PostMapping("/edit")
    public AjaxResult edit(@RequestBody @Validated(Update.class) PointCheckTaskDeal pointCheckTaskDeal) {
        if (pointCheckTaskDealService.update(pointCheckTaskDeal)) {
            return AjaxResult.success("修改成功");
        }
        return AjaxResult.error("修改失败");
    }

    /**
     * 删除
     *
     * @param id
     * @return
     */
    @PreAuthorize("@ss.hasPermi('system:point:check:task:deal:delete')")
    @DeleteMapping("/{id}")
    public AjaxResult delete(@PathVariable @NotNull(message = "id不能为null") Integer id) {
        PointCheckTaskDeal param = new PointCheckTaskDeal();
        param.setId(id);
        if (pointCheckTaskDealService.delete(param)) {
            return AjaxResult.success("删除成功");
        }
        return AjaxResult.error("删除失败");
    }

    /**
     * 根据产品检查Id查询处理方式
     *
     * @param detailId
     * @return
     */
    @PreAuthorize("@ss.hasPermi('system:point:check:task:deal:query_deal')")
    @GetMapping("/query_deal")
    public AjaxResult queryDeal(Integer detailId) {
        PointCheckTaskDeal param = new PointCheckTaskDeal();
        param.setDetailId(detailId);
        PointCheckTaskDeal pointCheckTaskDeal = pointCheckTaskDealService.getOne(param);
        if (pointCheckTaskDeal == null){
            pointCheckTaskDeal = new PointCheckTaskDeal();
        }
        return AjaxResult.success(pointCheckTaskDeal);
    }
}
