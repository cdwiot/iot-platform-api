package com.ruoyi.web.controller.system;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotNull;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.constant.HttpStatus;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.enums.MonitorRuleType;
import com.ruoyi.common.utils.MessageUtils;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.framework.web.service.TokenService;
import com.ruoyi.system.domain.MonitorRule;
import com.ruoyi.system.domain.ValidationGroups.Create;
import com.ruoyi.system.domain.ValidationGroups.Select;
import com.ruoyi.system.domain.ValidationGroups.Update;
import com.ruoyi.system.domain.vo.QueryVo;
import com.ruoyi.system.service.IMonitorRuleService;
import com.ruoyi.system.service.ISysDeptService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

@SuppressWarnings("AlibabaLowerCamelCaseVariableNaming")
@Slf4j
@RestController
@RequestMapping("/system/monitor/rule")
public class MonitorRuleController extends BaseController {
    @Autowired
    private TokenService tokenService;

    @Autowired
    private ISysDeptService sysDeptService;

    @Autowired
    private IMonitorRuleService monitorRuleService;

    private AjaxResult checkExistance(MonitorRule monitorRule) {
        Map<String, Object> existance = monitorRuleService.findExistance(monitorRule);
        if (existance != null) {
            Object name_exists = existance.get("name_exists");
            if (name_exists != null && Integer.parseInt(name_exists.toString()) > 0) { 
                return AjaxResult.error(HttpStatus.ERROR, "监控规则名称已经存在");
            }
        }
        return null;
    }

    @PreAuthorize("@ss.hasPermi('system:monitor:rule:add')")
    @PostMapping(value = "/add", produces = "application/json;charset=UTF-8")
    @Log(title = "监控规则", businessType = BusinessType.INSERT)
    public AjaxResult add(@RequestBody @Validated(Create.class) MonitorRule monitorRule) {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        Long companyId = sysDeptService.selectCompanyIdByUser(loginUser.getUser());
        String createBy = loginUser.getUsername();
        monitorRule.setCompanyId(companyId);
        monitorRule.setCreateBy(createBy);
        monitorRule.setCreateTime(new Date());
        monitorRule.setTriggerLogic(0);
        AjaxResult ret = checkExistance(monitorRule);
        if (ret != null) {
            return ret;
        }
        Integer id = monitorRuleService.create(monitorRule);
        if (id != null) {
            return AjaxResult.success(monitorRule);
        }
        return AjaxResult.error(MessageUtils.message("error.operation_failed"));
    }

    @PreAuthorize("@ss.hasPermi('system:monitor:rule:list')")
    @GetMapping(value = "/list", produces = "application/json;charset=UTF-8")
    public Object index(@Validated(Select.class) MonitorRule monitorRule) {
        QueryVo queryVo = new QueryVo();
        queryVo.filters.put("name", monitorRule.getName());
        queryVo.filters.put("description", monitorRule.getDescription());
        queryVo.filters.put("type", monitorRule.getType());
        startPage();
        List<MonitorRule> monitorRules = monitorRuleService.index(queryVo);
        return getDataTable(monitorRules);
    }

    @PreAuthorize("@ss.hasPermi('system:monitor:rule:enum')")
    @GetMapping(value = "/enum", produces = "application/json;charset=UTF-8")
    public AjaxResult enumerate() {
        QueryVo queryVo = new QueryVo();
        queryVo.filters.put("type", MonitorRuleType.PRODUCT);
        return AjaxResult.success(monitorRuleService.enumerate(queryVo));
    }

    @PreAuthorize("@ss.hasPermi('system:monitor:rule:query')")
    @GetMapping(value = "/{id}", produces = "application/json;charset=UTF-8")
    public AjaxResult retrieve(@PathVariable("id") @NotNull(message = "id不能为null") Integer id) {
        QueryVo queryVo = new QueryVo();
        queryVo.filters.put("id", id);
        MonitorRule monitorRule = monitorRuleService.retrieve(queryVo);
        if (monitorRule == null) {
            return AjaxResult.error(HttpStatus.NOT_FOUND, MessageUtils.message("error.not_found"));
        }
        return AjaxResult.success(monitorRule);
    }

    @PreAuthorize("@ss.hasPermi('system:monitor:rule:edit')")
    @Log(title = "监控规则", businessType = BusinessType.UPDATE)
    @PutMapping(value = "/edit", produces = "application/json;charset=UTF-8")
    public AjaxResult edit(@RequestBody @Validated(Update.class) MonitorRule monitorRule) {
        QueryVo queryVo = new QueryVo();
        queryVo.filters.put("id", monitorRule.getId());
        MonitorRule target = monitorRuleService.retrieve(queryVo);
        if (target == null) {
            return AjaxResult.error(HttpStatus.NOT_FOUND, MessageUtils.message("error.not_found"));
        }
        monitorRule.setCompanyId(target.getCompanyId());
        AjaxResult ret = checkExistance(monitorRule);
        if (ret != null) {
            return ret;
        }
        if (monitorRuleService.update(monitorRule)) {
            return AjaxResult.success();
        }
        return AjaxResult.error(MessageUtils.message("error.operation_failed"));
    }

    @PreAuthorize("@ss.hasPermi('system:monitor:rule:remove')")
    @Log(title = "监控规则", businessType = BusinessType.DELETE)
    @DeleteMapping(value = "/{id}", produces = "application/json;charset=UTF-8")
    public AjaxResult remove(@PathVariable("id") @NotNull(message = "id不能为null") Integer id) {
        QueryVo queryVo = new QueryVo();
        queryVo.filters.put("id", id);
        if (monitorRuleService.delete(queryVo)) {
            return AjaxResult.success();
        }
        return AjaxResult.error(MessageUtils.message("error.operation_failed"));
    }
}
