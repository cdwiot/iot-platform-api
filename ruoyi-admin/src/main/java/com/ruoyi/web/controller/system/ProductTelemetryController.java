package com.ruoyi.web.controller.system;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.constant.HttpStatus;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.MessageUtils;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.framework.web.service.TokenService;
import com.ruoyi.system.domain.Product;
import com.ruoyi.system.domain.ProductTelemetry;
import com.ruoyi.system.domain.ProductTerminal;
import com.ruoyi.system.domain.ValidationGroups.Create;
import com.ruoyi.system.domain.ValidationGroups.Update;
import com.ruoyi.system.domain.vo.QueryVo;
import com.ruoyi.system.service.FileService;
import com.ruoyi.system.service.IProductService;
import com.ruoyi.system.service.IProductTelemetryService;
import com.ruoyi.system.service.ISysDeptService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings("AlibabaLowerCamelCaseVariableNaming")
@Slf4j
@RestController
@RequestMapping("/system/product.v2")
public class ProductTelemetryController extends BaseController {
    @Autowired
    IProductService productService;

    @Autowired
    IProductTelemetryService productTelemetryService;

    @Autowired
    TokenService tokenService;

    @Autowired
    ISysDeptService sysDeptService;

    private AjaxResult checkExistance(ProductTelemetry productTelemetry) {
        Map<String, Object> existance = productTelemetryService.findExistance(productTelemetry);
        if (existance != null) {
            Object name_exists = existance.get("name_exists");
            if (name_exists != null && Integer.parseInt(name_exists.toString()) > 0) { 
                return AjaxResult.error(HttpStatus.ERROR, "名称已经存在");
            }
            Object terminal_identifier_exists = existance.get("terminal_identifier_exists");
            if (terminal_identifier_exists != null && Integer.parseInt(terminal_identifier_exists.toString()) > 0) { 
                return AjaxResult.error(HttpStatus.ERROR, "终端侧标识符已经存在");
            }
            Object user_identifier_exists = existance.get("user_identifier_exists");
            if (user_identifier_exists != null && Integer.parseInt(user_identifier_exists.toString()) > 0) { 
                return AjaxResult.error(HttpStatus.ERROR, "用户侧标识符已经存在");
            }
        }
        return null;
    }

    @PreAuthorize("@ss.hasPermi('system:product:telemetry:add')")
    @PostMapping(value = "/telemetry/add", produces = "application/json;charset=UTF-8")
    @Log(title = "产品遥测值", businessType = BusinessType.INSERT)
    public AjaxResult addTelemetryOfProduct(@RequestBody @Validated(Create.class) ProductTelemetry productTelemetry) {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        Long companyId = sysDeptService.selectCompanyIdByUser(loginUser.getUser());
        productTelemetry.setCompanyId(companyId);
        AjaxResult ret = checkExistance(productTelemetry);
        if (ret != null) {
            return ret;
        }
        Integer id = productTelemetryService.create(productTelemetry);
        if (id != null) {
            return AjaxResult.success(productTelemetry);
        }
        return AjaxResult.error(MessageUtils.message("error.operation_failed"));
    }

    @PreAuthorize("@ss.hasPermi('system:product:telemetry:list')")
    @GetMapping(value = "/{id}/telemetry/list", produces = "application/json;charset=UTF-8")
    public Object indexTelemetries(@PathVariable("id") @NotNull(message = "id不能为null") Integer id) {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        Long companyId = sysDeptService.selectCompanyIdByUser(loginUser.getUser());
        QueryVo queryVo = new QueryVo();
        queryVo.filters.put("product_id", id);
        queryVo.filters.put("company_id", companyId);
        startPage();
        List<ProductTelemetry> productTelemetries = productTelemetryService.index(queryVo);
        return getDataTable(productTelemetries);
    }

    @PreAuthorize("@ss.hasPermi('system:product:telemetry:list')")
    @GetMapping(value = "/{id}/telemetry/export")
    public void exportTelemetries(@PathVariable("id") @NotNull(message = "id不能为null") Integer id, HttpServletResponse response) {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        Long companyId = sysDeptService.selectCompanyIdByUser(loginUser.getUser());
        QueryVo queryVo = new QueryVo();
        queryVo.filters.put("id", id);
        Product product = productService.retrieve(queryVo);
        if (product == null) {
            return;
        }
        try {
            queryVo.filters.clear();
            queryVo.filters.put("product_id", id);
            queryVo.filters.put("company_id", companyId);
            List<ProductTelemetry> productTelemetries = productTelemetryService.index(queryVo);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
            String filename = String.format("product-telemetry-%s.%s.json", product.getName(), sdf.format(new Date()));
            response.setContentType("application/json");
            FileService.buildFileStream(filename, JSON.toJSONString(productTelemetries), response);
        } catch (Exception e) {
            log.info("",e);
            StackTraceElement[] traces = e.getStackTrace();
            for (StackTraceElement trace : traces) {
                log.debug(trace.toString());
            }
        }
    }

    @PreAuthorize("@ss.hasPermi('system:product:telemetry:add')")
    @PostMapping(value = "/{id}/telemetry/import", produces = "application/json;charset=UTF-8")
    @Log(title = "产品遥测值", businessType = BusinessType.INSERT)
    public AjaxResult importTelemetryOfProduct(@PathVariable("id") @NotNull(message = "id不能为null") Integer productId, @RequestBody List<ProductTelemetry> productTelemetries) {
        List<Object> results = new ArrayList<>();
        QueryVo queryVo = new QueryVo();
        queryVo.filters.put("id", productId);
        Product product = productService.retrieve(queryVo);
        if (product == null) {
            return AjaxResult.error(HttpStatus.NOT_FOUND, MessageUtils.message("error.not_found"));
        }
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        Long companyId = sysDeptService.selectCompanyIdByUser(loginUser.getUser());
        for (ProductTelemetry productTelemetry : productTelemetries) {
            productTelemetry.setId(null);
            productTelemetry.setProductId(productId);
            productTelemetry.setCompanyId(companyId);
            Integer maxTerminalNumber = product.getTerminals().size();
            Integer terminalNumber = productTelemetry.getTerminalNumber();
            if (terminalNumber != null && terminalNumber > maxTerminalNumber) {
                productTelemetry.setTerminalNumber(maxTerminalNumber);
            }
            AjaxResult ret = checkExistance(productTelemetry);
            if (ret != null) {
                results.add(ret);
                continue;
            }
            Integer id = productTelemetryService.create(productTelemetry);
            if (id != null) {
                results.add(AjaxResult.success(productTelemetry));
            } else {
                results.add(AjaxResult.error(MessageUtils.message("error.operation_failed")));
            }
        }
        return AjaxResult.success(results);
    }

    @PreAuthorize("@ss.hasPermi('system:product:telemetry:list')")
    @GetMapping(value = "/{id}/telemetry/enum", produces = "application/json;charset=UTF-8")
    public AjaxResult enumerateTelemetries(@PathVariable("id") @NotNull(message = "id不能为null") Integer id) {
        QueryVo queryVo = new QueryVo();
        queryVo.filters.put("product_id", id);
        return AjaxResult.success(productTelemetryService.enumerate(queryVo));
    }

    @PreAuthorize("@ss.hasPermi('system:product:telemetry:cascader')")
    @GetMapping(value = "/{id}/telemetry/cascader", produces = "application/json;charset=UTF-8")
    public AjaxResult cascaderTelemetries(@PathVariable("id") @NotNull(message = "id不能为null") Integer id) {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        Long companyId = sysDeptService.selectCompanyIdByUser(loginUser.getUser());
        QueryVo queryVo = new QueryVo();
        queryVo.filters.put("id", id);
        Product product = productService.retrieve(queryVo);
        queryVo.filters.clear();
        queryVo.filters.put("product_id", id);
        queryVo.filters.put("company_id", companyId);
        List<ProductTelemetry> productTelemetries = productTelemetryService.index(queryVo);
        List<String> userIdentifiers = new ArrayList<>();
        Map<String, ProductTelemetry> telemetryMap = new HashMap<>();
        for (ProductTelemetry productTelemetry : productTelemetries) {
            userIdentifiers.add(productTelemetry.getUserIdentifier());
            telemetryMap.put(productTelemetry.getUserIdentifier(), productTelemetry);
        }
//        Collections.sort(userIdentifiers);
        JSONArray cascader = new JSONArray();
        for (ProductTerminal productTerminal : product.getTerminals()) {
            JSONObject level_1_node = new JSONObject();
            level_1_node.put("label", productTerminal.getName());
            level_1_node.put("name", productTerminal.getName());
            level_1_node.put("value", productTerminal.getNumber());
            JSONArray children = new JSONArray();
            for (String userIdentifier : userIdentifiers) {
                JSONObject child = new JSONObject();
                ProductTelemetry productTelemetry = telemetryMap.get(userIdentifier);
                boolean noHidden = null == productTelemetry.getIsHidden() || !productTelemetry.getIsHidden();
                if (productTerminal.getNumber().equals(productTelemetry.getTerminalNumber()) && noHidden) {
                    child.put("label", productTelemetry.getUserIdentifier());
                    child.put("value", productTelemetry.getId());
                    child.put("name", productTelemetry.getName());

                    child.put("min", productTelemetry.getMin());
                    child.put("max", productTelemetry.getMax());
                    child.put("groupName", productTelemetry.getGroupName());
                    children.add(child);
                }
            }
            level_1_node.put("children", children);
            cascader.add(level_1_node);
        }
//        JSONArray computed_children = new JSONArray();
//        for (String userIdentifier : userIdentifiers) {
//            ProductTelemetry productTelemetry = telemetryMap.get(userIdentifier);
//            boolean noHidden = null == productTelemetry.getIsHidden() || !productTelemetry.getIsHidden();
//            if (productTelemetry.getIsComputed()&&noHidden) {
//                JSONObject child = new JSONObject();
//                child.put("label", productTelemetry.getUserIdentifier());
//                child.put("value", productTelemetry.getId());
//                child.put("name", productTelemetry.getName());
//                child.put("groupName", productTelemetry.getGroupName());
//                computed_children.add(child);
//            }
//        }
//        if (computed_children.size() > 0) {
//            JSONObject level_1_node = new JSONObject();
//            level_1_node.put("label", "计算属性");
//            level_1_node.put("name", "计算属性");
//            level_1_node.put("value", null);
//            level_1_node.put("children", computed_children);
//            cascader.add(level_1_node);
//        }
        return AjaxResult.success(cascader);
    }

    @PreAuthorize("@ss.hasPermi('system:product:telemetry:query')")
    @GetMapping(value = "/telemetry/{id}", produces = "application/json;charset=UTF-8")
    public AjaxResult retrieveTelemetryOfProduct(@PathVariable("id") @NotNull(message = "id不能为null") Integer id) {
        QueryVo queryVo = new QueryVo();
        queryVo.filters.put("id", id);
        ProductTelemetry productTelemetry = productTelemetryService.retrieve(queryVo);
        if (productTelemetry == null) {
            return AjaxResult.error(HttpStatus.NOT_FOUND, MessageUtils.message("error.not_found"));
        }
        return AjaxResult.success(productTelemetry);
    }

    @PreAuthorize("@ss.hasPermi('system:product:telemetry:edit')")
    @Log(title = "产品遥测值", businessType = BusinessType.UPDATE)
    @PutMapping(value = "/telemetry/edit", produces = "application/json;charset=UTF-8")
    public AjaxResult editTelemetry(@RequestBody @Validated(Update.class) ProductTelemetry productTelemetry) {
        QueryVo queryVo = new QueryVo();
        queryVo.filters.put("id", productTelemetry.getId());
        ProductTelemetry target = productTelemetryService.retrieve(queryVo);
        if (target == null) {
            return AjaxResult.error(HttpStatus.NOT_FOUND, MessageUtils.message("error.not_found"));
        }
        if (!target.getProductId().equals(productTelemetry.getProductId())) {
            return AjaxResult.error(MessageUtils.message("error.operation_failed"));
        }
        if (!target.getIsComputed().equals(productTelemetry.getIsComputed())) {
            return AjaxResult.error(MessageUtils.message("error.operation_failed"));
        }

        if (StringUtils.isNoneBlank(productTelemetry.getMax())&&StringUtils.isNoneBlank(productTelemetry.getMin())) {
            if (StringUtils.isNumeric(productTelemetry.getMax())&&StringUtils.isNumeric(productTelemetry.getMin())) {
                if (Double.parseDouble(productTelemetry.getMax()) < Double.parseDouble(productTelemetry.getMin())) {
                    return AjaxResult.error(MessageUtils.message("error.operation_failed"));
                }
            }
        }
        productTelemetry.setProductId(target.getProductId());
        AjaxResult ret = checkExistance(productTelemetry);
        if (ret != null) {
            return ret;
        }
        if (productTelemetryService.update(productTelemetry)) {
            return AjaxResult.success();
        }
        return AjaxResult.error(MessageUtils.message("error.operation_failed"));
    }

    @PreAuthorize("@ss.hasPermi('system:product:telemetry:remove')")
    @Log(title = "产品遥测值", businessType = BusinessType.DELETE)
    @DeleteMapping(value = "/telemetry/{id}", produces = "application/json;charset=UTF-8")
    public AjaxResult removeTelemetry(@PathVariable("id") @NotNull(message = "id不能为null") Integer id) {
        // TODO: [ZY] check related modules
        QueryVo queryVo = new QueryVo();
        queryVo.filters.put("id", id);
        if (productTelemetryService.delete(queryVo)) {
            return AjaxResult.success();
        }
        return AjaxResult.error(MessageUtils.message("error.operation_failed"));
    }
}
