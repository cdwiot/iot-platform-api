package com.ruoyi.web.controller.system;

import com.ruoyi.framework.web.service.TokenService;
import com.ruoyi.system.service.*;
import com.ruoyi.system.domain.DeviceTerminal;
import com.ruoyi.system.domain.MqttCredentials;
import com.ruoyi.system.domain.SysCompanyThingsboard;
import com.ruoyi.system.domain.ValidationGroups.Create;
import com.ruoyi.system.domain.ValidationGroups.Update;
import com.ruoyi.system.domain.vo.QueryVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;

import lombok.extern.slf4j.Slf4j;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

import javax.validation.constraints.NotNull;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.constant.HttpStatus;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.MessageUtils;
import com.ruoyi.common.utils.ServletUtils;

@SuppressWarnings("AlibabaLowerCamelCaseVariableNaming")
@Slf4j
@RestController
@RequestMapping("/system/device.v2")
public class DeviceTerminalMqttController extends BaseController
{
    @Autowired
    private ISysCompanyThingsboardService sysCompanyThingsboardService;

    @Autowired
    IThingsBoardTenantService thingsBoardTenantService;

    @Autowired
    IProductService productService;

    @Autowired
    private IDeviceTerminalService deviceTerminalService;

    @Autowired
    private IDeviceTerminalMqttService deviceTerminalMqttService;

    @Autowired
    private TokenService tokenService;

    @Autowired
    private ISysDeptService sysDeptService;
    @Autowired
    private IProjectAuthorizeService projectAuthorizeService;

    private AjaxResult checkExistance(MqttCredentials mqttCredentials) {
        Map<String, Object> existance = deviceTerminalMqttService.findExistance(mqttCredentials);
        if (existance != null) {
            Object device_terminal_id_exists = existance.get("device_terminal_id_exists");
            if (device_terminal_id_exists != null && Integer.parseInt(device_terminal_id_exists.toString()) > 0) { 
                return AjaxResult.error(HttpStatus.ERROR, "产品终端id已经存在");
            }
            Object client_id_exists = existance.get("client_id_exists");
            if (client_id_exists != null && Integer.parseInt(client_id_exists.toString()) > 0) { 
                return AjaxResult.error(HttpStatus.ERROR, "客户端id已经存在");
            }
        }
        return null;
    }

    @PreAuthorize("@ss.hasPermi('system:device:terminal:mqtt:credentials:set')")
    @PostMapping(value = "/terminal/mqtt/credentials", produces = "application/json;charset=UTF-8")
    @Log(title = "产品终端", businessType = BusinessType.INSERT)
    public AjaxResult addTerminalMqttCredentials(@RequestBody @Validated(Create.class) MqttCredentials mqttCredentials) {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        SysCompanyThingsboard sysCompanyThingsboard = sysCompanyThingsboardService.selectSysCompanyThingsboardByUser(loginUser.getUser());
        Long companyId = sysDeptService.selectCompanyIdByUser(loginUser.getUser());
        mqttCredentials.setCompanyId(companyId);
        AjaxResult ret = checkExistance(mqttCredentials);
        if (ret != null) {
            return ret;
        }
        QueryVo queryVo = new QueryVo();
        queryVo.filters.put("id", mqttCredentials.getDeviceTerminalId());
        DeviceTerminal deviceTerminal = deviceTerminalService.retrieve(queryVo);
        if (deviceTerminal == null) {
            return AjaxResult.error("没有找到指定的产品终端");
        }
        projectAuthorizeService.checkProjectIsAuthorizeWithDeviceId(loginUser.getUser(),deviceTerminal.getDeviceId());
        if (!"MQTT".equals(deviceTerminal.getProductTerminalProtocol())) {
            return AjaxResult.error("指定的产品终端不是MQTT产品");
        }
        mqttCredentials.setTbDeviceId(deviceTerminal.getTbDeviceId());
        Integer id = deviceTerminalMqttService.createCredentials(mqttCredentials, sysCompanyThingsboard);
        if (id != null) {
            return AjaxResult.success(mqttCredentials);
        }
        return AjaxResult.error(MessageUtils.message("error.operation_failed"));
    }

    @PreAuthorize("@ss.hasPermi('system:device:terminal:mqtt:credentials:get')")
    @GetMapping(value = "/terminal/mqtt/credentials/{id}", produces = "application/json;charset=UTF-8")
    public AjaxResult retrieveTerminalMqttCredentials(@PathVariable("id") @NotNull(message = "id不能为null") Integer id) {
        QueryVo queryVo = new QueryVo();
        queryVo.filters.put("id", id);
        MqttCredentials mqttCredentials = deviceTerminalMqttService.retrieveCredentials(queryVo);
        if (mqttCredentials == null) {
            return AjaxResult.error(HttpStatus.NOT_FOUND, MessageUtils.message("error.not_found"));
        }
        return AjaxResult.success(mqttCredentials);
    }

    @PreAuthorize("@ss.hasPermi('system:device:terminal:mqtt:credentials:set')")
    @PutMapping(value = "/terminal/mqtt/credentials/edit", produces = "application/json;charset=UTF-8")
    @Log(title = "产品终端", businessType = BusinessType.UPDATE)
    public AjaxResult editTerminalMqttCredentials(@RequestBody @Validated(Update.class) MqttCredentials mqttCredentials) {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        SysCompanyThingsboard sysCompanyThingsboard = sysCompanyThingsboardService.selectSysCompanyThingsboardByUser(loginUser.getUser());
        QueryVo queryVo = new QueryVo();
        queryVo.filters.put("id", mqttCredentials.getId());
        MqttCredentials target = deviceTerminalMqttService.retrieveCredentials(queryVo);
        if (target == null) {
            return AjaxResult.error(HttpStatus.NOT_FOUND, MessageUtils.message("error.not_found"));
        }
        mqttCredentials.setDeviceTerminalId(target.getDeviceTerminalId());
        mqttCredentials.setTbDeviceId(target.getTbDeviceId());
        AjaxResult ret = checkExistance(mqttCredentials);
        if (ret != null) {
            return ret;
        }
        if (deviceTerminalMqttService.updateCredentials(mqttCredentials, sysCompanyThingsboard)) {
            return AjaxResult.success();
        }
        return AjaxResult.error(MessageUtils.message("error.operation_failed"));
    }
}
