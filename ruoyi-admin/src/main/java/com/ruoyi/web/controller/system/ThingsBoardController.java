package com.ruoyi.web.controller.system;

import com.ruoyi.framework.web.service.TokenService;
import com.ruoyi.system.service.IThingsBoardTenantService;
import com.ruoyi.system.domain.SysCompanyThingsboard;
import com.ruoyi.system.service.FileService;
import com.ruoyi.system.service.ISysCompanyThingsboardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;

import lombok.extern.slf4j.Slf4j;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.JsonNode;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.utils.ServletUtils;

@Slf4j
@RestController
@RequestMapping("/system/thingsboard")
public class ThingsBoardController extends BaseController
{
    @Autowired
    private ISysCompanyThingsboardService sysCompanyThingsboardService;

    @Autowired
    IThingsBoardTenantService thingsBoardTenantService;

    @Autowired
    private TokenService tokenService;

    @PreAuthorize("@ss.hasPermi('system:develop')")
    @PostMapping(value = "/rule_chain/inject", produces = "application/json;charset=UTF-8")
    public AjaxResult importRuleChain(@RequestBody JsonNode request) {
        String name = request.get("name").asText();
        String filename = request.get("filename").asText();
        String content = FileService.load("/thingsboard/rulechain/" + filename, false);
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        SysCompanyThingsboard sysCompanyThingsboard = sysCompanyThingsboardService.selectSysCompanyThingsboardByUser(loginUser.getUser());
        String ruleChainId = thingsBoardTenantService.importRuleChainEx(name, content, sysCompanyThingsboard);
        return AjaxResult.success(ruleChainId);
    }

    @PreAuthorize("@ss.hasPermi('system:develop')")
    @PostMapping(value = "/device_profile/inject", produces = "application/json;charset=UTF-8")
    public AjaxResult injectDeviceProfile(@RequestBody JsonNode request) {
        String name = request.get("name").asText();
        String filename = request.get("filename").asText();
        String ruleChainId = request.get("ruleChainId").asText();
        String content = FileService.load("/thingsboard/deviceprofile/" + filename, false);
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        SysCompanyThingsboard sysCompanyThingsboard = sysCompanyThingsboardService.selectSysCompanyThingsboardByUser(loginUser.getUser());
        String deviceProfileId = thingsBoardTenantService.importDeviceProfileEx(name, content, ruleChainId, sysCompanyThingsboard);
        return AjaxResult.success(deviceProfileId);
    }
}
