package com.ruoyi.web.controller.system;

import cn.hutool.json.JSONUtil;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.framework.web.service.TokenService;
import com.ruoyi.system.domain.ProductEvent;
import com.ruoyi.system.domain.ValidationGroups.*;
import com.ruoyi.system.service.IProductEventService;
import com.ruoyi.system.service.ISysDeptService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/system/product.v2")
public class ProductEventController extends BaseController {
    @Autowired
    private TokenService tokenService;

    @Autowired
    private ISysDeptService sysDeptService;

    @Autowired
    private IProductEventService productEventService;

    @PreAuthorize("@ss.hasPermi('system:product:event:list')")
    @GetMapping(value = "/event/list", produces = "application/json;charset=UTF-8")
    public TableDataInfo list(@Validated(Select.class)ProductEvent productEvent) {
        startPage();
        List<ProductEvent> productEvents = productEventService.queryList(productEvent);
        return getDataTable(productEvents);
    }

    @PreAuthorize("@ss.hasPermi('system:product:event:list')")
    @GetMapping(value = "/event/enum", produces = "application/json;charset=UTF-8")
    public AjaxResult enumerate(@Validated(Select.class)ProductEvent productEvent) {
        List<ProductEvent> productEvents = productEventService.queryList(productEvent);
        return AjaxResult.success(productEvents);
    }

    @PreAuthorize("@ss.hasPermi('system:product:event:add')")
    @PostMapping(value = "/event/add", produces = "application/json;charset=UTF-8")
    @Log(title = "事件点位值", businessType = BusinessType.INSERT)
    public AjaxResult addTelemetryOfProduct(@RequestBody @Validated(Create.class) ProductEvent productEvent) {
        productEventService.checkIsExit(productEvent);
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        Long companyId = sysDeptService.selectCompanyIdByUser(loginUser.getUser());
        productEvent.setCompanyId(companyId);
        productEvent.setCreateBy(loginUser.getUsername());
        productEvent.setCreateTime(new Date());
        productEvent.setParam(JSONUtil.toJsonStr(productEvent.getParamList()));
        if (productEventService.add(productEvent)) {
            return AjaxResult.success("添加成功");
        }
        return AjaxResult.error("添加失败");
    }

    @PreAuthorize("@ss.hasPermi('system:product:event:query')")
    @GetMapping(value = "/event/{id}", produces = "application/json;charset=UTF-8")
    public AjaxResult getInfo(@PathVariable("id") @NotNull(message = "id不能为null") Integer id) {
        ProductEvent param = new ProductEvent();
        param.setId(id);
        ProductEvent productEvent = productEventService.getOne(param);
        productEvent.setParamList(JSONUtil.parseArray(productEvent.getParam()));
        return AjaxResult.success(productEvent);
    }

    @PreAuthorize("@ss.hasPermi('system:product:event:edit')")
    @Log(title = "事件点位值", businessType = BusinessType.UPDATE)
    @PostMapping(value = "/event/edit", produces = "application/json;charset=UTF-8")
    public AjaxResult editTelemetry(@RequestBody @Validated(Update.class) ProductEvent productEvent) {
        productEventService.checkIsExit(productEvent);
        productEvent.setParam(JSONUtil.toJsonStr(productEvent.getParamList()));
        if (productEventService.update(productEvent)) {
            return AjaxResult.success("修改成功");
        }
        return AjaxResult.error("修改失败");
    }

    @PreAuthorize("@ss.hasPermi('system:product:telemetry:remove')")
    @Log(title = "事件点位值", businessType = BusinessType.DELETE)
    @DeleteMapping(value = "/event/{id}", produces = "application/json;charset=UTF-8")
    public AjaxResult delete(@PathVariable("id") @NotNull(message = "id不能为null") Integer id) {
        ProductEvent param = new ProductEvent();
        param.setId(id);
        if (productEventService.delete(param)) {
            return AjaxResult.success("删除成功");
        }
        return AjaxResult.error("删除失败");
    }
}
