package com.ruoyi.web.controller.system;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.framework.web.service.TokenService;
import com.ruoyi.system.domain.TerminalEventDetail;
import com.ruoyi.system.domain.ValidationGroups.*;
import com.ruoyi.system.service.IProjectAuthorizeService;
import com.ruoyi.system.service.ISysDeptService;
import com.ruoyi.system.service.ISysUserService;
import com.ruoyi.system.service.ITerminalEventDetailService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/system/terminal/event/detail")
public class TerminalEventDetailController extends BaseController {

    @Autowired
    private ITerminalEventDetailService terminalEventDetailService;
    @Autowired
    private TokenService tokenService;
    @Autowired
    private ISysDeptService sysDeptService;
    @Autowired
    private IProjectAuthorizeService projectAuthorizeService;
    @Autowired
    private ISysUserService sysUserService;

    @PreAuthorize("@ss.hasPermi('system:terminal:event:detail:list')")
    @GetMapping("/list")
    public TableDataInfo list(@Validated(Select.class) TerminalEventDetail terminalEventDetail) {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        Long companyId = sysDeptService.selectCompanyIdByUser(loginUser.getUser());
        boolean commonRoleFlag = sysUserService.checkUserIsCommonRole(loginUser.getUser());
        if (!commonRoleFlag){
            projectAuthorizeService.checkProjectIsAuthorizeWithDeviceId(loginUser.getUser(),terminalEventDetail.getDeviceId());
        }
        startPage();
        terminalEventDetail.setCompanyId(companyId);
        List<TerminalEventDetail> terminalEventDetails = terminalEventDetailService.queryList(terminalEventDetail);
        return getDataTable(terminalEventDetails);
    }

    /**
     * 事件详情
     *
     * @param id
     * @return
     */
    @PreAuthorize("@ss.hasPermi('system:terminal:event:detail:query')")
    @GetMapping("/get_info")
    public AjaxResult getInfo(Integer id) {
        TerminalEventDetail param = new TerminalEventDetail();
        param.setId(id);
        TerminalEventDetail terminalEventDetail = terminalEventDetailService.getOne(param);
        return AjaxResult.success(terminalEventDetail);
    }
}
