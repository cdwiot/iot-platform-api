package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.framework.web.service.TokenService;
import com.ruoyi.system.domain.SmsAccount;
import com.ruoyi.system.domain.ValidationGroups.Create;
import com.ruoyi.system.domain.ValidationGroups.Update;
import com.ruoyi.system.service.ISmsAccountService;
import com.ruoyi.system.service.ISysDeptService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Slf4j
@RestController
@RequestMapping("/system/sms/account")
public class SmsAccountController extends BaseController {
//    @Autowired
//    private TokenService tokenService;
//    @Autowired
//    private ISysDeptService sysDeptService;
//    @Autowired
//    private ISmsAccountService messageAccountService;
//
//    /**
//     * 短信配置账户分页查询
//     */
//    @PreAuthorize("@ss.hasPermi('system:messageAccount:list')")
//    @GetMapping("/list")
//    public AjaxResult list() {
//        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
//        SysUser user = loginUser.getUser();
//        Long companyId = sysDeptService.selectCompanyIdByUser(user);
//        SmsAccount smsAccount = new SmsAccount();
//        smsAccount.setCompanyId(companyId);
//        SmsAccount one = messageAccountService.getOne(smsAccount);
//        //是admin账号
//        if (user.isAdmin() && one != null){
//            one.setAccessFlag("0");
//            one.setAddFlag("0");
//            return AjaxResult.success(one);
//        }
//        if (one != null){
//            one.setAccessFlag("1");
//            one.setAddFlag("0");
//            return AjaxResult.success(one);
//        }
//        //默认查询大账号
//        smsAccount.setCompanyId(100L);
//        one = messageAccountService.getOne(smsAccount);
//        if (one != null){
//            one.setAddFlag("1");
//            one.setAccessFlag("0");
//            //将大账号的密钥进行加密
//            one.setSecret("******");
//        }
//        return AjaxResult.success(one);
//    }
//
//    /**
//     * 获取短信配置账户详细信息
//     */
//    @PreAuthorize("@ss.hasPermi('system:messageAccount:query')")
//    @GetMapping(value = "/getInfo")
//    public AjaxResult getInfo(Integer id) {
//        SmsAccount param = new SmsAccount();
//        param.setId(id);
//        SmsAccount smsAccount = messageAccountService.getOne(param);
//        return AjaxResult.success(smsAccount);
//    }
//
//    /**
//     * 添加短信配置账户
//     */
//    @PreAuthorize("@ss.hasPermi('system:messageAccount:add')")
//    @Log(title = "短信配置账户", businessType = BusinessType.INSERT)
//    @PostMapping("/add")
//    public AjaxResult add(@RequestBody @Validated(Create.class) SmsAccount smsAccount) {
//        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
//        // 设置companyId
//        Long companyId = sysDeptService.selectCompanyIdByUser(loginUser.getUser());
//        smsAccount.setCompanyId(companyId);
//        smsAccount.setCreateBy(loginUser.getUsername());
//        smsAccount.setCreateTime(new Date());
//        //messageAccountService.checkParam(productProperty);
//        if (messageAccountService.addMessageAccount(smsAccount)) {
//            return AjaxResult.success("添加成功");
//        }
//        return AjaxResult.error("添加失败");
//    }
//
//    /**
//     * 修改短信配置账户
//     */
//    @PreAuthorize("@ss.hasPermi('system:messageAccount:edit')")
//    @Log(title = "短信配置账户", businessType = BusinessType.UPDATE)
//    @PostMapping("/edit")
//    public AjaxResult edit(@RequestBody @Validated(Update.class) SmsAccount smsAccount) {
//        //messageAccountService.checkParam(messageAccount);
//        if (messageAccountService.updateMessageAccount(smsAccount)) {
//            return AjaxResult.success("修改成功");
//        }
//        return AjaxResult.error("修改失败");
//    }
//
//    /**
//     * 删除短信配置账户
//     */
//    @PreAuthorize("@ss.hasPermi('system:messageAccount:remove')")
//    @Log(title = "短信配置账户", businessType = BusinessType.DELETE)
//    @DeleteMapping("/{id}")
//    public AjaxResult remove(@PathVariable @NotNull(message = "id不能为null") Integer id) {
//        SmsAccount smsAccount = new SmsAccount();
//        smsAccount.setId(id);
//        if (messageAccountService.deleteMessageAccount(smsAccount)) {
//            return AjaxResult.success("删除成功");
//        }
//        return AjaxResult.error("删除失败");
//    }

}
