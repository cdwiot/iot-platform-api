package com.ruoyi.web.controller.system;

import cn.hutool.core.collection.CollUtil;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.framework.web.service.TokenService;
import com.ruoyi.system.service.*;
import com.ruoyi.system.domain.AsyncDetail;
import com.ruoyi.system.domain.Device;
import com.ruoyi.system.domain.Product;
import com.ruoyi.system.domain.SysCompanyThingsboard;
import com.ruoyi.system.domain.ValidationGroups.Create;
import com.ruoyi.system.domain.ValidationGroups.Select;
import com.ruoyi.system.domain.ValidationGroups.Update;
import com.ruoyi.system.domain.vo.QueryVo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;

import lombok.extern.slf4j.Slf4j;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

import javax.validation.constraints.NotNull;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.config.RuoYiConfig;
import com.ruoyi.common.constant.HttpStatus;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.ExcelUtils;
import com.ruoyi.common.utils.MessageUtils;
import com.ruoyi.common.utils.ServletUtils;

@SuppressWarnings("AlibabaLowerCamelCaseVariableNaming")
@Slf4j
@RestController
@RequestMapping("/system/device.v2")
public class DeviceController extends BaseController
{
    @Autowired
    private ISysCompanyThingsboardService sysCompanyThingsboardService;

    @Autowired
    IThingsBoardTenantService thingsBoardTenantService;

    @Autowired
    IProductService productService;

    @Autowired
    private IDeviceService deviceService;

    @Autowired
    private TokenService tokenService;

    @Autowired
    private ISysDeptService sysDeptService;

    @Autowired
    private AsyncService asyncService;

    @Autowired
    private IAsyncDetailService asyncDetailService;

    @Autowired
    private IProjectAuthorizeService projectAuthorizeService;
    @Autowired
    private ICustomPermissionService customPermissionService;
    @Autowired
    private ISysUserService sysUserService;

    private AjaxResult checkExistance(Device device) {
        Map<String, Object> existance = deviceService.findExistance(device);
        if (existance != null) {
            // Object name_exists = existance.get("name_exists");
            // if (name_exists != null && Integer.parseInt(name_exists.toString()) > 0) {
            //     return AjaxResult.error(HttpStatus.ERROR, "设备名称已经存在");
            // }
            Object sn_exists = existance.get("sn_exists");
            if (sn_exists != null && Integer.parseInt(sn_exists.toString()) > 0) {
                return AjaxResult.error(HttpStatus.ERROR, "设备产品代码已经存在");
            }
        }
        return null;
    }

    @PreAuthorize("@ss.hasPermi('system:device:add')")
    @PostMapping(value = "/add", produces = "application/json;charset=UTF-8")
    @Log(title = "产品", businessType = BusinessType.INSERT)
    public AjaxResult add(@RequestBody @Validated(Create.class) Device device) {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        SysCompanyThingsboard sysCompanyThingsboard = sysCompanyThingsboardService.selectSysCompanyThingsboardByUser(loginUser.getUser());
        Long companyId = sysDeptService.selectCompanyIdByUser(loginUser.getUser());
        String createBy = loginUser.getUsername();
        device.setCompanyId(companyId);
        device.setCreateBy(createBy);
        device.setCreateTime(new Date());
        AjaxResult ret = checkExistance(device);
        if (ret != null) {
            return ret;
        }
        QueryVo queryVo = new QueryVo();
        queryVo.filters.put("id", device.getProductId());
        Product product = productService.retrieve(queryVo);
        if (product == null) {
            return AjaxResult.error("没有找到指定的产品");
        }
        Integer id = deviceService.create(device, sysCompanyThingsboard);
        if (id != null) {
            return AjaxResult.success(device);
        }
        return AjaxResult.error(MessageUtils.message("error.operation_failed"));
    }

    @PreAuthorize("@ss.hasPermi('system:device:add')")
    @PostMapping(value = "/import", produces = "application/json;charset=UTF-8")
    @Log(title = "产品", businessType = BusinessType.INSERT)
    public AjaxResult inject(@RequestBody JSONObject request) {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        Long companyId = sysDeptService.selectCompanyIdByUser(loginUser.getUser());
        String filename = request.getString("filename");
        String tid = asyncDetailService.createTid("导入产品", companyId);
        asyncService.injectDevices(tid, filename, loginUser.getUsername(), companyId);
        return AjaxResult.success(tid);
    }

    @PostMapping(value = "/init_device_check_config", produces = "application/json;charset=UTF-8")
    public AjaxResult initDeviceCheckConfig() {
        Boolean aBoolean = deviceService.initDeviceCheckConfig();
        if(aBoolean){
            return AjaxResult.success();
        }
        return AjaxResult.error();

    }
    @PostMapping(value = "/init_device_field", produces = "application/json;charset=UTF-8")
    public AjaxResult initDeviceField() {
        Boolean aBoolean = deviceService.initDeviceField();
        if(aBoolean){
            return AjaxResult.success();
        }
        return AjaxResult.error();

    }

    @PreAuthorize("@ss.hasPermi('system:device:add')")
    @PostMapping(value = "/import_result", produces = "application/json;charset=UTF-8")
    public AjaxResult injectResult(@RequestBody JSONObject request) {
        String tid = request.getString("tid");
        QueryVo queryVo = new QueryVo();
        queryVo.filters.put("tid", tid);
        AsyncDetail asyncDetail = asyncDetailService.retrieve(queryVo);
        JSONObject result = new JSONObject();
        Integer status = asyncDetail.getStatus();
        result.put("status", status);
        if (status > 0) {
            String output = asyncDetail.getData();
            try {
                result.put("output", JSON.parse(output));
            } catch (Exception e) {
                result.put("output", output);
            }
        }
        return AjaxResult.success(result);
    }

    @PreAuthorize("@ss.hasPermi('system:device:list')")
    @GetMapping(value = "/list", produces = "application/json;charset=UTF-8")
    public Object index(@Validated(Select.class) Device device) {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        Long companyId = sysDeptService.selectCompanyIdByUser(loginUser.getUser());
        boolean commonRoleFlag = sysUserService.checkUserIsCommonRole(loginUser.getUser());
        if (device.getProjectId() != null && !commonRoleFlag){
            projectAuthorizeService.checkProjectIsAuthorizeWithProjectId(loginUser.getUser(),device.getProjectId());
        }
        QueryVo queryVo = new QueryVo();
        queryVo.filters.put("name", device.getName());
        queryVo.filters.put("project_code", device.getProjectCode());
        queryVo.filters.put("peer_code", device.getPeerCode());
        queryVo.filters.put("project_id", device.getProjectId());
        queryVo.filters.put("product_id", device.getProductId());
        queryVo.filters.put("sn", device.getSn());
        queryVo.filters.put("terminal_sn", device.getTerminalSn());
        queryVo.filters.put("system", device.getSystem());
        queryVo.filters.put("position_number", device.getPositionNumber());
        queryVo.filters.put("description", device.getDescription());
        queryVo.filters.put("address", device.getAddress());
        queryVo.filters.put("discarded", device.getDiscarded());
        queryVo.filters.put("company_id", companyId);
        //验证当前用户属于客户公司还是总公司
        boolean flag = sysDeptService.checkIsCustomer(loginUser.getUser().getDeptId());
        if (flag){
            queryVo.filters.put("customer_name",loginUser.getUsername());
        }
        if (!commonRoleFlag){
            String sql = customPermissionService.getProjectAuthorizePermission("tbl_device", "project_id", loginUser.getUser());
            queryVo.setCustomPermissionSql(sql);
        }
        startPage();
        List<Device> devices = deviceService.index(queryVo);
        //客户公司过滤项目合同号
        if (flag && CollUtil.isNotEmpty(devices)){
            devices.forEach(a ->{
                a.setProjectCode(null);
            });
        }
        return getDataTable(devices);
    }

    @PreAuthorize("@ss.hasPermi('system:device:list')")
    @GetMapping(value = "/enum", produces = "application/json;charset=UTF-8")
    public Object enumerate() {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        Long companyId = sysDeptService.selectCompanyIdByUser(loginUser.getUser());
        QueryVo queryVo = new QueryVo();
        queryVo.filters.put("company_id", companyId);
        //验证当前用户属于客户公司还是总公司
        boolean flag = sysDeptService.checkIsCustomer(loginUser.getUser().getDeptId());
        if (flag) {
            queryVo.filters.put("customer_name", loginUser.getUsername());
        }
        String sql = customPermissionService.getProjectAuthorizePermission("tbl_device", "project_id", loginUser.getUser());
        queryVo.setCustomPermissionSql(sql);
        List<Map<String, Object>> devices = deviceService.enumerate(queryVo);
        return devices;
    }

    @PreAuthorize("@ss.hasPermi('system:device:enum_available')")
    @GetMapping(value = "/enum_available", produces = "application/json;charset=UTF-8")
    public AjaxResult enumerateAvailable(@Validated(Select.class) Device device) {
        SysUser user = SecurityUtils.getLoginUser().getUser();
        boolean flag = sysDeptService.checkIsCustomer(user.getDeptId());
        QueryVo queryVo = new QueryVo();
        if (flag){
            //客户公司用户只能看自己创建的产品
            queryVo.filters.put("create_by",user.getUserName());
        }
        queryVo.filters.put("product_id", device.getProductId());
        List<Map<Integer, String>> devices = deviceService.enumerateAvailable(queryVo);
        return AjaxResult.success(devices);
    }

    @PreAuthorize("@ss.hasPermi('system:device:query')")
    @GetMapping(value = "/{id}", produces = "application/json;charset=UTF-8")
    public AjaxResult retrieve(@PathVariable("id") @NotNull(message = "id不能为null") Integer id) {
        //TODO 验证当前设备是否属于当前用户所建的项目
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        boolean commonRoleFlag = sysUserService.checkUserIsCommonRole(loginUser.getUser());
        if (!commonRoleFlag){
            projectAuthorizeService.checkProjectIsAuthorizeWithDeviceId(loginUser.getUser(),id);
        }
        QueryVo queryVo = new QueryVo();
        queryVo.filters.put("id", id);
        Device device = deviceService.retrieve(queryVo);
        if (device == null) {
            return AjaxResult.error(HttpStatus.NOT_FOUND, MessageUtils.message("error.not_found"));
        }
        return AjaxResult.success(device);
    }

    @PreAuthorize("@ss.hasPermi('system:device:edit')")
    @Log(title = "产品", businessType = BusinessType.UPDATE)
    @PutMapping(value = "/edit", produces = "application/json;charset=UTF-8")
    public AjaxResult edit(@RequestBody @Validated(Update.class) Device device)
    {
        //TODO 验证当前设备是否属于当前用户所建的项目
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        projectAuthorizeService.checkProjectIsAuthorizeWithDeviceId(loginUser.getUser(),device.getId());
        //验证当前用户是否是该产品创建人
        projectAuthorizeService.checkDeviceOperationStatus(loginUser.getUser(),device.getId());
        QueryVo queryVo = new QueryVo();
        queryVo.filters.put("id", device.getId());
        Device target = deviceService.retrieve(queryVo);
        if (target == null) {
            return AjaxResult.error(HttpStatus.NOT_FOUND, MessageUtils.message("error.not_found"));
        }
        device.setCompanyId(target.getCompanyId());
        AjaxResult ret = checkExistance(device);
        if (ret != null) {
            return ret;
        }
        if (deviceService.update(device)) {
            return AjaxResult.success();
        }
        return AjaxResult.error(MessageUtils.message("error.operation_failed"));
    }

    @PreAuthorize("@ss.hasPermi('system:device:discard')")
    @Log(title = "产品", businessType = BusinessType.UPDATE)
    @PutMapping(value = "/discard/{id}", produces = "application/json;charset=UTF-8")
    public AjaxResult discard(@PathVariable("id") @NotNull(message = "id不能为null") Integer id)
    {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        Long companyId = sysDeptService.selectCompanyIdByUser(loginUser.getUser());
        projectAuthorizeService.checkProjectIsAuthorizeWithDeviceId(loginUser.getUser(),id);
        //验证当前用户是否是该产品创建人
        projectAuthorizeService.checkDeviceOperationStatus(loginUser.getUser(),id);
        // TODO: [ZY] check related modules
        QueryVo queryVo = new QueryVo();
        queryVo.filters.put("id", id);
        queryVo.filters.put("company_id", companyId);
        if (deviceService.discard(queryVo)) {
            return AjaxResult.success();
        }
        return AjaxResult.error(MessageUtils.message("error.operation_failed"));
    }

    @PreAuthorize("@ss.hasPermi('system:device:remove')")
    @Log(title = "产品", businessType = BusinessType.DELETE)
    @DeleteMapping(value = "/{id}", produces = "application/json;charset=UTF-8")
    public AjaxResult remove(@PathVariable("id") @NotNull(message = "id不能为null") Integer id)
    {
        //TODO 验证当前设备是否属于当前用户所建的项目
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        projectAuthorizeService.checkProjectIsAuthorizeWithDeviceId(loginUser.getUser(),id);
        //验证当前用户是否是该产品创建人
        projectAuthorizeService.checkDeviceOperationStatus(loginUser.getUser(),id);
        //LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        SysCompanyThingsboard sysCompanyThingsboard = sysCompanyThingsboardService.selectSysCompanyThingsboardByUser(loginUser.getUser());
        Long companyId = sysDeptService.selectCompanyIdByUser(loginUser.getUser());
        // TODO: [ZY] check related modules
        QueryVo queryVo = new QueryVo();
        queryVo.filters.put("id", id);
        queryVo.filters.put("company_id", companyId);
        if (deviceService.delete(queryVo, sysCompanyThingsboard)) {
            return AjaxResult.success();
        }
        return AjaxResult.error(MessageUtils.message("error.operation_failed"));
    }
}
