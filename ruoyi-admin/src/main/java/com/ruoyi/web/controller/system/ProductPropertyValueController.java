package com.ruoyi.web.controller.system;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.framework.web.service.TokenService;
import com.ruoyi.system.domain.ProductPropertyValue;
import com.ruoyi.system.domain.vo.ProductPropertyValueResultVO;
import com.ruoyi.system.domain.vo.ProductPropertyValueVO;
import com.ruoyi.system.service.IProductPropertyValueService;
import com.ruoyi.system.service.ISysDeptService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@SuppressWarnings("AlibabaLowerCamelCaseVariableNaming")
@Slf4j
@RestController
@RequestMapping("/system/product/property/value")
public class ProductPropertyValueController extends BaseController {
    @Autowired
    private TokenService tokenService;
    @Autowired
    private ISysDeptService sysDeptService;
    @Autowired
    private IProductPropertyValueService productPropertyValueService;

    @PreAuthorize("@ss.hasPermi('system:product:property:value:list')")
    @GetMapping("/productId")
    public TableDataInfo queryProductPropertyByProductId(Integer productId, Integer propertyId, String propertyGroup) {
        ProductPropertyValue productPropertyValue = new ProductPropertyValue();
        productPropertyValue.setProductId(productId);
        productPropertyValue.setPropertyId(propertyId);
        productPropertyValue.setPropertyGroup(propertyGroup);
        List<ProductPropertyValueResultVO> productPropertyValueVOS = productPropertyValueService.queryProductPropertyValue(productPropertyValue);
        return getDataTable(productPropertyValueVOS);
    }

    @PreAuthorize("@ss.hasPermi('system:product:property:value:saveOrUpdate')")
    @PostMapping("/saveOrUpdate")
    public AjaxResult saveOrUpdate(@RequestBody List<ProductPropertyValue> productPropertyValues) {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        // 设置companyId
        Long companyId = sysDeptService.selectCompanyIdByUser(loginUser.getUser());
        Boolean aBoolean = productPropertyValueService.saveOrUpdate(productPropertyValues, loginUser.getUsername(), companyId);
        if (aBoolean) {
            return AjaxResult.success("修改成功！");
        }
        return AjaxResult.error("修改失败！");
    }

    /**
     * 产品售后服务查询
     *
     * @param productName 产品名称
     * @param code        产品Id
     * @param propertyId  属性Id
     * @return
     */
    @PreAuthorize("@ss.hasPermi('system:productPropertyValue:getProductValue')")
    @GetMapping("/get_product_value")
    public TableDataInfo getProductValue(String productName, String code, Integer propertyId) {
        ProductPropertyValueVO value = new ProductPropertyValueVO();
        value.setPropertyId(propertyId);
        value.setProductCode(code);
        value.setProductName(productName);
        startPage();
        List<ProductPropertyValueVO> productValue = productPropertyValueService.getProductValue(value);
        return getDataTable(productValue);
    }
}
