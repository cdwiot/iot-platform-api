package com.ruoyi.web.controller.system;

import com.ruoyi.framework.web.service.TokenService;
import com.ruoyi.system.service.IThingsBoardTenantService;
import com.ruoyi.system.domain.CustomConfig;
import com.ruoyi.system.domain.SysCompanyThingsboard;
import com.ruoyi.system.domain.ValidationGroups.Update;
import com.ruoyi.system.domain.vo.QueryVo;
import com.ruoyi.system.service.FileService;
import com.ruoyi.system.service.ICustomConfigService;
import com.ruoyi.system.service.ISysCompanyThingsboardService;
import com.ruoyi.system.service.ISysDeptService;

import org.hibernate.validator.constraints.Length;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;

import lombok.extern.slf4j.Slf4j;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.databind.JsonNode;
import com.ruoyi.common.constant.HttpStatus;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.utils.MessageUtils;
import com.ruoyi.common.utils.ServletUtils;

@Slf4j
@RestController
@RequestMapping("/system/custom_config")
public class CustomConfigController extends BaseController {
    @Autowired
    private TokenService tokenService;

    @Autowired
    private ISysDeptService sysDeptService;

    @Autowired
    private ICustomConfigService customConfigService;

    @PreAuthorize("@ss.hasPermi('system:custom_config:get')")
    @GetMapping(value = "/{name}", produces = "application/json;charset=UTF-8")
    public AjaxResult load(@PathVariable("name")
        @NotBlank(message = "配置项名称不能为空")
        @Length(min = 0, max = 128, message = "配置项名称应该少于{max}字符") String name) {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        Long companyId = sysDeptService.selectCompanyIdByUser(loginUser.getUser());
        QueryVo queryVo = new QueryVo();
        queryVo.filters.put("name", name);
        queryVo.filters.put("company_id", companyId);
        CustomConfig customConfig = customConfigService.load(queryVo);
        if (customConfig == null) {
            return AjaxResult.error(HttpStatus.NOT_FOUND, MessageUtils.message("error.not_found"));
        }
        return AjaxResult.success(customConfig);
    }

    @PreAuthorize("@ss.hasPermi('system:custom_config:set')")
    @PostMapping(value = "/{name}", produces = "application/json;charset=UTF-8")
    public AjaxResult save(@PathVariable("name")
        @NotBlank(message = "配置项名称不能为空")
        @Length(min = 0, max = 128, message = "配置项名称应该少于{max}字符") String name,
        @RequestBody @Validated(Update.class) CustomConfig customConfig) {
        customConfig.setName(name);
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        customConfig.setCreateBy(loginUser.getUsername());
        Long companyId = sysDeptService.selectCompanyIdByUser(loginUser.getUser());
        customConfig.setCompanyId(companyId);
        if (customConfigService.save(customConfig)) {
            return AjaxResult.success();
        }
        return AjaxResult.error(MessageUtils.message("error.operation_failed"));
    }
}
