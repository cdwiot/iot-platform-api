package com.ruoyi.web.controller.system;

import cn.hutool.core.util.StrUtil;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.exception.CustomException;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.framework.web.service.TokenService;
import com.ruoyi.system.domain.ProductPointCheckConfig;
import com.ruoyi.system.domain.ValidationGroups.*;
import com.ruoyi.system.domain.vo.QueryVo;
import com.ruoyi.system.service.IProductPointCheckConfigService;
import com.ruoyi.system.service.ISysDeptService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Slf4j
@RestController
@RequestMapping("/system/product/point/check/config")
public class ProductPointCheckConfigController extends BaseController {
    @Autowired
    private IProductPointCheckConfigService productPointCheckConfigService;
    @Autowired
    private TokenService tokenService;
    @Autowired
    private ISysDeptService sysDeptService;

//    @PreAuthorize("@ss.hasPermi('system:product:point:check:config:list')")
//    @GetMapping("/list")
//    public TableDataInfo queryList(@Validated(Select.class)ProductPointCheckConfig productPointCheckConfig) {
//        QueryVo queryVo = new QueryVo();
//        queryVo.filters.put("type", productPointCheckConfig.getType());
//        queryVo.filters.put("product_id", productPointCheckConfig.getProductId());
//        queryVo.filters.put("project_id", productPointCheckConfig.getProjectId());
//        queryVo.filters.put("item", productPointCheckConfig.getItem());
//        queryVo.filters.put("data_type", productPointCheckConfig.getDataType());
//        startPage();
//        List<ProductPointCheckConfig> productPointCheckConfigs = productPointCheckConfigService.queryPageList(queryVo);
//        return getDataTable(productPointCheckConfigs);
//    }
//
//    @PreAuthorize("@ss.hasPermi('system:product:point:check:config:getInfo')")
//    @GetMapping("/get_info")
//    public AjaxResult getInfo(Integer id) {
//        QueryVo queryVo = new QueryVo();
//        queryVo.filters.put("id", id);
//        ProductPointCheckConfig productPointCheckConfig = productPointCheckConfigService.getOne(queryVo);
//        return AjaxResult.success(productPointCheckConfig);
//    }
//
//    @PreAuthorize("@ss.hasPermi('system:product:point:check:config:add')")
//    @Log(title = "产品型号点检配置", businessType = BusinessType.INSERT)
//    @PostMapping("/add")
//    public AjaxResult add(@RequestBody @Validated(Create.class) ProductPointCheckConfig productPointCheckConfig) {
//        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
//        // 设置companyId
//        Long companyId = sysDeptService.selectCompanyIdByUser(loginUser.getUser());
//        productPointCheckConfigService.checkIsExist(productPointCheckConfig);
//        productPointCheckConfig.setCompanyId(companyId);
//        productPointCheckConfig.setCreateBy(loginUser.getUsername());
//        productPointCheckConfig.setCreateTime(new Date());
//        if (productPointCheckConfigService.add(productPointCheckConfig)) {
//            return AjaxResult.success("添加成功");
//        }
//        return AjaxResult.error("添加失败");
//    }
//
//    @PreAuthorize("@ss.hasPermi('system:product:point:check:config:edit')")
//    @Log(title = "产品型号点检配置", businessType = BusinessType.UPDATE)
//    @PostMapping("/edit")
//    public AjaxResult edit(@RequestBody @Validated(Update.class) ProductPointCheckConfig productPointCheckConfig) {
//        productPointCheckConfigService.checkIsExist(productPointCheckConfig);
//        if (productPointCheckConfigService.update(productPointCheckConfig)) {
//            return AjaxResult.success("修改成功");
//        }
//        return AjaxResult.error("修改失败");
//    }
//
//    @PreAuthorize("@ss.hasPermi('system:product:point:check:config:delete')")
//    @Log(title = "产品型号点检配置", businessType = BusinessType.DELETE)
//    @DeleteMapping("/{id}")
//    public AjaxResult delete(@PathVariable @NotNull(message = "id不能为null") Integer id) {
//        QueryVo queryVo = new QueryVo();
//        queryVo.filters.put("id", id);
//        if (productPointCheckConfigService.delete(queryVo)) {
//            return AjaxResult.success("删除成功");
//        }
//        return AjaxResult.error("删除失败");
//    }
//
//    @PreAuthorize("@ss.hasPermi('system:product:point:check:config:delete')")
//    @DeleteMapping("/delete_batch")
//    public AjaxResult deleteBatch(String ids) {
//        if (StrUtil.isBlank(ids)) {
//            throw new CustomException("配置id不能为空");
//        }
//        if (productPointCheckConfigService.deleteBatch(StrUtil.splitTrim(ids, ","))) {
//            return AjaxResult.success("删除成功");
//        }
//        return AjaxResult.error("删除失败");
//    }
//
//    @PreAuthorize("@ss.hasPermi('system:product:point:check:config:list')")
//    @GetMapping("/enum/{projectId}/{productId}")
//    public AjaxResult typeEnum(@PathVariable @NotNull(message = "项目id不能为null") Integer projectId,@PathVariable @NotNull(message = "产品型号id不能为null") Integer productId) {
//        QueryVo queryVo = new QueryVo();
//        queryVo.filters.put("product_id", productId);
//        queryVo.filters.put("project_id", projectId);
//        Set<String> typeEnum = productPointCheckConfigService.typeEnum(queryVo);
//        return AjaxResult.success(typeEnum);
//    }
//
//    @PreAuthorize("@ss.hasPermi('system:product:point:check:config:list')")
//    @GetMapping("/config/{projectId}/{productId}")
//    public AjaxResult queryConfig(@PathVariable @NotNull(message = "项目id不能为null") Integer projectId,@PathVariable @NotNull(message = "产品型号id不能为null") Integer productId){
//        QueryVo queryVo = new QueryVo();
//        queryVo.filters.put("product_id", productId);
//        queryVo.filters.put("project_id", projectId);
//        List<Map<String, Object>> mapList = productPointCheckConfigService.queryConfig(queryVo);
//        return AjaxResult.success(mapList);
//    }
}
