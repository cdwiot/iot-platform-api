package com.ruoyi.web.controller.system;

import com.ruoyi.framework.web.service.TokenService;
import com.ruoyi.system.domain.Portal;
import com.ruoyi.system.domain.ValidationGroups.Update;
import com.ruoyi.system.domain.vo.QueryVo;
import com.ruoyi.system.service.IPortalService;
import com.ruoyi.system.service.ISysDeptService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.DigestUtils;
import org.springframework.validation.annotation.Validated;

import lombok.extern.slf4j.Slf4j;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Base64;

import com.ruoyi.common.constant.HttpStatus;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.utils.MessageUtils;
import com.ruoyi.common.utils.ServletUtils;

@Slf4j
@RestController
@RequestMapping("/system/portal")
public class PortalController extends BaseController {
    @Autowired
    private TokenService tokenService;

    @Autowired
    private ISysDeptService sysDeptService;

    @Autowired
    private IPortalService portalService;

    @GetMapping(value = "/lookup", produces = "application/json;charset=UTF-8")
    public AjaxResult lookup(@RequestParam(name = "code") String code) {
        QueryVo queryVo = new QueryVo();
        queryVo.filters.put("code", code);
        Portal portal = portalService.load(queryVo);
        if (portal == null) {
            return AjaxResult.error(HttpStatus.NOT_FOUND, MessageUtils.message("error.not_found"));
        }
        return AjaxResult.success(portal);
    }

    @GetMapping(value = "/get", produces = "application/json;charset=UTF-8")
    public AjaxResult load() {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        Long companyId = sysDeptService.selectCompanyIdByUser(loginUser.getUser());
        QueryVo queryVo = new QueryVo();
        queryVo.filters.put("company_id", companyId);
        Portal portal = portalService.load(queryVo);
        if (portal == null) {
            return AjaxResult.error(HttpStatus.NOT_FOUND, MessageUtils.message("error.not_found"));
        }
        return AjaxResult.success(portal);
    }

    @PreAuthorize("@ss.hasPermi('system:portal:set')")
    @PostMapping(value = "/set", produces = "application/json;charset=UTF-8")
    public AjaxResult save(@RequestBody @Validated(Update.class) Portal portal) {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        Long companyId = sysDeptService.selectCompanyIdByUser(loginUser.getUser());
        String code = DigestUtils.md5DigestAsHex(companyId.toString().getBytes()).toLowerCase();
        portal.setCode(code);
        portal.setCompanyId(companyId);
        if (portalService.save(portal)) {
            return AjaxResult.success();
        }
        return AjaxResult.error(MessageUtils.message("error.operation_failed"));
    }
}
