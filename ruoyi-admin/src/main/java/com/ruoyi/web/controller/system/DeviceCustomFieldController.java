package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.framework.web.service.TokenService;
import com.ruoyi.system.domain.DeviceCustomField;
import com.ruoyi.system.domain.DeviceProperty;
import com.ruoyi.system.domain.DevicePropertyValue;
import com.ruoyi.system.domain.ValidationGroups.Create;
import com.ruoyi.system.domain.ValidationGroups.Select;
import com.ruoyi.system.domain.ValidationGroups.Update;
import com.ruoyi.system.domain.vo.QueryVo;
import com.ruoyi.system.service.IDeviceCustomFieldService;
import com.ruoyi.system.service.IDevicePropertyService;
import com.ruoyi.system.service.IDevicePropertyValueService;
import com.ruoyi.system.service.ISysDeptService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/system/device/custom/field")
public class DeviceCustomFieldController extends BaseController {

    @Autowired
    private IDeviceCustomFieldService deviceCustomFieldService;
    @Autowired
    private TokenService tokenService;
    @Autowired
    private ISysDeptService sysDeptService;


    /**
     * 产品自定义字段分页查询
     */
    @PreAuthorize("@ss.hasPermi('system:device:property:value:list')")
    @GetMapping("/list")
    public TableDataInfo list(@Validated(Select.class) DeviceCustomField deviceCustomField) {
        LoginUser loginUser = SecurityUtils.getLoginUser();
        // 设置companyId
        Long companyId = sysDeptService.selectCompanyIdByUser(loginUser.getUser());
        QueryVo queryVo = new QueryVo();
        queryVo.filters.put("company_id", companyId);
        queryVo.filters.put("device_id", deviceCustomField.getDeviceId());
        queryVo.filters.put("name", deviceCustomField.getName());
        queryVo.filters.put("value", deviceCustomField.getValue());
        startPage();
        List<DeviceCustomField> deviceCustomFields = deviceCustomFieldService.queryPageList(queryVo);
        return getDataTable(deviceCustomFields);
    }

    /**
     * 获取产品自定义字段详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:device:property:value:list')")
    @GetMapping(value = "/getInfo")
    public AjaxResult getInfo(Integer id) {
        QueryVo queryVo = new QueryVo();
        queryVo.filters.put("id",id);
        LoginUser loginUser = SecurityUtils.getLoginUser();
        // 设置companyId
        Long companyId = sysDeptService.selectCompanyIdByUser(loginUser.getUser());
        queryVo.filters.put("company_id",companyId);
        DeviceCustomField deviceCustomField = deviceCustomFieldService.getOne(queryVo);
        return AjaxResult.success(deviceCustomField);
    }

    /**
     * 新增产品自定义字段
     */
    @PreAuthorize("@ss.hasPermi('system:device:property:value:saveOrUpdate')")
    @Log(title = "产品额外属性", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    public AjaxResult add(@RequestBody @Validated(Create.class) DeviceCustomField deviceCustomField) {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        // 设置companyId
        Long companyId = sysDeptService.selectCompanyIdByUser(loginUser.getUser());
        deviceCustomField.setCompanyId(companyId);
        deviceCustomField.setCreateBy(loginUser.getUsername());
        deviceCustomField.setCreateTime(new Date());
        //验证属性名称是否重复
        deviceCustomFieldService.checkIsExist(deviceCustomField);
        if (deviceCustomFieldService.add(deviceCustomField)) {
            return AjaxResult.success("添加成功");
        }
        return AjaxResult.error("添加失败");
    }

    /**
     * 修改产品自定义字段
     */
    @PreAuthorize("@ss.hasPermi('system:device:property:value:saveOrUpdate')")
    @Log(title = "产品额外属性", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    public AjaxResult edit(@RequestBody @Validated(Update.class) DeviceCustomField deviceCustomField) {
        //验证属性名称是否重复
        deviceCustomFieldService.checkIsExist(deviceCustomField);
        if (deviceCustomFieldService.update(deviceCustomField)) {
            return AjaxResult.success("修改成功");
        }
        return AjaxResult.error("修改失败");
    }

    /**
     * 删除产品自定义字段
     */
    @PreAuthorize("@ss.hasPermi('system:device:property:value:saveOrUpdate')")
    @Log(title = "产品额外属性", businessType = BusinessType.DELETE)
    @DeleteMapping("/{id}")
    public AjaxResult remove(@PathVariable @NotNull(message = "id不能为null") Integer id) {
        QueryVo queryVo = new QueryVo();
        //删除
        queryVo.filters.put("id",id);
        LoginUser loginUser = SecurityUtils.getLoginUser();
        // 设置companyId
        Long companyId = sysDeptService.selectCompanyIdByUser(loginUser.getUser());
        queryVo.filters.put("company_id",companyId);
        deviceCustomFieldService.delete(queryVo);
        return AjaxResult.success("删除成功");
    }

}
