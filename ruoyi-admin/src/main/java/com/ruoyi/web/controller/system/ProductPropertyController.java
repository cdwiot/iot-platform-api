package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.framework.web.service.TokenService;
import com.ruoyi.system.domain.ProductProperty;
import com.ruoyi.system.domain.ProductPropertyValue;
import com.ruoyi.system.domain.ValidationGroups;
import com.ruoyi.system.service.IProductPropertyService;
import com.ruoyi.system.service.IProductPropertyValueService;
import com.ruoyi.system.service.ISysDeptService;
import com.ruoyi.system.domain.ValidationGroups.Create;
import com.ruoyi.system.domain.ValidationGroups.Update;
import com.ruoyi.system.domain.ValidationGroups.Select;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/system/productProperty")
public class ProductPropertyController extends BaseController {
    @Autowired
    private IProductPropertyService productPropertyService;
    @Autowired
    private IProductPropertyValueService productPropertyValueService;
    @Autowired
    private TokenService tokenService;
    @Autowired
    private ISysDeptService sysDeptService;


    /**
     * 产品额外属性分页查询
     */
    @PreAuthorize("@ss.hasPermi('system:product:property:list')")
    @GetMapping("/list")
    public TableDataInfo list(@Validated(Select.class) ProductProperty productProperty) {
        startPage();
        List<ProductProperty> productProperties = productPropertyService.queryPageList(productProperty);
        return getDataTable(productProperties);
    }

    /**
     * 获取产品额外属性详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:product:property:get_info')")
    @GetMapping(value = "/getInfo")
    public AjaxResult getInfo(Integer id) {
        ProductProperty param = new ProductProperty();
        param.setId(id);
        ProductProperty productProperty = productPropertyService.queryById(param);
        return AjaxResult.success(productProperty);
    }

    /**
     * 新增产品额外属性
     */
    @PreAuthorize("@ss.hasPermi('system:product:property:add')")
    @Log(title = "产品额外属性", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    public AjaxResult add(@RequestBody @Validated(Create.class) ProductProperty productProperty) {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        // 设置companyId
        Long companyId = sysDeptService.selectCompanyIdByUser(loginUser.getUser());
        productProperty.setCompanyId(companyId);
        productProperty.setCreateBy(loginUser.getUsername());
        productProperty.setCreateTime(new Date());
        productPropertyService.checkParam(productProperty);
        if (productPropertyService.addProductProperty(productProperty) > 0) {
            return AjaxResult.success("添加成功");
        }
        return AjaxResult.error("添加失败");
    }

    /**
     * 修改产品额外属性
     */
    @PreAuthorize("@ss.hasPermi('system:product:property:edit')")
    @Log(title = "产品额外属性", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    public AjaxResult edit(@RequestBody @Validated(Update.class) ProductProperty productProperty) {
        productPropertyService.checkParam(productProperty);
        if (productPropertyService.updateProductProperty(productProperty) > 0) {
            return AjaxResult.success("修改成功");
        }
        return AjaxResult.error("修改失败");
    }

    /**
     * 删除产品额外属性
     */
    @PreAuthorize("@ss.hasPermi('system:product:property:remove')")
    @Log(title = "产品额外属性", businessType = BusinessType.DELETE)
    @DeleteMapping("/{id}")
    public AjaxResult remove(@PathVariable @NotNull(message = "id不能为null") Integer id) {
        ProductProperty productProperty = new ProductProperty();
        productProperty.setId(id);
        productPropertyService.deleteProductProperty(productProperty);
        //删除属性值
        ProductPropertyValue productPropertyValue = new ProductPropertyValue();
        productPropertyValue.setPropertyId(id);
        productPropertyValueService.deleteProductPropertyValue(productPropertyValue);
        return AjaxResult.success("删除成功");
    }
}
