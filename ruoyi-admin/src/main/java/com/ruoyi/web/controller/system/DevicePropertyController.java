package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.framework.web.service.TokenService;
import com.ruoyi.system.domain.DeviceProperty;
import com.ruoyi.system.domain.DevicePropertyValue;
import com.ruoyi.system.domain.ValidationGroups.Select;
import com.ruoyi.system.service.IDevicePropertyService;
import com.ruoyi.system.service.IDevicePropertyValueService;
import com.ruoyi.system.service.ISysDeptService;
import com.ruoyi.system.domain.ValidationGroups.Create;
import com.ruoyi.system.domain.ValidationGroups.Update;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/system/deviceProperty")
public class DevicePropertyController extends BaseController {

    @Autowired
    private IDevicePropertyService devicePropertyService;
    @Autowired
    private IDevicePropertyValueService devicePropertyValueService;
    @Autowired
    private TokenService tokenService;
    @Autowired
    private ISysDeptService sysDeptService;


    /**
     * 产品额外属性分页查询
     */
    @PreAuthorize("@ss.hasPermi('system:device:property:list')")
    @GetMapping("/list")
    public TableDataInfo list(@Validated(Select.class) DeviceProperty deviceProperty) {
        startPage();
        List<DeviceProperty> deviceProperties = devicePropertyService.queryPageList(deviceProperty);
        return getDataTable(deviceProperties);
    }

    /**
     * 获取产品额外属性详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:device:property:get_info')")
    @GetMapping(value = "/getInfo")
    public AjaxResult getInfo(Integer id) {
        DeviceProperty param = new DeviceProperty();
        param.setId(id);
        DeviceProperty deviceProperty = devicePropertyService.queryById(param);
        return AjaxResult.success(deviceProperty);
    }

    /**
     * 新增产品额外属性
     */
    @PreAuthorize("@ss.hasPermi('system:device:property:add')")
    @Log(title = "产品额外属性", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    public AjaxResult add(@RequestBody @Validated(Create.class) DeviceProperty deviceProperty) {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        // 设置companyId
        Long companyId = sysDeptService.selectCompanyIdByUser(loginUser.getUser());
        deviceProperty.setCompanyId(companyId);
        deviceProperty.setCreateBy(loginUser.getUsername());
        deviceProperty.setCreateTime(new Date());
        //验证属性名称是否重复
        devicePropertyService.checkParam(deviceProperty);
        if (devicePropertyService.addDeviceProperty(deviceProperty) > 0) {
            return AjaxResult.success("添加成功");
        }
        return AjaxResult.error("添加失败");
    }

    /**
     * 修改产品额外属性
     */
    @PreAuthorize("@ss.hasPermi('system:device:property:edit')")
    @Log(title = "产品额外属性", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    public AjaxResult edit(@RequestBody @Validated(Update.class) DeviceProperty deviceProperty) {
        //验证属性名称是否重复
        devicePropertyService.checkParam(deviceProperty);
        if (devicePropertyService.updateDeviceProperty(deviceProperty) > 0) {
            return AjaxResult.success("修改成功");
        }
        return AjaxResult.error("修改失败");
    }

    /**
     * 删除产品额外属性
     */
    @PreAuthorize("@ss.hasPermi('system:device:property:remove')")
    @Log(title = "产品额外属性", businessType = BusinessType.DELETE)
    @DeleteMapping("/{id}")
    public AjaxResult remove(@PathVariable @NotNull(message = "id不能为null") Integer id) {
        DeviceProperty deviceProperty = new DeviceProperty();
        deviceProperty.setId(id);
        //删除属性
        devicePropertyService.deleteDeviceProperty(deviceProperty);
        DevicePropertyValue devicePropertyValue = new  DevicePropertyValue();
        devicePropertyValue.setPropertyId(id);
        //删除属性值
        devicePropertyValueService.deleteDevicePropertyValue(devicePropertyValue);
        return AjaxResult.success("删除成功");
    }

}
