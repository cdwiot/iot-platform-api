{
    "first": {
        "value": "项目 [${project}] 所属产品 [${names}]",
        "color": "#173177"
    },
    "content": {
        "value": "触发报警规则 [${rule}]",
        "color": "#173177"
    },
    "occurtime": {
        "value": "${time}",
        "color": "#173177"
    },
    "remark": {
        "value": "报警等级为 [${severity}]，请及时处理",
        "color": "#173177"
    }
}