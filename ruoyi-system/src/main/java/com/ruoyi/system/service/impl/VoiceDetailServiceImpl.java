package com.ruoyi.system.service.impl;

import com.ruoyi.common.annotation.UserDataIsolation;
import com.ruoyi.system.domain.VoiceDetail;
import com.ruoyi.system.mapper.VoiceDetailMapper;
import com.ruoyi.system.service.IVoiceDetailService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class VoiceDetailServiceImpl implements IVoiceDetailService {

    @Autowired
    private VoiceDetailMapper voiceDetailMapper;

    @Override
    public Boolean create(VoiceDetail voiceDetail) {
        return voiceDetailMapper.create(voiceDetail) > 0;
    }

    @Override
    @UserDataIsolation(tableAlias = "t1")
    public List<VoiceDetail> index(VoiceDetail voiceDetail) {
        return voiceDetailMapper.index(voiceDetail);
    }

    @Override
    @UserDataIsolation(tableAlias = "t1")
    public VoiceDetail retrieve(VoiceDetail voiceDetail) {
        return voiceDetailMapper.retrieve(voiceDetail);
    }
}
