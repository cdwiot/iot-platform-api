package com.ruoyi.system.service;

import com.alibaba.fastjson.JSONObject;
import com.ruoyi.system.domain.RpcMsgLog;
import com.ruoyi.system.domain.SysCompanyThingsboard;
import com.ruoyi.system.domain.vo.QueryVo;

import java.util.List;

/**
 * 阿里云短信服务
 */
public interface IRpcMsgLogService {

    void saveFailureRpcReques(String deviceId, JSONObject requestBody, SysCompanyThingsboard sysCompanyThingsboard);

    boolean exeRpcReques(Long id);

    List<RpcMsgLog> queryPage(QueryVo queryVo);

    RpcMsgLog getModeById(Long id);
}
