package com.ruoyi.system.service;

import com.ruoyi.system.domain.TerminalEventDetail;

import java.util.List;

public interface ITerminalEventDetailService {

    /**
     * 事件详情的查询
     *
     * @param terminalEventDetail
     * @return
     */
    List<TerminalEventDetail> queryList(TerminalEventDetail terminalEventDetail);

    /**
     *
     * @param terminalEventDetail
     * @return
     */
    TerminalEventDetail getOne(TerminalEventDetail terminalEventDetail);
}
