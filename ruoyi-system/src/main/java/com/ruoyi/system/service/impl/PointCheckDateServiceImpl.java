package com.ruoyi.system.service.impl;

import com.ruoyi.common.annotation.UserDataIsolation;
import com.ruoyi.system.domain.PointCheckDate;
import com.ruoyi.system.mapper.PointCheckDateMapper;
import com.ruoyi.system.service.IPointCheckDateService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class PointCheckDateServiceImpl implements IPointCheckDateService {

    @Autowired
    private PointCheckDateMapper pointCheckDateMapper;

    @Override
    public List<PointCheckDate> queryList(PointCheckDate pointCheckDate) {
        return pointCheckDateMapper.queryList(pointCheckDate);
    }

    @Override
    public Boolean add(PointCheckDate pointCheckDate) {
        return pointCheckDateMapper.add(pointCheckDate) > 0;
    }

    @Override
    @UserDataIsolation
    public Boolean update(PointCheckDate pointCheckDate) {
        return pointCheckDateMapper.update(pointCheckDate) > 0;
    }

    @Override
    @UserDataIsolation
    public Boolean delete(PointCheckDate pointCheckDate) {
        return pointCheckDateMapper.delete(pointCheckDate) > 0;
    }
}
