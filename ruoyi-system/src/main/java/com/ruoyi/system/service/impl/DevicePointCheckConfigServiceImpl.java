package com.ruoyi.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.annotation.UserDataIsolation;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.SysDept;
import com.ruoyi.common.exception.CustomException;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.system.domain.DevicePointCheckConfig;
import com.ruoyi.system.domain.PointCheckDeviceDetail;
import com.ruoyi.system.domain.Project;
import com.ruoyi.system.domain.vo.QueryVo;
import com.ruoyi.system.mapper.DevicePointCheckConfigMapper;
import com.ruoyi.system.service.ChinaAreaDataService;
import com.ruoyi.system.service.FileService;
import com.ruoyi.system.service.IDevicePointCheckConfigService;
import org.springframework.beans.factory.annotation.Value;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Slf4j
public class DevicePointCheckConfigServiceImpl implements IDevicePointCheckConfigService {
    @Autowired
    private DevicePointCheckConfigMapper devicePointCheckConfigMapper;
    @Autowired
    private DevicePointCheckConfigServiceImpl devicePointCheckConfigService;

    @Value("${device.checkConfig.path:/device/check_config.json}")
    private String checkConfigPath;

    @Override
    public List<DevicePointCheckConfig> queryList(QueryVo queryVo) {
        return devicePointCheckConfigMapper.queryList(queryVo);
    }



    @Override
    @Transactional
    @UserDataIsolation
    public DevicePointCheckConfig getOne(QueryVo queryVo) {
        return devicePointCheckConfigMapper.getOne(queryVo);
    }

    @Override
    @UserDataIsolation
    public Boolean add(DevicePointCheckConfig devicePointCheckConfig) {
        if(devicePointCheckConfig.getCategory()==0){
            List<DevicePointCheckConfig> arr = initPointCheckConfigDetail(devicePointCheckConfig);
            for (DevicePointCheckConfig d:arr
            ) {
                if (!(devicePointCheckConfigMapper.add(d)>0)) {
                    return  false;
                }
            }
            return true;
        }
        return devicePointCheckConfigMapper.add(devicePointCheckConfig) > 0;
    }

    private JSONArray checkConfig() {

        JSONArray checkConfig = null;
        try {
            String content = FileService.load(checkConfigPath, false);
            checkConfig = JSONArray.parseArray(content);
        } catch (Exception e) {
            log.info("",e);
            StackTraceElement[] traces = e.getStackTrace();
            for (StackTraceElement trace : traces) {
                log.debug(trace.toString());
            }
        }


        return checkConfig;
    }

    @Override
    @Transactional
    @UserDataIsolation
    public Boolean init(Integer deviceId,Long companyId,String userName) {
        devicePointCheckConfigMapper.deleteDeviceId(deviceId);
        //初始点检配置
        Integer id = deviceId;
        JSONArray jsonObject = checkConfig();
        for(int i=0;i<jsonObject.size();i++) {
            Integer category =(Integer) jsonObject.getJSONObject(i).get("category");
            String type =(String) jsonObject.getJSONObject(i).get("type");
            String item =(String) jsonObject.getJSONObject(i).get("item");
            if(category==0){
                DevicePointCheckConfig config = new DevicePointCheckConfig();
                config.setDeviceId(id);
                config.setCategory(category);
                config.setType(type);
                config.setItem(item);
                config.setTypeOrder((Integer) jsonObject.getJSONObject(i).get("typeOrder"));
                config.setItemOrder((Integer) jsonObject.getJSONObject(i).get("itemOrder"));
                config.setOrderNum((Integer) jsonObject.getJSONObject(i).get("orderNum"));
                config.setCreateBy(userName);
                config.setCreateTime(new Date());
                config.setCompanyId(companyId);
                devicePointCheckConfigService.add(config);
            }else{
                JSONArray  checkConfigs = (JSONArray) jsonObject.getJSONObject(i).get("childData");
                for(int j=0;j<checkConfigs.size();j++) {
                    DevicePointCheckConfig config = new DevicePointCheckConfig();
                    config.setDeviceId(id);
                    config.setCategory(category);
                    config.setType(type);
                    config.setItem(item);
                    config.setField((String) checkConfigs.getJSONObject(j).get("field"));
                    config.setDataType((String) checkConfigs.getJSONObject(j).get("dataType"));
                    config.setDetail((String) checkConfigs.getJSONObject(j).get("detail"));
                    config.setTypeOrder((Integer) checkConfigs.getJSONObject(j).get("typeOrder"));
                    config.setItemOrder((Integer) checkConfigs.getJSONObject(j).get("itemOrder"));
                    config.setOrderNum((Integer) checkConfigs.getJSONObject(j).get("orderNum"));
                    config.setCreateBy(userName);
                    config.setCreateTime(new Date());
                    config.setCompanyId(companyId);
                    devicePointCheckConfigService.add(config);

                }


            }
        }
        return true;
    }
    private List<DevicePointCheckConfig> initPointCheckConfigDetail(DevicePointCheckConfig devicePointCheckConfig){

        DevicePointCheckConfig map1 = BeanUtil.copyProperties(devicePointCheckConfig, DevicePointCheckConfig.class);
        List<DevicePointCheckConfig> result = new ArrayList<>();
        map1.setField("运行数值");
        map1.setDataType("string");
        result.add(map1);

        DevicePointCheckConfig map2 = BeanUtil.copyProperties(devicePointCheckConfig, DevicePointCheckConfig.class);
        map2.setField("设计数值");
        map2.setDataType("string");
        result.add(map2);

        return  result;
    }

    @Override
    @Transactional
    @UserDataIsolation
    public Boolean update(DevicePointCheckConfig devicePointCheckConfig) {

        QueryVo vo = new QueryVo();
        vo.filters.put("id",devicePointCheckConfig.getId());
        DevicePointCheckConfig one = getOne(vo);
        if(!(devicePointCheckConfig.getType().equals(one.getType()))){
            checkIsType(devicePointCheckConfig);
            QueryVo queryVo1 = new QueryVo();
            queryVo1.filters.put("deviceId", one.getDeviceId());
            queryVo1.filters.put("type", one.getType());
            queryVo1.filters.put("updateType", devicePointCheckConfig.getType());
            queryVo1.filters.put("typeOrder", devicePointCheckConfig.getTypeOrder());
            int aBoolean = devicePointCheckConfigMapper.updateType(queryVo1);
            if(!(aBoolean>0)){
                return  false;
            }
            QueryVo queryVo3 = new QueryVo();
            queryVo3.filters.put("deviceId", one.getDeviceId());
            queryVo3.filters.put("type", devicePointCheckConfig.getType());
            queryVo3.filters.put("item", one.getItem());
            queryVo3.filters.put("updateItem", devicePointCheckConfig.getItem());
            queryVo3.filters.put("typeOrder", devicePointCheckConfig.getTypeOrder());
            queryVo3.filters.put("itemOrder", devicePointCheckConfig.getItemOrder());
            queryVo3.filters.put("orderNum", devicePointCheckConfig.getOrderNum());

            int i = devicePointCheckConfigMapper.updateItem(queryVo3);
            if(!(i>0)){
                return  false;
            }
        }else{
            QueryVo queryVo1 = new QueryVo();
            queryVo1.filters.put("deviceId", one.getDeviceId());
            queryVo1.filters.put("type", one.getType());
            queryVo1.filters.put("updateType", devicePointCheckConfig.getType());
            queryVo1.filters.put("typeOrder", devicePointCheckConfig.getTypeOrder());
            int aBoolean = devicePointCheckConfigMapper.updateType(queryVo1);
            if(!(aBoolean>0)){
                return  false;
            }
            if(!(devicePointCheckConfig.getItem().equals(one.getItem()))){
                checkIsItem(devicePointCheckConfig);
            }
            QueryVo queryVo3 = new QueryVo();
            queryVo3.filters.put("deviceId", one.getDeviceId());
            queryVo3.filters.put("type",  one.getType());
            queryVo3.filters.put("item", one.getItem());
            queryVo3.filters.put("updateItem", devicePointCheckConfig.getItem());
            queryVo3.filters.put("typeOrder", devicePointCheckConfig.getTypeOrder());
            queryVo3.filters.put("itemOrder", devicePointCheckConfig.getItemOrder());
            queryVo3.filters.put("orderNum", devicePointCheckConfig.getOrderNum());

            int i = devicePointCheckConfigMapper.updateItem(queryVo3);
            if(!(i>0)){
                return  false;
            }
        }



        int update = devicePointCheckConfigMapper.update(devicePointCheckConfig);
        if(!(update>0)){
            return false;
        }

        return true;
    }

    @Override
    @UserDataIsolation
    public Boolean updateSys(QueryVo queryVo) {
        int i = devicePointCheckConfigMapper.updateSys(queryVo);
        if(i>0){
            return true;
        }
        return false;
    }

    @Override
    @UserDataIsolation
    public Boolean delete(QueryVo queryVo) {
        return devicePointCheckConfigMapper.delete(queryVo) > 0;
    }
    @Override
    @UserDataIsolation
    public Boolean deleteSys(QueryVo queryVo) {
        return devicePointCheckConfigMapper.deleteSys(queryVo) > 0;
    }

    @Override
    public Boolean deleteBatch(List<String> ids) {
        return devicePointCheckConfigMapper.deleteBatch(ids) > 0;
    }

    @Override
    public void checkIsExist(DevicePointCheckConfig devicePointCheckConfig) {
        DevicePointCheckConfig  result = devicePointCheckConfigMapper.checkIsExist(devicePointCheckConfig);
        if (result == null){
            return;
        }
        if (devicePointCheckConfig.getId() == null || devicePointCheckConfig.getId().compareTo(result.getId()) != 0) {
            throw new CustomException("该点检内容类别的字段已存在!");
        }
    }
    @Override
    public void checkIsType(DevicePointCheckConfig devicePointCheckConfig) {
        List<DevicePointCheckConfig>  result = devicePointCheckConfigMapper.checkIsType(devicePointCheckConfig);
        if (result.size()==0){
            return;
        }
        throw new CustomException("该点检項目已存在!");
    }

    @Override
    public void checkIsItem(DevicePointCheckConfig devicePointCheckConfig) {
        List<DevicePointCheckConfig>  result = devicePointCheckConfigMapper.checkIsItem(devicePointCheckConfig);
        if (result.size()==0){
            return;
        }
        throw new CustomException("该系统项目点检内容已存在");
    }


    @Override
    public List<Map<String, Object>> typeItemEnum(QueryVo queryVo) {
        List<DevicePointCheckConfig> devicePointCheckConfigs = devicePointCheckConfigMapper.getEnum(queryVo);
        if (CollUtil.isEmpty(devicePointCheckConfigs)){
            return new ArrayList<>();
        }
        return getPointCheckConfig(devicePointCheckConfigs);
    }
   @Override
    public List<Map<String, Object>> typeItemEnumCheck(QueryVo queryVo) {
        List<DevicePointCheckConfig> devicePointCheckConfigs = devicePointCheckConfigMapper.queryList(queryVo);
        if (CollUtil.isEmpty(devicePointCheckConfigs)){
            return new ArrayList<>();
        }
        return getPointCheckConfigCheck(devicePointCheckConfigs);
    }

    @Override
    public List<Map<String, Object>> typeItemDefaultEnum(QueryVo queryVo) {
        Date startTime = queryVo.filters.getDate("start_time");
        if (startTime != null){
            DateTime beginOfDay = DateUtil.beginOfDay(startTime);
            DateTime endOfDay = DateUtil.endOfDay(startTime);
            queryVo.filters.put("begin_of_day",beginOfDay);
            queryVo.filters.put("end_of_day",endOfDay);
        }
        List<DevicePointCheckConfig> devicePointCheckConfigs = devicePointCheckConfigMapper.typeItemDefaultEnum(queryVo);
        if (CollUtil.isEmpty(devicePointCheckConfigs)){
            devicePointCheckConfigs = initPointCheckConfig();
        }
        return getPointCheckConfig(devicePointCheckConfigs);
    }

    @Override
    public int initPointCheckConfigByDeviceId(QueryVo queryVo) {
        Integer deviceId = queryVo.filters.getInteger("device_id");
        String userName = queryVo.filters.getString("user_name");
        Long companyId = queryVo.filters.getLong("company_id");
        List<DevicePointCheckConfig> result = new ArrayList<>();
        List<Map<String, Object>> mapList = initPointCheckConfigDetail();
        for (Map<String, Object> map : mapList) {
            String type = (String) map.get("type");
            String item = (String) map.get("item");
            List<DevicePointCheckConfig> checkConfigs = (List<DevicePointCheckConfig>) map.get("childData");
            if (CollUtil.isEmpty(checkConfigs)){
                DevicePointCheckConfig config = new DevicePointCheckConfig();
                config.setDeviceId(deviceId);
                config.setType(type);
                config.setItem(item);
                config.setOrderNum(0);
                config.setCreateBy(userName);
                config.setCreateTime(new Date());
                config.setCompanyId(companyId);
                result.add(config);
                continue;
            }
            for (DevicePointCheckConfig checkConfig : checkConfigs) {
                DevicePointCheckConfig config = new DevicePointCheckConfig();
                config.setDeviceId(deviceId);
                config.setType(type);
                config.setItem(item);
                config.setField(checkConfig.getField());
                config.setDataType(checkConfig.getDataType());
                config.setOrderNum(checkConfig.getOrderNum());
                config.setCreateBy(userName);
                config.setCreateTime(new Date());
                config.setCompanyId(companyId);
                result.add(config);
            }
        }
        //批量保存
        return devicePointCheckConfigMapper.addBatch(result);
    }

    @Override
    public Map<String, Object> parseInjectionItem(JSONObject item, Long companyId) {
        Map<String, Object> parsed = new HashMap<>();
        try {
            String category = item.getString("点检类别");
            if (category == null) {
                parsed.put("message", "点检类别不能为空");
                return parsed;
            }

            String type = item.getString("点检项目");
            if (type == null) {
                parsed.put("message", "点检项目不能为空");
                return parsed;
            }
            String pointCheckItem = item.getString("点检内容");
            if (pointCheckItem == null) {
                parsed.put("message", "点检内容不能为空");
                return parsed;
            }
            String field = item.getString("字段");
//            if (field == null) {
//                parsed.put("message", "检查内容不能为空");
//                return parsed;
//            }
            String dataTypeStr = item.getString("数据类型");
//            if(type.equals("工况")&&(pointCheckItem.equals("温度")||pointCheckItem.equals("压力")||pointCheckItem.equals("运行周期"))){
//                dataTypeStr="";
//                field="";
//            }
            if(category.equals("产品点检")){
                dataTypeStr = dataTypeStr.trim();
                if(!StrUtil.equals("文本",dataTypeStr) && !StrUtil.equals("图片",dataTypeStr) && !StrUtil.equals("视频",dataTypeStr) && !StrUtil.equals("选项",dataTypeStr) && (dataTypeStr != null && !dataTypeStr.equals("")) ){

                    parsed.put("message", "’数据类型’填写错误，数据类型只能填写‘文本/图片/视频/选项’");
                    return parsed;

                }
            }


            String dataType = StrUtil.equals("文本",dataTypeStr) ? "string" : StrUtil.equals("图片",dataTypeStr) ? "photo" :
                    StrUtil.equals("视频",dataTypeStr) ? "video" : StrUtil.equals("选项",dataTypeStr) ? "select" : "nothing";

            Integer orderNum = item.getInteger("字段排序");
            orderNum = orderNum == null ? 0 : orderNum;

            Integer typeOrder = item.getInteger("项目排序");
            typeOrder = typeOrder == null ? 0 : orderNum;

            Integer itemOrder = item.getInteger("内容排序");
            itemOrder = itemOrder == null ? 0 : orderNum;

            String detailStr = item.getString("选项内容(多个用,分割)");
            if (detailStr != null && !StrUtil.equals(dataType,"select")){
//                parsed.put("message", "数据类型为选项时，选项内容才能有数据");
//                return parsed;
                detailStr="";
            }
            List<String> detailList = new ArrayList<>();
            if (detailStr != null){
                detailList = StrUtil.splitTrim(detailStr, ",");
            }
            Integer categortInt = 0;
            if(category.equals("系统点检")){
                type = "工况";
            }
            if(category.equals("产品点检")){
                categortInt = 1;
            }


            DevicePointCheckConfig devicePointCheckConfig = new DevicePointCheckConfig();
            devicePointCheckConfig.setCategory(categortInt);
            devicePointCheckConfig.setType(type);
            devicePointCheckConfig.setItem(pointCheckItem);
            devicePointCheckConfig.setField(field);
            devicePointCheckConfig.setDataType(dataType);
            if (CollUtil.isNotEmpty(detailList)){
                devicePointCheckConfig.setDetail(JSONUtil.toJsonStr(detailList));
            }
            devicePointCheckConfig.setOrderNum(orderNum);
            devicePointCheckConfig.setTypeOrder(typeOrder);
            devicePointCheckConfig.setItemOrder(itemOrder);

            parsed.put("devicePointCheckConfig", devicePointCheckConfig);
        } catch (Exception e) {
            log.info("",e);
            StackTraceElement[] traces = e.getStackTrace();
            for (StackTraceElement trace : traces) {
                log.debug(trace.toString());
            }
        }
        return parsed;
    }

    @Override
    public Map<String, Object> findExistance(DevicePointCheckConfig devicePointCheckConfig) {
        String field = devicePointCheckConfig.getField();
        if (StrUtil.equals("",field)){
            devicePointCheckConfig.setField(null);
        }
        DevicePointCheckConfig  result = devicePointCheckConfigMapper.checkIsExist(devicePointCheckConfig);
        if (result == null){
            return null;
        }
        Map<String, Object> resultMap = new HashMap<>();
        if (devicePointCheckConfig.getId() == null || devicePointCheckConfig.getId().compareTo(result.getId()) != 0) {
            resultMap.put("message","该检查类别的检查项或自定义字段已存在!");
            return resultMap;
        }
        return null;
    }
    @Override
    public Map<String, Object> findExistanceItem(DevicePointCheckConfig devicePointCheckConfig) {
        String field = devicePointCheckConfig.getField();
        if (StrUtil.equals("",field)){
            devicePointCheckConfig.setField(null);
        }
        List<DevicePointCheckConfig> result = devicePointCheckConfigMapper.checkIsItem(devicePointCheckConfig);
        if (CollectionUtil.isEmpty(result)){
            return null;
        }
        Map<String, Object> resultMap = new HashMap<>();
        if (devicePointCheckConfig.getId() == null) {
            resultMap.put("message","该检查类别的检查项或自定义字段已存在!");
            return resultMap;
        }
        return null;
    }

    private List<Map<String, Object>> getPointCheckConfig(List<DevicePointCheckConfig> devicePointCheckConfigs){
        List<Map<String,Object>> result = new ArrayList<>();
        LinkedHashMap<String, List<DevicePointCheckConfig>> typeMap = devicePointCheckConfigs.stream().collect(Collectors.groupingBy(DevicePointCheckConfig::getType, LinkedHashMap::new, Collectors.toList()));
        for (Map.Entry<String, List<DevicePointCheckConfig>> typeEntry : typeMap.entrySet()) {
            String type = typeEntry.getKey();
            List<DevicePointCheckConfig> typeValue = typeEntry.getValue();
            Map<String,Object> resultMap = new HashMap<>();
            resultMap.put("name",type);
            for (DevicePointCheckConfig d:typeEntry.getValue()
                 ) {
                if(d.getType().equals(type)){
                    resultMap.put("category",d.getCategory());
                }

            }

            if (CollUtil.isEmpty(typeValue)){
                resultMap.put("childData", new ArrayList<>());
                result.add(resultMap);
                continue;
            }
            LinkedHashMap<String, List<DevicePointCheckConfig>> itemMap = typeValue.stream().collect(Collectors.groupingBy(DevicePointCheckConfig::getItem, LinkedHashMap::new, Collectors.toList()));
            List<String> itemList = new ArrayList<>();
            for (Map.Entry<String, List<DevicePointCheckConfig>> itemValue : itemMap.entrySet()) {
                String item = itemValue.getKey();
                itemList.add(item);
            }
//            Collections.sort(itemList, new Comparator<String>() {
//                @Override
//                public int compare(String o1, String o2) {
//                    return o1.compareTo(o2);
//                }
//            });
            resultMap.put("childData", itemList);
            result.add(resultMap);
        }
//        Collections.sort(result, new Comparator<Map<String, Object>>() {
//            @Override
//            public int compare(Map<String, Object> o1, Map<String, Object> o2) {
//                String name1 = (String)  o1.get("name");
//                String name2 = (String)  o2.get("name");
//                return name1.compareTo(name2);
//            }
//        });
        return result;
    }

    private List<Map<String, Object>> getPointCheckConfigCheck(List<DevicePointCheckConfig> devicePointCheckConfigs){
        List<Map<String,Object>> result = new ArrayList<>();
        LinkedHashMap<String, List<DevicePointCheckConfig>> typeMap = devicePointCheckConfigs.stream().collect(Collectors.groupingBy(DevicePointCheckConfig::getType, LinkedHashMap::new, Collectors.toList()));
        for (Map.Entry<String, List<DevicePointCheckConfig>> entry : typeMap.entrySet()) {
            Map<String,Object> map1 = new HashMap<>();
            map1.put("name",entry.getKey());
            map1.put("label",entry.getKey());
            List<DevicePointCheckConfig> value = entry.getValue();
            LinkedHashMap<String, List<DevicePointCheckConfig>> collect = value.stream().collect(Collectors.groupingBy(DevicePointCheckConfig::getItem, LinkedHashMap::new, Collectors.toList()));
            List<Map<String,Object>> list = new ArrayList<>();
            for (Map.Entry<String, List<DevicePointCheckConfig>> entry1 : collect.entrySet()) {
                Map<String,Object> map2 = new HashMap<>();
                String key = entry1.getKey();
                map2.put("name",key);
                map2.put("label",entry.getKey()+key);
                map2.put("list",entry1.getValue());
                list.add(map2);
            }
            map1.put("children",list);
            result.add(map1);
        }
        return result;
    }

    /**
     * 初始化默认点检项
     * @return
     */
    private List<DevicePointCheckConfig> initPointCheckConfig() {
        List<DevicePointCheckConfig> result = new ArrayList<>();
        DevicePointCheckConfig checkConfig1 = new DevicePointCheckConfig();
        checkConfig1.setType("波纹管");
        checkConfig1.setItem("裂纹");
        result.add(checkConfig1);
        DevicePointCheckConfig checkConfig2 = new DevicePointCheckConfig();
        checkConfig2.setType("波纹管");
        checkConfig2.setItem("腐蚀");
        result.add(checkConfig2);
        DevicePointCheckConfig checkConfig3 = new DevicePointCheckConfig();
        checkConfig3.setType("波纹管");
        checkConfig3.setItem("泄漏");
        result.add(checkConfig3);
        DevicePointCheckConfig checkConfig4 = new DevicePointCheckConfig();
        checkConfig4.setType("波纹管");
        checkConfig4.setItem("异常变形");
        result.add(checkConfig4);
        DevicePointCheckConfig checkConfig5 = new DevicePointCheckConfig();
        checkConfig5.setType("波纹管");
        checkConfig5.setItem("其他");
        result.add(checkConfig5);
        DevicePointCheckConfig checkConfig6 = new DevicePointCheckConfig();
        checkConfig6.setType("承力承压件");
        checkConfig6.setItem("焊缝");
        result.add(checkConfig6);
        DevicePointCheckConfig checkConfig7 = new DevicePointCheckConfig();
        checkConfig7.setType("承力承压件");
        checkConfig7.setItem("大变形");
        result.add(checkConfig7);
        DevicePointCheckConfig checkConfig8 = new DevicePointCheckConfig();
        checkConfig8.setType("承力承压件");
        checkConfig8.setItem("其他");
        result.add(checkConfig8);
        DevicePointCheckConfig checkConfig9 = new DevicePointCheckConfig();
        checkConfig9.setType("工况");
        checkConfig9.setItem("温度");
        result.add(checkConfig9);
        DevicePointCheckConfig checkConfig10 = new DevicePointCheckConfig();
        checkConfig10.setType("工况");
        checkConfig10.setItem("压力");
        result.add(checkConfig10);
        DevicePointCheckConfig checkConfig11 = new DevicePointCheckConfig();
        checkConfig11.setType("工况");
        checkConfig11.setItem("振动");
        result.add(checkConfig11);
        DevicePointCheckConfig checkConfig12 = new DevicePointCheckConfig();
        checkConfig12.setType("工况");
        checkConfig12.setItem("运行周期");
        result.add(checkConfig12);
        DevicePointCheckConfig checkConfig13 = new DevicePointCheckConfig();
        checkConfig13.setType("附属件");
        checkConfig13.setItem("脱落");
        result.add(checkConfig13);
        DevicePointCheckConfig checkConfig14 = new DevicePointCheckConfig();
        checkConfig14.setType("附属件");
        checkConfig14.setItem("干涉");
        result.add(checkConfig14);
        DevicePointCheckConfig checkConfig15 = new DevicePointCheckConfig();
        checkConfig15.setType("附属件");
        checkConfig15.setItem("其他");
        result.add(checkConfig15);
        return result;
    }

    /**
     * 初始化默认点检项及自定义字段
     *
     * [{"type":"波纹管","item":"裂纹","childData":[{"customField":"工具","dataType":"string"}]}]
     * @return
     */
    private List<Map<String,Object>> initPointCheckConfigDetail(){
        List<Map<String,Object>> result = new ArrayList<>();
        Map<String,Object> map1 = new HashMap<>();
        map1.put("type","波纹管");
        map1.put("item","裂纹");
        List<DevicePointCheckConfig> fieldConfig1 = new ArrayList<>();
        DevicePointCheckConfig checkConfig11 = new DevicePointCheckConfig();
        checkConfig11.setField("工具");
        checkConfig11.setDataType("string");
        checkConfig11.setOrderNum(0);
        fieldConfig1.add(checkConfig11);
        DevicePointCheckConfig checkConfig12 = new DevicePointCheckConfig();
        checkConfig12.setField("位置");
        checkConfig12.setDataType("string");
        checkConfig12.setOrderNum(0);
        fieldConfig1.add(checkConfig12);
        DevicePointCheckConfig checkConfig13 = new DevicePointCheckConfig();
        checkConfig13.setField("异常状况");
        checkConfig13.setDataType("string");
        checkConfig13.setOrderNum(0);
        fieldConfig1.add(checkConfig13);
        DevicePointCheckConfig checkConfig14 = new DevicePointCheckConfig();
        checkConfig14.setField("异常数值");
        checkConfig14.setDataType("string");
        checkConfig14.setOrderNum(0);
        fieldConfig1.add(checkConfig14);
        DevicePointCheckConfig checkConfig15 = new DevicePointCheckConfig();
        checkConfig15.setField("图片");
        checkConfig15.setDataType("photo");
        checkConfig15.setOrderNum(0);
        fieldConfig1.add(checkConfig15);
        map1.put("childData",fieldConfig1);
        result.add(map1);

        Map<String,Object> map2 = new HashMap<>();
        map2.put("type","波纹管");
        map2.put("item","腐蚀");
        List<DevicePointCheckConfig> fieldConfig2 = new ArrayList<>();
        DevicePointCheckConfig checkConfig21 = new DevicePointCheckConfig();
        checkConfig21.setField("工具");
        checkConfig21.setDataType("string");
        checkConfig21.setOrderNum(0);
        fieldConfig2.add(checkConfig21);
        DevicePointCheckConfig checkConfig22 = new DevicePointCheckConfig();
        checkConfig22.setField("位置");
        checkConfig22.setDataType("string");
        checkConfig22.setOrderNum(0);
        fieldConfig2.add(checkConfig22);
        DevicePointCheckConfig checkConfig23 = new DevicePointCheckConfig();
        checkConfig23.setField("异常状况");
        checkConfig23.setDataType("string");
        checkConfig23.setOrderNum(0);
        fieldConfig2.add(checkConfig23);
        DevicePointCheckConfig checkConfig24 = new DevicePointCheckConfig();
        checkConfig24.setField("异常数值");
        checkConfig24.setDataType("string");
        checkConfig24.setOrderNum(0);
        fieldConfig2.add(checkConfig24);
        DevicePointCheckConfig checkConfig25 = new DevicePointCheckConfig();
        checkConfig25.setField("图片");
        checkConfig25.setDataType("photo");
        checkConfig25.setOrderNum(0);
        fieldConfig2.add(checkConfig25);
        map2.put("childData",fieldConfig2);
        result.add(map2);

        Map<String,Object> map3 = new HashMap<>();
        map3.put("type","波纹管");
        map3.put("item","泄漏");
        List<DevicePointCheckConfig> fieldConfig3 = new ArrayList<>();
        DevicePointCheckConfig checkConfig31 = new DevicePointCheckConfig();
        checkConfig31.setField("工具");
        checkConfig31.setDataType("string");
        checkConfig31.setOrderNum(0);
        fieldConfig3.add(checkConfig21);
        DevicePointCheckConfig checkConfig32 = new DevicePointCheckConfig();
        checkConfig32.setField("位置");
        checkConfig32.setDataType("string");
        checkConfig32.setOrderNum(0);
        fieldConfig3.add(checkConfig32);
        DevicePointCheckConfig checkConfig33 = new DevicePointCheckConfig();
        checkConfig33.setField("异常状况");
        checkConfig33.setDataType("string");
        checkConfig33.setOrderNum(0);
        fieldConfig3.add(checkConfig33);
        DevicePointCheckConfig checkConfig34 = new DevicePointCheckConfig();
        checkConfig34.setField("异常数值");
        checkConfig34.setDataType("string");
        checkConfig34.setOrderNum(0);
        fieldConfig3.add(checkConfig34);
        DevicePointCheckConfig checkConfig35 = new DevicePointCheckConfig();
        checkConfig35.setField("图片");
        checkConfig35.setDataType("photo");
        checkConfig35.setOrderNum(0);
        fieldConfig3.add(checkConfig35);
        map3.put("childData",fieldConfig3);
        result.add(map3);

        Map<String,Object> map4 = new HashMap<>();
        map4.put("type","波纹管");
        map4.put("item","异常变形");
        List<DevicePointCheckConfig> fieldConfig4 = new ArrayList<>();
        DevicePointCheckConfig checkConfig41 = new DevicePointCheckConfig();
        checkConfig41.setField("工具");
        checkConfig41.setDataType("string");
        checkConfig41.setOrderNum(0);
        fieldConfig4.add(checkConfig41);
        DevicePointCheckConfig checkConfig42 = new DevicePointCheckConfig();
        checkConfig42.setField("位置");
        checkConfig42.setDataType("string");
        checkConfig42.setOrderNum(0);
        fieldConfig4.add(checkConfig42);
        DevicePointCheckConfig checkConfig43 = new DevicePointCheckConfig();
        checkConfig43.setField("异常状况");
        checkConfig43.setDataType("string");
        checkConfig43.setOrderNum(0);
        fieldConfig4.add(checkConfig43);
        DevicePointCheckConfig checkConfig44 = new DevicePointCheckConfig();
        checkConfig44.setField("异常数值");
        checkConfig44.setDataType("string");
        checkConfig44.setOrderNum(0);
        fieldConfig4.add(checkConfig44);
        DevicePointCheckConfig checkConfig45 = new DevicePointCheckConfig();
        checkConfig45.setField("图片");
        checkConfig45.setDataType("photo");
        checkConfig45.setOrderNum(0);
        fieldConfig4.add(checkConfig45);
        map4.put("childData",fieldConfig4);
        result.add(map4);

        Map<String,Object> map5 = new HashMap<>();
        map5.put("type","波纹管");
        map5.put("item","其他");
        List<DevicePointCheckConfig> fieldConfig5 = new ArrayList<>();
        DevicePointCheckConfig checkConfig51 = new DevicePointCheckConfig();
        checkConfig51.setField("工具");
        checkConfig51.setDataType("string");
        checkConfig51.setOrderNum(0);
        fieldConfig5.add(checkConfig51);
        DevicePointCheckConfig checkConfig52 = new DevicePointCheckConfig();
        checkConfig52.setField("位置");
        checkConfig52.setDataType("string");
        checkConfig52.setOrderNum(0);
        fieldConfig5.add(checkConfig52);
        DevicePointCheckConfig checkConfig53 = new DevicePointCheckConfig();
        checkConfig53.setField("异常状况");
        checkConfig53.setDataType("string");
        checkConfig53.setOrderNum(0);
        fieldConfig5.add(checkConfig53);
        DevicePointCheckConfig checkConfig54 = new DevicePointCheckConfig();
        checkConfig54.setField("异常数值");
        checkConfig54.setDataType("string");
        checkConfig54.setOrderNum(0);
        fieldConfig5.add(checkConfig54);
        DevicePointCheckConfig checkConfig55 = new DevicePointCheckConfig();
        checkConfig55.setField("图片");
        checkConfig55.setDataType("photo");
        checkConfig55.setOrderNum(0);
        fieldConfig5.add(checkConfig55);
        map5.put("childData",fieldConfig5);
        result.add(map5);

        Map<String,Object> map6 = new HashMap<>();
        map6.put("type","承力承压件");
        map6.put("item","焊缝");
        List<DevicePointCheckConfig> fieldConfig6 = new ArrayList<>();
        DevicePointCheckConfig checkConfig61 = new DevicePointCheckConfig();
        checkConfig61.setField("工具");
        checkConfig61.setDataType("string");
        checkConfig61.setOrderNum(0);
        fieldConfig6.add(checkConfig61);
        DevicePointCheckConfig checkConfig62 = new DevicePointCheckConfig();
        checkConfig62.setField("位置");
        checkConfig62.setDataType("string");
        checkConfig62.setOrderNum(0);
        fieldConfig6.add(checkConfig62);
        DevicePointCheckConfig checkConfig63 = new DevicePointCheckConfig();
        checkConfig63.setField("异常状况");
        checkConfig63.setDataType("string");
        checkConfig63.setOrderNum(0);
        fieldConfig6.add(checkConfig63);
        DevicePointCheckConfig checkConfig64 = new DevicePointCheckConfig();
        checkConfig64.setField("异常数值");
        checkConfig64.setDataType("string");
        checkConfig64.setOrderNum(0);
        fieldConfig6.add(checkConfig64);
        DevicePointCheckConfig checkConfig65 = new DevicePointCheckConfig();
        checkConfig65.setField("图片");
        checkConfig65.setDataType("photo");
        checkConfig65.setOrderNum(0);
        fieldConfig6.add(checkConfig65);
        map6.put("childData",fieldConfig6);
        result.add(map6);

        Map<String,Object> map7 = new HashMap<>();
        map7.put("type","承力承压件");
        map7.put("item","大变形");
        List<DevicePointCheckConfig> fieldConfig7 = new ArrayList<>();
        DevicePointCheckConfig checkConfig71 = new DevicePointCheckConfig();
        checkConfig71.setField("工具");
        checkConfig71.setDataType("string");
        checkConfig71.setOrderNum(0);
        fieldConfig7.add(checkConfig71);
        DevicePointCheckConfig checkConfig72 = new DevicePointCheckConfig();
        checkConfig72.setField("位置");
        checkConfig72.setDataType("string");
        checkConfig72.setOrderNum(0);
        fieldConfig7.add(checkConfig72);
        DevicePointCheckConfig checkConfig73 = new DevicePointCheckConfig();
        checkConfig73.setField("异常状况");
        checkConfig73.setDataType("string");
        checkConfig73.setOrderNum(0);
        fieldConfig7.add(checkConfig73);
        DevicePointCheckConfig checkConfig74 = new DevicePointCheckConfig();
        checkConfig74.setField("异常数值");
        checkConfig74.setDataType("string");
        checkConfig74.setOrderNum(0);
        fieldConfig7.add(checkConfig74);
        DevicePointCheckConfig checkConfig75 = new DevicePointCheckConfig();
        checkConfig75.setField("图片");
        checkConfig75.setDataType("photo");
        checkConfig75.setOrderNum(0);
        fieldConfig7.add(checkConfig75);
        map7.put("childData",fieldConfig7);
        result.add(map7);

        Map<String,Object> map8 = new HashMap<>();
        map8.put("type","承力承压件");
        map8.put("item","其他");
        List<DevicePointCheckConfig> fieldConfig8 = new ArrayList<>();
        DevicePointCheckConfig checkConfig81 = new DevicePointCheckConfig();
        checkConfig81.setField("工具");
        checkConfig81.setDataType("string");
        checkConfig81.setOrderNum(0);
        fieldConfig8.add(checkConfig81);
        DevicePointCheckConfig checkConfig82 = new DevicePointCheckConfig();
        checkConfig82.setField("位置");
        checkConfig82.setDataType("string");
        checkConfig82.setOrderNum(0);
        fieldConfig8.add(checkConfig82);
        DevicePointCheckConfig checkConfig83 = new DevicePointCheckConfig();
        checkConfig83.setField("异常状况");
        checkConfig83.setDataType("string");
        checkConfig83.setOrderNum(0);
        fieldConfig8.add(checkConfig83);
        DevicePointCheckConfig checkConfig84 = new DevicePointCheckConfig();
        checkConfig84.setField("异常数值");
        checkConfig84.setDataType("string");
        checkConfig84.setOrderNum(0);
        fieldConfig8.add(checkConfig84);
        DevicePointCheckConfig checkConfig85 = new DevicePointCheckConfig();
        checkConfig85.setField("图片");
        checkConfig85.setDataType("photo");
        checkConfig85.setOrderNum(0);
        fieldConfig8.add(checkConfig85);
        map8.put("childData",fieldConfig8);
        result.add(map8);

        Map<String,Object> map9 = new HashMap<>();
        map9.put("type","工况");
        map9.put("item","温度");
        List<DevicePointCheckConfig> fieldConfig9 = new ArrayList<>();
//        DevicePointCheckConfig checkConfig91 = new DevicePointCheckConfig();
//        checkConfig91.setField("数值");
//        checkConfig91.setDataType("string");
//        checkConfig91.setOrderNum(0);
//        fieldConfig9.add(checkConfig91);
        map9.put("childData",fieldConfig9);
        result.add(map9);

        Map<String,Object> map10 = new HashMap<>();
        map10.put("type","工况");
        map10.put("item","压力");
        List<DevicePointCheckConfig> fieldConfig10 = new ArrayList<>();
//        DevicePointCheckConfig checkConfig101 = new DevicePointCheckConfig();
//        checkConfig101.setField("数值");
//        checkConfig101.setDataType("string");
//        checkConfig101.setOrderNum(0);
//        fieldConfig10.add(checkConfig101);
        map10.put("childData",fieldConfig10);
        result.add(map10);

        Map<String,Object> map11 = new HashMap<>();
        map11.put("type","工况");
        map11.put("item","振动");
        List<DevicePointCheckConfig> fieldConfig11 = new ArrayList<>();
        DevicePointCheckConfig checkConfig111 = new DevicePointCheckConfig();
        checkConfig111.setField("工具");
        checkConfig111.setDataType("string");
        checkConfig111.setOrderNum(0);
        fieldConfig11.add(checkConfig111);
        DevicePointCheckConfig checkConfig112 = new DevicePointCheckConfig();
        checkConfig112.setField("位置");
        checkConfig112.setDataType("string");
        checkConfig112.setOrderNum(0);
        fieldConfig11.add(checkConfig112);
        DevicePointCheckConfig checkConfig113 = new DevicePointCheckConfig();
        checkConfig113.setField("异常状况");
        checkConfig113.setDataType("string");
        checkConfig113.setOrderNum(0);
        fieldConfig11.add(checkConfig113);
        DevicePointCheckConfig checkConfig114 = new DevicePointCheckConfig();
        checkConfig114.setField("异常数值");
        checkConfig114.setDataType("string");
        checkConfig114.setOrderNum(0);
        fieldConfig11.add(checkConfig114);
        DevicePointCheckConfig checkConfig115 = new DevicePointCheckConfig();
        checkConfig115.setField("视频");
        checkConfig115.setDataType("video");
        checkConfig115.setOrderNum(0);
        fieldConfig11.add(checkConfig115);
        map11.put("childData",fieldConfig11);
        result.add(map11);

        Map<String,Object> map12 = new HashMap<>();
        map12.put("type","工况");
        map12.put("item","运行周期");
        List<DevicePointCheckConfig> fieldConfig12 = new ArrayList<>();
//        DevicePointCheckConfig checkConfig121 = new DevicePointCheckConfig();
//        checkConfig121.setField("数值");
//        checkConfig121.setDataType("string");
//        checkConfig121.setOrderNum(0);
//        fieldConfig12.add(checkConfig121);
        map12.put("childData",fieldConfig12);
        result.add(map12);

        Map<String,Object> map13 = new HashMap<>();
        map13.put("type","工况");
        map13.put("item","介质");
        List<DevicePointCheckConfig> fieldConfig13 = new ArrayList<>();
//        DevicePointCheckConfig checkConfig131 = new DevicePointCheckConfig();
//        checkConfig131.setField("数值");
//        checkConfig131.setDataType("string");
//        checkConfig131.setOrderNum(0);
//        fieldConfig13.add(checkConfig131);
        map13.put("childData",fieldConfig13);
        result.add(map13);

        Map<String,Object> map14 = new HashMap<>();
        map14.put("type","附属件");
        map14.put("item","脱落");
        List<DevicePointCheckConfig> fieldConfig14 = new ArrayList<>();
        DevicePointCheckConfig checkConfig141 = new DevicePointCheckConfig();
        checkConfig141.setField("工具");
        checkConfig141.setDataType("string");
        checkConfig141.setOrderNum(0);
        fieldConfig14.add(checkConfig141);
        DevicePointCheckConfig checkConfig142 = new DevicePointCheckConfig();
        checkConfig142.setField("位置");
        checkConfig142.setDataType("string");
        checkConfig142.setOrderNum(0);
        fieldConfig14.add(checkConfig142);
        DevicePointCheckConfig checkConfig143 = new DevicePointCheckConfig();
        checkConfig143.setField("异常状况");
        checkConfig143.setDataType("string");
        checkConfig143.setOrderNum(0);
        fieldConfig14.add(checkConfig143);
        DevicePointCheckConfig checkConfig144 = new DevicePointCheckConfig();
        checkConfig144.setField("异常数值");
        checkConfig144.setDataType("string");
        checkConfig144.setOrderNum(0);
        fieldConfig14.add(checkConfig144);
        DevicePointCheckConfig checkConfig145 = new DevicePointCheckConfig();
        checkConfig145.setField("图片");
        checkConfig145.setDataType("photo");
        checkConfig145.setOrderNum(0);
        fieldConfig14.add(checkConfig145);
        map14.put("childData",fieldConfig14);
        result.add(map14);

        Map<String,Object> map15 = new HashMap<>();
        map15.put("type","附属件");
        map15.put("item","干涉");
        List<DevicePointCheckConfig> fieldConfig15 = new ArrayList<>();
        DevicePointCheckConfig checkConfig151 = new DevicePointCheckConfig();
        checkConfig151.setField("工具");
        checkConfig151.setDataType("string");
        checkConfig151.setOrderNum(0);
        fieldConfig15.add(checkConfig151);
        DevicePointCheckConfig checkConfig152 = new DevicePointCheckConfig();
        checkConfig152.setField("位置");
        checkConfig152.setDataType("string");
        checkConfig152.setOrderNum(0);
        fieldConfig15.add(checkConfig152);
        DevicePointCheckConfig checkConfig153 = new DevicePointCheckConfig();
        checkConfig153.setField("异常状况");
        checkConfig153.setDataType("string");
        checkConfig153.setOrderNum(0);
        fieldConfig15.add(checkConfig153);
        DevicePointCheckConfig checkConfig154 = new DevicePointCheckConfig();
        checkConfig154.setField("异常数值");
        checkConfig154.setDataType("string");
        checkConfig154.setOrderNum(0);
        fieldConfig15.add(checkConfig154);
        DevicePointCheckConfig checkConfig155 = new DevicePointCheckConfig();
        checkConfig155.setField("图片");
        checkConfig155.setDataType("photo");
        checkConfig155.setOrderNum(0);
        fieldConfig15.add(checkConfig155);
        map15.put("childData",fieldConfig15);
        result.add(map15);

        Map<String,Object> map16 = new HashMap<>();
        map16.put("type","附属件");
        map16.put("item","其他");
        List<DevicePointCheckConfig> fieldConfig16 = new ArrayList<>();
        DevicePointCheckConfig checkConfig161 = new DevicePointCheckConfig();
        checkConfig161.setField("工具");
        checkConfig161.setDataType("string");
        checkConfig161.setOrderNum(0);
        fieldConfig16.add(checkConfig161);
        DevicePointCheckConfig checkConfig162 = new DevicePointCheckConfig();
        checkConfig162.setField("位置");
        checkConfig162.setDataType("string");
        checkConfig162.setOrderNum(0);
        fieldConfig16.add(checkConfig162);
        DevicePointCheckConfig checkConfig163 = new DevicePointCheckConfig();
        checkConfig163.setField("异常状况");
        checkConfig163.setDataType("string");
        checkConfig163.setOrderNum(0);
        fieldConfig16.add(checkConfig163);
        DevicePointCheckConfig checkConfig164 = new DevicePointCheckConfig();
        checkConfig164.setField("异常数值");
        checkConfig164.setDataType("string");
        checkConfig164.setOrderNum(0);
        fieldConfig16.add(checkConfig164);
        DevicePointCheckConfig checkConfig165 = new DevicePointCheckConfig();
        checkConfig165.setField("图片");
        checkConfig165.setDataType("photo");
        checkConfig165.setOrderNum(0);
        fieldConfig16.add(checkConfig165);
        map16.put("childData",fieldConfig16);
        result.add(map16);
        return result;
    }
}
