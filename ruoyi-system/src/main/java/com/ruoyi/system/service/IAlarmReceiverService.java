package com.ruoyi.system.service;

import java.util.List;
import java.util.Map;

import com.ruoyi.system.domain.AlarmReceiver;
import com.ruoyi.system.domain.vo.QueryVo;

public interface IAlarmReceiverService {
    public Integer set(List<AlarmReceiver> receivers);
    public List<Map<String, Object>> get(QueryVo queryVo);
    public void clear(QueryVo queryVo);
}
