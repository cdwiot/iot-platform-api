package com.ruoyi.system.service;

import com.alibaba.fastjson.JSONObject;

/**
 * 阿里云短信服务
 */
public interface IAliVoiceService {
    void send(JSONObject request, String tid);
}
