package com.ruoyi.system.service.impl;

import com.ruoyi.common.annotation.UserDataIsolation;
import com.ruoyi.system.domain.EmailDetail;
import com.ruoyi.system.mapper.EmailDetailMapper;
import com.ruoyi.system.service.IEmailDetailService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class EmailDetailServiceImpl implements IEmailDetailService {
    @Autowired
    private EmailDetailMapper emailDetailMapper;

    @Override
    public Boolean add(EmailDetail emailDetail) {
        return emailDetailMapper.add(emailDetail) > 0;
    }

    @Override
    @UserDataIsolation(tableAlias = "t1")
    public List<EmailDetail> list(EmailDetail emailDetail) {
        return emailDetailMapper.list(emailDetail);
    }

    @Override
    @UserDataIsolation(tableAlias = "t1")
    public EmailDetail getOne(EmailDetail emailDetail) {
        return emailDetailMapper.getOne(emailDetail);
    }
}
