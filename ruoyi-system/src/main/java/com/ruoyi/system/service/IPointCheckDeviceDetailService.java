package com.ruoyi.system.service;

import com.ruoyi.system.domain.DevicePointCheckConfig;
import com.ruoyi.system.domain.PointCheckDeviceDetail;
import com.ruoyi.system.domain.vo.PointCheckDeviceDetailVO;
import com.ruoyi.system.domain.vo.QueryVo;

import java.util.List;
import java.util.Map;

public interface IPointCheckDeviceDetailService {

    /**
     * 产品检查项的列表查询
     *
     * @param pointCheckDeviceDetail
     * @return
     */
    List<PointCheckDeviceDetail> queryList(PointCheckDeviceDetail pointCheckDeviceDetail);
    /**
     * 产品异常检查项的列表查询
     *
     * @param pointCheckDeviceDetail
     * @return
     */
    Map<String,Object> queryListAbnormalList(Integer deviceId);

    /**
     * 根据任务产品关系Id获取统计信息
     *
     * @param taskDeviceId
     * @return
     */
    Map<String,PointCheckDeviceDetailVO> getDeviceDetailVO(Integer taskDeviceId);
    List<Map<String, Object>> getDeviceDetailVOV2(Integer taskDeviceId);

    /**
     * 获取点检类别、点检项枚举
     * @param queryVo
     * @return
     */
    Map<String,Object> getDeviceItemEnum(QueryVo queryVo);

    /**
     * 产品检查项的详情
     *
     * @param pointCheckDeviceDetail
     * @return
     */
    PointCheckDeviceDetail getOne(PointCheckDeviceDetail pointCheckDeviceDetail);

    /**
     * 添加
     *
     * @param pointCheckDeviceDetail
     * @return
     */
    Boolean add(PointCheckDeviceDetail pointCheckDeviceDetail,List<DevicePointCheckConfig> childData);

    /**
     * 修改
     *
     * @param pointCheckDeviceDetail
     * @return
     */
    Boolean update(PointCheckDeviceDetail pointCheckDeviceDetail);

    /**
     * 删除
     *
     * @param pointCheckDeviceDetail
     * @return
     */
    Boolean delete(PointCheckDeviceDetail pointCheckDeviceDetail);

    /**
     * 验证是否重复
     *
     * @param pointCheckDeviceDetail
     * @return
     */
    void checkIsExist(PointCheckDeviceDetail pointCheckDeviceDetail);
}
