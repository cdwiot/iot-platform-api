package com.ruoyi.system.service;

import java.util.List;
import java.util.Map;

import com.ruoyi.system.domain.MonitorRule;
import com.ruoyi.system.domain.vo.QueryVo;

public interface IMonitorRuleService {
    public Map<String, Object> findExistance(MonitorRule monitorRule);

    /**
     * 监控规则的添加
     *
     * @param monitorRule
     * @return
     */
    public Integer create(MonitorRule monitorRule);

    /**
     * 监控规则的列表查询
     *
     * @param queryVo
     * @return
     */
    public List<MonitorRule> index(QueryVo queryVo);
    public List<Map<String, Object>> enumerate(QueryVo queryVo);

    /**
     * 获取监控规则详情
     *
     * @param queryVo
     * @return
     */
    public MonitorRule retrieve(QueryVo queryVo);

    /**
     * 监控规则的修改
     *
     * @param monitorRule
     * @return
     */
    public Boolean update(MonitorRule monitorRule);

    /**
     * 监控规则的删除
     *
     * @param queryVo
     * @return
     */
    public Boolean delete(QueryVo queryVo);
}
