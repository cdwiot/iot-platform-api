package com.ruoyi.system.service;

import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.system.domain.ProjectAuthorize;

import java.util.List;
import java.util.Set;

public interface IProjectAuthorizeService {

    /**
     * 获取所有的用户信息
     *
     * @param projectAuthorize
     * @return
     */
    List<ProjectAuthorize> queryAllUserInfo(ProjectAuthorize projectAuthorize);

    /**
     * 获取已绑定的用户信息
     *
     * @param projectAuthorize
     * @return
     */
    List<ProjectAuthorize> queryAuthorizeUserInfo(ProjectAuthorize projectAuthorize);

    /**
     * 新增(批量)
     *
     * @param projectAuthorizes
     * @return
     */
    Boolean addBatch(List<ProjectAuthorize> projectAuthorizes);

    /**
     * 新增
     *
     * @param projectAuthorize
     * @return
     */
    Boolean add(ProjectAuthorize projectAuthorize);

    /**
     * 删除
     *
     * @param ids 多个用,分割
     * @return
     */
    Boolean delete(String ids, SysUser sysUser);

    /**
     * 根据用户Id查询该用户创建的项目
     *
     * @param projectAuthorize
     * @return
     */
    Set<Integer> getProjectIdsByUserId(ProjectAuthorize projectAuthorize);

    /**
     * 根据用户Id和项目Id验证该用户是否是该项目创建者
     *
     * @param projectAuthorize
     * @return
     */
    Boolean checkProjectIsOwner(ProjectAuthorize projectAuthorize);

    /**
     * 根据用户Id和项目Id验证该用户是否授权
     *
     * @param projectAuthorize
     * @return
     */
    Boolean checkProjectIsAuthorize(ProjectAuthorize projectAuthorize);

    /**
     * 根据用户信息和产品Id，验证该用户是否授权
     *
     * @param sysUser  用户信息
     * @param deviceId 产品Id
     * @return
     */
    void checkProjectIsAuthorizeWithDeviceId(SysUser sysUser, Integer deviceId);

    /**
     * 根据用户信息和项目Id，验证该用户是否授权
     *
     * @param sysUser   用户信息
     * @param projectId 项目Id
     * @return
     */
    void checkProjectIsAuthorizeWithProjectId(SysUser sysUser, Integer projectId);

    /**
     * 验证产品修改权限
     *
     * @param sysUser
     * @param deviceId
     */
    void checkDeviceOperationStatus(SysUser sysUser, Integer deviceId);

    /**
     * 验证点检修改权限
     *
     * @param sysUser
     * @param pointId
     */
    void checkPointCheckOperationStatus(SysUser sysUser, Integer pointId);

    /**
     * 验证点检任务修改权限
     *
     * @param sysUser
     * @param taskId
     */
    void checkPointCheckTaskOperationStatus(SysUser sysUser, Integer taskId);

    /**
     * 验证报表修改权限
     *
     * @param sysUser
     * @param reportId
     */
    void checkReportOperationStatus(SysUser sysUser, Integer reportId);

}
