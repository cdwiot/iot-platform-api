package com.ruoyi.system.service;

import java.util.List;

import com.ruoyi.system.domain.AlarmDetail;
import com.ruoyi.system.domain.vo.QueryVo;

public interface IAlarmDetailService {
    public List<AlarmDetail> index(QueryVo queryVo);
}
