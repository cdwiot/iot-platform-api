package com.ruoyi.system.service;

import java.util.List;
import java.util.Map;

import com.ruoyi.system.domain.Terminal;
import com.ruoyi.system.domain.vo.QueryVo;

public interface ITerminalService
{
    public Map<String, Object> findExistance(Terminal terminal);

    /**
     * 传感器信息的添加
     *
     * @param terminal
     * @return
     */
    public Integer create(Terminal terminal);

    /**
     * 获取传感器列表信息
     *
     * @param queryVo
     * @return
     */
    public List<Terminal> index(QueryVo queryVo);
    public List<Map<String, Object>> enumerate(QueryVo queryVo);

    /**
     * 获取传感器详情
     *
     * @param queryVo
     * @return
     */
    public Terminal retrieve(QueryVo queryVo);

    /**
     * 传感器信息的修改
     *
     * @param terminal
     * @return
     */
    public Boolean update(Terminal terminal);

    /**
     * 传感器的删除
     *
     * @param queryVo
     * @return
     */
    public Boolean delete(QueryVo queryVo);
}
