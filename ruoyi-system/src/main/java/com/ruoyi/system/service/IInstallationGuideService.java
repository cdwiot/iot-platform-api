package com.ruoyi.system.service;

import java.util.List;
import java.util.Map;

import com.ruoyi.system.domain.InstallationGuide;
import com.ruoyi.system.domain.vo.QueryVo;

public interface IInstallationGuideService
{
    public Map<String, Object> findExistance(InstallationGuide installationGuide);

    /**
     * 安装指导配置的创建
     *
     * @param installationGuide
     * @return
     */
    public Integer create(InstallationGuide installationGuide);

    /**
     * 获取安装指导配置列表
     *
     * @param queryVo
     * @return
     */
    public List<InstallationGuide> index(QueryVo queryVo);

    /**
     * 获取安装指导配置详情
     *
     * @param queryVo
     * @return
     */
    public InstallationGuide retrieve(QueryVo queryVo);

    /**
     * 安装指导配置的修改
     *
     * @param installationGuide
     * @return
     */
    public Boolean update(InstallationGuide installationGuide);

    /**
     * 安装指导配置的删除
     *
     * @param queryVo
     * @return
     */
    public Boolean delete(QueryVo queryVo);
}
