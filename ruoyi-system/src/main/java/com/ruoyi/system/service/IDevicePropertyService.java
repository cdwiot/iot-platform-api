package com.ruoyi.system.service;

import com.ruoyi.system.domain.DeviceProperty;

import java.util.List;

public interface IDevicePropertyService {

    /**
     * 产品额外属性的分页查询
     *
     * @param deviceProperty
     * @return
     */
    List<DeviceProperty> queryPageList(DeviceProperty deviceProperty);

    /**
     * 产品额外属性的添加
     *
     * @param deviceProperty
     * @return
     */
    Integer addDeviceProperty(DeviceProperty deviceProperty);

    /**
     * 产品额外属性的详情
     *
     * @param deviceProperty
     * @return
     */
    DeviceProperty queryById(DeviceProperty deviceProperty);

    /**
     * 产品额外属性的修改
     *
     * @param deviceProperty
     * @return
     */
    Integer updateDeviceProperty(DeviceProperty deviceProperty);

    /**
     * 产品额外属性的删除
     *
     * @param deviceProperty
     */
    void deleteDeviceProperty(DeviceProperty deviceProperty);


    /**
     * 验证属性名称是否重复
     *
     * @param deviceProperty
     */
    void checkParam(DeviceProperty deviceProperty);
}
