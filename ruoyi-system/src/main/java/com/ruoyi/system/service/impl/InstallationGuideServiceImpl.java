package com.ruoyi.system.service.impl;

import java.util.*;

import com.ruoyi.common.annotation.UserDataIsolation;
import com.ruoyi.system.domain.Device;
import com.ruoyi.system.domain.InstallationGuide;
import com.ruoyi.system.domain.vo.QueryVo;
import com.ruoyi.system.mapper.InstallationGuideMapper;
import com.ruoyi.system.service.IDeviceService;
import com.ruoyi.system.service.IInstallationGuideService;
import com.ruoyi.system.service.IProjectService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class InstallationGuideServiceImpl implements IInstallationGuideService
{
    @Autowired
    IDeviceService deviceService;

    @Autowired
    IProjectService projectService;

    @Autowired
    InstallationGuideMapper installationGuideMapper;

    @Override
    @UserDataIsolation(tableAlias = "tbl_installation_guide")
    public Map<String, Object> findExistance(InstallationGuide installationGuide) {
        return installationGuideMapper.findExistance(installationGuide);
    }

    @Override
    public Integer create(InstallationGuide installationGuide) {
        Integer id = null;
        try {
            Integer affected = installationGuideMapper.create(installationGuide);
            if (affected > 0) {
                id = installationGuide.getId();
            }
        } catch (Exception e) {
            log.info("",e);
            StackTraceElement[] traces = e.getStackTrace();
            for (StackTraceElement trace : traces) {
                log.debug(trace.toString());
            }
            id = null;
        }
        return id;
    }

    @Override
    public List<InstallationGuide> index(QueryVo queryVo) {
        List<InstallationGuide> installationGuides = installationGuideMapper.index(queryVo);
        for (InstallationGuide installationGuide : installationGuides) {
            Integer deviceId = installationGuide.getDeviceId();
            if (deviceId != null && deviceId > 0) {
                QueryVo subQueryVo = new QueryVo();
                subQueryVo.filters.put("id", deviceId);
                Device device = deviceService.retrieve(subQueryVo);
                if (device == null) {
                    continue;
                }
                installationGuide.setDeviceId(device.getId());
                installationGuide.setDeviceCode(device.getSn());
                installationGuide.setDeviceName(device.getName());
                // subQueryVo.filters.clear();
                // subQueryVo.filters.put("id", projectId);
                // Project project = projectService.retrieve(subQueryVo);
                // installationGuide.setProjectCode(project.getCode());
                // installationGuide.setProjectName(project.getName());
            }
        }
        return installationGuides;
    }

    @Override
    public InstallationGuide retrieve(QueryVo queryVo) {
        InstallationGuide installationGuide = installationGuideMapper.retrieve(queryVo);
        return installationGuide;
    }

    @Override
    public Boolean update(InstallationGuide installationGuide) {
        Boolean done = false;
        try {
            Integer affected = installationGuideMapper.update(installationGuide);
            if (affected > 0) {
                done = true;
            }
        } catch (Exception e) {
            log.info("",e);
            StackTraceElement[] traces = e.getStackTrace();
            for (StackTraceElement trace : traces) {
                log.debug(trace.toString());
            }
        }
        return done;
    }

    @Override
    public Boolean delete(QueryVo queryVo) {
        Boolean done = false;
        try {
            Integer affected = installationGuideMapper.delete(queryVo);
            if (affected > 0) {
                done = true;
            }
        } catch (Exception e) {
            log.info("",e);
            StackTraceElement[] traces = e.getStackTrace();
            for (StackTraceElement trace : traces) {
                log.debug(trace.toString());
            }
            done = false;
        }
        return done;
    }
}
