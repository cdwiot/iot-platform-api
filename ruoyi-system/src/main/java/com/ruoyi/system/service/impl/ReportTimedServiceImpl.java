package com.ruoyi.system.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONUtil;
import com.ruoyi.common.annotation.UserDataIsolation;
import com.ruoyi.common.exception.CustomException;
import com.ruoyi.system.domain.ReportTimed;
import com.ruoyi.system.mapper.ReportTimedMapper;
import com.ruoyi.system.service.IReportTimedService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class ReportTimedServiceImpl implements IReportTimedService {
    @Autowired
    private ReportTimedMapper reportTimedMapper;

    @Override
    @UserDataIsolation
    public List<ReportTimed> queryList(ReportTimed reportTimed) {
        List<ReportTimed> reportTimeds = reportTimedMapper.queryList(reportTimed);
        if (CollUtil.isNotEmpty(reportTimeds)) {
            reportTimeds.forEach(a -> {
                String data = a.getData();
                com.alibaba.fastjson.JSONArray jsonArray = com.alibaba.fastjson.JSONArray.parseArray(data);
                a.setChoseData(jsonArray);
            });
        }
        return reportTimeds;
    }

    @Override
    @UserDataIsolation
    public ReportTimed getOne(ReportTimed reportTimed) {
        ReportTimed timed = reportTimedMapper.getOne(reportTimed);
        if (timed != null) {
            String data = timed.getData();
            com.alibaba.fastjson.JSONArray jsonArray = com.alibaba.fastjson.JSONArray.parseArray(data);
            timed.setChoseData(jsonArray);
        }
        return timed;
    }

    @Override
    public Boolean add(ReportTimed reportTimed) {
        return reportTimedMapper.add(reportTimed) > 0;
    }

    @Override
    @UserDataIsolation
    public Boolean update(ReportTimed reportTimed) {
        return reportTimedMapper.update(reportTimed) > 0;
    }

    @Override
    @UserDataIsolation
    public Boolean delete(ReportTimed reportTimed) {
        return reportTimedMapper.delete(reportTimed) > 0;
    }

    @Override
    @UserDataIsolation
    public void checkIsExit(ReportTimed reportTimed) {
        ReportTimed result = reportTimedMapper.checkIsExit(reportTimed);
        if (result == null) {
            return;
        }
        if (reportTimed.getId() == null || result.getId().compareTo(reportTimed.getId()) != 0) {
            throw new CustomException("该报表已存在");
        }
    }
}
