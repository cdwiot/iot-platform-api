package com.ruoyi.system.service;

import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.system.domain.DevicePointCheckConfig;
import com.ruoyi.system.domain.PointCheckTask;
import com.ruoyi.system.domain.PointCheckTaskDevice;
import com.ruoyi.system.domain.vo.PointCheckCreatReportVO;
import com.ruoyi.system.domain.vo.PointCheckDeviceDetailDTO;
import com.ruoyi.system.domain.vo.PointCheckPCReportVO;
import com.ruoyi.system.domain.vo.QueryVo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public interface IPointCheckTaskDeviceService {

    /**
     * 任务产品关系的列表查询
     *
     * @param pointCheckTaskDevice
     * @return
     */
    List<PointCheckTaskDevice> queryList(PointCheckTaskDevice pointCheckTaskDevice);
    List<PointCheckTaskDevice> getSwitchDevice(QueryVo queryVo,Integer type);

    /**
     * 获取详情
     *
     * @param pointCheckTaskDevice
     * @return
     */
    PointCheckTaskDevice getOne(PointCheckTaskDevice pointCheckTaskDevice);

    /**
     * 添加
     *
     * @param pointCheckTaskDevice
     * @return
     */
    Boolean add(PointCheckTaskDevice pointCheckTaskDevice);
    Boolean addV2(PointCheckTaskDevice pointCheckTaskDevice, ArrayList<DevicePointCheckConfig> checkConfigList);

    /**
     * 修改
     *
     * @param pointCheckTaskDevice
     * @return
     */
    Boolean update(PointCheckTaskDevice pointCheckTaskDevice);

    /**
     * 删除
     *
     * @param pointCheckTaskDevice
     * @return
     */
    Boolean delete(PointCheckTaskDevice pointCheckTaskDevice);

    /**
     * 验证产品是否重复
     *
     * @param pointCheckTaskDevice
     * @return
     */
    void checkIsExist(PointCheckTaskDevice pointCheckTaskDevice);

    /**
     * @param pointCheckTaskDevice
     * @return
     */
    PointCheckTask queryTask(PointCheckTaskDevice pointCheckTaskDevice);

    /**
     * 生成报告
     *
     * @param id        任务产品Id
     * @param startTime 任务时间
     */
    List<PointCheckCreatReportVO> createReport(QueryVo queryVo);
    List<PointCheckCreatReportVO> createReportV2(QueryVo queryVo,Integer taskId,Integer taskDeviceId);

    /**
     * 一键生成报告
     *
     * @param id
     * @param sysUser
     * @param companyId
     * @return
     */
    void quickCreateReport(Integer id, SysUser sysUser, Long companyId);

    /**
     * PC端查询点检记录
     *
     * @param sn
     * @param startTime
     */
    PointCheckPCReportVO getReport(QueryVo queryVo);
    PointCheckPCReportVO getReportV2(QueryVo queryVo,Integer taskDeviceId,Integer taskId);

    /**
     * 查询所有的任务产品
     *
     * @param queryVo
     * @return
     */
    List<PointCheckTaskDevice> queryAllTaskDevice(QueryVo queryVo);

    /**
     * 查询所有异常检查项数据
     *
     * @return
     */
    List<Map<String, Object>> queryAllErrorItem(QueryVo queryVo);

    /**
     * APP根据产品代码获取产品跳转信息
     *
     * @param queryVo
     * @return
     */
    PointCheckDeviceDetailDTO getDeviceDetail(QueryVo queryVo);
}
