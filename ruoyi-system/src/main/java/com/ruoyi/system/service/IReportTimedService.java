package com.ruoyi.system.service;

import com.ruoyi.system.domain.ReportTimed;

import java.util.List;

public interface IReportTimedService {

    /**
     * 查询列表
     *
     * @param reportTimed
     * @return
     */
    List<ReportTimed> queryList(ReportTimed reportTimed);

    /**
     * 获取单个详情
     *
     * @param reportTimed
     * @return
     */
    ReportTimed getOne(ReportTimed reportTimed);

    /**
     * 添加
     *
     * @param reportTimed
     * @return
     */
    Boolean add(ReportTimed reportTimed);

    /**
     * 修改
     *
     * @param reportTimed
     * @return
     */
    Boolean update(ReportTimed reportTimed);

    /**
     * 删除
     *
     * @param reportTimed
     * @return
     */
    Boolean delete(ReportTimed reportTimed);

    /**
     * 验证是否重复
     *
     * @param reportTimed
     * @return
     */
    void checkIsExit(ReportTimed reportTimed);
}
