package com.ruoyi.system.service;

import java.util.List;
import java.util.Map;

import com.ruoyi.system.domain.ProductChart;
import com.ruoyi.system.domain.vo.QueryVo;

public interface IProjectWithoutProductChartService
{
    public Boolean set(List<Integer> productChartIdList, Integer id);
    public List<ProductChart> get(QueryVo queryVo);
    public List<Map<String, Object>> availability(QueryVo queryVo);
}
