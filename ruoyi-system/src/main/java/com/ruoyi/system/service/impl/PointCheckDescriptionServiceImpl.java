package com.ruoyi.system.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.ruoyi.common.annotation.UserDataIsolation;
import com.ruoyi.common.exception.CustomException;
import com.ruoyi.system.domain.PointCheckDescription;
import com.ruoyi.system.domain.vo.PointCheckDescriptionVO;
import com.ruoyi.system.mapper.PointCheckDescriptionMapper;
import com.ruoyi.system.service.IPointCheckDescriptionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Slf4j
@Service
public class PointCheckDescriptionServiceImpl implements IPointCheckDescriptionService {

    @Autowired
    private PointCheckDescriptionMapper pointCheckDescriptionMapper;

    @Override
    @UserDataIsolation
    public List<PointCheckDescription> queryList(PointCheckDescription pointCheckDescription) {
        return pointCheckDescriptionMapper.queryList(pointCheckDescription);
    }

    @Override
    @UserDataIsolation
    public PointCheckDescription getOne(PointCheckDescription pointCheckDescription) {
        return pointCheckDescriptionMapper.getOne(pointCheckDescription);
    }

    @Override
    public int addBatch(List<PointCheckDescription> pointCheckDescriptions) {
        return pointCheckDescriptionMapper.addBatch(pointCheckDescriptions);
    }

    @Override
    @UserDataIsolation
    public Boolean update(PointCheckDescription pointCheckDescription) {
        return pointCheckDescriptionMapper.update(pointCheckDescription) > 0;
    }

    @Override
    @Transactional
    public void updateBatch(List<PointCheckDescription> pointCheckDescriptions) {
        for (PointCheckDescription description : pointCheckDescriptions) {
            PointCheckDescription result = getOne(description);
            if (result == null) {
                throw new CustomException("没有查询到自动生成的自定义字段");
            }
            update(description);
        }
    }

    @Override
    @UserDataIsolation
    public Boolean delete(PointCheckDescription pointCheckDescription) {
        return pointCheckDescriptionMapper.delete(pointCheckDescription) > 0;
    }

    @Override
    public List<PointCheckDescriptionVO> queryDescriptionVO(Set<Integer> detailId) {
        if (CollUtil.isEmpty(detailId)){
            return new ArrayList<>();
        }
        return pointCheckDescriptionMapper.queryDescriptionVO(detailId);
    }
}
