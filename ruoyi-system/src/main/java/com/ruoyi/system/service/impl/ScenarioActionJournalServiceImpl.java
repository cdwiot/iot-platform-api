package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.common.annotation.UserDataIsolation;
import com.ruoyi.common.enums.Severity;
import com.ruoyi.common.enums.TriggerLogic;
import com.ruoyi.system.domain.ScenarioActionJournal;
import com.ruoyi.system.domain.vo.QueryVo;
import com.ruoyi.system.mapper.ScenarioActionJournalMapper;
import com.ruoyi.system.service.IScenarioActionJournalService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ScenarioActionJournalServiceImpl implements IScenarioActionJournalService
{
    @Autowired
    private ScenarioActionJournalMapper scenarioActionJournalMapper;

    @Override
    @UserDataIsolation(tableAlias = "p")
    public List<ScenarioActionJournal> index(QueryVo queryVo) {
        List<ScenarioActionJournal> scenarioActionJournals = scenarioActionJournalMapper.index(queryVo);
        for (ScenarioActionJournal scenarioActionJournal : scenarioActionJournals) {
            if (scenarioActionJournal.getState() == 0) {
                scenarioActionJournal.setStateString("失败");
            } else if (scenarioActionJournal.getState() == 0) {
                scenarioActionJournal.setStateString("成功");
            } else {}
        }
        return scenarioActionJournals;
    }
}
