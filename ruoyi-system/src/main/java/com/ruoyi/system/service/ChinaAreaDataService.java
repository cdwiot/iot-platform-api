package com.ruoyi.system.service;

import java.util.HashMap;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ChinaAreaDataService {
    private static JSONObject data = null;
    private static Map<String, String> nameMap = null;

    private static Boolean load() {
        Boolean done = false;
        try {
            String content = FileService.load("/china-area-data/data.json", false);
            JSONObject parsed = JSON.parseObject(content);
            data = parsed;
            done = true;
        } catch (Exception e) {
            log.info("",e);
            StackTraceElement[] traces = e.getStackTrace();
            for (StackTraceElement trace : traces) {
                log.debug(trace.toString());
            }
        }
        return done;
    }

    private static Boolean buildFullNames() {
        Boolean done = false;
        try {
            if (nameMap == null) {
                nameMap = new HashMap<>();
            }
            if (data == null) {
                if (!ChinaAreaDataService.load()) {
                    return done;
                }
            }
            JSONObject provinces = data.getJSONObject("86");
            String code = null;
            String name = null;
            for (String province : provinces.keySet()) {
                JSONObject cities = data.getJSONObject(province);
                code = String.format("%s", province);
                name = String.format("%s", provinces.getString(province));
                nameMap.put(name, code);
                log.info("! province: {} {}", code, name);
                if (cities == null) {
                    continue;
                }
                for (String city : cities.keySet()) {
                    JSONObject districts = data.getJSONObject(city);
                    code = String.format("%s,%s", province, city);
                    name = String.format("%s%s", provinces.getString(province), cities.getString(city));
                    nameMap.put(name, code);
                    log.info("!! city: {} {}", code, name);
                    if (districts == null) {
                        continue;
                    }
                    for (String district : districts.keySet()) {
                        code = String.format("%s,%s,%s", province, city, district);
                        name = String.format("%s%s%s", provinces.getString(province), cities.getString(city), districts.getString(district));
                        nameMap.put(name, code);
                        log.info("!!! district: {} {}", code, name);
                    }
                }
            }
            done = true;
        } catch (Exception e) {
            log.info("",e);
            StackTraceElement[] traces = e.getStackTrace();
            for (StackTraceElement trace : traces) {
                log.debug(trace.toString());
            }
            nameMap = null;
        }
        return done;
    }

    public static String codeToName(String code) {
        String name = "";
        if (code == null) {
            return name;
        }
        try {
            String[] pieces = code.split(",");
            if (data == null) {
                if (!ChinaAreaDataService.load()) {
                    return name;
                }
            }
            for (int i = 0; i < pieces.length; i++) {
                String piece = pieces[i];
                String parentCode = null;
                if (i == 0) {
                    parentCode = "86";
                } else if (i < 3) {
                    parentCode = pieces[i - 1];
                }
                if (parentCode != null) {
                    JSONObject group = data.getJSONObject(parentCode);
                    String found = group.getString(piece);
                    if (found != null) {
                        if (name.length() > 0) {
                            name += " ";
                        }
                        name += found;
                    }
                }
            }
        } catch (Exception e) {
            log.info("",e);
            StackTraceElement[] traces = e.getStackTrace();
            for (StackTraceElement trace : traces) {
                log.debug(trace.toString());
            }
        }
        return name;
    }

    public static String nameToCode(String name) {
        String code = null;
        if (nameMap == null) {
            if (!ChinaAreaDataService.buildFullNames()) {
                return null;
            }
        }
        try {
            code = nameMap.get(name);
        } catch (Exception e) {
            log.info("",e);
            StackTraceElement[] traces = e.getStackTrace();
            for (StackTraceElement trace : traces) {
                log.debug(trace.toString());
            }
        }
        return code;
    }
}
