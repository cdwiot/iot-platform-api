package com.ruoyi.system.service;

import com.ruoyi.system.domain.PointCheckDate;

import java.util.List;

public interface IPointCheckDateService {

    /**
     * 点检计划时间列表
     *
     * @param pointCheckDate
     * @return
     */
    List<PointCheckDate> queryList(PointCheckDate pointCheckDate);

    /**
     * 添加
     *
     * @param pointCheckDate
     * @return
     */
    Boolean add(PointCheckDate pointCheckDate);

    /**
     * 修改
     *
     * @param pointCheckDate
     * @return
     */
    Boolean update(PointCheckDate pointCheckDate);

    /**
     * 删除
     *
     * @param pointCheckDate
     * @return
     */
    Boolean delete(PointCheckDate pointCheckDate);
}
