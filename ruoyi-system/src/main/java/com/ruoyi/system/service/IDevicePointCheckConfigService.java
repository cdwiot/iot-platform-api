package com.ruoyi.system.service;

import com.alibaba.fastjson.JSONObject;
import com.ruoyi.system.domain.DevicePointCheckConfig;
import com.ruoyi.system.domain.Project;
import com.ruoyi.system.domain.vo.QueryVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface IDevicePointCheckConfigService {

    /**
     * 产品点检配置分页查询
     *
     * @param queryVo
     * @return
     */
    List<DevicePointCheckConfig> queryList(QueryVo queryVo);



    /**
     * 获取产品点检配置详情
     *
     * @param queryVo
     * @return
     */
    DevicePointCheckConfig getOne(QueryVo queryVo);

    /**
     * 添加
     *
     * @param devicePointCheckConfig
     * @return
     */
    Boolean add(DevicePointCheckConfig devicePointCheckConfig);
    /**
     * 重置
     *
     * @param devicePointCheckConfig
     * @return
     */
    Boolean init(Integer deviceId,Long companyId,String userName);

    /**
     * 修改
     *
     * @param devicePointCheckConfig
     * @return
     */
    Boolean update(DevicePointCheckConfig devicePointCheckConfig);
    Boolean updateSys(QueryVo queryVo);

    /**
     * 删除
     *
     * @param queryVo
     * @return
     */
    Boolean delete(QueryVo queryVo);
    Boolean deleteSys(QueryVo queryVo);

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    Boolean deleteBatch(@Param("ids") List<String> ids);

    /**
     * 验证检查项是否重复
     *
     * @param devicePointCheckConfig
     * @return
     */
    void checkIsExist(DevicePointCheckConfig devicePointCheckConfig);
    void checkIsType(DevicePointCheckConfig devicePointCheckConfig);

    /**
     * 验证检查内容是否重复
     *
     * @param devicePointCheckConfig
     * @return
     */
    void checkIsItem(DevicePointCheckConfig devicePointCheckConfig);

    /**
     * 获取点检配置枚举，没有查询到返回空
     *
     * @param queryVo
     * @return
     */
    List<Map<String,Object>> typeItemEnum(QueryVo queryVo);


    /**
     * 获取点检配置枚举，没有查询到返回空
     *
     * @param queryVo
     * @return
     */
    List<Map<String,Object>> typeItemEnumCheck(QueryVo queryVo);

    /**
     * 获取点检配置枚举,没有查询到赋予默认值
     *
     * @param queryVo
     * @return
     */
    List<Map<String,Object>> typeItemDefaultEnum(QueryVo queryVo);

    /**
     * 初始化自定义点检配置
     *
     * @param queryVo
     */
    int initPointCheckConfigByDeviceId(QueryVo queryVo);

    /**
     * 导入产品点检配置
     *
     * @param item
     * @param companyId
     * @return
     */
    public Map<String, Object> parseInjectionItem(JSONObject item, Long companyId);

    /**
     *  验证检查项是否重复
     * @param devicePointCheckConfig
     * @return
     */
    public Map<String, Object> findExistance(DevicePointCheckConfig devicePointCheckConfig);
    public Map<String, Object> findExistanceItem(DevicePointCheckConfig devicePointCheckConfig);
}
