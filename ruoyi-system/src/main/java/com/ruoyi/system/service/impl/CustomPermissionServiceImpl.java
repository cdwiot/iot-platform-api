package com.ruoyi.system.service.impl;

import cn.hutool.core.util.StrUtil;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.service.ICustomPermissionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class CustomPermissionServiceImpl implements ICustomPermissionService {

    @Override
    public String getProjectAuthorizePermission(String tableAlias, String field, SysUser sysUser) {
        StringBuilder sb = new StringBuilder();
        sb.append(" AND ");
        if (StrUtil.isNotBlank(tableAlias)){
            sb.append("`").append(tableAlias).append("`").append(".");
        }
        if (StrUtil.isNotBlank(field)){
            sb.append(field);
        }else {
            sb.append("`id`");
        }
        sb.append(StringUtils.format(" IN ( SELECT `project_id` FROM `tbl_project_authorize` WHERE `deleted` = 0 AND `user_id` = {} )", sysUser.getUserId()));
        return sb.toString();
    }
}
