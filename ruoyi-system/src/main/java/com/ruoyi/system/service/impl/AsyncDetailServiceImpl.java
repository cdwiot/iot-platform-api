package com.ruoyi.system.service.impl;

import com.ruoyi.common.annotation.UserDataIsolation;
import com.ruoyi.system.domain.AsyncDetail;
import com.ruoyi.system.domain.vo.QueryVo;
import com.ruoyi.system.mapper.AsyncDetailMapper;
import com.ruoyi.system.service.IAsyncDetailService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;

@Slf4j
@Service
public class AsyncDetailServiceImpl implements IAsyncDetailService {

    @Autowired
    private AsyncDetailMapper asyncDetailMapper;

    @Override
    public String createTid(String business, Long companyId) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
        String tid = String.format("%s%s", sdf.format(new Date()), RandomStringUtils.randomAlphanumeric(4));
        AsyncDetail detail = new AsyncDetail();
        detail.setBusiness(business);
        detail.setCompanyId(companyId);
        detail.setStatus(0);
        detail.setTid(tid);
        detail.setCreateTime(new Date());
        asyncDetailMapper.create(detail);
        return tid;
    }

    @Override
    public Boolean update(String tid, Integer status, String data) {
        AsyncDetail asyncDetail = new AsyncDetail();
        asyncDetail.setStatus(status);
        asyncDetail.setTid(tid);
        asyncDetail.setData(data);
        return asyncDetailMapper.update(asyncDetail) > 0;
    }

    @Override
    @UserDataIsolation
    public AsyncDetail retrieve(QueryVo queryVo) {
        AsyncDetail asyncDetail = asyncDetailMapper.retrieve(queryVo);
        return asyncDetail;
    }

}
