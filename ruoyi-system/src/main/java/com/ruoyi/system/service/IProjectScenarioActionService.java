package com.ruoyi.system.service;

import java.util.List;
import java.util.Map;

import com.ruoyi.system.domain.ProjectScenarioAction;
import com.ruoyi.system.domain.vo.QueryVo;

public interface IProjectScenarioActionService {
    public Map<String, Object> findExistance(ProjectScenarioAction projectScenarioAction);
    public Integer create(ProjectScenarioAction projectScenarioAction);
    public List<ProjectScenarioAction> index(QueryVo queryVo);
    public ProjectScenarioAction retrieve(QueryVo queryVo);
    public Boolean update(ProjectScenarioAction projectScenarioAction);
    public Boolean delete(QueryVo queryVo);
}
