package com.ruoyi.system.service;

import org.thingsboard.server.common.data.Tenant;
import org.thingsboard.server.common.data.User;

public interface IThingsBoardService {

	/** 创建新的租户 */
	public Tenant addNewTenant(String tenantTitle);
	
	/** 创建租户用户 */
	public User addNewTenantUser(Tenant tenant, String userEmail, String userPassword);
	
	/** 检查租户名称是已存在 */
	public String checkTenantTitleUnique(String tenantTitle);
}
