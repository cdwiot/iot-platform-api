package com.ruoyi.system.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.ruoyi.common.annotation.UserDataIsolation;
import com.ruoyi.common.exception.CustomException;
import com.ruoyi.system.domain.ProductPointCheckConfig;
import com.ruoyi.system.domain.vo.ProductPointCheckConfigVO;
import com.ruoyi.system.domain.vo.QueryVo;
import com.ruoyi.system.mapper.ProductPointCheckConfigMapper;
import com.ruoyi.system.service.IProductPointCheckConfigService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
public class ProductPointCheckConfigServiceImpl implements IProductPointCheckConfigService {
//
//    @Autowired
//    private ProductPointCheckConfigMapper productPointCheckConfigMapper;
//
//    @Override
//    @UserDataIsolation
//    public List<ProductPointCheckConfig> queryPageList(QueryVo queryVo) {
//        return productPointCheckConfigMapper.queryList(queryVo);
//    }
//
//
//    @Override
//    @UserDataIsolation
//    public ProductPointCheckConfig getOne(QueryVo queryVo) {
//        return productPointCheckConfigMapper.getOne(queryVo);
//    }
//
//    @Override
//    public Boolean add(ProductPointCheckConfig productPointCheckConfig) {
//        return productPointCheckConfigMapper.add(productPointCheckConfig) > 0;
//    }
//
//    @Override
//    @UserDataIsolation
//    public Boolean update(ProductPointCheckConfig productPointCheckConfig) {
//        return productPointCheckConfigMapper.update(productPointCheckConfig) > 0;
//    }
//
//    @Override
//    public Boolean delete(QueryVo queryVo) {
//        return productPointCheckConfigMapper.delete(queryVo) > 0;
//    }
//
//    @Override
//    public Boolean deleteBatch(List<String> ids) {
//        return productPointCheckConfigMapper.deleteBatch(ids) > 0;
//    }
//
//    @Override
//    @UserDataIsolation
//    public Set<String> typeEnum(QueryVo queryVo) {
//        List<ProductPointCheckConfig> productPointCheckConfigs = productPointCheckConfigMapper.queryList(queryVo);
//        if (CollUtil.isEmpty(productPointCheckConfigs)){
//            return new HashSet<>();
//        }
//        return productPointCheckConfigs.stream().map(ProductPointCheckConfig::getType).collect(Collectors.toSet());
//    }
//
//    @Override
//    public List<Map<String, Object>> queryConfig(QueryVo queryVo) {
//        List<ProductPointCheckConfig> productPointCheckConfigs = productPointCheckConfigMapper.queryList(queryVo);
//        if (CollUtil.isEmpty(productPointCheckConfigs)){
//            productPointCheckConfigs = initPointCheckConfig();
//        }
//        Map<String, List<ProductPointCheckConfig>> collect = productPointCheckConfigs.stream().collect(Collectors.groupingBy(ProductPointCheckConfig::getType));
//        List<Map<String, Object>> result = new ArrayList<>();
//        for (Map.Entry<String, List<ProductPointCheckConfig>> entry : collect.entrySet()) {
//            Map<String, Object> map = new HashMap<>();
//            String key = entry.getKey();
//            List<ProductPointCheckConfig> value = entry.getValue();
//            List<Map<String,String>> childList = new ArrayList<>();
//            value.forEach(a ->{
//                Map<String,String> childMap = new HashMap<>();
//                childMap.put("name",a.getItem());
//                childMap.put("dataType",a.getDataType());
//                childList.add(childMap);
//            });
//            map.put("name",key);
//            map.put("childData",childList);
//            result.add(map);
//        }
//        Collections.sort(result, new Comparator<Map<String, Object>>() {
//            @Override
//            public int compare(Map<String, Object> o1, Map<String, Object> o2) {
//                String name1 = (String)  o1.get("name");
//                String name2 = (String)  o2.get("name");
//                return name1.compareTo(name2);
//            }
//        });
//        return result;
//    }
//
//    @Override
//    public void checkIsExist(ProductPointCheckConfig productPointCheckConfig) {
//        ProductPointCheckConfig  result = productPointCheckConfigMapper.checkIsExist(productPointCheckConfig);
//        if (result == null){
//            return;
//        }
//        if (productPointCheckConfig.getId() == null || productPointCheckConfig.getId().compareTo(result.getId()) != 0) {
//            throw new CustomException("该检查类别下的检查项已存在!");
//        }
//    }
//
//    @Override
//    public List<ProductPointCheckConfig> queryPointCheckConfig(QueryVo queryVo) {
//        return productPointCheckConfigMapper.queryPointCheckConfig(queryVo);
//    }
//
//    /**
//     * 初始化默认点检项
//     * @return
//     */
//    private List<ProductPointCheckConfig> initPointCheckConfig() {
//        List<ProductPointCheckConfig> result = new ArrayList<>();
//        ProductPointCheckConfig checkConfig1 = new ProductPointCheckConfig();
//        checkConfig1.setType("波纹管");
//        checkConfig1.setItem("裂纹");
//        checkConfig1.setDataType("photo");
//        result.add(checkConfig1);
//        ProductPointCheckConfig checkConfig2 = new ProductPointCheckConfig();
//        checkConfig2.setType("波纹管");
//        checkConfig2.setItem("腐蚀");
//        checkConfig2.setDataType("photo");
//        result.add(checkConfig2);
//        ProductPointCheckConfig checkConfig3 = new ProductPointCheckConfig();
//        checkConfig3.setType("波纹管");
//        checkConfig3.setItem("泄漏");
//        checkConfig3.setDataType("photo");
//        result.add(checkConfig3);
//        ProductPointCheckConfig checkConfig4 = new ProductPointCheckConfig();
//        checkConfig4.setType("波纹管");
//        checkConfig4.setItem("异常变形");
//        checkConfig4.setDataType("photo");
//        result.add(checkConfig4);
//        ProductPointCheckConfig checkConfig5 = new ProductPointCheckConfig();
//        checkConfig5.setType("波纹管");
//        checkConfig5.setItem("其他");
//        checkConfig5.setDataType("photo");
//        result.add(checkConfig5);
//        ProductPointCheckConfig checkConfig6 = new ProductPointCheckConfig();
//        checkConfig6.setType("承力承压件");
//        checkConfig6.setItem("焊缝");
//        checkConfig6.setDataType("photo");
//        result.add(checkConfig6);
//        ProductPointCheckConfig checkConfig7 = new ProductPointCheckConfig();
//        checkConfig7.setType("承力承压件");
//        checkConfig7.setItem("大变形");
//        checkConfig7.setDataType("photo");
//        result.add(checkConfig7);
//        ProductPointCheckConfig checkConfig8 = new ProductPointCheckConfig();
//        checkConfig8.setType("承力承压件");
//        checkConfig8.setItem("其他");
//        checkConfig8.setDataType("photo");
//        result.add(checkConfig8);
//        ProductPointCheckConfig checkConfig9 = new ProductPointCheckConfig();
//        checkConfig9.setType("工况");
//        checkConfig9.setItem("");
//        checkConfig9.setDataType("string");
//        result.add(checkConfig9);
//        ProductPointCheckConfig checkConfig10 = new ProductPointCheckConfig();
//        checkConfig10.setType("工况");
//        checkConfig10.setItem("压力");
//        checkConfig10.setDataType("string");
//        result.add(checkConfig10);
//        ProductPointCheckConfig checkConfig11 = new ProductPointCheckConfig();
//        checkConfig11.setType("工况");
//        checkConfig11.setItem("振动");
//        checkConfig11.setDataType("video");
//        result.add(checkConfig11);
//        ProductPointCheckConfig checkConfig12 = new ProductPointCheckConfig();
//        checkConfig12.setType("工况");
//        checkConfig12.setItem("运行周期");
//        checkConfig12.setDataType("string");
//        result.add(checkConfig12);
//        ProductPointCheckConfig checkConfig13 = new ProductPointCheckConfig();
//        checkConfig13.setType("附属件");
//        checkConfig13.setItem("脱落");
//        checkConfig13.setDataType("photo");
//        result.add(checkConfig13);
//        ProductPointCheckConfig checkConfig14 = new ProductPointCheckConfig();
//        checkConfig14.setType("附属件");
//        checkConfig14.setItem("干涉");
//        checkConfig14.setDataType("photo");
//        result.add(checkConfig14);
//        ProductPointCheckConfig checkConfig15 = new ProductPointCheckConfig();
//        checkConfig15.setType("附属件");
//        checkConfig15.setItem("其他");
//        checkConfig15.setDataType("photo");
//        result.add(checkConfig15);
//        return result;
//    }
}
