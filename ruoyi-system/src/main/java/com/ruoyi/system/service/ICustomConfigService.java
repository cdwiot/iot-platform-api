package com.ruoyi.system.service;

import com.ruoyi.system.domain.CustomConfig;
import com.ruoyi.system.domain.vo.QueryVo;

public interface ICustomConfigService {
    public CustomConfig load(QueryVo queryVo);
    public Boolean save(CustomConfig customConfig);
}
