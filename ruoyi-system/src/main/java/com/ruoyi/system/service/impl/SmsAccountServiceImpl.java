package com.ruoyi.system.service.impl;

import com.ruoyi.common.annotation.UserDataIsolation;
import com.ruoyi.system.domain.SmsAccount;
import com.ruoyi.system.mapper.SmsAccountMapper;
import com.ruoyi.system.service.ISmsAccountService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class SmsAccountServiceImpl implements ISmsAccountService {
    @Autowired
    private SmsAccountMapper smsAccountMapper;

    @Override
    public List<SmsAccount> queryList(SmsAccount smsAccount) {
        return smsAccountMapper.queryList(smsAccount);
    }

    @Override
    public SmsAccount getOne(SmsAccount smsAccount) {
        return smsAccountMapper.getOne(smsAccount);
    }

    @Override
    @UserDataIsolation(tableAlias = "t1")
    public SmsAccount getMessageAccount(SmsAccount smsAccount) {
        return smsAccountMapper.getMessageAccount(smsAccount);
    }

    @Override
    @UserDataIsolation(tableAlias = "t1")
    public SmsAccount getCompanyOne(SmsAccount smsAccount) {
        return smsAccountMapper.getCompanyOne(smsAccount);
    }

    @Override
    public Boolean addMessageAccount(SmsAccount smsAccount) {
        return smsAccountMapper.addMessageAccount(smsAccount) > 0;
    }

    @Override
    public Boolean updateMessageAccount(SmsAccount smsAccount) {
        return smsAccountMapper.updateMessageAccount(smsAccount) > 0;
    }

    @Override
    public Boolean deleteMessageAccount(SmsAccount smsAccount) {
        return smsAccountMapper.deleteMessageAccount(smsAccount) > 0;
    }
}
