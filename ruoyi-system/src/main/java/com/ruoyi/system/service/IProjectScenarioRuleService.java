package com.ruoyi.system.service;

import java.util.List;
import java.util.Map;

import com.ruoyi.system.domain.ProjectScenarioRule;
import com.ruoyi.system.domain.vo.QueryVo;

public interface IProjectScenarioRuleService {
    public Map<String, Object> findExistance(ProjectScenarioRule projectScenarioRule);
    public Integer create(ProjectScenarioRule projectScenarioRule);
    public List<ProjectScenarioRule> index(QueryVo queryVo);
    public List<Map<String, Object>> available(QueryVo queryVo);
    public ProjectScenarioRule retrieve(QueryVo queryVo);
    public Boolean update(ProjectScenarioRule projectScenarioRule);
    public Boolean delete(QueryVo queryVo);
}
