package com.ruoyi.system.service.impl;

import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.annotation.UserDataIsolation;
import com.ruoyi.system.domain.CoapCredentials;
import com.ruoyi.system.domain.SysCompanyThingsboard;
import com.ruoyi.system.domain.vo.QueryVo;
import com.ruoyi.system.mapper.CoapCredentialsMapper;
import com.ruoyi.system.service.IDeviceTerminalCoapService;
import com.ruoyi.system.service.IThingsBoardTenantService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class DeviceTerminalCoapServiceImpl implements IDeviceTerminalCoapService
{
    @Autowired
    CoapCredentialsMapper coapCredentialsMapper;

    @Autowired
    IThingsBoardTenantService thingsBoardTenantService;

    @Override
    @UserDataIsolation(tableAlias = "tbl_coap_credentials")
    public Map<String, Object> findExistance(CoapCredentials coapCredentials) {
        return coapCredentialsMapper.findExistance(coapCredentials);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Integer createCredentials(CoapCredentials coapCredentials, SysCompanyThingsboard sysCompanyThingsboard) {
        Integer id = null;
        try {
            Integer affected = coapCredentialsMapper.create(coapCredentials);
            if (affected > 0) {
                JSONObject fields = new JSONObject();
                fields.put("deviceId", coapCredentials.getTbDeviceId());
                JSONObject credentials = new JSONObject();
                credentials.put("accessToken", coapCredentials.getAccessToken());
                fields.put("credentials", credentials);
                thingsBoardTenantService.saveCoapDeviceCredentials(fields, sysCompanyThingsboard);
                id = coapCredentials.getId();
            }
        } catch (Exception e) {
            id = null;
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            log.info("",e);
            StackTraceElement[] traces = e.getStackTrace();
            for (StackTraceElement trace : traces) {
                log.debug(trace.toString());
            }
        }
        return id;
    }

    @Override
    @UserDataIsolation(tableAlias = "mc")
    public CoapCredentials retrieveCredentials(QueryVo queryVo) {
        CoapCredentials coapCredentials = coapCredentialsMapper.retrieve(queryVo);
        return coapCredentials;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    @UserDataIsolation(tableAlias = "tbl_coap_credentials")
    public Boolean updateCredentials(CoapCredentials coapCredentials, SysCompanyThingsboard sysCompanyThingsboard) {
        Boolean done = false;
        try {
            Integer affected = coapCredentialsMapper.update(coapCredentials);
            if (affected > 0) {
                JSONObject fields = new JSONObject();
                fields.put("deviceId", coapCredentials.getTbDeviceId());
                JSONObject credentials = new JSONObject();
                credentials.put("accessToken", coapCredentials.getAccessToken());
                fields.put("credentials", credentials);
                thingsBoardTenantService.saveCoapDeviceCredentials(fields, sysCompanyThingsboard);
                done = true;
            }
        } catch (Exception e) {
            done = false;
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            log.info("",e);
            StackTraceElement[] traces = e.getStackTrace();
            for (StackTraceElement trace : traces) {
                log.debug(trace.toString());
            }
        }
        return done;
    }
}
