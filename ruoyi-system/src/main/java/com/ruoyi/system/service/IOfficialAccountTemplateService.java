package com.ruoyi.system.service;

import com.ruoyi.system.domain.OfficialAccountTemplate;

import java.util.List;

public interface IOfficialAccountTemplateService {

    /**
     * 查询公众号模板列表
     *
     * @param officialAccountTemplate
     * @return
     */
    List<OfficialAccountTemplate> queryList(OfficialAccountTemplate officialAccountTemplate);

    /**
     * 获取模板详情
     *
     * @param officialAccountTemplate
     * @return
     */
    OfficialAccountTemplate getOne(OfficialAccountTemplate officialAccountTemplate);

    /**
     * 添加模板
     *
     * @param officialAccountTemplate
     * @return
     */
    Boolean add(OfficialAccountTemplate officialAccountTemplate);

    /**
     * 修改模板
     *
     * @param officialAccountTemplate
     * @return
     */
    Boolean update(OfficialAccountTemplate officialAccountTemplate);

    /**
     * 删除模板
     *
     * @param officialAccountTemplate
     * @return
     */
    Boolean delete(OfficialAccountTemplate officialAccountTemplate);

    /**
     * 验证模板编码是否重复
     *
     * @param officialAccountTemplate
     * @return
     */
    void checkTemplateCodeExist(OfficialAccountTemplate officialAccountTemplate);
}
