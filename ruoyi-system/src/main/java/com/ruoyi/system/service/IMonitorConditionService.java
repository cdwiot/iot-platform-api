package com.ruoyi.system.service;

import java.util.List;

import com.ruoyi.system.domain.MonitorCondition;
import com.ruoyi.system.domain.vo.QueryVo;

public interface IMonitorConditionService 
{
    public Integer create(MonitorCondition monitorCondition);
    public List<MonitorCondition> index(QueryVo queryVo);
    public MonitorCondition retrieve(QueryVo queryVo);
    public Boolean update(MonitorCondition monitorCondition);
    public Boolean delete(QueryVo queryVo);
}
