package com.ruoyi.system.service;

import com.ruoyi.common.core.domain.TreeSelect;
import com.ruoyi.common.core.domain.entity.SysDept;
import com.ruoyi.common.core.domain.entity.SysUser;

import java.util.List;
import java.util.Map;

/**
 * 部门管理 服务层
 *
 * @author ruoyi
 */
public interface ISysDeptService {
    /**
     * 查询部门管理数据
     *
     * @param dept        部门信息
     * @param currentDept 当前登录部门信息
     * @param flag        当前用户是否是超级管理员（true是flase不是）
     * @return 部门信息集合
     */
    public List<SysDept> selectDeptList(SysDept dept, SysDept currentDept, Boolean flag);

    /**
     * 构建前端所需要树结构
     *
     * @param depts 部门列表
     * @return 树结构列表
     */
    public List<SysDept> buildDeptTree(List<SysDept> depts);

    /**
     * 构建前端所需要下拉树结构
     *
     * @param depts 部门列表
     * @return 下拉树结构列表
     */
    public List<TreeSelect> buildDeptTreeSelect(List<SysDept> depts);

    /**
     * 根据用户id查询公司id
     *
     * @param userId 用户id
     * @return 公司id
     */
    public Long selectCompanyIdByUserId(Long userId);

    /**
     * 根据用户查询公司id
     *
     * @param userId 用户id
     * @return 公司id
     */
    public Long selectCompanyIdByUser(SysUser user);

    /**
     * 根据角色ID查询部门树信息
     *
     * @param roleId 角色ID
     * @return 选中部门列表
     */
    public List<Integer> selectDeptListByRoleId(Long roleId);

    /**
     * 根据部门ID查询信息
     *
     * @param deptId 部门ID
     * @return 部门信息
     */
    public SysDept selectDeptById(Long deptId);

    /**
     * 根据ID查询所有子部门（正常状态）
     *
     * @param deptId 部门ID
     * @return 子部门数
     */
    public int selectNormalChildrenDeptById(Long deptId);

    /**
     * 是否存在部门子节点
     *
     * @param deptId 部门ID
     * @return 结果
     */
    public boolean hasChildByDeptId(Long deptId);

    /**
     * 查询部门是否存在用户
     *
     * @param deptId 部门ID
     * @return 结果 true 存在 false 不存在
     */
    public boolean checkDeptExistUser(Long deptId);

    /**
     * 校验部门名称是否唯一
     *
     * @param dept 部门信息
     * @return 结果
     */
    public String checkDeptNameUnique(SysDept dept);

    /**
     * 新增保存部门信息
     *
     * @param dept 部门信息
     * @return 结果
     */
    public int insertDept(SysDept dept);

    /**
     * 修改保存部门信息
     *
     * @param dept 部门信息
     * @return 结果
     */
    public int updateDept(SysDept dept);

    /**
     * 删除部门管理信息
     *
     * @param deptId 部门ID
     * @return 结果
     */
    public int deleteDeptById(Long deptId);

    /**
     * 设置部门logo
     *
     * @param deptId 部门id
     * @param logo
     */
    public void setLogo(Long deptId, String logo);

    /**
     * 获取部门logo
     *
     * @param deptId
     * @return
     */
    public Map<String, String> getLogo(Long deptId);

    public SysDept lookupByName(String name, Long companyId);

    /**
     * 验证部门是否属于客户
     *
     * @param deptId
     * @return true 客户公司 false 非客户公司
     */
    public boolean checkIsCustomer(Long deptId);

    /**
     * 获取客户公司Id（已知当前公司在客户公司下）
     *
     * @param deptId
     * @return
     */
    public Long getCustomerId(Long deptId);

    /**
     * 获取客户公司id（如果没有获取到则返回总公司id）
     *
     * @param deptId
     * @return
     */
    public Long getTopDeptId(Long deptId);

    int systestDept();
}
