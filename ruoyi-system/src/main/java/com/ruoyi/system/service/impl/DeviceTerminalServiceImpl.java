package com.ruoyi.system.service.impl;

import java.util.List;
import java.util.Map;

import com.ruoyi.common.annotation.UserDataIsolation;
import com.ruoyi.system.domain.DeviceTerminal;
import com.ruoyi.system.domain.vo.QueryVo;
import com.ruoyi.system.mapper.DeviceTerminalMapper;
import com.ruoyi.system.service.IDeviceTerminalMqttService;
import com.ruoyi.system.service.IDeviceTerminalService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class DeviceTerminalServiceImpl implements IDeviceTerminalService
{
    @Autowired
    DeviceTerminalMapper deviceTerminalMapper;

    @Autowired
    IDeviceTerminalMqttService deviceTerminalMqttService;

    @Override
    @UserDataIsolation(tableAlias = "tbl_device_terminal")
    public Map<String, Object> findExistance(DeviceTerminal deviceTerminal) {
        return deviceTerminalMapper.findExistance(deviceTerminal);
    }

    @Override
    public Integer create(DeviceTerminal deviceTerminal) {
        Integer id = null;
        try {
            Integer affected = deviceTerminalMapper.create(deviceTerminal);
            if (affected > 0) {
                id = deviceTerminal.getId();
            }
        } catch (Exception e) {
            log.info("",e);
            StackTraceElement[] traces = e.getStackTrace();
            for (StackTraceElement trace : traces) {
                log.debug(trace.toString());
            }
        }
        return id;
    }

    @Override
    @UserDataIsolation(tableAlias = "dt")
    public List<DeviceTerminal> index(QueryVo queryVo) {
        List<DeviceTerminal> deviceTerminals = deviceTerminalMapper.index(queryVo);
        return deviceTerminals;
    }

    @Override
    @UserDataIsolation(tableAlias = "dt")
    public List<Map<String, Object>> enumerate(QueryVo queryVo) {
        return deviceTerminalMapper.enumerate(queryVo);
    }

    @Override
    @UserDataIsolation(tableAlias = "dt")
    public DeviceTerminal retrieve(QueryVo queryVo) {
        DeviceTerminal deviceTerminal = deviceTerminalMapper.retrieve(queryVo);
        return deviceTerminal;
    }

    @Override
    @UserDataIsolation(tableAlias = "tbl_device_terminal")
    public Boolean update(DeviceTerminal deviceTerminal) {
        Boolean done = false;
        try {
            Integer affected = deviceTerminalMapper.update(deviceTerminal);
            if (affected > 0) {
                done = true;
            }
        } catch (Exception e) {
            log.info("",e);
            StackTraceElement[] traces = e.getStackTrace();
            for (StackTraceElement trace : traces) {
                log.debug(trace.toString());
            }
        }
        return done;
    }

    @Override
    @UserDataIsolation(tableAlias = "tbl_device_terminal")
    public Boolean delete(QueryVo queryVo) {
        Boolean done = false;
        try {
            Integer affected = deviceTerminalMapper.delete(queryVo);
            if (affected > 0) {
                done = true;
            }
        } catch (Exception e) {
            log.info("",e);
            StackTraceElement[] traces = e.getStackTrace();
            for (StackTraceElement trace : traces) {
                log.debug(trace.toString());
            }
        }
        return done;
    }
}
