package com.ruoyi.system.service;

import com.ruoyi.system.domain.SmsTemplate;

import java.util.List;

public interface ISmsTemplateService {

    /**
     * 查询模板配置列表
     *
     * @param smsTemplate
     * @return
     */
    List<SmsTemplate> queryList(SmsTemplate smsTemplate);

    /**
     * 查询单个模板
     *
     * @param smsTemplate
     * @return
     */
    SmsTemplate getOne(SmsTemplate smsTemplate);

    /**
     * 添加模板
     *
     * @param smsTemplate
     * @return
     */
    Boolean addMessageTemplate(SmsTemplate smsTemplate);

    /**
     * 修改模板
     *
     * @param smsTemplate
     * @return
     */
    Boolean updateMessageTemplate(SmsTemplate smsTemplate);

    /**
     * 删除模板
     *
     * @param smsTemplate
     * @return
     */
    Boolean deleteMessageTemplate(SmsTemplate smsTemplate);

    /**
     * 验证模板code是否存在
     *
     * @param smsTemplate
     */
    void checkTemplateCodeExist(SmsTemplate smsTemplate);
}
