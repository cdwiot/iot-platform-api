package com.ruoyi.system.service;

import com.ruoyi.system.domain.SmsAccount;

import java.util.List;

public interface ISmsAccountService {

    /**
     * 查询配置列表
     *
     * @param smsAccount
     * @return
     */
    List<SmsAccount> queryList(SmsAccount smsAccount);

    /**
     * 查询单个配置(总账号)
     *
     * @param smsAccount
     * @return
     */
    SmsAccount getOne(SmsAccount smsAccount);

    /**
     * 根据数据权限获取账户信息
     *
     * @param smsAccount
     * @return
     */
    SmsAccount getMessageAccount(SmsAccount smsAccount);

    /**
     * 默认查询大账户信息
     *
     * @param smsAccount
     * @return
     */
    SmsAccount getCompanyOne(SmsAccount smsAccount);

    /**
     * 添加配置
     *
     * @param smsAccount
     * @return
     */
    Boolean addMessageAccount(SmsAccount smsAccount);

    /**
     * 修改配置
     *
     * @param smsAccount
     * @return
     */
    Boolean updateMessageAccount(SmsAccount smsAccount);

    /**
     * 删除配置
     *
     * @param smsAccount
     * @return
     */
    Boolean deleteMessageAccount(SmsAccount smsAccount);


}
