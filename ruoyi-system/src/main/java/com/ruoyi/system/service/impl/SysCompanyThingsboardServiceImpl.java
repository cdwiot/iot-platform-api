package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.SysCompanyThingsboardMapper;
import com.ruoyi.system.mapper.SysDeptMapper;
import com.ruoyi.system.mapper.SysUserMapper;
import com.ruoyi.common.core.domain.entity.SysDept;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.system.domain.SysCompanyThingsboard;
import com.ruoyi.system.service.ISysCompanyThingsboardService;

/**
 * 公司与thingsboard关联Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-08-02
 */
@Service
public class SysCompanyThingsboardServiceImpl implements ISysCompanyThingsboardService 
{
    @Autowired
    private SysCompanyThingsboardMapper sysCompanyThingsboardMapper;
    
    @Autowired
    private SysUserMapper sysUserMapper;
    
    @Autowired
    private SysDeptMapper sysDeptMapper;

    /**
     * 查询公司与thingsboard关联
     * 
     * @param id 公司与thingsboard关联ID
     * @return 公司与thingsboard关联
     */
    @Override
    public SysCompanyThingsboard selectSysCompanyThingsboardById(Long id)
    {
        return sysCompanyThingsboardMapper.selectSysCompanyThingsboardById(id);
    }

    /**
     * 查询公司与thingsboard关联列表
     * 
     * @param sysCompanyThingsboard 公司与thingsboard关联
     * @return 公司与thingsboard关联
     */
    @Override
    public List<SysCompanyThingsboard> selectSysCompanyThingsboardList(SysCompanyThingsboard sysCompanyThingsboard)
    {
        return sysCompanyThingsboardMapper.selectSysCompanyThingsboardList(sysCompanyThingsboard);
    }

    /**
     * 新增公司与thingsboard关联
     * 
     * @param sysCompanyThingsboard 公司与thingsboard关联
     * @return 结果
     */
    @Override
    public int insertSysCompanyThingsboard(SysCompanyThingsboard sysCompanyThingsboard)
    {
        return sysCompanyThingsboardMapper.insertSysCompanyThingsboard(sysCompanyThingsboard);
    }

    /**
     * 修改公司与thingsboard关联
     * 
     * @param sysCompanyThingsboard 公司与thingsboard关联
     * @return 结果
     */
    @Override
    public int updateSysCompanyThingsboard(SysCompanyThingsboard sysCompanyThingsboard)
    {
        return sysCompanyThingsboardMapper.updateSysCompanyThingsboard(sysCompanyThingsboard);
    }

    /**
     * 批量删除公司与thingsboard关联
     * 
     * @param ids 需要删除的公司与thingsboard关联ID
     * @return 结果
     */
    @Override
    public int deleteSysCompanyThingsboardByIds(Long[] ids)
    {
        return sysCompanyThingsboardMapper.deleteSysCompanyThingsboardByIds(ids);
    }

    /**
     * 删除公司与thingsboard关联信息
     * 
     * @param id 公司与thingsboard关联ID
     * @return 结果
     */
    @Override
    public int deleteSysCompanyThingsboardById(Long id)
    {
        return sysCompanyThingsboardMapper.deleteSysCompanyThingsboardById(id);
    }

    /**
     * 根据用户id获取用户所属公司绑定的thingsboard信息
     * @param userId
     * @return 用户所属公司绑定的thingsboard信息
     */
    @Override
    public SysCompanyThingsboard selectSysCompanyThingsboardByUserId(Long userId) {
        SysUser user = sysUserMapper.selectUserById(userId);
        return selectSysCompanyThingsboardByUser(user);
    }

    /**
     * 根据用户获取用户所属公司绑定的thingsboard信息
     * @param user
     * @return 用户所属公司绑定的thingsboard信息
     */
    @Override
    public SysCompanyThingsboard selectSysCompanyThingsboardByUser(SysUser user) {
        SysDept userDept = sysDeptMapper.selectDeptById(user.getDeptId());
        long companyId = userDept.getParentId()==0L ? 
                userDept.getDeptId().longValue():
                Long.parseLong(userDept.getAncestors().split(",")[1]);
        SysCompanyThingsboard companyThingsboard = new SysCompanyThingsboard();
        companyThingsboard.setCompanyId(companyId);
        List<SysCompanyThingsboard> list = sysCompanyThingsboardMapper.selectSysCompanyThingsboardList(companyThingsboard);
        if(list!=null && list.size()>0) {
            return list.get(0);
        } else {
            return null;
        }
    }

    @Override
    public SysCompanyThingsboard selectSysCompanyThingsboardByCompanyId(Long companyId) {
        SysCompanyThingsboard companyThingsboard = new SysCompanyThingsboard();
        companyThingsboard.setCompanyId(companyId);
        List<SysCompanyThingsboard> list = sysCompanyThingsboardMapper.selectSysCompanyThingsboardList(companyThingsboard);
        if(list!=null && list.size()>0) {
            return list.get(0);
        } else {
            return null;
        }
    }
}
