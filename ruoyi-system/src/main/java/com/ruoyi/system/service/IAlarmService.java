package com.ruoyi.system.service;

import java.util.List;
import java.util.Map;

import com.ruoyi.system.domain.Alarm;
import com.ruoyi.system.domain.vo.QueryVo;

public interface IAlarmService {
    public List<Alarm> index(QueryVo queryVo);
    public List<Map<String, Object>> summary(QueryVo queryVo, Map<String, Object> options);
    public Alarm retrieve(QueryVo queryVo);
    public Boolean update(Alarm alarm);
}
