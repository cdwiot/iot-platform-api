package com.ruoyi.system.service;

import com.alibaba.fastjson.JSONObject;
import com.ruoyi.system.domain.RpcMsgLog;
import com.ruoyi.system.domain.SysCompanyThingsboard;
import com.ruoyi.system.domain.vo.QueryVo;
import org.thingsboard.server.common.data.Device;

public interface IThingsBoardTenantService {
    public Device saveDefaultDevice(String name, SysCompanyThingsboard sysCompanyThingsboard);

    public Device saveMqttDevice(String name, SysCompanyThingsboard sysCompanyThingsboard);

    public Device saveHttpDevice(String name, SysCompanyThingsboard sysCompanyThingsboard);

    public Device saveCoapDevice(String name, SysCompanyThingsboard sysCompanyThingsboard);

    public Device saveLwm2mDevice(String name, SysCompanyThingsboard sysCompanyThingsboard);

    public void deleteDevice(String id, SysCompanyThingsboard sysCompanyThingsboard);

    public Boolean saveMqttDeviceCredentials(JSONObject fields, SysCompanyThingsboard sysCompanyThingsboard);

    public Boolean saveHttpDeviceCredentials(JSONObject fields, SysCompanyThingsboard sysCompanyThingsboard);

    public Boolean saveCoapDeviceCredentials(JSONObject fields, SysCompanyThingsboard sysCompanyThingsboard);

    public Boolean saveLwm2mDeviceCredentials(JSONObject fields, SysCompanyThingsboard sysCompanyThingsboard);

    public JSONObject getConnectivity(String tbDeviceId, SysCompanyThingsboard sysCompanyThingsboard);

    public JSONObject getLatestTimeSeries(String tbDeviceId, SysCompanyThingsboard sysCompanyThingsboard);

    public JSONObject getTimeSeriesMeta(String tbDeviceId, QueryVo queryVo, SysCompanyThingsboard sysCompanyThingsboard);

    public JSONObject getHistoricalTimeSeries(String tbDeviceId, QueryVo queryVo, SysCompanyThingsboard sysCompanyThingsboard);

    public JSONObject addTelemetryDelta(String tbDeviceId, JSONObject deltas, SysCompanyThingsboard sysCompanyThingsboard);

    public String importRuleChainEx(String name, String content, SysCompanyThingsboard sysCompanyThingsboard);

    public String importDeviceProfileEx(String name, String content, String ruleChainId, SysCompanyThingsboard sysCompanyThingsboard);

    public String getWsUrl(SysCompanyThingsboard sysCompanyThingsboard);

    public String sendRpcRequest(String deviceId, JSONObject requestBody, SysCompanyThingsboard sysCompanyThingsboard);

    void exeRpcRequest(RpcMsgLog rpcMsgLog);
}
