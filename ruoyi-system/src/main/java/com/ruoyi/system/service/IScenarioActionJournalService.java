package com.ruoyi.system.service;

import java.util.List;

import com.ruoyi.system.domain.ScenarioActionJournal;
import com.ruoyi.system.domain.vo.QueryVo;

public interface IScenarioActionJournalService {
    public List<ScenarioActionJournal> index(QueryVo queryVo);
}
