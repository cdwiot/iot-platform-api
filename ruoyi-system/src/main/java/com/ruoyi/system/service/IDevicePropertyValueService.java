package com.ruoyi.system.service;

import com.ruoyi.system.domain.DevicePropertyValue;
import com.ruoyi.system.domain.vo.DevicePropertyValueResultVO;
import com.ruoyi.system.domain.vo.DevicePropertyValueVO;

import java.util.List;

public interface IDevicePropertyValueService {

    /**
     * 根据产品Id查询产品额外属性值
     *
     * @param devicePropertyValue
     * @return
     */
    List<DevicePropertyValueResultVO> queryDeviceValueByDeviceId(DevicePropertyValue devicePropertyValue);

    /**
     * 产品属性的添加或修改
     *
     * @param devicePropertyValues
     * @param userName
     * @param companyId
     * @return
     */
    Boolean saveOrUpdate(List<DevicePropertyValue> devicePropertyValues, String userName, Long companyId);

    /**
     * 产品属性值的删除
     *
     * @param devicePropertyValue
     * @return
     */
    Boolean deleteDevicePropertyValue(DevicePropertyValue devicePropertyValue);
}
