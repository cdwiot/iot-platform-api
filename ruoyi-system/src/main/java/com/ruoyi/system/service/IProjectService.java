package com.ruoyi.system.service;

import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.ruoyi.system.domain.Project;
import com.ruoyi.system.domain.vo.QueryVo;

public interface IProjectService {
    public Map<String, Object> parseInjectionItem(JSONObject item, Long companyId);

    public Map<String, Object> findExistance(Project project);

    /**
     * 项目信息的添加
     *
     * @param project
     * @return
     */
    public Integer create(Project project);

    /**
     * 根据条件获取项目列表信息
     *
     * @param queryVo
     * @return
     */
    public List<Project> index(QueryVo queryVo);

    /**
     * 获取项目详情信息
     *
     * @param queryVo
     * @return
     */
    public Project retrieve(QueryVo queryVo);

    /**
     * 项目信息的修改
     *
     * @param project
     * @return
     */
    public Boolean update(Project project);

    public List<Map<String, Object>> summary(QueryVo queryVo, Map<String, Object> options);

    public List<Map<String, Object>> deviceCount(QueryVo queryVo, Map<String, Object> options);

    public List<Map<String, Object>> configuredTerminalCount(QueryVo queryVo, Map<String, Object> options);

    public List<Map<String, Object>> valueByMonth(QueryVo queryVo, Map<String, Object> options);

    public List<Map<String, Object>> statusByMonth(QueryVo queryVo, Map<String, Object> options);

    /**
     * 验证当前项目是否是客户公司创建
     *
     * @param queryVo
     * @return true 客户公司创建 false 非客户公司创建
     */
    boolean checkProjectCreateByIsCustomer(QueryVo queryVo);
}
