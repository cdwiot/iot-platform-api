package com.ruoyi.system.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import cn.hutool.core.util.StrUtil;
import com.ruoyi.system.domain.ProductPointCheckConfig;
import com.ruoyi.system.domain.vo.QueryVo;

import com.ruoyi.system.service.IProductPointCheckConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import com.ruoyi.system.mapper.ProductMapper;
import com.ruoyi.common.annotation.UserDataIsolation;
import com.ruoyi.common.utils.DataUtils;
import com.ruoyi.system.domain.Product;
import com.ruoyi.system.domain.ProductTerminal;
import com.ruoyi.system.service.IProductService;
import com.ruoyi.system.service.IProductTerminalService;

import lombok.extern.slf4j.Slf4j;

@SuppressWarnings("AlibabaLowerCamelCaseVariableNaming")
@Slf4j
@Service
public class ProductServiceImpl implements IProductService
{
    @Autowired
    private ProductMapper productMapper;

    @Autowired
    private IProductTerminalService productTerminalService;
    @Autowired
    private IProductPointCheckConfigService productPointCheckConfigService;

    @Override
    @UserDataIsolation(tableAlias = "tbl_product")
    public Map<String, Object> findExistance(Product product) {
        return productMapper.findExistance(product);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Integer create(Product product) {
        Integer id = null;
        try {
            Integer affected = productMapper.create(product);
            if (affected > 0) {
                id = product.getId();
                Long companyId = product.getCompanyId();
                List<ProductTerminal> productTerminals = product.getTerminals();
                for (ProductTerminal productTerminal : productTerminals) {
                    productTerminal.setProductId(id);
                    productTerminal.setCompanyId(companyId);
                    Integer tid = productTerminalService.create(productTerminal);
                    if (tid == null) {
                        throw new Exception("rollback");
                    }
                }
//                //初始化点检表单管理
//                initPointCheckConfig(product);
            }
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            log.info("",e);
            StackTraceElement[] traces = e.getStackTrace();
            for (StackTraceElement trace : traces) {
                log.debug(trace.toString());
            }
            id = null;
        }
        return id;
    }

    @Override
    public List<Product> index(QueryVo queryVo) {
        List<Product> products = productMapper.index(queryVo);
        for (Product product : products) {
            QueryVo subQueryVo = new QueryVo();
            subQueryVo.filters.put("product_id", product.getId());
            subQueryVo.filters.put("company_id", product.getCompanyId());
            List<ProductTerminal> terminals = productTerminalService.index(subQueryVo);
            product.setTerminals(terminals);
        }
        return products;
    }

    @Override
    @UserDataIsolation(tableAlias = "tbl_product")
    public List<Map<String, Object>> enumerate(QueryVo queryVo) {
        return productMapper.enumerate(queryVo);
    }

    @Override
    @UserDataIsolation(tableAlias = "tbl_product")
    public Product retrieve(QueryVo queryVo) {
        Product product = productMapper.retrieve(queryVo);
        if (product != null) {
            QueryVo subQueryVo = new QueryVo();
            subQueryVo.filters.put("product_id", product.getId());
            subQueryVo.filters.put("company_id", product.getCompanyId());
            List<ProductTerminal> terminals = productTerminalService.index(subQueryVo);
            product.setTerminals(terminals);
        }
        return product;
    }

    @Override
    @UserDataIsolation(tableAlias = "tbl_product")
    public Product lookup(QueryVo queryVo) {
        Product product = productMapper.lookup(queryVo);
        if (product != null) {
            QueryVo subQueryVo = new QueryVo();
            subQueryVo.filters.put("product_id", product.getId());
            subQueryVo.filters.put("company_id", product.getCompanyId());
            List<ProductTerminal> terminals = productTerminalService.index(subQueryVo);
            product.setTerminals(terminals);
        }
        return product;
    }

    @Override
    @UserDataIsolation(tableAlias = "tbl_product")
    public Boolean update(Product product) {
        Boolean done = false;
        try {
            Integer affected = productMapper.update(product);
            if (affected > 0) {
                done = true;
            }
        } catch (Exception e) {
            log.info("",e);
            StackTraceElement[] traces = e.getStackTrace();
            for (StackTraceElement trace : traces) {
                log.debug(trace.toString());
            }
        }
        return done;
    }

    @Override
    @UserDataIsolation(tableAlias = "tbl_product")
    @Transactional(rollbackFor = Exception.class)
    public Boolean delete(QueryVo queryVo) {
        Boolean done = false;
        try {
            QueryVo subQueryVo = new QueryVo();
            subQueryVo.filters.put("product_id", queryVo.filters.get("id"));
            subQueryVo.filters.put("company_id", queryVo.filters.get("company_id"));
            productTerminalService.delete(subQueryVo);
            Integer affected = productMapper.delete(queryVo);
            if (affected > 0) {
                done = true;
            }
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            log.info("",e);
            StackTraceElement[] traces = e.getStackTrace();
            for (StackTraceElement trace : traces) {
                log.debug(trace.toString());
            }
        }
        return done;
    }

    @Override
    @UserDataIsolation(tableAlias = "tbl_product")
    public List<Product> projectIndex(QueryVo queryVo) {
        List<Product> products = productMapper.projectIndex(queryVo);
        return products;
    }

    @Override
    public List<Map<String, Object>> summary(QueryVo queryVo, Map<String, Object> options) {
        List<Map<String, Object>> summary = null;
        try {
            Map<String, Object> summaryKV = productMapper.summary(queryVo);
            summary = DataUtils.KVtoList(summaryKV, options);
        } catch (Exception e) {
            log.info("",e);
            StackTraceElement[] traces = e.getStackTrace();
            for (StackTraceElement trace : traces) {
                log.debug(trace.toString());
            }
        }
        return summary;
    }


}
