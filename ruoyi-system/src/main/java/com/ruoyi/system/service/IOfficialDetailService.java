package com.ruoyi.system.service;

import com.ruoyi.system.domain.EmailDetail;
import com.ruoyi.system.domain.OfficialDetail;

import java.util.List;

public interface IOfficialDetailService {

    /**
     * 添加
     *
     * @param officialDetail
     * @return
     */
    Boolean add(OfficialDetail officialDetail);

    /**
     * 列表查询
     *
     * @param officialDetail
     * @return
     */
    List<OfficialDetail> list(OfficialDetail officialDetail);

    /**
     * 详情
     *
     * @param officialDetail
     * @return
     */
    OfficialDetail getOne(OfficialDetail officialDetail);
}
