package com.ruoyi.system.service;

import com.ruoyi.system.domain.VoiceDetail;

import java.util.List;

public interface IVoiceDetailService {
    Boolean create(VoiceDetail voiceDetail);
    List<VoiceDetail> index(VoiceDetail voiceDetail);
    VoiceDetail retrieve(VoiceDetail voiceDetail);
}
