package com.ruoyi.system.service;

import com.ruoyi.system.domain.EmailDetail;

import java.util.List;

public interface IEmailDetailService {

    /**
     * 添加
     *
     * @param emailDetail
     * @return
     */
    Boolean add(EmailDetail emailDetail);

    /**
     * 列表查询
     *
     * @param emailDetail
     * @return
     */
    List<EmailDetail> list(EmailDetail emailDetail);

    /**
     * 详情
     *
     * @param emailDetail
     * @return
     */
    EmailDetail getOne(EmailDetail emailDetail);
}
