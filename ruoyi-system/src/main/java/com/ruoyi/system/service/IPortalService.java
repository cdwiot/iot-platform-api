package com.ruoyi.system.service;

import com.ruoyi.system.domain.Portal;
import com.ruoyi.system.domain.vo.QueryVo;

public interface IPortalService {
    /**
     * 根据条件获取门户配置信息
     *
     * @param queryVo 查询条件
     * @return
     */
    public Portal load(QueryVo queryVo);

    /**
     * 保存门户配置信息
     *
     * @param portal 配置信息
     * @return
     */
    public Boolean save(Portal portal);
}
