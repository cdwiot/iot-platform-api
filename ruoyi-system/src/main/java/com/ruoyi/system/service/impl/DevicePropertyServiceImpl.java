package com.ruoyi.system.service.impl;

import com.ruoyi.common.annotation.UserDataIsolation;
import com.ruoyi.common.exception.CustomException;
import com.ruoyi.system.domain.DeviceProperty;
import com.ruoyi.system.domain.DevicePropertyValue;
import com.ruoyi.system.mapper.DevicePropertyMapper;
import com.ruoyi.system.service.IDevicePropertyService;
import com.ruoyi.system.service.IDevicePropertyValueService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.List;

@Service
@Slf4j
public class DevicePropertyServiceImpl implements IDevicePropertyService {
    @Autowired
    private DevicePropertyMapper devicePropertyMapper;
    @Autowired
    private IDevicePropertyValueService devicePropertyValueService;

    @Override
    @UserDataIsolation(tableAlias = "t1")
    public List<DeviceProperty> queryPageList(DeviceProperty deviceProperty) {
        return devicePropertyMapper.queryPageList(deviceProperty);
    }

    @Override
    public Integer addDeviceProperty(DeviceProperty deviceProperty) {
        //校验参数
        return devicePropertyMapper.addDeviceProperty(deviceProperty);
    }

    @Override
    @UserDataIsolation
    public DeviceProperty queryById(DeviceProperty deviceProperty) {
        DeviceProperty result = devicePropertyMapper.queryById(deviceProperty);
        return result == null ? new DeviceProperty() : result;
    }

    @Override
    @UserDataIsolation
    public Integer updateDeviceProperty(DeviceProperty deviceProperty) {
        //校验参数
        if (deviceProperty.getId() == null){
            throw new CustomException("产品额外属性id不能为空");
        }
        return devicePropertyMapper.updateDeviceProperty(deviceProperty);
    }

    @Override
    @UserDataIsolation
    public void deleteDeviceProperty(DeviceProperty deviceProperty) {
        devicePropertyMapper.deleteDeviceProperty(deviceProperty);
    }

    @Override
    @UserDataIsolation
    public void checkParam(DeviceProperty deviceProperty) {
        DeviceProperty property = devicePropertyMapper.checkPropertyName(deviceProperty);
        if (property == null){
            return;
        }
        if (deviceProperty.getId() == null || property.getId().compareTo(deviceProperty.getId()) != 0){
            throw new CustomException("该属性名称已经存在");
        }
    }
}
