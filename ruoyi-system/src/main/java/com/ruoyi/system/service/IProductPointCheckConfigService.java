package com.ruoyi.system.service;

import com.ruoyi.system.domain.ProductPointCheckConfig;
import com.ruoyi.system.domain.vo.ProductPointCheckConfigVO;
import com.ruoyi.system.domain.vo.QueryVo;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface IProductPointCheckConfigService {
//
//    /**
//     * 产品型号点检配置分页查询
//     *
//     * @param queryVo
//     * @return
//     */
//    List<ProductPointCheckConfig> queryPageList(QueryVo queryVo);
//
//
//    /**
//     * 获取产品型号点检配置详情
//     *
//     * @param queryVo
//     * @return
//     */
//    ProductPointCheckConfig getOne(QueryVo queryVo);
//
//    /**
//     * 添加
//     *
//     * @param productPointCheckConfig
//     * @return
//     */
//    Boolean add(ProductPointCheckConfig productPointCheckConfig);
//
//    /**
//     * 修改
//     *
//     * @param productPointCheckConfig
//     * @return
//     */
//    Boolean update(ProductPointCheckConfig productPointCheckConfig);
//
//    /**
//     * 删除
//     *
//     * @param queryVo
//     * @return
//     */
//    Boolean delete(QueryVo queryVo);
//
//    /**
//     * 批量删除
//     *
//     * @param ids
//     * @return
//     */
//    Boolean deleteBatch(List<String> ids);
//
//    /**
//     * 检查类别枚举
//     *
//     * @return
//     */
//    Set<String> typeEnum(QueryVo queryVo);
//
//    /**
//     * 查询所有的点检配置
//     * @param queryVo
//     * @return
//     */
//    List<Map<String, Object>> queryConfig(QueryVo queryVo);
//
//    /**
//     * 验证检查项是否重复
//     *
//     * @param productPointCheckConfig
//     * @return
//     */
//    void checkIsExist(ProductPointCheckConfig productPointCheckConfig);
//
//    /**
//     * 根据产品代码查询产品型号点检表单配置
//     *
//     * @param queryVo
//     * @return
//     */
//    List<ProductPointCheckConfig> queryPointCheckConfig(QueryVo queryVo);
}
