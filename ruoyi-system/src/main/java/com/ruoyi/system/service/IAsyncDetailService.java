package com.ruoyi.system.service;

import java.util.Map;

import com.ruoyi.system.domain.AsyncDetail;
import com.ruoyi.system.domain.vo.QueryVo;

public interface IAsyncDetailService {

    /**
     * 创建tid
     *
     * @param business  业务功能名称
     * @param companyId 所属公司Id
     * @return
     */
    String createTid(String business, Long companyId);

    /**
     * 修改
     *
     * @param tid 异步Id
     * @param status 成功状态（0进行中1成功2失败）
     * @param data   异常数据
     * @return
     */
    Boolean update(String tid, Integer status, String data);

    AsyncDetail retrieve(QueryVo queryVo);
}
