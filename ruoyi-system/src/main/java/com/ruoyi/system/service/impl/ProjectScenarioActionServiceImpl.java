package com.ruoyi.system.service.impl;

import java.util.List;
import java.util.Map;

import com.ruoyi.common.annotation.UserDataIsolation;
import com.ruoyi.common.enums.ActionType;
import com.ruoyi.system.domain.ProjectScenarioAction;
import com.ruoyi.system.domain.vo.QueryVo;
import com.ruoyi.system.mapper.ProjectScenarioActionMapper;
import com.ruoyi.system.service.IProjectScenarioActionService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ch.qos.logback.core.joran.action.Action;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ProjectScenarioActionServiceImpl implements IProjectScenarioActionService
{
    @Autowired
    ProjectScenarioActionMapper projectScenarioActionMapper;

    @Override
    @UserDataIsolation(tableAlias = "tbl_project_scenario_action")
    public Map<String, Object> findExistance(ProjectScenarioAction projectScenarioAction) {
        return projectScenarioActionMapper.findExistance(projectScenarioAction);
    }

    @Override
    public Integer create(ProjectScenarioAction projectScenarioAction) {
        Integer id = null;
        try {
            Integer affected = projectScenarioActionMapper.create(projectScenarioAction);
            if (affected > 0) {
                id = projectScenarioAction.getId();
            }
        } catch (Exception e) {
            log.info("",e);
            StackTraceElement[] traces = e.getStackTrace();
            for (StackTraceElement trace : traces) {
                log.debug(trace.toString());
            }
        }
        return id;
    }

    @Override
    @UserDataIsolation(tableAlias = "psr")
    public List<ProjectScenarioAction> index(QueryVo queryVo) {
        List<ProjectScenarioAction> projectScenarioActions = projectScenarioActionMapper.index(queryVo);
        for (ProjectScenarioAction projectScenarioAction : projectScenarioActions) {
            projectScenarioAction.setTypeString(ActionType.valueOfCode(projectScenarioAction.getType()).getDescription());
        }
        return projectScenarioActions;
    }

    @Override
    @UserDataIsolation(tableAlias = "tbl_project_scenario_action")
    public ProjectScenarioAction retrieve(QueryVo queryVo) {
        ProjectScenarioAction projectScenarioAction = projectScenarioActionMapper.retrieve(queryVo);
        if (projectScenarioAction != null) {
            projectScenarioAction.setTypeString(ActionType.valueOfCode(projectScenarioAction.getType()).getDescription());
        }
        return projectScenarioAction;
    }

    @Override
    @UserDataIsolation(tableAlias = "tbl_project_scenario_action")
    public Boolean update(ProjectScenarioAction projectScenarioAction) {
        Boolean done = false;
        try {
            Integer affected = projectScenarioActionMapper.update(projectScenarioAction);
            if (affected > 0) {
                done = true;
            }
        } catch (Exception e) {
            log.info("",e);
            StackTraceElement[] traces = e.getStackTrace();
            for (StackTraceElement trace : traces) {
                log.debug(trace.toString());
            }
        }
        return done;
    }

    @Override
    @UserDataIsolation(tableAlias = "tbl_project_scenario_action")
    public Boolean delete(QueryVo queryVo) {
        Boolean done = false;
        try {
            Integer affected = projectScenarioActionMapper.delete(queryVo);
            if (affected > 0) {
                done = true;
            }
        } catch (Exception e) {
            log.info("",e);
            StackTraceElement[] traces = e.getStackTrace();
            for (StackTraceElement trace : traces) {
                log.debug(trace.toString());
            }
        }
        return done;
    }
}
