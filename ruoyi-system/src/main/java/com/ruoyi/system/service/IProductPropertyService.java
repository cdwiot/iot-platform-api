package com.ruoyi.system.service;

import com.ruoyi.system.domain.ProductProperty;

import java.util.List;

public interface IProductPropertyService {

    /**
     * 产品额外属性的分页查询
     *
     * @param productProperty
     * @return
     */
    List<ProductProperty> queryPageList(ProductProperty productProperty);

    /**
     * 产品额外属性的添加
     *
     * @param productProperty
     * @return
     */
    Integer addProductProperty(ProductProperty productProperty);

    /**
     * 产品额外属性的详情
     *
     * @param productProperty
     * @return
     */
    ProductProperty queryById(ProductProperty productProperty);

    /**
     * 产品额外属性的修改
     *
     * @param productProperty
     * @return
     */
    Integer updateProductProperty(ProductProperty productProperty);

    /**
     * 产品额外属性的删除
     *
     * @param productProperty
     */
    void deleteProductProperty(ProductProperty productProperty);


    /**
     * 验证属性名称是否重复
     *
     * @param productProperty
     */
    void checkParam(ProductProperty productProperty);
}
