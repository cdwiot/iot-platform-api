package com.ruoyi.system.service.impl;

import com.ruoyi.common.annotation.UserDataIsolation;
import com.ruoyi.system.domain.PointCheckConfig;
import com.ruoyi.system.domain.vo.QueryVo;
import com.ruoyi.system.mapper.PointCheckConfigMapper;
import com.ruoyi.system.mapper.PointCheckDateMapper;
import com.ruoyi.system.service.IPointCheckConfigService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class PointCheckConfigServiceImpl implements IPointCheckConfigService {
    @Autowired
    private PointCheckConfigMapper pointCheckConfigMapper;

    @Override
    public List<PointCheckConfig> queryList(PointCheckConfig pointCheckConfig) {
        return pointCheckConfigMapper.queryList(pointCheckConfig);
    }

    @Override
    public Boolean add(PointCheckConfig pointCheckConfig) {
        return pointCheckConfigMapper.add(pointCheckConfig) > 0;
    }



    @Override
    public Boolean deletePointId(Integer pointId) {
        return pointCheckConfigMapper.deletePointId(pointId) > 0;
    }

    @Override
    public Boolean deletePointIdDeviceId(QueryVo queryVo) {
        return pointCheckConfigMapper.deletePointIdDeviceId(queryVo) > 0;
    }
}
