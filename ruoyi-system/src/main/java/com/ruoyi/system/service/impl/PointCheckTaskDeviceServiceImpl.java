package com.ruoyi.system.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.ruoyi.common.annotation.UserDataIsolation;
import com.ruoyi.common.core.domain.entity.SysDept;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.text.Convert;
import com.ruoyi.common.exception.CustomException;
import com.ruoyi.system.domain.*;
import com.ruoyi.system.domain.vo.*;
import com.ruoyi.system.mapper.PointCheckTaskDeviceMapper;
import com.ruoyi.system.service.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.hssf.record.DVALRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collectors;

@SuppressWarnings("AlibabaLowerCamelCaseVariableNaming")
@Slf4j
@Service
public class PointCheckTaskDeviceServiceImpl implements IPointCheckTaskDeviceService {

    @Autowired
    private PointCheckTaskDeviceMapper pointCheckTaskDeviceMapper;
    @Autowired
    private IPointCheckDeviceDetailService pointCheckDeviceDetailService;
    @Autowired
    private IPointCheckDescriptionService pointCheckDescriptionService;
    @Autowired
    private IPointCheckTaskDealService pointCheckTaskDealService;
    @Autowired
    private IPointCheckTaskService pointCheckTaskService;
    @Autowired
    private IDevicePointCheckConfigService devicePointCheckConfigService;
    @Autowired
    private ISysDeptService sysDeptService;
    @Autowired
    private IDeviceCustomFieldService deviceCustomFieldService;

    @Override
    public List<PointCheckTaskDevice> getSwitchDevice(QueryVo queryVo,Integer type) {
        Integer deviceId = queryVo.filters.getInteger("deviceId");
        Date startTime = queryVo.filters.getDate("startTime");
        Long companyId = queryVo.filters.getLong("companyId");
        Timestamp beginOfDay = new Timestamp(startTime.getTime());
        Timestamp endOfDay =  new Timestamp(startTime.getTime());
        PointCheckTaskDevice pointCheckTaskDevice = new PointCheckTaskDevice();
        if(type==0){
            pointCheckTaskDevice.setStartTimeEnd(endOfDay);

        }else{
            pointCheckTaskDevice.setStartTimeBegin(beginOfDay);
        }


        pointCheckTaskDevice.setCompanyId(companyId);
        pointCheckTaskDevice.setDeviceId(deviceId);
        List<PointCheckTaskDevice> pointCheckTaskDevices = pointCheckTaskDeviceMapper.queryDeviceList(pointCheckTaskDevice);


        return pointCheckTaskDevices;
    }

    @Override
    public List<PointCheckTaskDevice> queryList(PointCheckTaskDevice pointCheckTaskDevice) {
        List<PointCheckTaskDevice> taskDevices = pointCheckTaskDeviceMapper.queryList(pointCheckTaskDevice);
        if (CollUtil.isEmpty(taskDevices)) {
            return new ArrayList<>();
        }
        for (PointCheckTaskDevice taskDevice : taskDevices) {
            if (StrUtil.isBlank(taskDevice.getPhoto())) {
                taskDevice.setPhoto(taskDevice.getProductPhoto());
            }
            QueryVo queryVo = new QueryVo();
            queryVo.filters.put("task_device_id", taskDevice.getId());
            List<Map<String, Object>> allErrorItem = pointCheckTaskDeviceMapper.queryAllErrorItem(queryVo);
            if (CollUtil.isEmpty(allErrorItem)) {
                taskDevice.setDeal(true);
                continue;
            }
            boolean flag = false;
            for (Map<String, Object> map : allErrorItem) {
                Integer taskId = (Integer) map.get("taskId");
                Integer id = (Integer) map.get("id");
                String type = (String) map.get("type");
                String item = (String) map.get("item");
                String detail = (String) map.get("detail");
                //TODO 处理特殊情况下数据（波纹管-腐蚀、异常变形）
//                if (StrUtil.equals("波纹管", type) && (StrUtil.equals("腐蚀", item) || StrUtil.equals("异常变形", item))) {
//                    if (StrUtil.isNotBlank(detail)) {
//                        JSONObject jsonObject = JSONUtil.parseObj(detail);
//                        Boolean handle = jsonObject.getBool("handle", true);
//                        if (!handle) {
//                            continue;
//                        }
//                    }
//                }
                if(( (Integer)  map.get("category"))==1){
                    PointCheckTaskDeal deal = new PointCheckTaskDeal();
                    deal.setDetailId(id);
                    List<PointCheckTaskDeal> pointCheckTaskDealList = pointCheckTaskDealService.queryList(deal);
                    //异常但是未处理
                    if (CollUtil.isEmpty(pointCheckTaskDealList)) {
                        taskDevice.setDeal(false);
                        flag = true;
                        break;
                    }
                }

            }
            if (!flag) {
                taskDevice.setDeal(true);
            }
        }
        return taskDevices;
    }

    @Override
    @UserDataIsolation(tableAlias = "td")
    public PointCheckTaskDevice getOne(PointCheckTaskDevice pointCheckTaskDevice) {
        return pointCheckTaskDeviceMapper.getOne(pointCheckTaskDevice);
    }



    @Override
    public Boolean add(PointCheckTaskDevice pointCheckTaskDevice) {
        boolean flag = pointCheckTaskDeviceMapper.add(pointCheckTaskDevice) > 0;
        if (flag) {
            //初始化点检字段
            Integer deviceId = pointCheckTaskDevice.getDeviceId();
            QueryVo queryVo = new QueryVo();
            queryVo.filters.put("device_id", deviceId);
            queryVo.filters.put("company_id", pointCheckTaskDevice.getCompanyId());
            List<Map<String,Object>> configList = new ArrayList<>();
            List<DevicePointCheckConfig> devicePointCheckConfigs = devicePointCheckConfigService.queryList(queryVo);
            if (CollUtil.isEmpty(devicePointCheckConfigs)) {
                //没有查询到配置的自定义点检项，采用默认点检项
                configList = initPointCheckConfig();
            }else {
                configList = getPointCheckConfig(devicePointCheckConfigs);
            }
            for (Map<String,Object> map : configList) {
                String type = (String) map.get("type");
                String item = (String) map.get("item");
                Integer category = (Integer) map.get("category");
                Integer orderNum = (Integer) map.get("order_num");
                Integer typeOrder = (Integer) map.get("type_order");
                Integer itemOrder = (Integer) map.get("item_order");
                List<DevicePointCheckConfig> childData = (List<DevicePointCheckConfig>) map.get("childData");
                PointCheckDeviceDetail deviceDetail = new PointCheckDeviceDetail();
                deviceDetail.setTaskDeviceId(pointCheckTaskDevice.getId());
                deviceDetail.setType(type);
                deviceDetail.setItem(item);
                deviceDetail.setOrderNum(orderNum);
                deviceDetail.setTypeOrder(typeOrder);
                deviceDetail.setItemOrder(itemOrder);
                deviceDetail.setCompanyId(pointCheckTaskDevice.getCompanyId());
                deviceDetail.setCreateTime(new Date());
                deviceDetail.setCategory(category);
                pointCheckDeviceDetailService.add(deviceDetail,childData);
            }
        }
        return flag;
    }

    @Override
    public Boolean addV2(PointCheckTaskDevice pointCheckTaskDevice,ArrayList<DevicePointCheckConfig> checkConfigList) {
        boolean flag = pointCheckTaskDeviceMapper.add(pointCheckTaskDevice) > 0;
        if (flag) {
            //初始化点检字段
            Integer deviceId = pointCheckTaskDevice.getDeviceId();
            QueryVo queryVo = new QueryVo();
            queryVo.filters.put("device_id", deviceId);
            queryVo.filters.put("company_id", pointCheckTaskDevice.getCompanyId());
            List<Map<String,Object>> configList = new ArrayList<>();
//            List<DevicePointCheckConfig> devicePointCheckConfigs = devicePointCheckConfigService.queryList(queryVo);

            if (CollUtil.isEmpty(checkConfigList)) {
                //没有查询到配置的自定义点检项，采用默认点检项
                configList = initPointCheckConfig();
            }else {
                configList = getPointCheckConfig(checkConfigList);
            }
            for (Map<String,Object> map : configList) {
                String type = (String) map.get("type");
                String item = (String) map.get("item");
                Integer category = (Integer) map.get("category");
                Integer orderNum = (Integer) map.get("orderNum");
                Integer typeOrder = (Integer) map.get("typeOrder");
                Integer itemOrder = (Integer) map.get("itemOrder");
                List<DevicePointCheckConfig> childData = (List<DevicePointCheckConfig>) map.get("childData");
                PointCheckDeviceDetail deviceDetail = new PointCheckDeviceDetail();
                deviceDetail.setTaskDeviceId(pointCheckTaskDevice.getId());
                deviceDetail.setType(type);
                deviceDetail.setItem(item);
                deviceDetail.setOrderNum(orderNum);
                deviceDetail.setTypeOrder(typeOrder);
                deviceDetail.setItemOrder(itemOrder);
                deviceDetail.setCompanyId(pointCheckTaskDevice.getCompanyId());
                deviceDetail.setCreateTime(new Date());
                deviceDetail.setCategory(category);
                pointCheckDeviceDetailService.add(deviceDetail,childData);
            }
        }
        return flag;
    }

    @Override
    @UserDataIsolation
    public Boolean update(PointCheckTaskDevice pointCheckTaskDevice) {
        return pointCheckTaskDeviceMapper.update(pointCheckTaskDevice) > 0;
    }

    @Override
    @UserDataIsolation
    public Boolean delete(PointCheckTaskDevice pointCheckTaskDevice) {
        return pointCheckTaskDeviceMapper.delete(pointCheckTaskDevice) > 0;
    }

    @Override
    @UserDataIsolation
    public void checkIsExist(PointCheckTaskDevice pointCheckTaskDevice) {
//        PointCheckTask param = new PointCheckTask();
//        param.setId(pointCheckTaskDevice.getTaskId());
//        param.setCompanyId(pointCheckTaskDevice.getCompanyId());
//        //查询任务计划
//        PointCheckTask pointCheckTask = pointCheckTaskService.getOne(param);
//        Integer pointId = pointCheckTask.getPointId();
//        PointCheck pointCheck = new PointCheck();
//        pointCheck.setId(pointId);
//        //查询计划
//        PointCheck check = pointCheckService.getOne(pointCheck);
//        List<Device> devices = projectDeviceGroupMapper.getDeviceByGroupId(check.getGroupId());
//        if (CollUtil.isEmpty(devices)){
//            throw new CustomException("该计划所关联的分组中没有绑定设备");
//        }
//        boolean flag = false;
//        for (Device device : devices) {
//            if (StrUtil.equals(device.getSn(),pointCheckTaskDevice.getSn())){
//                flag = true;
//                if (!check.getProjectId().equals(device.getProjectId())){
//                    throw new CustomException("该产品不属于该计划所关联的项目");
//                }
//                break;
//            }
//        }
//        if (!flag){
//            throw new CustomException("该产品不属于该计划所关联的分组中绑定的设备");
//        }
//        PointCheckTaskDevice result = pointCheckTaskDeviceMapper.checkIsExist(pointCheckTaskDevice);
//        if (result == null) {
//            return;
//        }
//        if (pointCheckTaskDevice.getId() == null || pointCheckTaskDevice.getId().compareTo(result.getId()) != 0) {
//            throw new CustomException("该任务下该产品已存在");
//        }

    }

    @Override
    @UserDataIsolation(tableAlias = "t")
    public PointCheckTask queryTask(PointCheckTaskDevice pointCheckTaskDevice) {
        return pointCheckTaskDeviceMapper.queryTask(pointCheckTaskDevice);
    }

    @Override
    @Transactional
    public List<PointCheckCreatReportVO> createReport(QueryVo queryVo) {
        Date startTime = queryVo.filters.getDate("startTime");
        Integer id = queryVo.filters.getInteger("id");
        Long companyId = queryVo.filters.getLong("companyId");
        String executor = queryVo.filters.getString("executor");
        if (id == null) {
            return new ArrayList<>();
        }
        if (startTime == null) {
            startTime = new Date();
        }
        DateTime endOfDay = DateUtil.endOfDay(startTime);
        DateTime beginOfDay = DateUtil.beginOfDay(startTime);
        //查询产品系列号
        PointCheckTaskDevice taskDeviceParam = new PointCheckTaskDevice();
        taskDeviceParam.setId(id);
        PointCheckTaskDevice taskDevice = getOne(taskDeviceParam);
        if (taskDevice == null) {
            return new ArrayList<>();
        }
        Integer deviceId = taskDevice.getDeviceId();
        QueryVo vo = new QueryVo();
        vo.filters.put("device_id",deviceId);
        vo.filters.put("company_id",companyId);
        vo.filters.put("begin_of_day",beginOfDay);
        vo.filters.put("end_of_day",endOfDay);
        vo.filters.put("executor",executor);
        List<PointCheckTaskDevice> taskDevices = pointCheckTaskDeviceMapper.queryTaskDevice(vo);
        if (CollUtil.isEmpty(taskDevices)) {
            return new ArrayList<>();
        }
        //查询产品自定义字段
        List<DeviceCustomField> deviceCustomFields = deviceCustomFieldService.queryPageList(vo);
        List<PointCheckCreatReportVO> result = new ArrayList<>();
        List<Map<String, Object>> mapList = devicePointCheckConfigService.typeItemDefaultEnum(vo);
        for (PointCheckTaskDevice checkTaskDevice : taskDevices) {
            Integer taskDeviceId = checkTaskDevice.getId();
            PointCheckCreatReportVO reportVO = getReportVO(taskDeviceId, companyId,mapList);
            if (CollUtil.isEmpty(deviceCustomFields)){
                deviceCustomFields = new ArrayList<>();
            }
            reportVO.setCustomFields(deviceCustomFields);
            result.add(reportVO);
        }
        if (CollUtil.isNotEmpty(result)) {
            Collections.sort(result, new Comparator<PointCheckCreatReportVO>() {
                @Override
                public int compare(PointCheckCreatReportVO o1, PointCheckCreatReportVO o2) {
                    return o1.getStartTime().compareTo(o2.getStartTime());
                }
            });
        }
        return result;
    }

    @Override
    @Transactional
    public List<PointCheckCreatReportVO> createReportV2(QueryVo queryVo,Integer taskId,Integer taskDeviceId) {
        Date startTime = queryVo.filters.getDate("startTime");
        Integer id = queryVo.filters.getInteger("id");
        Long companyId = queryVo.filters.getLong("companyId");
        String executor = queryVo.filters.getString("executor");
        if (id == null) {
            return new ArrayList<>();
        }
        if (startTime == null) {
            startTime = new Date();
        }
        DateTime endOfDay = DateUtil.endOfDay(startTime);
        DateTime beginOfDay = DateUtil.beginOfDay(startTime);
        //查询产品系列号
        PointCheckTaskDevice taskDeviceParam = new PointCheckTaskDevice();
        taskDeviceParam.setId(id);
        PointCheckTaskDevice taskDevice = getOne(taskDeviceParam);
        if (taskDevice == null) {
            return new ArrayList<>();
        }
        Integer deviceId = taskDevice.getDeviceId();
        QueryVo vo = new QueryVo();
        vo.filters.put("device_id",deviceId);
        vo.filters.put("task_device_id",taskDeviceId);
        vo.filters.put("company_id",companyId);

        vo.filters.put("executor",executor);
        List<PointCheckTaskDevice> taskDevices = pointCheckTaskDeviceMapper.queryTaskDevice(vo);
        if (CollUtil.isEmpty(taskDevices)) {
            return new ArrayList<>();
        }
        //查询产品自定义字段
        List<DeviceCustomField> deviceCustomFields = deviceCustomFieldService.queryPageList(vo);
        List<PointCheckCreatReportVO> result = new ArrayList<>();
        List<Map<String, Object>> mapList = devicePointCheckConfigService.typeItemDefaultEnum(vo);
        for (PointCheckTaskDevice checkTaskDevice : taskDevices) {
//            Integer taskDeviceId = checkTaskDevice.getId();
            PointCheckCreatReportVO reportVO = getReportVO(taskDeviceId, companyId,mapList);
            if (CollUtil.isEmpty(deviceCustomFields)){
                deviceCustomFields = new ArrayList<>();
            }
            reportVO.setCustomFields(deviceCustomFields);
            result.add(reportVO);
        }
        if (CollUtil.isNotEmpty(result)) {
            Collections.sort(result, new Comparator<PointCheckCreatReportVO>() {
                @Override
                public int compare(PointCheckCreatReportVO o1, PointCheckCreatReportVO o2) {
                    return o1.getStartTime().compareTo(o2.getStartTime());
                }
            });
        }
        return result;
    }

    private PointCheckCreatReportVO getReportVO(Integer id, Long companyId,List<Map<String, Object>> mapList) {
        // 查询产品基本信息
        PointCheckTaskDevice param = new PointCheckTaskDevice();
        param.setId(id);
        PointCheckTaskDevice one = getOne(param);
        if (one == null) {
            throw new CustomException("没有查询到该产品信息");
        }
        Integer deviceId = one.getDeviceId();
        String sn = one.getSn();
        String code = one.getCode();
        String name = one.getName();
        String description = one.getDescription();
        // 查询点检任务
        Integer taskId = one.getTaskId();
        PointCheckTask taskParam = new PointCheckTask();
        taskParam.setId(taskId);
        taskParam.setCompanyId(companyId);
        PointCheckTask task = pointCheckTaskService.getOne(taskParam);
        if (task == null) {
            throw new CustomException("没有查询到该点检任务信息");
        }
        Date endTime = task.getEndTime();
        String executor = task.getExecutor();
        String nickName = task.getNickName();
        String status = task.getStatus();
        Date startTime = task.getStartTime();
        String deptName = task.getDeptName();
        PointCheckCreatReportVO reportVO = new PointCheckCreatReportVO();
        reportVO.setCode(code);
        reportVO.setEndTime(endTime);
        reportVO.setExecutor(executor);
        reportVO.setName(name);
        reportVO.setNickName(nickName);
        reportVO.setSn(sn);
        reportVO.setDescription(description);
        reportVO.setStartTime(startTime);
        reportVO.setStatus(status);
        reportVO.setUseUnit(getUseUnit(task.getDeptId()));

        //根据产品差异常信息

        Map<String, Object> abnormal = pointCheckDeviceDetailService.queryListAbnormalList(deviceId);
        reportVO.setDeviceAbnormal(abnormal);

        //根据任务Id查询产品检查信息
        PointCheckDeviceDetail pointCheckDeviceDetail = new PointCheckDeviceDetail();
        pointCheckDeviceDetail.setTaskDeviceId(id);
        List<PointCheckDeviceDetail> deviceDetails = pointCheckDeviceDetailService.queryList(pointCheckDeviceDetail);
        Map<String, List<PointCheckDeviceDetail>> checkItemMap = deviceDetails.stream().collect(Collectors.groupingBy(PointCheckDeviceDetail::getType));
        Map<String, List<PointCheckDeviceDetail>> listMap = initPcReport(mapList, checkItemMap);
        reportVO.setCheckItemMap(listMap);
        if (CollUtil.isEmpty(deviceDetails)) {
            return reportVO;
        }
//        //获取检查类别和检查项的字符串
//        Set<String> checkItemSet = getTypeItem(deviceDetails);
//        String errMsg = checkItemExist(checkItemSet);
//        if (StrUtil.isNotBlank(errMsg)) {
//            throw new CustomException("生成失败，还存在未检查项：" + errMsg);
//        }
        //修改该设备为已生成报告状态
        boolean flag = false;
        for (PointCheckDeviceDetail deviceDetail : deviceDetails) {
            if (StrUtil.isBlank(deviceDetail.getStatus())) {
                flag = true;
                break;
            }
        }
        if (one.getReportFlag() == 0 && !flag) {
            one.setReportFlag(1);
            update(one);
            PointCheckTaskDevice taskDevice = new PointCheckTaskDevice();
            taskDevice.setTaskId(taskId);
            taskDevice.setCompanyId(companyId);
            List<PointCheckTaskDevice> taskDevices = queryList(taskDevice);
            boolean flag2 = false;
            for (PointCheckTaskDevice device : taskDevices) {
                if (device.getReportFlag() == 0) {
                    flag2 = true;
                    break;
                }
            }
            if (!flag2) {
                task.setStatus("3");
                pointCheckTaskService.update(task);
            }
        }
        //查讯异常信息
        List<PointCheckDeviceDetail> errorDeviceDetail = deviceDetails.stream().filter(a -> StrUtil.equals(a.getStatus(), "异常")).collect(Collectors.toList());
        if (CollUtil.isEmpty(errorDeviceDetail)) {
            return reportVO;
        }
        Set<Integer> detailIdSet = errorDeviceDetail.stream().map(PointCheckDeviceDetail::getId).collect(Collectors.toSet());
        //根据产品检查Id查询文字描述
        List<PointCheckDescriptionVO> descriptionVOS = pointCheckDescriptionService.queryDescriptionVO(detailIdSet);
        //根据产品检查Id查询处理方式
        List<PointCheckTaskDealVO> taskDealVOS = pointCheckTaskDealService.queryDeviceDealVO(detailIdSet);
        if (CollUtil.isNotEmpty(descriptionVOS)) {
            List<Map<String, Object>> descriptionVOList = initDescriptionVO(descriptionVOS);
            reportVO.setDescriptionVOS(descriptionVOList);
        }
        if (CollUtil.isNotEmpty(taskDealVOS)) {
            //initDealVO(mapList, taskDealVOS);
            Map<String, List<PointCheckTaskDealVO>> collect = taskDealVOS.stream().collect(Collectors.groupingBy(PointCheckTaskDealVO::getDetailType));
            reportVO.setTaskDealVOS(collect);
        }
        return reportVO;
    }

    /**
     * 封装描述数据结构
     *
     * @param descriptionVOS
     */
    private List<Map<String,Object>> initDescriptionVO(List<PointCheckDescriptionVO> descriptionVOS) {
        List<Map<String,Object>> result = new ArrayList<>();
        Map<Integer, List<PointCheckDescriptionVO>> itemDetailMap = descriptionVOS.stream().collect(Collectors.groupingBy(PointCheckDescription::getDetailId));
        for (Map.Entry<Integer, List<PointCheckDescriptionVO>> entry : itemDetailMap.entrySet()) {
            List<PointCheckDescriptionVO> value = entry.getValue();
            if (CollUtil.isEmpty(value)){
                continue;
            }
            PointCheckDescriptionVO descriptionVO = value.get(0);
            Map<String,Object> map = new HashMap<>();
            map.put("type",descriptionVO.getType());
            map.put("item",descriptionVO.getItem());
            map.put("fields",value);
            result.add(map);
        }
        return result;
    }

    /**
     * @param mapList
     * @param dealVOS
     */
    private void initDealVO(List<Map<String, Object>> mapList, List<PointCheckTaskDealVO> dealVOS) {
        Map<String, List<PointCheckTaskDealVO>> collect = dealVOS.stream().collect(Collectors.groupingBy(PointCheckTaskDealVO::getDetailType));
        for (Map<String, Object> map : mapList) {
            String name = (String) map.get("name");
            List<Map<String, String>> childData = (List<Map<String, String>>) map.get("childData");
            List<PointCheckTaskDealVO> dealVOList = collect.get(name);
            if (CollUtil.isEmpty(dealVOList)) {
                continue;
            }
            for (PointCheckTaskDealVO dealVO : dealVOList) {
                for (Map<String, String> childMap : childData) {
                    if (StrUtil.equals(dealVO.getItem(), childMap.get("name"))) {
                        dealVO.setDataType(childMap.get("dataType"));
                        break;
                    }
                }
            }
        }
    }

    @Override
    @Transactional
    public void quickCreateReport(Integer id, SysUser sysUser, Long companyId) {
        if (id == null) {
            return;
        }
        //根据任务Id查询产品检查信息
        PointCheckDeviceDetail pointCheckDeviceDetail = new PointCheckDeviceDetail();
        pointCheckDeviceDetail.setTaskDeviceId(id);
        List<PointCheckDeviceDetail> deviceDetails = pointCheckDeviceDetailService.queryList(pointCheckDeviceDetail);
        for (PointCheckDeviceDetail deviceDetail : deviceDetails) {
            //状态未空并
            if (StrUtil.isBlank(deviceDetail.getStatus())) {
                deviceDetail.setStatus("正常");
                deviceDetail.setCreateTime(new Date());
                pointCheckDeviceDetailService.update(deviceDetail);
            }
        }
        //赋值 最后一次更新时间
        PointCheckTaskDevice taskDevice = new PointCheckTaskDevice();
        taskDevice.setId(id);
        taskDevice.setLastUpdateTime(new Date());
        update(taskDevice);
    }

    @Override
    public PointCheckPCReportVO getReport(QueryVo queryVo) {
        String sn = queryVo.filters.getString("sn");
        Date startTime = queryVo.filters.getDate("startTime");
        Long companyId = queryVo.filters.getLong("companyId");
        if (startTime == null) {
            startTime = new Date();
        }
        DateTime beginOfDay = DateUtil.beginOfDay(startTime);
        DateTime endOfDay = DateUtil.endOfDay(startTime);
        PointCheckTask taskParam = new PointCheckTask();
        taskParam.setStartTimeBegin(beginOfDay);
        taskParam.setStartTimeEnd(endOfDay);
        taskParam.setCompanyId(companyId);
        //查询当天的任务
        List<PointCheckTask> pointCheckTasks = pointCheckTaskService.queryList(taskParam);
        if (CollUtil.isEmpty(pointCheckTasks)) {
            return new PointCheckPCReportVO();
        }
        PointCheckTaskDevice taskDeviceParam = new PointCheckTaskDevice();
        taskDeviceParam.setSnParam(sn);
        taskDeviceParam.setCompanyId(companyId);
        List<PointCheckTaskDevice> pointCheckTaskDeviceList = queryList(taskDeviceParam);
        if (CollUtil.isEmpty(pointCheckTaskDeviceList)) {
            return new PointCheckPCReportVO();
        }

        List<PointCheckTaskDevice> taskDevices = new ArrayList<>();
        pointCheckTaskDeviceList.forEach(a -> {
            pointCheckTasks.forEach(b -> {
                if (a.getTaskId().compareTo(b.getId()) == 0) {
                    taskDevices.add(a);
                }
            });
        });
        if (CollUtil.isEmpty(taskDevices)) {
            return new PointCheckPCReportVO();
        }
        PointCheckPCReportVO reportVO = new PointCheckPCReportVO();
        List<PointCheckPCDeviceDetailVO> pcDeviceDetailVOList = new ArrayList<>();
        List<Map<String,Object>> descriptionVOList = new ArrayList<>();
        List<PointCheckTaskDealVO> taskDealVOList = new ArrayList<>();
        PointCheckTaskDevice device = new PointCheckTaskDevice();
        Integer deviceId = taskDevices.get(0).getDeviceId();
        QueryVo queryVo1 = new QueryVo();
        queryVo1.filters.put("device_id", deviceId);
        queryVo1.filters.put("company_id", companyId);
        queryVo1.filters.put("begin_of_day", beginOfDay);
        queryVo1.filters.put("end_of_day", endOfDay);
        List<Map<String, Object>> mapList = devicePointCheckConfigService.typeItemDefaultEnum(queryVo1);
        //查询产品自定义字段
        List<DeviceCustomField> deviceCustomFields = deviceCustomFieldService.queryPageList(queryVo1);
        for (PointCheckTaskDevice taskDevice : taskDevices) {
            //查询产品信息
            if (device.getId() == null) {
                PointCheckTaskDevice deviceParam = new PointCheckTaskDevice();
                deviceParam.setId(taskDevice.getId());
                device = getOne(deviceParam);
            }
            PointCheckPCDeviceDetailVO deviceDetailVO = new PointCheckPCDeviceDetailVO();
            PointCheckDeviceDetail deviceDetail = new PointCheckDeviceDetail();
            deviceDetail.setTaskDeviceId(taskDevice.getId());
            List<PointCheckDeviceDetail> deviceDetails = pointCheckDeviceDetailService.queryList(deviceDetail);
            //计划任务信息
            PointCheckTask pointCheckTask = new PointCheckTask();
            for (PointCheckTask task : pointCheckTasks) {
                if (task.getId().compareTo(taskDevice.getTaskId()) == 0) {
                    pointCheckTask = task;
                }
            }
            if (CollUtil.isNotEmpty(deviceDetails)) {
                Map<String, List<PointCheckDeviceDetail>> listMap = deviceDetails.stream().collect(Collectors.groupingBy(PointCheckDeviceDetail::getType));
                List<Map<String, Object>> descriptionVOs = new ArrayList<>();
                List<PointCheckTaskDealVO> taskDealVOS = new ArrayList<>();
                List<PointCheckDeviceDetail> errorDetails = deviceDetails.stream().filter(a -> StrUtil.equals(a.getStatus(), "异常")).collect(Collectors.toList());
                if (CollUtil.isNotEmpty(errorDetails)){
                    Set<Integer> detailIdSet = errorDetails.stream().map(PointCheckDeviceDetail::getId).collect(Collectors.toSet());
                    //根据产品检查Id查询文字描述
                    List<PointCheckDescriptionVO> descriptionVOS = pointCheckDescriptionService.queryDescriptionVO(detailIdSet);
                    if (CollUtil.isNotEmpty(descriptionVOS)) {
                        PointCheckTask finalPointCheckTask = pointCheckTask;
                        descriptionVOS.forEach(a -> {
                            a.setStartTime(finalPointCheckTask.getStartTime());
                            a.setEndTime(finalPointCheckTask.getEndTime());
                            a.setNickName(finalPointCheckTask.getNickName());
                        });
                        descriptionVOs = initDescriptionVO(descriptionVOS);
                    }
                    //根据产品检查Id查询处理方式
                    taskDealVOS = pointCheckTaskDealService.queryDeviceDealVO(detailIdSet);
                    if (CollUtil.isNotEmpty(taskDealVOS)) {
                        //initDealVO(mapList, taskDealVOS);
                        PointCheckTask finalPointCheckTask = pointCheckTask;
                        taskDealVOS.forEach(a -> {
                            a.setStartTime(finalPointCheckTask.getStartTime());
                            a.setEndTime(finalPointCheckTask.getEndTime());
                            a.setNickName(finalPointCheckTask.getNickName());
                        });
                    }
                }
                descriptionVOList.addAll(descriptionVOs);
                taskDealVOList.addAll(taskDealVOS);
                deviceDetailVO.setCheckItemMap(listMap);
            }
            deviceDetailVO.setStartTime(pointCheckTask.getStartTime());
            deviceDetailVO.setEndTime(pointCheckTask.getEndTime());
            deviceDetailVO.setExecutor(pointCheckTask.getExecutor());
            deviceDetailVO.setNickName(pointCheckTask.getNickName());
            deviceDetailVO.setStatus(pointCheckTask.getStatus());
            pcDeviceDetailVOList.add(deviceDetailVO);
        }
        pcDeviceDetailVOList.sort(new Comparator<PointCheckPCDeviceDetailVO>() {
            @Override
            public int compare(PointCheckPCDeviceDetailVO o1, PointCheckPCDeviceDetailVO o2) {
                return o1.getStartTime().compareTo(o2.getStartTime());
            }
        });
        reportVO.setCode(device.getCode());
        reportVO.setName(device.getName());
        reportVO.setSn(sn);
        reportVO.setDescription(device.getDescription());
        reportVO.setUseUnit(getUseUnit(device.getDeptId()));
        if(CollUtil.isEmpty(deviceCustomFields)){
            deviceCustomFields = new ArrayList<>();
        }
        reportVO.setCustomFields(deviceCustomFields);
        for (PointCheckPCDeviceDetailVO detailVO : pcDeviceDetailVOList) {
            Map<String, List<PointCheckDeviceDetail>> checkItemMap = detailVO.getCheckItemMap();
            //封装pc端报表数据结构
            Map<String, List<PointCheckDeviceDetail>> listMap = initPcReport(mapList, checkItemMap);
            detailVO.setCheckItemMap(listMap);
        }
        reportVO.setPcDeviceDetailVOList(pcDeviceDetailVOList);
//        Map<String, List<PointCheckDescriptionVO>> collect = descriptionVOList.stream().collect(Collectors.groupingBy(PointCheckDescriptionVO::getType));
//        //排序
//        for (Map.Entry<String, List<PointCheckDescriptionVO>> entry : collect.entrySet()) {
//            entry.getValue().sort(new Comparator<PointCheckDescriptionVO>() {
//                @Override
//                public int compare(PointCheckDescriptionVO o1, PointCheckDescriptionVO o2) {
//                    return o1.getStartTime().compareTo(o2.getStartTime());
//                }
//            });
//        }
        reportVO.setDescriptionVOS(descriptionVOList);
        Map<String, List<PointCheckTaskDealVO>> listMap = taskDealVOList.stream().collect(Collectors.groupingBy(PointCheckTaskDealVO::getDetailType));
        for (Map.Entry<String, List<PointCheckTaskDealVO>> entry : listMap.entrySet()) {
            entry.getValue().sort(new Comparator<PointCheckTaskDealVO>() {
                @Override
                public int compare(PointCheckTaskDealVO o1, PointCheckTaskDealVO o2) {
                    return o1.getStartTime().compareTo(o2.getStartTime());
                }
            });
        }
        reportVO.setTaskDealVOS(listMap);
        return reportVO;
    }
    @Override
    public PointCheckPCReportVO getReportV2(QueryVo queryVo,Integer taskDeviceId,Integer taskId) {

        String sn = queryVo.filters.getString("sn");
        Date startTime = queryVo.filters.getDate("startTime");
        Long companyId = queryVo.filters.getLong("companyId");
        if (startTime == null) {
            startTime = new Date();
        }
        DateTime beginOfDay = DateUtil.beginOfDay(startTime);
        DateTime endOfDay = DateUtil.endOfDay(startTime);
        PointCheckTask taskParam = new PointCheckTask();

        taskParam.setId(taskId);
        taskParam.setCompanyId(companyId);
        //查询当天的任务
        List<PointCheckTask> pointCheckTasks = pointCheckTaskService.queryList(taskParam);
        if (CollUtil.isEmpty(pointCheckTasks)) {
            return new PointCheckPCReportVO();
        }
        PointCheckTaskDevice taskDeviceParam = new PointCheckTaskDevice();
        taskDeviceParam.setId(taskDeviceId);
        taskDeviceParam.setCompanyId(companyId);
        List<PointCheckTaskDevice> pointCheckTaskDeviceList = queryList(taskDeviceParam);
        if (CollUtil.isEmpty(pointCheckTaskDeviceList)) {
            return new PointCheckPCReportVO();
        }

        List<PointCheckTaskDevice> taskDevices = new ArrayList<>();
        pointCheckTaskDeviceList.forEach(a -> {
            pointCheckTasks.forEach(b -> {
                if (a.getTaskId().compareTo(b.getId()) == 0) {
                    taskDevices.add(a);
                }
            });
        });
        if (CollUtil.isEmpty(taskDevices)) {
            return new PointCheckPCReportVO();
        }
        PointCheckPCReportVO reportVO = new PointCheckPCReportVO();
        List<PointCheckPCDeviceDetailVO> pcDeviceDetailVOList = new ArrayList<>();
        List<Map<String,Object>> descriptionVOList = new ArrayList<>();
        List<PointCheckTaskDealVO> taskDealVOList = new ArrayList<>();
        PointCheckTaskDevice device = new PointCheckTaskDevice();
        Integer deviceId = taskDevices.get(0).getDeviceId();
        QueryVo queryVo1 = new QueryVo();
        queryVo1.filters.put("task_device_id", taskDeviceId);
        queryVo1.filters.put("company_id", companyId);
        List<Map<String, Object>> mapList = devicePointCheckConfigService.typeItemDefaultEnum(queryVo1);
        //查询产品自定义字段
        QueryVo queryVo2 = new QueryVo();
        queryVo2.filters.put("device_id",deviceId);
        List<DeviceCustomField> deviceCustomFields = deviceCustomFieldService.queryPageList(queryVo2);
        for (PointCheckTaskDevice taskDevice : taskDevices) {
            //查询产品信息
            if (device.getId() == null) {
                PointCheckTaskDevice deviceParam = new PointCheckTaskDevice();
                deviceParam.setId(taskDevice.getId());
                device = getOne(deviceParam);
            }
            PointCheckPCDeviceDetailVO deviceDetailVO = new PointCheckPCDeviceDetailVO();
            PointCheckDeviceDetail deviceDetail = new PointCheckDeviceDetail();
            deviceDetail.setTaskDeviceId(taskDevice.getId());
            List<PointCheckDeviceDetail> deviceDetails = pointCheckDeviceDetailService.queryList(deviceDetail);
            //计划任务信息
            PointCheckTask pointCheckTask = new PointCheckTask();
            for (PointCheckTask task : pointCheckTasks) {
                if (task.getId().compareTo(taskDevice.getTaskId()) == 0) {
                    pointCheckTask = task;
                }
            }
            if (CollUtil.isNotEmpty(deviceDetails)) {
                Map<String, List<PointCheckDeviceDetail>> listMap = deviceDetails.stream().collect(Collectors.groupingBy(PointCheckDeviceDetail::getType));
                List<Map<String, Object>> descriptionVOs = new ArrayList<>();
                List<PointCheckTaskDealVO> taskDealVOS = new ArrayList<>();
                List<PointCheckDeviceDetail> errorDetails = deviceDetails.stream().filter(a -> StrUtil.equals(a.getStatus(), "异常")).collect(Collectors.toList());
                if (CollUtil.isNotEmpty(errorDetails)){
                    Set<Integer> detailIdSet = errorDetails.stream().map(PointCheckDeviceDetail::getId).collect(Collectors.toSet());
                    //根据产品检查Id查询文字描述
                    List<PointCheckDescriptionVO> descriptionVOS = pointCheckDescriptionService.queryDescriptionVO(detailIdSet);
                    if (CollUtil.isNotEmpty(descriptionVOS)) {
                        PointCheckTask finalPointCheckTask = pointCheckTask;
                        descriptionVOS.forEach(a -> {
                            a.setStartTime(finalPointCheckTask.getStartTime());
                            a.setEndTime(finalPointCheckTask.getEndTime());
                            a.setNickName(finalPointCheckTask.getNickName());
                        });
                        descriptionVOs = initDescriptionVO(descriptionVOS);
                    }
                    //根据产品检查Id查询处理方式
                    taskDealVOS = pointCheckTaskDealService.queryDeviceDealVO(detailIdSet);
                    if (CollUtil.isNotEmpty(taskDealVOS)) {
                        //initDealVO(mapList, taskDealVOS);
                        PointCheckTask finalPointCheckTask = pointCheckTask;
                        taskDealVOS.forEach(a -> {
                            a.setStartTime(finalPointCheckTask.getStartTime());
                            a.setEndTime(finalPointCheckTask.getEndTime());
                            a.setNickName(finalPointCheckTask.getNickName());
                        });
                    }
                }
                descriptionVOList.addAll(descriptionVOs);
                taskDealVOList.addAll(taskDealVOS);
                deviceDetailVO.setCheckItemMap(listMap);
            }
            deviceDetailVO.setStartTime(pointCheckTask.getStartTime());
            deviceDetailVO.setEndTime(pointCheckTask.getEndTime());
            deviceDetailVO.setExecutor(pointCheckTask.getExecutor());
            deviceDetailVO.setNickName(pointCheckTask.getNickName());
            deviceDetailVO.setStatus(pointCheckTask.getStatus());
            pcDeviceDetailVOList.add(deviceDetailVO);
        }
        pcDeviceDetailVOList.sort(new Comparator<PointCheckPCDeviceDetailVO>() {
            @Override
            public int compare(PointCheckPCDeviceDetailVO o1, PointCheckPCDeviceDetailVO o2) {
                return o1.getStartTime().compareTo(o2.getStartTime());
            }
        });
        reportVO.setCode(device.getCode());
        reportVO.setName(device.getName());
        reportVO.setSn(sn);
        reportVO.setDescription(device.getDescription());
        reportVO.setUseUnit(getUseUnit(device.getDeptId()));

        //根据产品差异常信息

        Map<String, Object> abnormal = pointCheckDeviceDetailService.queryListAbnormalList(deviceId);
        reportVO.setDeviceAbnormal(abnormal);

        if(CollUtil.isEmpty(deviceCustomFields)){
            deviceCustomFields = new ArrayList<>();
        }
        reportVO.setCustomFields(deviceCustomFields);
        for (PointCheckPCDeviceDetailVO detailVO : pcDeviceDetailVOList) {
            Map<String, List<PointCheckDeviceDetail>> checkItemMap = detailVO.getCheckItemMap();
            //封装pc端报表数据结构
            Map<String, List<PointCheckDeviceDetail>> listMap = initPcReport(mapList, checkItemMap);
            detailVO.setCheckItemMap(listMap);
        }
        reportVO.setPcDeviceDetailVOList(pcDeviceDetailVOList);
//        Map<String, List<PointCheckDescriptionVO>> collect = descriptionVOList.stream().collect(Collectors.groupingBy(PointCheckDescriptionVO::getType));
//        //排序
//        for (Map.Entry<String, List<PointCheckDescriptionVO>> entry : collect.entrySet()) {
//            entry.getValue().sort(new Comparator<PointCheckDescriptionVO>() {
//                @Override
//                public int compare(PointCheckDescriptionVO o1, PointCheckDescriptionVO o2) {
//                    return o1.getStartTime().compareTo(o2.getStartTime());
//                }
//            });
//        }
        reportVO.setDescriptionVOS(descriptionVOList);
        Map<String, List<PointCheckTaskDealVO>> listMap = taskDealVOList.stream().collect(Collectors.groupingBy(PointCheckTaskDealVO::getDetailType));
        for (Map.Entry<String, List<PointCheckTaskDealVO>> entry : listMap.entrySet()) {
            entry.getValue().sort(new Comparator<PointCheckTaskDealVO>() {
                @Override
                public int compare(PointCheckTaskDealVO o1, PointCheckTaskDealVO o2) {
                    return o1.getStartTime().compareTo(o2.getStartTime());
                }
            });
        }
        reportVO.setTaskDealVOS(listMap);
        return reportVO;
    }

    /**
     * 封装pc端报表数据结构
     *
     * @param mapList
     */
    private Map<String, List<PointCheckDeviceDetail>> initPcReport(List<Map<String, Object>> mapList, Map<String, List<PointCheckDeviceDetail>> checkItemMap) {
        if (CollUtil.isEmpty(checkItemMap)) {
            checkItemMap = new HashMap<>();
            for (Map<String, Object> map : mapList) {
                String name = (String) map.get("name");
                List<String> childData = (List<String>) map.get("childData");
                List<PointCheckDeviceDetail> deviceDetails = new ArrayList<>();
                for (String item : childData) {
                    PointCheckDeviceDetail detail = new PointCheckDeviceDetail();
                    detail.setItem(item);
                    detail.setType(name);
                    deviceDetails.add(detail);
                }
                checkItemMap.put(name, deviceDetails);
            }
            return checkItemMap;
        }

        for (Map<String, Object> map : mapList) {
            String name = (String) map.get("name");
            List<String> childData = (List<String>) map.get("childData");
            List<PointCheckDeviceDetail> deviceDetails = checkItemMap.get(name);
            List<PointCheckDeviceDetail> result = new ArrayList<>();
            for (String item : childData) {
                boolean flag = false;
                if (CollUtil.isEmpty(deviceDetails)) {
                    PointCheckDeviceDetail detail = new PointCheckDeviceDetail();
                    detail.setItem(item);
                    detail.setType(name);
                    result.add(detail);
                    continue;
                }
                for (PointCheckDeviceDetail detail : deviceDetails) {
                    if (StrUtil.equals(item, detail.getItem())) {
                        result.add(detail);
                        flag = true;
                        continue;
                    }
                }
                if (!flag) {
                    PointCheckDeviceDetail detail = new PointCheckDeviceDetail();
                    detail.setItem(item);
                    detail.setType(name);
                    result.add(detail);
                }

            }
            checkItemMap.put(name, result);
        }
        return checkItemMap;
    }

    @Override
    public List<PointCheckTaskDevice> queryAllTaskDevice(QueryVo queryVo) {
        return pointCheckTaskDeviceMapper.queryAllTaskDevice(queryVo);
    }

    @Override
    public List<Map<String, Object>> queryAllErrorItem(QueryVo queryVo) {
        return pointCheckTaskDeviceMapper.queryAllErrorItem(queryVo);
    }

    @Override
    public PointCheckDeviceDetailDTO getDeviceDetail(QueryVo queryVo) {
        String sn = queryVo.filters.getString("sn");
        if (StrUtil.isBlank(sn)) {
            return new PointCheckDeviceDetailDTO();
        }
        List<PointCheckDeviceDetailDTO> deviceDetail = pointCheckTaskDeviceMapper.getDeviceDetail(queryVo);
        if (CollUtil.isEmpty(deviceDetail)) {
            return new PointCheckDeviceDetailDTO();
        }
        Date nowDate = queryVo.filters.getDate("now_date");
        List<PointCheckDeviceDetailDTO> collect = deviceDetail.stream().filter(a -> nowDate.compareTo(a.getStartTime()) >= 0).collect(Collectors.toList());
        if (CollUtil.isEmpty(collect)) {
            return new PointCheckDeviceDetailDTO();
        }
        return collect.get(0);
    }

//    /**
//     * 获取检查类别和检查项的字符串 用 - 分割
//     *
//     * @param deviceDetails
//     * @return
//     */
//    private Set<String> getTypeItem(List<PointCheckDeviceDetail> deviceDetails) {
//        Set<String> checkItemSet = new HashSet<>();
//        deviceDetails.stream().forEach(a -> {
//            String type = a.getType();
//            String item = a.getItem();
//            String str = type + "-" + item;
//            checkItemSet.add(str);
//        });
//        return checkItemSet;
//    }

//    /**
//     * 保存产品检查项
//     *
//     * @param taskDeviceId
//     * @param typeItem
//     */
//    private void saveItem(Integer taskDeviceId, String typeItem, SysUser sysUser, Long companyId) {
//        List<String> list = StrUtil.splitTrim(typeItem, "-");
//        PointCheckDeviceDetail deviceDetail = new PointCheckDeviceDetail();
//        String type = list.get(0);
//        String item = list.get(1);
//        if (StrUtil.equals("温度", item) || StrUtil.equals("压力", item) || StrUtil.equals("运行周期", item)) {
//            return;
//        }
//        deviceDetail.setType(type);
//        deviceDetail.setItem(item);
//        deviceDetail.setCompanyId(companyId);
//        deviceDetail.setCreateBy(sysUser.getUserName());
//        deviceDetail.setCreateTime(new Date());
//        deviceDetail.setTaskDeviceId(taskDeviceId);
//        deviceDetail.setStatus("正常");
//        pointCheckDeviceDetailService.add(deviceDetail);
//    }

//    /**
//     * 验证产品检查信息是否都已经存在
//     *
//     * @param checkItemSet
//     * @return
//     */
//    private String checkItemExist(Set<String> checkItemSet) {
//        List<String> result = getCheckItem();
//        StringBuilder sb = new StringBuilder();
//        result.stream().forEach(a -> {
//            if (!checkItemSet.contains(a)) {
//                sb.append(a).append(",");
//            }
//        });
//        String str = sb.toString();
//        if (StrUtil.isBlank(str)) {
//            return null;
//        }
//        return str.substring(0, str.length() - 1);
//    }

    /**
     * 获取产品检查项
     *
     * @return
     */
    private List<String> getCheckItem(Integer id) {
        PointCheckTaskDevice taskDevice = new PointCheckTaskDevice();
        taskDevice.setId(id);
        PointCheckTaskDevice pointCheckTaskDevice = getOne(taskDevice);
        QueryVo queryVo = new QueryVo();
        queryVo.filters.put("device_id", pointCheckTaskDevice.getDeviceId());
        List<DevicePointCheckConfig> devicePointCheckConfigs = devicePointCheckConfigService.queryList(queryVo);
        if (CollUtil.isEmpty(devicePointCheckConfigs)) {
            throw new CustomException("该产品型号还未自定义点检表单");
        }
        List<String> result = new ArrayList<>();
        for (DevicePointCheckConfig checkConfig : devicePointCheckConfigs) {
            String type = checkConfig.getType();
            String item = checkConfig.getItem();
            result.add(type + "-" + item);
        }
        return result;
    }

    /**
     * 获取客户名称(多级)
     *
     * @param deptId
     * @return
     */
    private String getUseUnit(Long deptId) {
        if (deptId == null) {
            return null;
        }
        SysDept sysDept = sysDeptService.selectDeptById(deptId);
        if (sysDept == null) {
            throw new CustomException("当前任务所属项目的客户公司不存在！");
        }
        if (sysDept.getParentId() == 0 || sysDept.getIsCustomer() == 1) {
            return sysDept.getDeptName();
        }
        List<String> deptIds = StrUtil.splitTrim(sysDept.getAncestors(), ",");
        List<String> resultList = new ArrayList<>();
        resultList.add(sysDept.getDeptName());
        for (int i = deptIds.size() - 1; i > 0; i--) {
            String deptIdStr = deptIds.get(i);
            SysDept dept = sysDeptService.selectDeptById(Convert.toLong(deptIdStr));
            resultList.add(0, dept.getDeptName());
            if (dept.getIsCustomer() == 1) {
                break;
            }
        }
        return StrUtil.join("-", resultList);
    }

    /**
     * 根据
     *
     * @param devicePointCheckConfigs
     * @return
     */
    private List<Map<String,Object>> getPointCheckConfig(List<DevicePointCheckConfig> devicePointCheckConfigs){
        List<Map<String,Object>> result = new ArrayList<>();
        Map<String, List<DevicePointCheckConfig>> typeMap = devicePointCheckConfigs.stream().collect(Collectors.groupingBy(DevicePointCheckConfig::getType));
        for (Map.Entry<String, List<DevicePointCheckConfig>> typeEntry : typeMap.entrySet()) {
            String type = typeEntry.getKey();
            List<DevicePointCheckConfig> typeValue = typeEntry.getValue();
            Integer category = typeValue.get(0).getCategory();

            Map<String, List<DevicePointCheckConfig>> itemMap = typeValue.stream().collect(Collectors.groupingBy(DevicePointCheckConfig::getItem));
            for (Map.Entry<String, List<DevicePointCheckConfig>> itemEntry : itemMap.entrySet()) {
                Map<String,Object> map = new HashMap<>();
                Integer typeOrder = itemEntry.getValue().get(0).getTypeOrder();
                Integer itemOrder = itemEntry.getValue().get(0).getItemOrder();
                map.put("type",type);
                map.put("category",category);
                map.put("orderNum",0);
                map.put("typeOrder",typeOrder);
                map.put("itemOrder",itemOrder);
                String item = itemEntry.getKey();
                map.put("item",item);
                List<DevicePointCheckConfig> itemValue = itemEntry.getValue();
                map.put("childData",itemValue);
                result.add(map);
            }
        }
        return result;
    }

    /**
     * 初始化默认点检项
     *
     * [{"type":"波纹管","item":"裂纹","childData":[{"customField":"工具","dataType":"string"}]}]
     * @return
     */
    private List<Map<String,Object>> initPointCheckConfig(){
        List<Map<String,Object>> result = new ArrayList<>();
        Map<String,Object> map1 = new HashMap<>();
        map1.put("type","波纹管");
        map1.put("item","裂纹");
        List<DevicePointCheckConfig> fieldConfig1 = new ArrayList<>();
        DevicePointCheckConfig checkConfig11 = new DevicePointCheckConfig();
        checkConfig11.setField("工具");
        checkConfig11.setDataType("string");
        checkConfig11.setOrderNum(0);
        fieldConfig1.add(checkConfig11);
        DevicePointCheckConfig checkConfig12 = new DevicePointCheckConfig();
        checkConfig12.setField("位置");
        checkConfig12.setDataType("string");
        checkConfig12.setOrderNum(0);
        fieldConfig1.add(checkConfig12);
        DevicePointCheckConfig checkConfig13 = new DevicePointCheckConfig();
        checkConfig13.setField("异常状况");
        checkConfig13.setDataType("string");
        checkConfig13.setOrderNum(0);
        fieldConfig1.add(checkConfig13);
        DevicePointCheckConfig checkConfig14 = new DevicePointCheckConfig();
        checkConfig14.setField("异常数值");
        checkConfig14.setDataType("string");
        checkConfig14.setOrderNum(0);
        fieldConfig1.add(checkConfig14);
        DevicePointCheckConfig checkConfig15 = new DevicePointCheckConfig();
        checkConfig15.setField("图片");
        checkConfig15.setDataType("photo");
        checkConfig15.setOrderNum(0);
        fieldConfig1.add(checkConfig15);
        map1.put("childData",fieldConfig1);
        result.add(map1);

        Map<String,Object> map2 = new HashMap<>();
        map2.put("type","波纹管");
        map2.put("item","腐蚀");
        List<DevicePointCheckConfig> fieldConfig2 = new ArrayList<>();
        DevicePointCheckConfig checkConfig21 = new DevicePointCheckConfig();
        checkConfig21.setField("工具");
        checkConfig21.setDataType("string");
        checkConfig21.setOrderNum(0);
        fieldConfig2.add(checkConfig21);
        DevicePointCheckConfig checkConfig22 = new DevicePointCheckConfig();
        checkConfig22.setField("位置");
        checkConfig22.setDataType("string");
        checkConfig22.setOrderNum(0);
        fieldConfig2.add(checkConfig22);
        DevicePointCheckConfig checkConfig23 = new DevicePointCheckConfig();
        checkConfig23.setField("异常状况");
        checkConfig23.setDataType("string");
        checkConfig23.setOrderNum(0);
        fieldConfig2.add(checkConfig23);
        DevicePointCheckConfig checkConfig24 = new DevicePointCheckConfig();
        checkConfig24.setField("异常数值");
        checkConfig24.setDataType("string");
        checkConfig24.setOrderNum(0);
        fieldConfig2.add(checkConfig24);
        DevicePointCheckConfig checkConfig25 = new DevicePointCheckConfig();
        checkConfig25.setField("图片");
        checkConfig25.setDataType("photo");
        checkConfig25.setOrderNum(0);
        fieldConfig2.add(checkConfig25);
        map2.put("childData",fieldConfig2);
        result.add(map2);

        Map<String,Object> map3 = new HashMap<>();
        map3.put("type","波纹管");
        map3.put("item","泄漏");
        List<DevicePointCheckConfig> fieldConfig3 = new ArrayList<>();
        DevicePointCheckConfig checkConfig31 = new DevicePointCheckConfig();
        checkConfig31.setField("工具");
        checkConfig31.setDataType("string");
        checkConfig31.setOrderNum(0);
        fieldConfig3.add(checkConfig21);
        DevicePointCheckConfig checkConfig32 = new DevicePointCheckConfig();
        checkConfig32.setField("位置");
        checkConfig32.setDataType("string");
        checkConfig32.setOrderNum(0);
        fieldConfig3.add(checkConfig32);
        DevicePointCheckConfig checkConfig33 = new DevicePointCheckConfig();
        checkConfig33.setField("异常状况");
        checkConfig33.setDataType("string");
        checkConfig33.setOrderNum(0);
        fieldConfig3.add(checkConfig33);
        DevicePointCheckConfig checkConfig34 = new DevicePointCheckConfig();
        checkConfig34.setField("异常数值");
        checkConfig34.setDataType("string");
        checkConfig34.setOrderNum(0);
        fieldConfig3.add(checkConfig34);
        DevicePointCheckConfig checkConfig35 = new DevicePointCheckConfig();
        checkConfig35.setField("图片");
        checkConfig35.setDataType("photo");
        checkConfig35.setOrderNum(0);
        fieldConfig3.add(checkConfig35);
        map3.put("childData",fieldConfig3);
        result.add(map3);

        Map<String,Object> map4 = new HashMap<>();
        map4.put("type","波纹管");
        map4.put("item","异常变形");
        List<DevicePointCheckConfig> fieldConfig4 = new ArrayList<>();
        DevicePointCheckConfig checkConfig41 = new DevicePointCheckConfig();
        checkConfig41.setField("工具");
        checkConfig41.setDataType("string");
        checkConfig41.setOrderNum(0);
        fieldConfig4.add(checkConfig41);
        DevicePointCheckConfig checkConfig42 = new DevicePointCheckConfig();
        checkConfig42.setField("位置");
        checkConfig42.setDataType("string");
        checkConfig42.setOrderNum(0);
        fieldConfig4.add(checkConfig42);
        DevicePointCheckConfig checkConfig43 = new DevicePointCheckConfig();
        checkConfig43.setField("异常状况");
        checkConfig43.setDataType("string");
        checkConfig43.setOrderNum(0);
        fieldConfig4.add(checkConfig43);
        DevicePointCheckConfig checkConfig44 = new DevicePointCheckConfig();
        checkConfig44.setField("异常数值");
        checkConfig44.setDataType("string");
        checkConfig44.setOrderNum(0);
        fieldConfig4.add(checkConfig44);
        DevicePointCheckConfig checkConfig45 = new DevicePointCheckConfig();
        checkConfig45.setField("图片");
        checkConfig45.setDataType("photo");
        checkConfig45.setOrderNum(0);
        fieldConfig4.add(checkConfig45);
        map4.put("childData",fieldConfig4);
        result.add(map4);

        Map<String,Object> map5 = new HashMap<>();
        map5.put("type","波纹管");
        map5.put("item","其他");
        List<DevicePointCheckConfig> fieldConfig5 = new ArrayList<>();
        DevicePointCheckConfig checkConfig51 = new DevicePointCheckConfig();
        checkConfig51.setField("工具");
        checkConfig51.setDataType("string");
        checkConfig51.setOrderNum(0);
        fieldConfig5.add(checkConfig51);
        DevicePointCheckConfig checkConfig52 = new DevicePointCheckConfig();
        checkConfig52.setField("位置");
        checkConfig52.setDataType("string");
        checkConfig52.setOrderNum(0);
        fieldConfig5.add(checkConfig52);
        DevicePointCheckConfig checkConfig53 = new DevicePointCheckConfig();
        checkConfig53.setField("异常状况");
        checkConfig53.setDataType("string");
        checkConfig53.setOrderNum(0);
        fieldConfig5.add(checkConfig53);
        DevicePointCheckConfig checkConfig54 = new DevicePointCheckConfig();
        checkConfig54.setField("异常数值");
        checkConfig54.setDataType("string");
        checkConfig54.setOrderNum(0);
        fieldConfig5.add(checkConfig54);
        DevicePointCheckConfig checkConfig55 = new DevicePointCheckConfig();
        checkConfig55.setField("图片");
        checkConfig55.setDataType("photo");
        checkConfig55.setOrderNum(0);
        fieldConfig5.add(checkConfig55);
        map5.put("childData",fieldConfig5);
        result.add(map5);

        Map<String,Object> map6 = new HashMap<>();
        map6.put("type","承力承压件");
        map6.put("item","焊缝");
        List<DevicePointCheckConfig> fieldConfig6 = new ArrayList<>();
        DevicePointCheckConfig checkConfig61 = new DevicePointCheckConfig();
        checkConfig61.setField("工具");
        checkConfig61.setDataType("string");
        checkConfig61.setOrderNum(0);
        fieldConfig6.add(checkConfig61);
        DevicePointCheckConfig checkConfig62 = new DevicePointCheckConfig();
        checkConfig62.setField("位置");
        checkConfig62.setDataType("string");
        checkConfig62.setOrderNum(0);
        fieldConfig6.add(checkConfig62);
        DevicePointCheckConfig checkConfig63 = new DevicePointCheckConfig();
        checkConfig63.setField("异常状况");
        checkConfig63.setDataType("string");
        checkConfig63.setOrderNum(0);
        fieldConfig6.add(checkConfig63);
        DevicePointCheckConfig checkConfig64 = new DevicePointCheckConfig();
        checkConfig64.setField("异常数值");
        checkConfig64.setDataType("string");
        checkConfig64.setOrderNum(0);
        fieldConfig6.add(checkConfig64);
        DevicePointCheckConfig checkConfig65 = new DevicePointCheckConfig();
        checkConfig65.setField("图片");
        checkConfig65.setDataType("photo");
        checkConfig65.setOrderNum(0);
        fieldConfig6.add(checkConfig65);
        map6.put("childData",fieldConfig6);
        result.add(map6);

        Map<String,Object> map7 = new HashMap<>();
        map7.put("type","承力承压件");
        map7.put("item","大变形");
        List<DevicePointCheckConfig> fieldConfig7 = new ArrayList<>();
        DevicePointCheckConfig checkConfig71 = new DevicePointCheckConfig();
        checkConfig71.setField("工具");
        checkConfig71.setDataType("string");
        checkConfig71.setOrderNum(0);
        fieldConfig7.add(checkConfig71);
        DevicePointCheckConfig checkConfig72 = new DevicePointCheckConfig();
        checkConfig72.setField("位置");
        checkConfig72.setDataType("string");
        checkConfig72.setOrderNum(0);
        fieldConfig7.add(checkConfig72);
        DevicePointCheckConfig checkConfig73 = new DevicePointCheckConfig();
        checkConfig73.setField("异常状况");
        checkConfig73.setDataType("string");
        checkConfig73.setOrderNum(0);
        fieldConfig7.add(checkConfig73);
        DevicePointCheckConfig checkConfig74 = new DevicePointCheckConfig();
        checkConfig74.setField("异常数值");
        checkConfig74.setDataType("string");
        checkConfig74.setOrderNum(0);
        fieldConfig7.add(checkConfig74);
        DevicePointCheckConfig checkConfig75 = new DevicePointCheckConfig();
        checkConfig75.setField("图片");
        checkConfig75.setDataType("photo");
        checkConfig75.setOrderNum(0);
        fieldConfig7.add(checkConfig75);
        map7.put("childData",fieldConfig7);
        result.add(map7);

        Map<String,Object> map8 = new HashMap<>();
        map8.put("type","承力承压件");
        map8.put("item","其他");
        List<DevicePointCheckConfig> fieldConfig8 = new ArrayList<>();
        DevicePointCheckConfig checkConfig81 = new DevicePointCheckConfig();
        checkConfig81.setField("工具");
        checkConfig81.setDataType("string");
        checkConfig81.setOrderNum(0);
        fieldConfig8.add(checkConfig81);
        DevicePointCheckConfig checkConfig82 = new DevicePointCheckConfig();
        checkConfig82.setField("位置");
        checkConfig82.setDataType("string");
        checkConfig82.setOrderNum(0);
        fieldConfig8.add(checkConfig82);
        DevicePointCheckConfig checkConfig83 = new DevicePointCheckConfig();
        checkConfig83.setField("异常状况");
        checkConfig83.setDataType("string");
        checkConfig83.setOrderNum(0);
        fieldConfig8.add(checkConfig83);
        DevicePointCheckConfig checkConfig84 = new DevicePointCheckConfig();
        checkConfig84.setField("异常数值");
        checkConfig84.setDataType("string");
        checkConfig84.setOrderNum(0);
        fieldConfig8.add(checkConfig84);
        DevicePointCheckConfig checkConfig85 = new DevicePointCheckConfig();
        checkConfig85.setField("图片");
        checkConfig85.setDataType("photo");
        checkConfig85.setOrderNum(0);
        fieldConfig8.add(checkConfig85);
        map8.put("childData",fieldConfig8);
        result.add(map8);

        Map<String,Object> map9 = new HashMap<>();
        map9.put("type","工况");
        map9.put("item","温度");
        List<DevicePointCheckConfig> fieldConfig9 = new ArrayList<>();
//        DevicePointCheckConfig checkConfig91 = new DevicePointCheckConfig();
//        checkConfig91.setField("数值");
//        checkConfig91.setDataType("string");
//        checkConfig91.setOrderNum(0);
//        fieldConfig9.add(checkConfig91);
        map9.put("childData",fieldConfig9);
        result.add(map9);

        Map<String,Object> map10 = new HashMap<>();
        map10.put("type","工况");
        map10.put("item","压力");
        List<DevicePointCheckConfig> fieldConfig10 = new ArrayList<>();
//        DevicePointCheckConfig checkConfig101 = new DevicePointCheckConfig();
//        checkConfig101.setField("数值");
//        checkConfig101.setDataType("string");
//        checkConfig101.setOrderNum(0);
//        fieldConfig10.add(checkConfig101);
        map10.put("childData",fieldConfig10);
        result.add(map10);

        Map<String,Object> map11 = new HashMap<>();
        map11.put("type","工况");
        map11.put("item","振动");
        List<DevicePointCheckConfig> fieldConfig11 = new ArrayList<>();
        DevicePointCheckConfig checkConfig111 = new DevicePointCheckConfig();
        checkConfig111.setField("工具");
        checkConfig111.setDataType("string");
        checkConfig111.setOrderNum(0);
        fieldConfig11.add(checkConfig111);
        DevicePointCheckConfig checkConfig112 = new DevicePointCheckConfig();
        checkConfig112.setField("位置");
        checkConfig112.setDataType("string");
        checkConfig112.setOrderNum(0);
        fieldConfig11.add(checkConfig112);
        DevicePointCheckConfig checkConfig113 = new DevicePointCheckConfig();
        checkConfig113.setField("异常状况");
        checkConfig113.setDataType("string");
        checkConfig113.setOrderNum(0);
        fieldConfig11.add(checkConfig113);
        DevicePointCheckConfig checkConfig114 = new DevicePointCheckConfig();
        checkConfig114.setField("异常数值");
        checkConfig114.setDataType("string");
        checkConfig114.setOrderNum(0);
        fieldConfig11.add(checkConfig114);
        DevicePointCheckConfig checkConfig115 = new DevicePointCheckConfig();
        checkConfig115.setField("视频");
        checkConfig115.setDataType("video");
        checkConfig115.setOrderNum(0);
        fieldConfig11.add(checkConfig115);
        map11.put("childData",fieldConfig11);
        result.add(map11);

        Map<String,Object> map12 = new HashMap<>();
        map12.put("type","工况");
        map12.put("item","运行周期");
        List<DevicePointCheckConfig> fieldConfig12 = new ArrayList<>();
//        DevicePointCheckConfig checkConfig121 = new DevicePointCheckConfig();
//        checkConfig121.setField("数值");
//        checkConfig121.setDataType("string");
//        checkConfig121.setOrderNum(0);
//        fieldConfig12.add(checkConfig121);
        map12.put("childData",fieldConfig12);
        result.add(map12);

        Map<String,Object> map13 = new HashMap<>();
        map13.put("type","工况");
        map13.put("item","介质");
        List<DevicePointCheckConfig> fieldConfig13 = new ArrayList<>();
//        DevicePointCheckConfig checkConfig131 = new DevicePointCheckConfig();
//        checkConfig131.setField("数值");
//        checkConfig131.setDataType("string");
//        checkConfig131.setOrderNum(0);
//        fieldConfig13.add(checkConfig131);
        map13.put("childData",fieldConfig13);
        result.add(map13);

        Map<String,Object> map14 = new HashMap<>();
        map14.put("type","附属件");
        map14.put("item","脱落");
        List<DevicePointCheckConfig> fieldConfig14 = new ArrayList<>();
        DevicePointCheckConfig checkConfig141 = new DevicePointCheckConfig();
        checkConfig141.setField("工具");
        checkConfig141.setDataType("string");
        checkConfig141.setOrderNum(0);
        fieldConfig14.add(checkConfig141);
        DevicePointCheckConfig checkConfig142 = new DevicePointCheckConfig();
        checkConfig142.setField("位置");
        checkConfig142.setDataType("string");
        checkConfig142.setOrderNum(0);
        fieldConfig14.add(checkConfig142);
        DevicePointCheckConfig checkConfig143 = new DevicePointCheckConfig();
        checkConfig143.setField("异常状况");
        checkConfig143.setDataType("string");
        checkConfig143.setOrderNum(0);
        fieldConfig14.add(checkConfig143);
        DevicePointCheckConfig checkConfig144 = new DevicePointCheckConfig();
        checkConfig144.setField("异常数值");
        checkConfig144.setDataType("string");
        checkConfig144.setOrderNum(0);
        fieldConfig14.add(checkConfig144);
        DevicePointCheckConfig checkConfig145 = new DevicePointCheckConfig();
        checkConfig145.setField("图片");
        checkConfig145.setDataType("photo");
        checkConfig145.setOrderNum(0);
        fieldConfig14.add(checkConfig145);
        map14.put("childData",fieldConfig14);
        result.add(map14);

        Map<String,Object> map15 = new HashMap<>();
        map15.put("type","附属件");
        map15.put("item","干涉");
        List<DevicePointCheckConfig> fieldConfig15 = new ArrayList<>();
        DevicePointCheckConfig checkConfig151 = new DevicePointCheckConfig();
        checkConfig151.setField("工具");
        checkConfig151.setDataType("string");
        checkConfig151.setOrderNum(0);
        fieldConfig15.add(checkConfig151);
        DevicePointCheckConfig checkConfig152 = new DevicePointCheckConfig();
        checkConfig152.setField("位置");
        checkConfig152.setDataType("string");
        checkConfig152.setOrderNum(0);
        fieldConfig15.add(checkConfig152);
        DevicePointCheckConfig checkConfig153 = new DevicePointCheckConfig();
        checkConfig153.setField("异常状况");
        checkConfig153.setDataType("string");
        checkConfig153.setOrderNum(0);
        fieldConfig15.add(checkConfig153);
        DevicePointCheckConfig checkConfig154 = new DevicePointCheckConfig();
        checkConfig154.setField("异常数值");
        checkConfig154.setDataType("string");
        checkConfig154.setOrderNum(0);
        fieldConfig15.add(checkConfig154);
        DevicePointCheckConfig checkConfig155 = new DevicePointCheckConfig();
        checkConfig155.setField("图片");
        checkConfig155.setDataType("photo");
        checkConfig155.setOrderNum(0);
        fieldConfig15.add(checkConfig155);
        map15.put("childData",fieldConfig15);
        result.add(map15);

        Map<String,Object> map16 = new HashMap<>();
        map16.put("type","附属件");
        map16.put("item","其他");
        List<DevicePointCheckConfig> fieldConfig16 = new ArrayList<>();
        DevicePointCheckConfig checkConfig161 = new DevicePointCheckConfig();
        checkConfig161.setField("工具");
        checkConfig161.setDataType("string");
        checkConfig161.setOrderNum(0);
        fieldConfig16.add(checkConfig161);
        DevicePointCheckConfig checkConfig162 = new DevicePointCheckConfig();
        checkConfig162.setField("位置");
        checkConfig162.setDataType("string");
        checkConfig162.setOrderNum(0);
        fieldConfig16.add(checkConfig162);
        DevicePointCheckConfig checkConfig163 = new DevicePointCheckConfig();
        checkConfig163.setField("异常状况");
        checkConfig163.setDataType("string");
        checkConfig163.setOrderNum(0);
        fieldConfig16.add(checkConfig163);
        DevicePointCheckConfig checkConfig164 = new DevicePointCheckConfig();
        checkConfig164.setField("异常数值");
        checkConfig164.setDataType("string");
        checkConfig164.setOrderNum(0);
        fieldConfig16.add(checkConfig164);
        DevicePointCheckConfig checkConfig165 = new DevicePointCheckConfig();
        checkConfig165.setField("图片");
        checkConfig165.setDataType("photo");
        checkConfig165.setOrderNum(0);
        fieldConfig16.add(checkConfig165);
        map16.put("childData",fieldConfig16);
        result.add(map16);
        return result;
    }
}
