package com.ruoyi.system.service.impl;

import com.ruoyi.common.annotation.UserDataIsolation;
import com.ruoyi.common.exception.CustomException;
import com.ruoyi.system.domain.OfficialAccountTemplate;
import com.ruoyi.system.mapper.OfficialAccountTemplateMapper;
import com.ruoyi.system.service.IOfficialAccountTemplateService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class OfficialAccountTemplateServiceImpl implements IOfficialAccountTemplateService {
    @Autowired
    private OfficialAccountTemplateMapper officialAccountTemplateMapper;

    @Override
    @UserDataIsolation(tableAlias = "t1")
    public List<OfficialAccountTemplate> queryList(OfficialAccountTemplate officialAccountTemplate) {
        return officialAccountTemplateMapper.queryList(officialAccountTemplate);
    }

    @Override
    @UserDataIsolation(tableAlias = "t1")
    public OfficialAccountTemplate getOne(OfficialAccountTemplate officialAccountTemplate) {
        return officialAccountTemplateMapper.getOne(officialAccountTemplate);
    }

    @Override
    public Boolean add(OfficialAccountTemplate officialAccountTemplate) {
        return officialAccountTemplateMapper.add(officialAccountTemplate) > 0;
    }

    @Override
    @UserDataIsolation
    public Boolean update(OfficialAccountTemplate officialAccountTemplate) {
        return officialAccountTemplateMapper.update(officialAccountTemplate) > 0;
    }

    @Override
    @UserDataIsolation
    public Boolean delete(OfficialAccountTemplate officialAccountTemplate) {
        return officialAccountTemplateMapper.delete(officialAccountTemplate) > 0;
    }

    @Override
    @UserDataIsolation
    public void checkTemplateCodeExist(OfficialAccountTemplate officialAccountTemplate) {
        OfficialAccountTemplate template = officialAccountTemplateMapper.checkTemplateCodeExist(officialAccountTemplate);
        if (template == null){
            return;
        }
        if (officialAccountTemplate.getId() == null || officialAccountTemplate.getId().compareTo(template.getId()) != 0){
            throw new CustomException("该模板编码已存在");
        }
    }
}
