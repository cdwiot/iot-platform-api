package com.ruoyi.system.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.ruoyi.common.exception.CustomException;
import com.ruoyi.system.domain.TerminalDetail;
import com.ruoyi.system.domain.TerminalEnum;
import com.ruoyi.system.domain.vo.QueryVo;
import com.ruoyi.system.domain.vo.TerminalDetailVO;
import com.ruoyi.system.mapper.TerminalDetailMapper;
import com.ruoyi.system.service.ITerminalDetailService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@Slf4j
public class TerminalDetailServiceImpl implements ITerminalDetailService {

    @Autowired
    private TerminalDetailMapper terminalDetailMapper;
    @Override
    public List<Map<String, Object>> getTerminalParamEnum(QueryVo queryVo) {
        List<TerminalEnum> terminalParamEnum = terminalDetailMapper.getTerminalParamEnum(queryVo);
        if (CollUtil.isEmpty(terminalParamEnum)){
            return new ArrayList<>();
        }
        Map<String, List<TerminalEnum>> collect = terminalParamEnum.stream().collect(Collectors.groupingBy(TerminalEnum::getType));
        List<Map<String,Object>> result = new ArrayList<>();
        for (Map.Entry<String, List<TerminalEnum>> entry : collect.entrySet()) {
            String type = entry.getKey();
            List<TerminalEnum> value = entry.getValue();
            Map<String,Object> map = new HashMap<>();
            map.put("type",type);
            map.put("childDate",value);
            result.add(map);
        }
        return result;
    }

    @Override
    public List<TerminalDetailVO> queryTerminalDetail(QueryVo queryVo) {
        List<TerminalDetailVO> terminalDetailVOS = terminalDetailMapper.queryTerminal(queryVo);
        if (CollUtil.isEmpty(terminalDetailVOS)){
            return new ArrayList<>();
        }
        Long companyId = queryVo.filters.getLong("company_id");
        for (TerminalDetailVO detailVO : terminalDetailVOS) {
            queryVo.filters.clear();
            queryVo.filters.put("company_id",companyId);
            queryVo.filters.put("terminal_id",detailVO.getTerminalId());
            List<TerminalDetail> terminalDetails = terminalDetailMapper.queryTerminalDetail(queryVo);
            if (CollUtil.isEmpty(terminalDetails)){
                terminalDetails = new ArrayList<>();
            }
            detailVO.setTerminalDetails(terminalDetails);
        }
        return terminalDetailVOS;
    }

    @Override
    public boolean add(TerminalDetail terminalDetail) {
        return terminalDetailMapper.add(terminalDetail) > 0;
    }

    @Override
    public boolean update(TerminalDetail terminalDetail) {
        return terminalDetailMapper.update(terminalDetail) > 0;
    }

    @Override
    public void checkIsExit(TerminalDetail terminalDetail) {
        TerminalDetail result = terminalDetailMapper.checkIsExit(terminalDetail);
        if (result == null){
            return;
        }
        if (terminalDetail.getId() == null || result.getId().compareTo(terminalDetail.getId()) != 0) {
            throw new CustomException("该属性值已存在");
        }
    }
}
