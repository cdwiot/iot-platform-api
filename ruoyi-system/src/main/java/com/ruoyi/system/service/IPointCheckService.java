package com.ruoyi.system.service;

import com.ruoyi.system.domain.PointCheck;
import com.ruoyi.system.domain.vo.QueryVo;

import java.util.List;
import java.util.Map;

public interface IPointCheckService {

    /**
     * 点检计划分页查询
     *
     * @param pointCheck
     * @return
     */
    List<PointCheck> queryList(PointCheck pointCheck);
    List<PointCheck> queryGroupList(PointCheck pointCheck);

    /**
     * 点检计划详情
     *
     * @param pointCheck
     * @return
     */
    PointCheck getOne(PointCheck pointCheck);

    /**
     * 添加
     *
     * @param pointCheck
     * @return
     */
    Boolean add(PointCheck pointCheck);
    Boolean getTask(PointCheck pointCheck);

    /**
     * 编辑
     *
     * @param pointCheck
     * @return
     */
    Boolean update(PointCheck pointCheck);
    Boolean updateInfo(PointCheck pointCheck);

    /**
     * 删除
     *
     * @param pointCheck
     * @return
     */
    Boolean delete(PointCheck pointCheck);

    /**
     * 根据名称和系统字段检查是否重复
     *
     * @param pointCheck
     * @return
     */
    void checkIsExist(PointCheck pointCheck);

    /**
     * 验证添加指派人角色权限
     * 客户只能添加巡检工角色用户
     * 非客户只能添加现场工程师角色用户
     *
     * @param pointCheck
     */
    void checkExecutorStatus(PointCheck pointCheck);

    /**
     * 绑定计划时间
     *
     * @param pointCheck
     */
    void addPointCheckDate(PointCheck pointCheck);

    /**
     * 获取该公司下面的用户信息
     *
     * @param queryVo
     * @return
     */
    List<Map<String, Object>> queryUserInfo(QueryVo queryVo);

    /**
     * 定时创建每日点检任务(每天凌晨两点半)
     */
    void createPointCheckTask();

    /**
     * 查询授权项目枚举
     *
     * @param queryVo
     * @return
     */
    List<Map<String, String>> queryAuthorizeEnum(QueryVo queryVo);
}
