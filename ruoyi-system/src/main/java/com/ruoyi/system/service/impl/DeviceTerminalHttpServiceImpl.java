package com.ruoyi.system.service.impl;

import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.annotation.UserDataIsolation;
import com.ruoyi.system.domain.HttpCredentials;
import com.ruoyi.system.domain.SysCompanyThingsboard;
import com.ruoyi.system.domain.vo.QueryVo;
import com.ruoyi.system.mapper.HttpCredentialsMapper;
import com.ruoyi.system.service.IDeviceTerminalHttpService;
import com.ruoyi.system.service.IThingsBoardTenantService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class DeviceTerminalHttpServiceImpl implements IDeviceTerminalHttpService
{
    @Autowired
    HttpCredentialsMapper httpCredentialsMapper;

    @Autowired
    IThingsBoardTenantService thingsBoardTenantService;

    @Override
    @UserDataIsolation(tableAlias = "tbl_http_credentials")
    public Map<String, Object> findExistance(HttpCredentials httpCredentials) {
        return httpCredentialsMapper.findExistance(httpCredentials);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Integer createCredentials(HttpCredentials httpCredentials, SysCompanyThingsboard sysCompanyThingsboard) {
        Integer id = null;
        try {
            Integer affected = httpCredentialsMapper.create(httpCredentials);
            if (affected > 0) {
                JSONObject fields = new JSONObject();
                fields.put("deviceId", httpCredentials.getTbDeviceId());
                JSONObject credentials = new JSONObject();
                credentials.put("accessToken", httpCredentials.getAccessToken());
                fields.put("credentials", credentials);
                thingsBoardTenantService.saveHttpDeviceCredentials(fields, sysCompanyThingsboard);
                id = httpCredentials.getId();
            }
        } catch (Exception e) {
            id = null;
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            log.info("",e);
            StackTraceElement[] traces = e.getStackTrace();
            for (StackTraceElement trace : traces) {
                log.debug(trace.toString());
            }
        }
        return id;
    }

    @Override
    @UserDataIsolation(tableAlias = "mc")
    public HttpCredentials retrieveCredentials(QueryVo queryVo) {
        HttpCredentials httpCredentials = httpCredentialsMapper.retrieve(queryVo);
        return httpCredentials;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    @UserDataIsolation(tableAlias = "tbl_http_credentials")
    public Boolean updateCredentials(HttpCredentials httpCredentials, SysCompanyThingsboard sysCompanyThingsboard) {
        Boolean done = false;
        try {
            Integer affected = httpCredentialsMapper.update(httpCredentials);
            if (affected > 0) {
                JSONObject fields = new JSONObject();
                fields.put("deviceId", httpCredentials.getTbDeviceId());
                JSONObject credentials = new JSONObject();
                credentials.put("accessToken", httpCredentials.getAccessToken());
                fields.put("credentials", credentials);
                thingsBoardTenantService.saveHttpDeviceCredentials(fields, sysCompanyThingsboard);
                done = true;
            }
        } catch (Exception e) {
            done = false;
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            log.info("",e);
            StackTraceElement[] traces = e.getStackTrace();
            for (StackTraceElement trace : traces) {
                log.debug(trace.toString());
            }
        }
        return done;
    }
}
