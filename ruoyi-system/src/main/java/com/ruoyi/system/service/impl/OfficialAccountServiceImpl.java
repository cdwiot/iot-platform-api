package com.ruoyi.system.service.impl;

import com.ruoyi.common.annotation.UserDataIsolation;
import com.ruoyi.system.domain.OfficialAccount;
import com.ruoyi.system.mapper.OfficialAccountMapper;
import com.ruoyi.system.service.IOfficialAccountService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class OfficialAccountServiceImpl implements IOfficialAccountService {
    @Autowired
    private OfficialAccountMapper officialAccountMapper;
    @Override
    public List<OfficialAccount> queryList(OfficialAccount officialAccount) {
        return officialAccountMapper.queryList(officialAccount);
    }

    @Override
    public OfficialAccount getOne(OfficialAccount officialAccount) {
        return officialAccountMapper.getOne(officialAccount);
    }

    @Override
    @UserDataIsolation(tableAlias = "t1")
    public OfficialAccount getOfficialAccount(OfficialAccount officialAccount) {
        return officialAccountMapper.getOfficialAccount(officialAccount);
    }

    @Override
    @UserDataIsolation(tableAlias = "t1")
    public OfficialAccount getCompanyOne(OfficialAccount officialAccount) {
        return officialAccountMapper.getCompanyOne(officialAccount);
    }

    @Override
    public Boolean addOfficialAccount(OfficialAccount officialAccount) {
        return officialAccountMapper.addOfficialAccount(officialAccount) > 0;
    }

    @Override
    public Boolean updateOfficialAccount(OfficialAccount officialAccount) {
        return officialAccountMapper.updateOfficialAccount(officialAccount) > 0;
    }

    @Override
    public Boolean deleteOfficialAccount(OfficialAccount officialAccount) {
        return officialAccountMapper.deleteOfficialAccount(officialAccount) > 0;
    }
}
