package com.ruoyi.system.service;

import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.core.domain.AjaxResult;

public interface ISpecificFunctionService {
    public AjaxResult productTelemetryIncrement(JSONObject request);
    public AjaxResult deviceTelemetryIncrement(JSONObject request);
}
