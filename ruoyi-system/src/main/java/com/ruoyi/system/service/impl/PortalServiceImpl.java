package com.ruoyi.system.service.impl;

import com.ruoyi.system.domain.Portal;
import com.ruoyi.system.domain.vo.QueryVo;
import com.ruoyi.system.mapper.PortalMapper;
import com.ruoyi.system.service.IPortalService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class PortalServiceImpl implements IPortalService
{
    @Autowired
    private PortalMapper portalMapper;

    @Override
    public Portal load(QueryVo queryVo) {
        Portal portal = portalMapper.retrieve(queryVo);
        if (portal == null) {
            portal = new Portal();
        }
        return portal;
    }

    @Override
    public Boolean save(Portal portal) {
        Boolean done = false;
        try {
            Integer affected = 0;
            QueryVo queryVo = new QueryVo();
            queryVo.filters.put("company_id", portal.getCompanyId());
            Portal target = portalMapper.retrieve(queryVo);
            if (target == null) {
                affected = portalMapper.create(portal);
            } else {
                portal.setId(target.getId());
                affected = portalMapper.update(portal);
            }
            if (affected > 0) {
                done = true;
            }
        } catch (Exception e) {
            log.info("",e);
            StackTraceElement[] traces = e.getStackTrace();
            for (StackTraceElement trace : traces) {
                log.debug(trace.toString());
            }
        }
        return done;
    }
}
