package com.ruoyi.system.service;

import com.ruoyi.common.core.domain.entity.SysUser;

/**
 * 自定义数据权限
 */
public interface ICustomPermissionService {

    /**
     * 获取自定义的项目授权权限
     * @param tableAlias 表别名
     * @param field 字段
     * @param sysUser 当前登录用户
     * @return
     */
    String getProjectAuthorizePermission(String tableAlias, String field, SysUser sysUser);
}
