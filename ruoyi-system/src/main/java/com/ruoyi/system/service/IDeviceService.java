package com.ruoyi.system.service;

import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ruoyi.system.domain.Device;
import com.ruoyi.system.domain.SysCompanyThingsboard;
import com.ruoyi.system.domain.vo.QueryVo;

public interface IDeviceService
{
    public Map<String, Object> parseInjectionItem(JSONObject item, Long companyId);
    public Map<String, Object> findExistance(Device device);
    public Integer create(Device device, SysCompanyThingsboard sysCompanyThingsboard);
    public List<Device> index(QueryVo queryVo);
    public List<Map<String, Object>> provinceSummary(QueryVo queryVo);
    public List<Map<String, Object>> enumerate(QueryVo queryVo);
    public List<Map<Integer, String>> enumerateAvailable(QueryVo queryVo);
    public Device retrieve(QueryVo queryVo);
    public Boolean initDeviceCheckConfig();
    public Boolean initDeviceField();
    public Boolean update(Device device);
    public Boolean discard(QueryVo queryVo);
    public Boolean delete(QueryVo queryVo, SysCompanyThingsboard sysCompanyThingsboard);
    public Integer addToProject(List<Integer> deviceIds, Integer projectId);
    public Integer removeFromProject(List<Integer> deviceIds);
    public JSONObject connectivity(Device device, SysCompanyThingsboard sysCompanyThingsboard);
    public JSONObject latestTimeSeries(Device device, SysCompanyThingsboard sysCompanyThingsboard);
    public JSONObject timeSeriesMeta(Device device, SysCompanyThingsboard sysCompanyThingsboard);
    public JSONArray historicalTimeSeries(Device device, QueryVo queryVo, SysCompanyThingsboard sysCompanyThingsboard);
    public List<Map<String, Object>> summary(QueryVo queryVo, Map<String, Object> options);
    public List<Map<String, Object>> deviceAndTerminalCountByMonth(QueryVo queryVo);
}
