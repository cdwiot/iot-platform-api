package com.ruoyi.system.service;

import java.util.Map;

import com.ruoyi.system.domain.Lwm2mCredentials;
import com.ruoyi.system.domain.SysCompanyThingsboard;
import com.ruoyi.system.domain.vo.QueryVo;

public interface IDeviceTerminalLwm2mService
{
    public Map<String, Object> findExistance(Lwm2mCredentials lwm2mCredentials);
    public Integer createCredentials(Lwm2mCredentials lwm2mCredentials, SysCompanyThingsboard sysCompanyThingsboard);
    public Lwm2mCredentials retrieveCredentials(QueryVo queryVo);
    public Boolean updateCredentials(Lwm2mCredentials lwm2mCredentials, SysCompanyThingsboard sysCompanyThingsboard);
}
