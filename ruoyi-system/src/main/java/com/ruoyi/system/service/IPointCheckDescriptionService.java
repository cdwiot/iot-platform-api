package com.ruoyi.system.service;

import com.ruoyi.system.domain.PointCheckDescription;
import com.ruoyi.system.domain.vo.PointCheckDescriptionVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Set;

public interface IPointCheckDescriptionService {

    /**
     * 文字描述分页查询
     *
     * @param pointCheckDescription
     * @return
     */
    List<PointCheckDescription> queryList(PointCheckDescription pointCheckDescription);

    /**
     * 获取详情
     *
     * @param pointCheckDescription
     * @return
     */
    PointCheckDescription getOne(PointCheckDescription pointCheckDescription);

    /**
     * 批量添加
     *
     * @param pointCheckDescriptions
     * @return
     */
    int addBatch(List<PointCheckDescription> pointCheckDescriptions);

    /**
     * 修改
     *
     * @param pointCheckDescription
     * @return
     */
    Boolean update(PointCheckDescription pointCheckDescription);

    /**
     * 批量修改
     *
     * @param pointCheckDescriptions
     * @return
     */
    void updateBatch(List<PointCheckDescription> pointCheckDescriptions);

    /**
     * 删除
     *
     * @param pointCheckDescription
     * @return
     */
    Boolean delete(PointCheckDescription pointCheckDescription);

    /**
     * 根据产品检查Id查询文字描述信息
     *
     * @param detailId
     * @return
     */
    List<PointCheckDescriptionVO> queryDescriptionVO(Set<Integer> detailId);
}
