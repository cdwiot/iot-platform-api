package com.ruoyi.system.service;

import com.ruoyi.system.domain.OfficialAccount;

import java.util.List;

public interface IOfficialAccountService {

    /**
     * 查询配置列表
     *
     * @param officialAccount
     * @return
     */
    List<OfficialAccount> queryList(OfficialAccount officialAccount);

    /**
     * 查询单个配置(总账号)
     *
     * @param officialAccount
     * @return
     */
    OfficialAccount getOne(OfficialAccount officialAccount);

    /**
     * 根据数据权限获取账户信息
     *
     * @param officialAccount
     * @return
     */
    OfficialAccount getOfficialAccount(OfficialAccount officialAccount);

    /**
     * 默认查询大账户信息
     *
     * @param officialAccount
     * @return
     */
    OfficialAccount getCompanyOne(OfficialAccount officialAccount);

    /**
     * 添加配置
     *
     * @param officialAccount
     * @return
     */
    Boolean addOfficialAccount(OfficialAccount officialAccount);

    /**
     * 修改配置
     *
     * @param officialAccount
     * @return
     */
    Boolean updateOfficialAccount(OfficialAccount officialAccount);

    /**
     * 删除配置
     *
     * @param officialAccount
     * @return
     */
    Boolean deleteOfficialAccount(OfficialAccount officialAccount);


}
