package com.ruoyi.system.service;

import com.alibaba.fastjson.JSONObject;

public interface IEmailService {
    public void send(String tid, JSONObject request);
}
