package com.ruoyi.system.service;

import com.ruoyi.system.domain.ProjectDeviceGroup;
import com.ruoyi.system.domain.vo.ProjectGroupDeviceResultVO;
import com.ruoyi.system.domain.vo.ProjectGroupParamVO;

import java.util.List;

/**
 * 产品分组Service接口
 *
 * @author ruoyi
 * @date 2021-08-10
 */
public interface IProjectDeviceGroupService {
    /**
     * 查询产品分组
     *
     * @param id 产品分组ID
     * @return 产品分组
     */
    public ProjectDeviceGroup selectTblProjectDeviceGroupById(Integer id);

    /**
     * 查询产品分组列表
     *
     * @param projectDeviceGroup 产品分组
     * @return 产品分组集合
     */
    public List<ProjectDeviceGroup> selectTblProjectDeviceGroupList(ProjectDeviceGroup projectDeviceGroup);

    /**
     * 新增产品分组
     *
     * @param projectDeviceGroup 产品分组
     * @return 结果
     */
    public Boolean saveProjectDeviceGroup(ProjectDeviceGroup projectDeviceGroup);

    /**
     * 修改产品分组
     *
     * @param projectDeviceGroup 产品分组
     * @return 结果
     */
    public Boolean updateProjectDeviceGroup(ProjectDeviceGroup projectDeviceGroup);

    /**
     * 绑定产品
     *
     * @param projectDeviceGroup
     * @return
     */
    public void bindDevice(ProjectDeviceGroup projectDeviceGroup);

//    /**
//     * 批量删除产品分组
//     *
//     * @param ids 需要删除的产品分组ID
//     * @return 结果
//     */
//    public int deleteTblProjectDeviceGroupByIds(Long[] ids);

    /**
     * 删除产品分组信息
     *
     * @param id 产品分组ID
     * @return 结果
     */
    public Boolean deleteProjectDeviceGroupById(Integer id);

//    public List<TblDevice> selectTblDeviceGroupByGid(Long groupId);
//
//    /**
//     * 添加产品分组
//     * @param projectDeviceGroup 产品分组添加信息
//     * @return
//     */
//    Boolean addGroupDevice(ProjectDeviceGroup projectDeviceGroup);

    /**
     * 查询需要绑定的产品
     *
     * @param projectGroupParamVO
     * @return
     */
    public ProjectGroupDeviceResultVO queryBindDevice(ProjectGroupParamVO projectGroupParamVO);
}