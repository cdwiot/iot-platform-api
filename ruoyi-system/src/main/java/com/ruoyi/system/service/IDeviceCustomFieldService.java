package com.ruoyi.system.service;

import com.ruoyi.system.domain.DeviceCustomField;
import com.ruoyi.system.domain.vo.QueryVo;

import java.util.List;

public interface IDeviceCustomFieldService {
    /**
     * 分页查询
     *
     * @param queryVo
     * @return
     */
    List<DeviceCustomField> queryPageList(QueryVo queryVo);

    /**
     * 添加
     *
     * @param deviceCustomField
     * @return
     */
    boolean add(DeviceCustomField deviceCustomField);

    /**
     * 查询详情
     *
     * @param queryVo
     * @return
     */
    DeviceCustomField getOne(QueryVo queryVo);

    /**
     * 修改
     *
     * @param deviceCustomField
     * @return
     */
    boolean update(DeviceCustomField deviceCustomField);

    /**
     * 删除
     *
     * @param queryVo
     * @return
     */
    boolean delete(QueryVo queryVo);

    /**
     * 验证子定义名称是否重复
     *
     * @param deviceCustomField
     * @return
     */
    void checkIsExist(DeviceCustomField deviceCustomField);
}
