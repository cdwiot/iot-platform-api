package com.ruoyi.system.service;

import com.ruoyi.system.domain.PointCheckTaskDeal;
import com.ruoyi.system.domain.vo.PointCheckTaskDealVO;

import java.util.List;
import java.util.Set;

public interface IPointCheckTaskDealService {

    /**
     * 处理方式的列表查询
     *
     * @param pointCheckTaskDeal
     * @return
     */
    List<PointCheckTaskDeal> queryList(PointCheckTaskDeal pointCheckTaskDeal);

    /**
     * 处理方式的详情
     *
     * @param pointCheckTaskDeal
     * @return
     */
    PointCheckTaskDeal getOne(PointCheckTaskDeal pointCheckTaskDeal);

    /**
     * 添加
     *
     * @param pointCheckTaskDeal
     * @return
     */
    Boolean add(PointCheckTaskDeal pointCheckTaskDeal);

    /**
     * 修改
     *
     * @param pointCheckTaskDeal
     * @return
     */
    Boolean update(PointCheckTaskDeal pointCheckTaskDeal);

    /**
     * 删除
     *
     * @param pointCheckTaskDeal
     * @return
     */
    Boolean delete(PointCheckTaskDeal pointCheckTaskDeal);

    /**
     * 根据产品检查Id查询处理方式
     *
     * @param detailIds
     * @return
     */
    List<PointCheckTaskDealVO> queryDeviceDealVO(Set<Integer> detailIds);
}
