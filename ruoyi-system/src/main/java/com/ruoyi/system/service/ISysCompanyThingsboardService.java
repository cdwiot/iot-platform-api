package com.ruoyi.system.service;

import java.util.List;

import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.system.domain.SysCompanyThingsboard;

/**
 * 公司与thingsboard关联Service接口
 * 
 * @author ruoyi
 * @date 2021-08-02
 */
public interface ISysCompanyThingsboardService 
{
    /**
     * 查询公司与thingsboard关联
     * 
     * @param id 公司与thingsboard关联ID
     * @return 公司与thingsboard关联
     */
    public SysCompanyThingsboard selectSysCompanyThingsboardById(Long id);

    /**
     * 查询公司与thingsboard关联列表
     * 
     * @param sysCompanyThingsboard 公司与thingsboard关联
     * @return 公司与thingsboard关联集合
     */
    public List<SysCompanyThingsboard> selectSysCompanyThingsboardList(SysCompanyThingsboard sysCompanyThingsboard);

    /**
     * 新增公司与thingsboard关联
     * 
     * @param sysCompanyThingsboard 公司与thingsboard关联
     * @return 结果
     */
    public int insertSysCompanyThingsboard(SysCompanyThingsboard sysCompanyThingsboard);

    /**
     * 修改公司与thingsboard关联
     * 
     * @param sysCompanyThingsboard 公司与thingsboard关联
     * @return 结果
     */
    public int updateSysCompanyThingsboard(SysCompanyThingsboard sysCompanyThingsboard);

    /**
     * 批量删除公司与thingsboard关联
     * 
     * @param ids 需要删除的公司与thingsboard关联ID
     * @return 结果
     */
    public int deleteSysCompanyThingsboardByIds(Long[] ids);

    /**
     * 删除公司与thingsboard关联信息
     * 
     * @param id 公司与thingsboard关联ID
     * @return 结果
     */
    public int deleteSysCompanyThingsboardById(Long id);
    
    /**
     * 根据用户id获取用户所属公司绑定的thingsboard信息
     * @param userId
     * @return 用户所属公司绑定的thingsboard信息
     */
    public SysCompanyThingsboard selectSysCompanyThingsboardByUserId(Long userId);
    
    /**
     * 根据用户获取用户所属公司绑定的thingsboard信息
     * @param user
     * @return 用户所属公司绑定的thingsboard信息
     */
    public SysCompanyThingsboard selectSysCompanyThingsboardByUser(SysUser user);
    public SysCompanyThingsboard selectSysCompanyThingsboardByCompanyId(Long companyId);
}
