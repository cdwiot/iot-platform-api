package com.ruoyi.system.service.impl;

import java.util.List;
import java.util.Map;

import com.ruoyi.common.annotation.UserDataIsolation;
import com.ruoyi.system.domain.MonitorRule;
import com.ruoyi.system.domain.ProjectScenarioRule;
import com.ruoyi.system.domain.vo.QueryVo;
import com.ruoyi.system.mapper.ProjectScenarioRuleMapper;
import com.ruoyi.system.service.IMonitorRuleService;
import com.ruoyi.system.service.IProjectScenarioRuleService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ProjectScenarioRuleServiceImpl implements IProjectScenarioRuleService
{
    @Autowired
    ProjectScenarioRuleMapper projectScenarioRuleMapper;

    @Autowired
    IMonitorRuleService monitorRuleService;

    @Override
    @UserDataIsolation(tableAlias = "tbl_project_scenario_rule")
    public Map<String, Object> findExistance(ProjectScenarioRule projectScenarioRule) {
        return projectScenarioRuleMapper.findExistance(projectScenarioRule);
    }

    @Override
    public Integer create(ProjectScenarioRule projectScenarioRule) {
        Integer id = null;
        try {
            Integer affected = projectScenarioRuleMapper.create(projectScenarioRule);
            if (affected > 0) {
                id = projectScenarioRule.getId();
            }
        } catch (Exception e) {
            log.info("",e);
            StackTraceElement[] traces = e.getStackTrace();
            for (StackTraceElement trace : traces) {
                log.debug(trace.toString());
            }
        }
        return id;
    }

    @Override
    @UserDataIsolation(tableAlias = "psr")
    public List<ProjectScenarioRule> index(QueryVo queryVo) {
        List<ProjectScenarioRule> projectScenarioRules = projectScenarioRuleMapper.index(queryVo);
        for (ProjectScenarioRule projectScenarioRule : projectScenarioRules) {
            log.debug("{}", projectScenarioRule);
            projectScenarioRule.setActions(null);
        }
        return projectScenarioRules;
    }

    @Override
    @UserDataIsolation(tableAlias = "mr")
    public List<Map<String, Object>> available(QueryVo queryVo) {
        List<Map<String, Object>> monitorRules = projectScenarioRuleMapper.available(queryVo);
        return monitorRules;
    }

    @Override
    @UserDataIsolation(tableAlias = "tbl_project_scenario_rule")
    public ProjectScenarioRule retrieve(QueryVo queryVo) {
        ProjectScenarioRule projectScenarioRule = projectScenarioRuleMapper.retrieve(queryVo);
        if (projectScenarioRule != null) {
            QueryVo subQueryVo = new QueryVo();
            subQueryVo.filters.put("id", projectScenarioRule.getMonitorRuleId());
            MonitorRule monitorRule = monitorRuleService.retrieve(subQueryVo);
            projectScenarioRule.setMonitorRule(monitorRule);
            projectScenarioRule.setActions(null);
        }
        return projectScenarioRule;
    }

    @Override
    @UserDataIsolation(tableAlias = "tbl_project_scenario_rule")
    public Boolean update(ProjectScenarioRule projectScenarioRule) {
        Boolean done = false;
        try {
            Integer affected = projectScenarioRuleMapper.update(projectScenarioRule);
            if (affected > 0) {
                done = true;
            }
        } catch (Exception e) {
            log.info("",e);
            StackTraceElement[] traces = e.getStackTrace();
            for (StackTraceElement trace : traces) {
                log.debug(trace.toString());
            }
        }
        return done;
    }

    @Override
    @UserDataIsolation(tableAlias = "tbl_project_scenario_rule")
    public Boolean delete(QueryVo queryVo) {
        Boolean done = false;
        try {
            Integer affected = projectScenarioRuleMapper.delete(queryVo);
            if (affected > 0) {
                done = true;
            }
        } catch (Exception e) {
            log.info("",e);
            StackTraceElement[] traces = e.getStackTrace();
            for (StackTraceElement trace : traces) {
                log.debug(trace.toString());
            }
        }
        return done;
    }
}
