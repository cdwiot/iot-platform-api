package com.ruoyi.system.service.impl;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.ruoyi.common.annotation.UserDataIsolation;
import com.ruoyi.system.domain.ProductChart;
import com.ruoyi.system.domain.vo.QueryVo;
import com.ruoyi.system.mapper.ProjectWithoutProductChartMapper;
import com.ruoyi.system.service.IProductChartService;
import com.ruoyi.system.service.IProjectWithoutProductChartService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ProjectWithoutProductChartServiceImpl implements IProjectWithoutProductChartService
{
    @Autowired
    ProjectWithoutProductChartMapper projectWithoutProductChartMapper;

    @Autowired
    IProductChartService productChartService;

    private Integer block(List<Integer> productChartIdList, Integer id) {
        return projectWithoutProductChartMapper.block(productChartIdList, id);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean set(List<Integer> productChartIdList, Integer id) {
        Boolean done = false;
        try {
            projectWithoutProductChartMapper.clear(id);
            if (!productChartIdList.isEmpty()) {
                block(productChartIdList, id);
            }
            done = true;
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            log.info("",e);
            StackTraceElement[] traces = e.getStackTrace();
            for (StackTraceElement trace : traces) {
                log.debug(trace.toString());
            }
            done = false;
        }
        return done;
    }

    @Override
    @UserDataIsolation(tableAlias = "pc")
    public List<ProductChart> get(QueryVo queryVo) {
        List<ProductChart> productCharts = projectWithoutProductChartMapper.get(queryVo);
        for (ProductChart productChart : productCharts) {
            productChart.setIdentifiers(Arrays.asList(productChart.getIdentifiersString().split(",")));
        }
        return productCharts;
    }

    @Override
    @UserDataIsolation(tableAlias = "pc")
    public List<Map<String, Object>> availability(QueryVo queryVo) {
        List<Map<String, Object>> result = projectWithoutProductChartMapper.availability(queryVo);
        return result;
    }

}
