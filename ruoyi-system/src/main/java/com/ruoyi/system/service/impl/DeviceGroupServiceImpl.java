package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.common.exception.CustomException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.DeviceGroupMapper;
import com.ruoyi.system.domain.DeviceGroup;
import com.ruoyi.system.service.IDeviceGroupService;

/**
 * 产品分组和产品关联Service业务层处理
 *
 * @author ruoyi
 * @date 2021-08-10
 */
@SuppressWarnings("AlibabaLowerCamelCaseVariableNaming")
@Service
public class DeviceGroupServiceImpl implements IDeviceGroupService {
    @Autowired
    private DeviceGroupMapper deviceGroupMapper;

    /**
     * 查询产品分组和产品关联
     *
     * @param groupId 产品分组和产品关联ID
     * @return 产品分组和产品关联
     */
    @Override
    public DeviceGroup selectTblDeviceGroupById(Long groupId) {
        return deviceGroupMapper.selectTblDeviceGroupById(groupId);
    }

    /**
     * 查询产品分组和产品关联列表
     *
     * @param deviceGroup 产品分组和产品关联
     * @return 产品分组和产品关联
     */
    @Override
    public List<DeviceGroup> selectTblDeviceGroupList(DeviceGroup deviceGroup) {
        return deviceGroupMapper.selectTblDeviceGroupList(deviceGroup);
    }

    /**
     * 新增产品分组和产品关联
     *
     * @param deviceGroup 产品分组和产品关联
     * @return 结果
     */
    @Override
    public int insertTblDeviceGroup(DeviceGroup deviceGroup) {
        if (deviceGroup == null || deviceGroup.getDeviceId() == null || deviceGroup.getGroupId() == null){
            throw new CustomException("产品Id或项目分组Id不能为空");
        }
        return deviceGroupMapper.insertTblDeviceGroup(deviceGroup);
    }

    /**
     * 修改产品分组和产品关联
     *
     * @param deviceGroup 产品分组和产品关联
     * @return 结果
     */
    @Override
    public int updateTblDeviceGroup(DeviceGroup deviceGroup) {
        return deviceGroupMapper.updateTblDeviceGroup(deviceGroup);
    }

    /**
     * 批量删除产品分组和产品关联
     *
     * @param groupIds 需要删除的产品分组和产品关联ID
     * @return 结果
     */
    @Override
    public int deleteTblDeviceGroupByIds(Long[] groupIds) {
        return deviceGroupMapper.deleteTblDeviceGroupByIds(groupIds);
    }

    /**
     * 删除产品分组和产品关联信息
     *
     * @param groupId 产品分组和产品关联ID
     * @return 结果
     */
    @Override
    public int deleteTblDeviceGroupById(Integer groupId) {
        return deviceGroupMapper.deleteTblDeviceGroupById(groupId);
    }

    @Override
    public int deleteTblDeviceGroupByGId(Long id) {
        return deviceGroupMapper.deleteTblDeviceGroupByGId(id);
    }
}