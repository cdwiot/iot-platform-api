package com.ruoyi.system.service.impl;

import java.util.List;
import java.util.Map;

import com.ruoyi.common.annotation.UserDataIsolation;
import com.ruoyi.system.domain.AlarmReceiver;
import com.ruoyi.system.domain.vo.QueryVo;
import com.ruoyi.system.mapper.AlarmReceiverMapper;
import com.ruoyi.system.service.IAlarmReceiverService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AlarmReceiverServiceImpl implements IAlarmReceiverService
{
    @Autowired
    private AlarmReceiverMapper alarmReceiverMapper;

    @Override
    @UserDataIsolation(tableAlias = "tbl_alarm_receiver")
    public void clear(QueryVo queryVo) {
        alarmReceiverMapper.clear(queryVo);
    }

    @Override
    public Integer set(List<AlarmReceiver> receivers) {
        QueryVo queryVo = new QueryVo();
        Integer affected = 0;
        try {
            queryVo.filters.put("rule_id", receivers.get(0).getRuleId());
            queryVo.filters.put("method", receivers.get(0).getMethod());
            clear(queryVo);
            log.info("{}", receivers);
            affected = alarmReceiverMapper.set(receivers);
        } catch (Exception e) {
            log.info("",e);
            StackTraceElement[] traces = e.getStackTrace();
            for (StackTraceElement trace : traces) {
                log.debug(trace.toString());
            }
        }
        return affected;
    }

    @Override
    @UserDataIsolation(tableAlias = "pa")
    public List<Map<String, Object>> get(QueryVo queryVo) {
        return alarmReceiverMapper.get(queryVo);
    }
}
