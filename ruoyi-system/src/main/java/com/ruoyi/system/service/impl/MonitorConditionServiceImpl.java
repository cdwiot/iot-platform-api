package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.common.annotation.UserDataIsolation;
import com.ruoyi.common.enums.TriggerType;
import com.ruoyi.common.enums.ComparisonOperator;
import com.ruoyi.common.enums.Connectivity;
import com.ruoyi.common.enums.TriggerSource;
import com.ruoyi.system.domain.MonitorCondition;
import com.ruoyi.system.domain.ProductEvent;
import com.ruoyi.system.domain.ProductTelemetry;
import com.ruoyi.system.domain.vo.QueryVo;
import com.ruoyi.system.mapper.MonitorConditionMapper;
import com.ruoyi.system.service.IMonitorConditionService;
import com.ruoyi.system.service.IProductEventService;
import com.ruoyi.system.service.IProductService;
import com.ruoyi.system.service.IProductTelemetryService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class MonitorConditionServiceImpl implements IMonitorConditionService
{
    @Autowired
    MonitorConditionMapper monitorConditionMapper;

    @Autowired
    IProductService productService;

    @Autowired
    IProductTelemetryService productTelemetryService;

    @Autowired
    IProductEventService productEventService;

    @Override
    public Integer create(MonitorCondition monitorCondition) {
        Integer id = null;
        try {
            Integer affected = monitorConditionMapper.create(monitorCondition);
            if (affected > 0) {
                id = monitorCondition.getId();
            }
        } catch (Exception e) {
            log.info("",e);
            StackTraceElement[] traces = e.getStackTrace();
            for (StackTraceElement trace : traces) {
                log.debug(trace.toString());
            }
        }
        return id;
    }

    @Override
    @UserDataIsolation(tableAlias = "mc")
    public List<MonitorCondition> index(QueryVo queryVo) {
        List<MonitorCondition> monitorConditions = monitorConditionMapper.index(queryVo);
        for (MonitorCondition monitorCondition : monitorConditions) {
            Integer triggerSource = monitorCondition.getTriggerSource();
            monitorCondition.setTriggerSourceString(TriggerSource.valueOfCode(triggerSource).getLabel());
            if (triggerSource.equals(TriggerSource.TELEMETRY.getCode())) {
                monitorCondition.setComparisonOperatorString(ComparisonOperator.valueOfCode(monitorCondition.getComparisonOperator()).getLabel());
                QueryVo subQueryVo = new QueryVo();
                subQueryVo.filters.put("id", monitorCondition.getTelemetryId());
                ProductTelemetry telemetry = productTelemetryService.retrieve(subQueryVo);
                monitorCondition.setTelemetry(telemetry);
            } else if (triggerSource.equals(TriggerSource.EVENT.getCode())) {
                ProductEvent event = new ProductEvent();
                event.setId(monitorCondition.getEventId());
                event = productEventService.getOne(event);
                monitorCondition.setEvent(event);
            } else if (triggerSource.equals(TriggerSource.CONNECTIVITY.getCode())) {
                monitorCondition.setConnectivityString(Connectivity.valueOfCode(monitorCondition.getConnectivity()).getLabel());
            }
            Integer repeatCount = monitorCondition.getRepeatCount();
            if (repeatCount == null) {
                monitorCondition.setTriggerType(TriggerType.DURATION.getCode());
                monitorCondition.setTriggerTypeString(TriggerType.DURATION.getLabel());
            } else {
                if (repeatCount == 1) {
                    monitorCondition.setTriggerType(TriggerType.SIMPLE.getCode());
                    monitorCondition.setTriggerTypeString(TriggerType.SIMPLE.getLabel());
                } else {
                    monitorCondition.setTriggerType(TriggerType.REPEAT.getCode());
                    monitorCondition.setTriggerTypeString(TriggerType.REPEAT.getLabel());
                }
            }
        }
        return monitorConditions;
    }

    @Override
    @UserDataIsolation(tableAlias = "mc")
    public MonitorCondition retrieve(QueryVo queryVo) {
        MonitorCondition monitorCondition = monitorConditionMapper.retrieve(queryVo);
        if (monitorCondition != null) {
            Integer triggerSource = monitorCondition.getTriggerSource();
            monitorCondition.setTriggerSourceString(TriggerSource.valueOfCode(triggerSource).getLabel());
            if (triggerSource.equals(TriggerSource.TELEMETRY.getCode())) {
                monitorCondition.setComparisonOperatorString(ComparisonOperator.valueOfCode(monitorCondition.getComparisonOperator()).getLabel());
                QueryVo subQueryVo = new QueryVo();
                subQueryVo.filters.put("id", monitorCondition.getTelemetryId());
                ProductTelemetry telemetry = productTelemetryService.retrieve(subQueryVo);
                monitorCondition.setTelemetry(telemetry);
            } else if (triggerSource.equals(TriggerSource.EVENT.getCode())) {
                ProductEvent event = new ProductEvent();
                event.setId(monitorCondition.getEventId());
                event = productEventService.getOne(event);
                monitorCondition.setEvent(event);
            } else if (triggerSource.equals(TriggerSource.CONNECTIVITY.getCode())) {
                monitorCondition.setConnectivityString(Connectivity.valueOfCode(monitorCondition.getConnectivity()).getLabel());
            }
            Integer repeatCount = monitorCondition.getRepeatCount();
            if (repeatCount == null) {
                monitorCondition.setTriggerType(TriggerType.DURATION.getCode());
                monitorCondition.setTriggerTypeString(TriggerType.DURATION.getLabel());
            } else {
                if (repeatCount == 1) {
                    monitorCondition.setTriggerType(TriggerType.SIMPLE.getCode());
                    monitorCondition.setTriggerTypeString(TriggerType.SIMPLE.getLabel());
                } else {
                    monitorCondition.setTriggerType(TriggerType.REPEAT.getCode());
                    monitorCondition.setTriggerTypeString(TriggerType.REPEAT.getLabel());
                }
            }
        }
        return monitorCondition;
    }

    @Override
    @UserDataIsolation(tableAlias = "tbl_monitor_condition")
    public Boolean update(MonitorCondition monitorCondition) {
        Boolean done = false;
        try {
            Integer affected = monitorConditionMapper.update(monitorCondition);
            if (affected > 0) {
                done = true;
            }
        } catch (Exception e) {
            log.info("",e);
            StackTraceElement[] traces = e.getStackTrace();
            for (StackTraceElement trace : traces) {
                log.debug(trace.toString());
            }
        }
        return done;
    }

    @Override
    @UserDataIsolation(tableAlias = "tbl_monitor_condition")
    public Boolean delete(QueryVo queryVo) {
        Boolean done = false;
        try {
            Integer affected = monitorConditionMapper.delete(queryVo);
            if (affected > 0) {
                done = true;
            }
        } catch (Exception e) {
            log.info("",e);
            StackTraceElement[] traces = e.getStackTrace();
            for (StackTraceElement trace : traces) {
                log.debug(trace.toString());
            }
        }
        return done;
    }
}
