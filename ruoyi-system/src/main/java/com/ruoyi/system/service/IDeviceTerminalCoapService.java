package com.ruoyi.system.service;

import java.util.Map;

import com.ruoyi.system.domain.CoapCredentials;
import com.ruoyi.system.domain.SysCompanyThingsboard;
import com.ruoyi.system.domain.vo.QueryVo;

public interface IDeviceTerminalCoapService
{
    public Map<String, Object> findExistance(CoapCredentials coapCredentials);
    public Integer createCredentials(CoapCredentials coapCredentials, SysCompanyThingsboard sysCompanyThingsboard);
    public CoapCredentials retrieveCredentials(QueryVo queryVo);
    public Boolean updateCredentials(CoapCredentials coapCredentials, SysCompanyThingsboard sysCompanyThingsboard);
}
