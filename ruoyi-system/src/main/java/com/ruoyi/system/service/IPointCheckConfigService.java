package com.ruoyi.system.service;

import com.ruoyi.system.domain.PointCheckConfig;
import com.ruoyi.system.domain.vo.QueryVo;

import java.util.ArrayList;
import java.util.List;

public interface IPointCheckConfigService {

    /**
     * 计划-点检配置查询
     *
     * @param PointCheckConfig
     * @return
     */
    List<PointCheckConfig> queryList(PointCheckConfig pointCheckConfig);

    /**
     * 添加
     *
     * @param PointCheckConfig
     * @return
     */
    Boolean add(PointCheckConfig pointCheckConfig);



    /**
     * 删除
     *
     * @param PointCheckConfig
     * @return
     */
    Boolean deletePointId(Integer pointId);
    Boolean deletePointIdDeviceId(QueryVo queryVo);
}
