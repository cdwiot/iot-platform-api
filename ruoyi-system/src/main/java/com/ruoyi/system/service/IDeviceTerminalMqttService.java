package com.ruoyi.system.service;

import java.util.Map;

import com.ruoyi.system.domain.MqttCredentials;
import com.ruoyi.system.domain.SysCompanyThingsboard;
import com.ruoyi.system.domain.vo.QueryVo;

public interface IDeviceTerminalMqttService
{
    public Map<String, Object> findExistance(MqttCredentials mqttCredentials);
    public Integer createCredentials(MqttCredentials mqttCredentials, SysCompanyThingsboard sysCompanyThingsboard);
    public MqttCredentials retrieveCredentials(QueryVo queryVo);
    public Boolean updateCredentials(MqttCredentials mqttCredentials, SysCompanyThingsboard sysCompanyThingsboard);
    public Boolean delete(QueryVo queryVo);
}
