package com.ruoyi.system.service.impl;

import java.util.List;
import java.util.Map;

import com.ruoyi.common.annotation.UserDataIsolation;
import com.ruoyi.system.domain.ProductTelemetry;
import com.ruoyi.system.domain.vo.QueryVo;
import com.ruoyi.system.mapper.ProductTelemetryMapper;
import com.ruoyi.system.service.IProductTelemetryService;
import com.ruoyi.system.service.IProductService;
import com.ruoyi.system.service.IThingsBoardTenantService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ProductTelemetryServiceImpl implements IProductTelemetryService
{
    @Autowired
    IThingsBoardTenantService thingsBoardTenantService;

    @Autowired
    IProductService productService;

    @Autowired
    ProductTelemetryMapper productTelemetryMapper;

    @Override
    @UserDataIsolation(tableAlias = "tbl_product_telemetry")
    public Map<String, Object> findExistance(ProductTelemetry productTelemetry) {
        return productTelemetryMapper.findExistance(productTelemetry);
    }

    @Override
    public Integer create(ProductTelemetry productTelemetry) {
        Integer id = null;
        try {
            Integer affected = productTelemetryMapper.create(productTelemetry);
            if (affected > 0) {
                id = productTelemetry.getId();
            }
        } catch (Exception e) {
            log.info("",e);
            StackTraceElement[] traces = e.getStackTrace();
            for (StackTraceElement trace : traces) {
                log.debug(trace.toString());
            }
        }
        return id;
    }

    @Override
    public List<ProductTelemetry> index(QueryVo queryVo) {
        List<ProductTelemetry> productTelemetries = productTelemetryMapper.index(queryVo);
        return productTelemetries;
    }

    @Override
    @UserDataIsolation(tableAlias = "tbl_product_telemetry")
    public List<Map<String, Object>> enumerate(QueryVo queryVo) {
        return productTelemetryMapper.enumerate(queryVo);
    }

    @Override
    @UserDataIsolation(tableAlias = "tbl_product_telemetry")
    public ProductTelemetry retrieve(QueryVo queryVo) {
        ProductTelemetry productTelemetry = productTelemetryMapper.retrieve(queryVo);
        return productTelemetry;
    }

    @Override
    @UserDataIsolation(tableAlias = "tbl_product_telemetry")
    public Boolean update(ProductTelemetry productTelemetry) {
        Boolean done = false;
        try {
            Integer affected = productTelemetryMapper.update(productTelemetry);
            if (affected > 0) {
                done = true;
            }
        } catch (Exception e) {
            log.info("",e);
            StackTraceElement[] traces = e.getStackTrace();
            for (StackTraceElement trace : traces) {
                log.debug(trace.toString());
            }
        }
        return done;
    }

    @Override
    @UserDataIsolation(tableAlias = "tbl_product_telemetry")
    public Boolean delete(QueryVo queryVo) {
        Boolean done = false;
        Integer affected = productTelemetryMapper.delete(queryVo);
        if (affected > 0) {
            done = true;
        }
        return done;
    }
}
