package com.ruoyi.system.service;

import java.util.List;
import java.util.Map;

import com.ruoyi.system.domain.Product;
import com.ruoyi.system.domain.vo.QueryVo;

public interface IProductService
{
    public Map<String, Object> findExistance(Product product);

    /**
     * 产品型号数据的创建
     *
     * @param product
     * @return
     */
    public Integer create(Product product);

    /**
     * 获取产品型号列表数据
     *
     * @param queryVo
     * @return
     */
    public List<Product> index(QueryVo queryVo);
    public List<Map<String, Object>> enumerate(QueryVo queryVo);

    /**
     * 获取产品型号详情
     *
     * @param queryVo
     * @return
     */
    public Product retrieve(QueryVo queryVo);
    public Product lookup(QueryVo queryVo);

    /**
     *产品型号的修改
     *
     * @param product
     * @return
     */
    public Boolean update(Product product);

    /**
     * 产品型号的删除
     *
     * @param queryVo
     * @return
     */
    public Boolean delete(QueryVo queryVo);
    public List<Product> projectIndex(QueryVo queryVo);
    public List<Map<String, Object>> summary(QueryVo queryVo, Map<String, Object> options);
}
