package com.ruoyi.system.service.impl;

import java.util.List;
import java.util.Map;

import com.ruoyi.system.domain.vo.QueryVo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruoyi.system.mapper.TerminalMapper;
import com.ruoyi.common.annotation.UserDataIsolation;
import com.ruoyi.system.domain.Terminal;
import com.ruoyi.system.service.ITerminalService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class TerminalServiceImpl implements ITerminalService
{
    @Autowired
    private TerminalMapper terminalMapper;

    @Override
    public Map<String, Object> findExistance(Terminal terminal) {
        return terminalMapper.findExistance(terminal);
    }

    @Override
    public Integer create(Terminal terminal) {
        Integer id = null;
        try {
            Integer affected = terminalMapper.create(terminal);
            if (affected > 0) {
                id = terminal.getId();
            }
        } catch (Exception e) {
            log.info("",e);
            StackTraceElement[] traces = e.getStackTrace();
            for (StackTraceElement trace : traces) {
                log.debug(trace.toString());
            }
        }
        return id;
    }

    @Override
    public List<Terminal> index(QueryVo queryVo) {
        List<Terminal> terminals = terminalMapper.index(queryVo);
        for (Terminal terminal : terminals) {
            log.debug("{}", terminal);
        }
        return terminals;
    }

    @Override
    public List<Map<String, Object>> enumerate(QueryVo queryVo) {
        return terminalMapper.enumerate(queryVo);
    }

    @Override
    public Terminal retrieve(QueryVo queryVo) {
        Terminal terminal = terminalMapper.retrieve(queryVo);
        return terminal;
    }

    @Override
    public Boolean update(Terminal terminal) {
        Boolean done = false;
        try {
            Integer affected = terminalMapper.update(terminal);
            if (affected > 0) {
                done = true;
            }
        } catch (Exception e) {
            log.info("",e);
            StackTraceElement[] traces = e.getStackTrace();
            for (StackTraceElement trace : traces) {
                log.debug(trace.toString());
            }
        }
        return done;
    }

    @Override
    public Boolean delete(QueryVo queryVo) {
        Boolean done = false;
        try {
            Integer affected = terminalMapper.delete(queryVo);
            if (affected > 0) {
                done = true;
            }
        } catch (Exception e) {
            log.info("",e);
            StackTraceElement[] traces = e.getStackTrace();
            for (StackTraceElement trace : traces) {
                log.debug(trace.toString());
            }
        }
        return done;
    }
}
