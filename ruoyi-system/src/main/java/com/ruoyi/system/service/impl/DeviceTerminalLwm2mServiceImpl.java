package com.ruoyi.system.service.impl;

import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.annotation.UserDataIsolation;
import com.ruoyi.system.domain.Lwm2mCredentials;
import com.ruoyi.system.domain.SysCompanyThingsboard;
import com.ruoyi.system.domain.vo.QueryVo;
import com.ruoyi.system.mapper.Lwm2mCredentialsMapper;
import com.ruoyi.system.service.IDeviceTerminalLwm2mService;
import com.ruoyi.system.service.IThingsBoardTenantService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class DeviceTerminalLwm2mServiceImpl implements IDeviceTerminalLwm2mService
{
    @Autowired
    Lwm2mCredentialsMapper lwm2mCredentialsMapper;

    @Autowired
    IThingsBoardTenantService thingsBoardTenantService;

    @Override
    @UserDataIsolation(tableAlias = "tbl_lwm2m_credentials")
    public Map<String, Object> findExistance(Lwm2mCredentials lwm2mCredentials) {
        return lwm2mCredentialsMapper.findExistance(lwm2mCredentials);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Integer createCredentials(Lwm2mCredentials lwm2mCredentials, SysCompanyThingsboard sysCompanyThingsboard) {
        Integer id = null;
        try {
            Integer affected = lwm2mCredentialsMapper.create(lwm2mCredentials);
            if (affected > 0) {
                JSONObject fields = new JSONObject();
                fields.put("deviceId", lwm2mCredentials.getTbDeviceId());
                JSONObject credentials = new JSONObject();
                credentials.put("accessToken", lwm2mCredentials.getAccessToken());
                fields.put("credentials", credentials);
                thingsBoardTenantService.saveLwm2mDeviceCredentials(fields, sysCompanyThingsboard);
                id = lwm2mCredentials.getId();
            }
        } catch (Exception e) {
            id = null;
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            log.info("",e);
            StackTraceElement[] traces = e.getStackTrace();
            for (StackTraceElement trace : traces) {
                log.debug(trace.toString());
            }
        }
        return id;
    }

    @Override
    @UserDataIsolation(tableAlias = "mc")
    public Lwm2mCredentials retrieveCredentials(QueryVo queryVo) {
        Lwm2mCredentials lwm2mCredentials = lwm2mCredentialsMapper.retrieve(queryVo);
        return lwm2mCredentials;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    @UserDataIsolation(tableAlias = "tbl_lwm2m_credentials")
    public Boolean updateCredentials(Lwm2mCredentials lwm2mCredentials, SysCompanyThingsboard sysCompanyThingsboard) {
        Boolean done = false;
        try {
            Integer affected = lwm2mCredentialsMapper.update(lwm2mCredentials);
            if (affected > 0) {
                JSONObject fields = new JSONObject();
                fields.put("deviceId", lwm2mCredentials.getTbDeviceId());
                JSONObject credentials = new JSONObject();
                credentials.put("accessToken", lwm2mCredentials.getAccessToken());
                fields.put("credentials", credentials);
                thingsBoardTenantService.saveLwm2mDeviceCredentials(fields, sysCompanyThingsboard);
                done = true;
            }
        } catch (Exception e) {
            done = false;
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            log.info("",e);
            StackTraceElement[] traces = e.getStackTrace();
            for (StackTraceElement trace : traces) {
                log.debug(trace.toString());
            }
        }
        return done;
    }
}
