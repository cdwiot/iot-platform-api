package com.ruoyi.system.service;

import java.util.Map;

import com.ruoyi.system.domain.HttpCredentials;
import com.ruoyi.system.domain.SysCompanyThingsboard;
import com.ruoyi.system.domain.vo.QueryVo;

public interface IDeviceTerminalHttpService
{
    public Map<String, Object> findExistance(HttpCredentials httpCredentials);
    public Integer createCredentials(HttpCredentials httpCredentials, SysCompanyThingsboard sysCompanyThingsboard);
    public HttpCredentials retrieveCredentials(QueryVo queryVo);
    public Boolean updateCredentials(HttpCredentials httpCredentials, SysCompanyThingsboard sysCompanyThingsboard);
}
