package com.ruoyi.system.service;

import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.system.domain.vo.WxCallBackParamVO;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * 微信模板消息通知服务
 */
@SuppressWarnings("AlibabaLowerCamelCaseVariableNaming")
public interface IWXMessageService {

    /**
     * 获取Token
     *
     * @param enforce 是否需要强制获取 true 强制获取 false 从redis里面获取
     * @return
     */
    String getToken(Boolean enforce);

    /**
     * 推送模板消息
     *
     * @param openId       关注用户openId
     * @param param        模板参数
     * @return
     */
    Boolean pushTemplateMessage(JSONObject request, String tid);

    /**
     * 获取关注用户openId
     */
    void syncUserInfo();

    /**
     * 获取用户信息
     *
     * @param openId
     */
    void getUserInfo(String openId);

    /**
     * 根据用户信息生成微信二维码
     *
     * @param sysUser
     * @return
     */
    String createWeChatQRCode(SysUser sysUser);

    /**
     * 微信公众号服务器配置回调接口
     *
     * @param signature
     * @param timestamp
     * @param nonce
     * @return
     */
    Boolean checkSign(String signature, String timestamp, String nonce);

    /**
     * 微信公众号处理具体业务数据
     *
     * @param request
     */
    void getCallBackParam(HttpServletRequest request);

    /**
     * 根据用户名称获取用户openId
     *
     * @param userName
     * @return
     */
    String getOpenId(String userName);
}
