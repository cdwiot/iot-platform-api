package com.ruoyi.system.service.impl;

import com.ruoyi.common.annotation.UserDataIsolation;
import com.ruoyi.common.exception.CustomException;
import com.ruoyi.system.domain.ProductProperty;
import com.ruoyi.system.domain.ProductPropertyValue;
import com.ruoyi.system.mapper.ProductPropertyMapper;
import com.ruoyi.system.service.IProductPropertyService;
import com.ruoyi.system.service.IProductPropertyValueService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.List;

@Service
@Slf4j
public class ProductPropertyServiceImpl implements IProductPropertyService {
    @Autowired
    private ProductPropertyMapper productPropertyMapper;
    @Autowired
    private IProductPropertyValueService productPropertyValueService;

    @Override
    @UserDataIsolation(tableAlias = "t1")
    public List<ProductProperty> queryPageList(ProductProperty productProperty) {
        return productPropertyMapper.queryPageList(productProperty);
    }

    @Override
    public Integer addProductProperty(ProductProperty productProperty) {
        //checkParam(productProperty);
        return productPropertyMapper.addProductProperty(productProperty);
    }

    @Override
    @UserDataIsolation(tableAlias = "t1")
    public ProductProperty queryById(ProductProperty productProperty) {
        ProductProperty result = productPropertyMapper.queryById(productProperty);
        return result == null ? new ProductProperty() : result;
    }

    @Override
    @UserDataIsolation
    public Integer updateProductProperty(ProductProperty productProperty) {
        if (productProperty.getId() == null) {
            throw new CustomException("产品额外属性Id不能为空");
        }
        return productPropertyMapper.updateProductProperty(productProperty);
    }

    @Override
    @UserDataIsolation
    public void deleteProductProperty(ProductProperty productProperty) {
        productPropertyMapper.deleteProductProperty(productProperty);
    }

    @Override
    @UserDataIsolation
    public void checkParam(ProductProperty productProperty) {
        ProductProperty property= productPropertyMapper.checkPropertyName(productProperty);
        if (property == null){
            return;
        }
        if (productProperty.getId() == null || property.getId().compareTo(productProperty.getId()) != 0){
            throw new CustomException("该属性名称已经存在");
        }
    }
}
