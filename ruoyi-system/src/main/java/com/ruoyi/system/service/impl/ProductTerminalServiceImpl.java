package com.ruoyi.system.service.impl;

import java.util.List;
import java.util.Map;

import com.ruoyi.system.domain.vo.QueryVo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruoyi.system.mapper.ProductTerminalMapper;
import com.ruoyi.common.annotation.UserDataIsolation;
import com.ruoyi.system.domain.ProductTerminal;
import com.ruoyi.system.domain.Terminal;
import com.ruoyi.system.service.IProductTerminalService;
import com.ruoyi.system.service.ITerminalService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ProductTerminalServiceImpl implements IProductTerminalService
{
    @Autowired
    private ProductTerminalMapper productTerminalMapper;

    @Autowired
    private ITerminalService terminalService;

    @Override
    public Map<String, Object> findExistance(ProductTerminal productTerminal) {
        return productTerminalMapper.findExistance(productTerminal);
    }

    @Override
    public Integer create(ProductTerminal productTerminal) {
        Integer id = null;
        try {
            Integer affected = productTerminalMapper.create(productTerminal);
            if (affected > 0) {
                id = productTerminal.getId();
            }
        } catch (Exception e) {
            log.info("",e);
            StackTraceElement[] traces = e.getStackTrace();
            for (StackTraceElement trace : traces) {
                log.debug(trace.toString());
            }
        }
        return id;
    }

    @Override
    public List<ProductTerminal> index(QueryVo queryVo) {
        List<ProductTerminal> productTerminals = productTerminalMapper.index(queryVo);
        for (ProductTerminal productTerminal : productTerminals) {
            if (productTerminal != null) {
                QueryVo subQueryVo = new QueryVo();
                subQueryVo.filters.put("id", productTerminal.getTerminalId());
                subQueryVo.filters.put("company_id", productTerminal.getCompanyId());
                Terminal terminal = terminalService.retrieve(subQueryVo);
                if (terminal != null) {
                    productTerminal.setCode(terminal.getCode());
                    productTerminal.setManufacturer(terminal.getManufacturer());
                    productTerminal.setProtocol(terminal.getProtocol());
                    productTerminal.setRemark(terminal.getRemark());
                    productTerminal.setGroupName(terminal.getGroupName());
                }
            }
        }
        return productTerminals;
    }

    @Override
    public List<Map<String, Object>> enumerate(QueryVo queryVo) {
        return productTerminalMapper.enumerate(queryVo);
    }

    @Override
    public ProductTerminal retrieve(QueryVo queryVo) {
        ProductTerminal productTerminal = productTerminalMapper.retrieve(queryVo);
        if (productTerminal != null) {
            QueryVo subQueryVo = new QueryVo();
            subQueryVo.filters.put("id", productTerminal.getTerminalId());
            subQueryVo.filters.put("company_id", productTerminal.getCompanyId());
            Terminal terminal = terminalService.retrieve(subQueryVo);
            if (terminal != null) {
                productTerminal.setCode(terminal.getCode());
                productTerminal.setManufacturer(terminal.getManufacturer());
                productTerminal.setProtocol(terminal.getProtocol());
                productTerminal.setRemark(terminal.getRemark());
            }
        }
        return productTerminal;
    }

    @Override
    public Boolean update(ProductTerminal productTerminal) {
        Boolean done = false;
        try {
            Integer affected = productTerminalMapper.update(productTerminal);
            if (affected > 0) {
                done = true;
            }
        } catch (Exception e) {
            log.info("",e);
            StackTraceElement[] traces = e.getStackTrace();
            for (StackTraceElement trace : traces) {
                log.debug(trace.toString());
            }
        }
        return done;
    }

    @Override
    public Boolean delete(QueryVo queryVo) {
        Boolean done = false;
        try {
            Integer affected = productTerminalMapper.delete(queryVo);
            if (affected > 0) {
                done = true;
            }
        } catch (Exception e) {
            log.info("",e);
            StackTraceElement[] traces = e.getStackTrace();
            for (StackTraceElement trace : traces) {
                log.debug(trace.toString());
            }
        }
        return done;
    }
}
