package com.ruoyi.system.service.impl;

import com.ruoyi.common.annotation.UserDataIsolation;
import com.ruoyi.common.enums.Severity;
import com.ruoyi.common.enums.TriggerLogic;
import com.ruoyi.common.utils.DataUtils;
import com.ruoyi.system.domain.Alarm;
import com.ruoyi.system.domain.vo.QueryVo;
import com.ruoyi.system.mapper.AlarmMapper;
import com.ruoyi.system.service.IAlarmDetailService;
import com.ruoyi.system.service.IAlarmService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@SuppressWarnings("AlibabaLowerCamelCaseVariableNaming")
@Slf4j
@Service
public class AlarmServiceImpl implements IAlarmService
{
    @Autowired
    private AlarmMapper alarmMapper;

    @Autowired
    private IAlarmDetailService alarmDetailService;

    @Override
    public List<Alarm> index(QueryVo queryVo) {
        List<Alarm> alarms = alarmMapper.index(queryVo);
        for (Alarm alarm : alarms) {
            if (alarm.getSeverity() != null) {
                alarm.setSeverityString(Severity.valueOfCode(alarm.getSeverity()).getLabel());
            }
            if (alarm.getTriggerLogic() != null) {
                alarm.setTriggerLogicString(TriggerLogic.valueOfCode(alarm.getTriggerLogic()).getLabel());
            }
            QueryVo subQueryVo = new QueryVo();
            subQueryVo.filters.put("alarm_id", alarm.getId());
            alarm.setDetails(alarmDetailService.index(subQueryVo));
        }
        return alarms;
    }

    @Override
    @UserDataIsolation(tableAlias = "mr")
    public Alarm retrieve(QueryVo queryVo) {
        Alarm alarm = alarmMapper.retrieve(queryVo);
        if (alarm != null) {
            if (alarm.getSeverity() != null) {
                alarm.setSeverityString(Severity.valueOfCode(alarm.getSeverity()).getLabel());
            }
            if (alarm.getTriggerLogic() != null) {
                alarm.setTriggerLogicString(TriggerLogic.valueOfCode(alarm.getTriggerLogic()).getLabel());
            }
            QueryVo subQueryVo = new QueryVo();
            subQueryVo.filters.put("alarm_id", alarm.getId());
            alarm.setDetails(alarmDetailService.index(subQueryVo));
        }
        return alarm;
    }

    @Override
    public Boolean update(Alarm alarm) {
        Boolean done = false;
        try {
            Integer affected = alarmMapper.update(alarm);
            if (affected > 0) {
                done = true;
            }
        } catch (Exception e) {
            log.info("",e);
            StackTraceElement[] traces = e.getStackTrace();
            for (StackTraceElement trace : traces) {
                log.debug(trace.toString());
            }
        }
        return done;
    }

    @Override
    @UserDataIsolation(tableAlias = "p")
    public List<Map<String, Object>> summary(QueryVo queryVo, Map<String, Object> options) {
        List<Map<String, Object>> summary = null;
        try {
            Map<String, Object> summaryKV = alarmMapper.summary(queryVo);
            summary = DataUtils.KVtoList(summaryKV, options);
        } catch (Exception e) {
            log.info("",e);
            StackTraceElement[] traces = e.getStackTrace();
            for (StackTraceElement trace : traces) {
                log.debug(trace.toString());
            }
        }
        return summary;
    }
}
