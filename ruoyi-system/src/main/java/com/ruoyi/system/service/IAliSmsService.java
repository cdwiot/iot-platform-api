package com.ruoyi.system.service;

import com.alibaba.fastjson.JSONObject;
import com.ruoyi.system.domain.SmsAccount;

/**
 * 阿里云短信服务
 */
public interface IAliSmsService {
    /**
     * 获取配置账户信息
     */
    SmsAccount getAccount();

    /**
     * 发送短信
     *
     * @param request 发送短信所需参数
     */
    void send(JSONObject request, String tid);
}
