package com.ruoyi.system.service.impl;

import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.annotation.UserDataIsolation;
import com.ruoyi.system.domain.MqttCredentials;
import com.ruoyi.system.domain.SysCompanyThingsboard;
import com.ruoyi.system.domain.vo.QueryVo;
import com.ruoyi.system.mapper.MqttCredentialsMapper;
import com.ruoyi.system.service.IDeviceTerminalMqttService;
import com.ruoyi.system.service.IThingsBoardTenantService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class DeviceTerminalMqttServiceImpl implements IDeviceTerminalMqttService
{
    @Autowired
    MqttCredentialsMapper mqttCredentialsMapper;

    @Autowired
    IThingsBoardTenantService thingsBoardTenantService;

    @Override
    @UserDataIsolation(tableAlias = "tbl_mqtt_credentials")
    public Map<String, Object> findExistance(MqttCredentials mqttCredentials) {
        return mqttCredentialsMapper.findExistance(mqttCredentials);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Integer createCredentials(MqttCredentials mqttCredentials, SysCompanyThingsboard sysCompanyThingsboard) {
        Integer id = null;
        try {
            Integer affected = mqttCredentialsMapper.create(mqttCredentials);
            if (affected > 0) {
                JSONObject fields = new JSONObject();
                fields.put("deviceId", mqttCredentials.getTbDeviceId());
                JSONObject credentials = new JSONObject();
                credentials.put("clientId", mqttCredentials.getClientId());
                credentials.put("userName", mqttCredentials.getUsername());
                credentials.put("password", mqttCredentials.getPassword());
                fields.put("credentials", credentials);
                thingsBoardTenantService.saveMqttDeviceCredentials(fields, sysCompanyThingsboard);
                id = mqttCredentials.getId();
            }
        } catch (Exception e) {
            id = null;
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            log.info("",e);
            StackTraceElement[] traces = e.getStackTrace();
            for (StackTraceElement trace : traces) {
                log.debug(trace.toString());
            }
        }
        return id;
    }

    @Override
    @UserDataIsolation(tableAlias = "mc")
    public MqttCredentials retrieveCredentials(QueryVo queryVo) {
        MqttCredentials mqttCredentials = mqttCredentialsMapper.retrieve(queryVo);
        return mqttCredentials;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    @UserDataIsolation(tableAlias = "tbl_mqtt_credentials")
    public Boolean updateCredentials(MqttCredentials mqttCredentials, SysCompanyThingsboard sysCompanyThingsboard) {
        Boolean done = false;
        try {
            Integer affected = mqttCredentialsMapper.update(mqttCredentials);
            if (affected > 0) {
                JSONObject fields = new JSONObject();
                fields.put("deviceId", mqttCredentials.getTbDeviceId());
                JSONObject credentials = new JSONObject();
                credentials.put("clientId", mqttCredentials.getClientId());
                credentials.put("userName", mqttCredentials.getUsername());
                credentials.put("password", mqttCredentials.getPassword());
                fields.put("credentials", credentials);
                thingsBoardTenantService.saveMqttDeviceCredentials(fields, sysCompanyThingsboard);
                done = true;
            }
        } catch (Exception e) {
            done = false;
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            log.info("",e);
            StackTraceElement[] traces = e.getStackTrace();
            for (StackTraceElement trace : traces) {
                log.debug(trace.toString());
            }
        }
        return done;
    }

    @Override
    @UserDataIsolation(tableAlias = "tbl_mqtt_credentials")
    public Boolean delete(QueryVo queryVo) {
        Boolean done = false;
        try {
            mqttCredentialsMapper.delete(queryVo);
            done = true;
        } catch (Exception e) {
            done = false;
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            log.info("",e);
            StackTraceElement[] traces = e.getStackTrace();
            for (StackTraceElement trace : traces) {
                log.debug(trace.toString());
            }
        }
        return done;
    }
}
