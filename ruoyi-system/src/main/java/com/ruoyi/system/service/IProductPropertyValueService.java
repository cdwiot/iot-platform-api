package com.ruoyi.system.service;

import com.ruoyi.system.domain.ProductPropertyValue;
import com.ruoyi.system.domain.vo.ProductPropertyValueResultVO;
import com.ruoyi.system.domain.vo.ProductPropertyValueVO;

import java.util.List;

public interface IProductPropertyValueService {

    /**
     * 查询产品额外属性值
     *
     * @param productPropertyValue
     * @return
     */
    List<ProductPropertyValueResultVO> queryProductPropertyValue(ProductPropertyValue productPropertyValue);

    /**
     * 添加或修改产品额外属性值
     *
     * @param productPropertyValues
     * @param userName
     * @param companyId
     * @return
     */
    Boolean saveOrUpdate(List<ProductPropertyValue> productPropertyValues, String userName, Long companyId);

    /**
     * 产品属性值的删除
     *
     * @param productPropertyValue
     * @return
     */
    Boolean deleteProductPropertyValue(ProductPropertyValue productPropertyValue);

    /**
     * 产品售后服务查询
     *
     * @param productPropertyValueVO
     * @return
     */
    List<ProductPropertyValueVO> getProductValue(ProductPropertyValueVO productPropertyValueVO);
}
