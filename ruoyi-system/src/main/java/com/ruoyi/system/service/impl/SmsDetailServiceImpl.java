package com.ruoyi.system.service.impl;

import com.ruoyi.common.annotation.UserDataIsolation;
import com.ruoyi.system.domain.SmsDetail;
import com.ruoyi.system.mapper.SmsDetailMapper;
import com.ruoyi.system.service.ISmsDetailService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class SmsDetailServiceImpl implements ISmsDetailService {

    @Autowired
    private SmsDetailMapper smsDetailMapper;

    @Override
    public Boolean addMessageDetail(SmsDetail smsDetail) {
        return smsDetailMapper.addMessageDetail(smsDetail) > 0;
    }

    @Override
    @UserDataIsolation(tableAlias = "t1")
    public List<SmsDetail> list(SmsDetail smsDetail) {
        return smsDetailMapper.list(smsDetail);
    }

    @Override
    @UserDataIsolation(tableAlias = "t1")
    public SmsDetail getOne(SmsDetail smsDetail) {
        return smsDetailMapper.getOne(smsDetail);
    }
}
