package com.ruoyi.system.service;

import com.ruoyi.system.domain.TerminalDetail;
import com.ruoyi.system.domain.vo.QueryVo;
import com.ruoyi.system.domain.vo.TerminalDetailVO;

import java.util.List;
import java.util.Map;

public interface ITerminalDetailService {
    /**
     * 获取传感器参数枚举
     *
     * @param queryVo
     * @return
     */
    List<Map<String, Object>> getTerminalParamEnum(QueryVo queryVo);

    /**
     * 查询终端信息
     *
     * @param queryVo
     * @return
     */
    List<TerminalDetailVO> queryTerminalDetail(QueryVo queryVo);

    /**
     * 添加
     *
     * @param terminalDetail
     * @return
     */
    boolean add(TerminalDetail terminalDetail);

    /**
     * 修改
     *
     * @param terminalDetail
     * @return
     */
    boolean update(TerminalDetail terminalDetail);

    /**
     * 验证添加的属性值是否重复
     *
     * @param terminalDetail
     */
    void checkIsExit(TerminalDetail terminalDetail);
}
