package com.ruoyi.system.service.impl;

import com.ruoyi.common.annotation.UserDataIsolation;
import com.ruoyi.common.exception.CustomException;
import com.ruoyi.system.domain.DeviceCustomField;
import com.ruoyi.system.domain.DeviceProperty;
import com.ruoyi.system.domain.vo.QueryVo;
import com.ruoyi.system.mapper.DeviceCustomFieldMapper;
import com.ruoyi.system.mapper.DevicePropertyMapper;
import com.ruoyi.system.service.IDeviceCustomFieldService;
import com.ruoyi.system.service.IDevicePropertyService;
import com.ruoyi.system.service.IDevicePropertyValueService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class DeviceCustomFieldServiceImpl implements IDeviceCustomFieldService {
    @Autowired
    private DeviceCustomFieldMapper deviceCustomFieldMapper;

    @Override
    @UserDataIsolation
    public List<DeviceCustomField> queryPageList(QueryVo queryVo) {
        return deviceCustomFieldMapper.queryPageList(queryVo);
    }

    @Override
    public boolean add(DeviceCustomField deviceCustomField) {
        return deviceCustomFieldMapper.add(deviceCustomField) > 0;
    }

    @Override
    @UserDataIsolation
    public DeviceCustomField getOne(QueryVo queryVo) {
        return deviceCustomFieldMapper.getOne(queryVo);
    }

    @Override
    @UserDataIsolation
    public boolean update(DeviceCustomField deviceCustomField) {
        return deviceCustomFieldMapper.update(deviceCustomField) > 0;
    }

    @Override
    @UserDataIsolation
    public boolean delete(QueryVo queryVo) {
        return deviceCustomFieldMapper.delete(queryVo) > 0;
    }

    @Override
    @UserDataIsolation
    public void checkIsExist(DeviceCustomField deviceCustomField) {
        DeviceCustomField field = deviceCustomFieldMapper.checkIsExist(deviceCustomField);
        if (field == null){
            return;
        }
        if (deviceCustomField.getId() == null || field.getId().compareTo(deviceCustomField.getId()) != 0){
            throw new CustomException("该自定义名称已经存在");
        }
    }
}
