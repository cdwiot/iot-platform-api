package com.ruoyi.system.service;

import java.util.List;
import java.util.Map;

import com.ruoyi.system.domain.ProductChart;
import com.ruoyi.system.domain.vo.QueryVo;

public interface IProductChartService
{
    public Map<String, Object> findExistance(ProductChart productChart);
    public Integer create(ProductChart productChart);
    public List<ProductChart> index(QueryVo queryVo);
    public List<Map<String, Object>> enumerate(QueryVo queryVo);
    public ProductChart retrieve(QueryVo queryVo);
    public Boolean update(ProductChart productChart);
    public Boolean delete(QueryVo queryVo);
}
