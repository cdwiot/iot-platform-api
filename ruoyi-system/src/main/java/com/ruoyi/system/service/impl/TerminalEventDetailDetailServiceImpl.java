package com.ruoyi.system.service.impl;

import com.ruoyi.common.annotation.UserDataIsolation;
import com.ruoyi.system.domain.TerminalEventDetail;
import com.ruoyi.system.mapper.TerminalEventDetailMapper;
import com.ruoyi.system.service.ITerminalEventDetailService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class TerminalEventDetailDetailServiceImpl implements ITerminalEventDetailService {
    @Autowired
    private TerminalEventDetailMapper terminalEventDetailMapper;

    @Override
    public List<TerminalEventDetail> queryList(TerminalEventDetail terminalEventDetail) {
        return terminalEventDetailMapper.queryList(terminalEventDetail);
    }

    @Override
    @UserDataIsolation(tableAlias = "t1")
    public TerminalEventDetail getOne(TerminalEventDetail terminalEventDetail) {
        return terminalEventDetailMapper.getOne(terminalEventDetail);
    }
}
