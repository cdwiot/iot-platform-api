package com.ruoyi.system.service.impl;

import com.ruoyi.common.annotation.UserDataIsolation;
import com.ruoyi.system.domain.OfficialDetail;
import com.ruoyi.system.mapper.OfficialDetailMapper;
import com.ruoyi.system.service.IOfficialDetailService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class OfficialDetailServiceImpl implements IOfficialDetailService {
    @Autowired
    private OfficialDetailMapper officialDetailMapper;

    @Override
    public Boolean add(OfficialDetail officialDetail) {
        return officialDetailMapper.add(officialDetail) > 0;
    }

    @Override
    @UserDataIsolation(tableAlias = "t1")
    public List<OfficialDetail> list(OfficialDetail officialDetail) {
        return officialDetailMapper.list(officialDetail);
    }

    @Override
    @UserDataIsolation(tableAlias = "t1")
    public OfficialDetail getOne(OfficialDetail officialDetail) {
        return officialDetailMapper.getOne(officialDetail);
    }
}
