package com.ruoyi.system.service.impl;

import java.util.List;
import java.util.Map;

import com.ruoyi.common.annotation.UserDataIsolation;
import com.ruoyi.common.enums.TriggerLogic;
import com.ruoyi.system.domain.MonitorCondition;
import com.ruoyi.system.domain.MonitorRule;
import com.ruoyi.system.domain.vo.QueryVo;
import com.ruoyi.system.mapper.MonitorRuleMapper;
import com.ruoyi.system.service.IMonitorConditionService;
import com.ruoyi.system.service.IMonitorRuleService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class MonitorRuleServiceImpl implements IMonitorRuleService
{
    @Autowired
    private MonitorRuleMapper monitorRuleMapper;

    @Autowired
    private IMonitorConditionService monitorConditionService;

    @Override
    @UserDataIsolation(tableAlias = "tbl_monitor_rule")
    public Map<String, Object> findExistance(MonitorRule monitorRule) {
        return monitorRuleMapper.findExistance(monitorRule);
    }

    @Override
    public Integer create(MonitorRule monitorRule) {
        Integer id = null;
        try {
            Integer affected = monitorRuleMapper.create(monitorRule);
            if (affected > 0) {
                id = monitorRule.getId();
            }
        } catch (Exception e) {
            log.info("",e);
            StackTraceElement[] traces = e.getStackTrace();
            for (StackTraceElement trace : traces) {
                log.debug(trace.toString());
            }
        }
        return id;
    }

    @Override
    @UserDataIsolation(tableAlias = "tbl_monitor_rule")
    public List<MonitorRule> index(QueryVo queryVo) {
        List<MonitorRule> monitorRules = monitorRuleMapper.index(queryVo);
        for (MonitorRule monitorRule : monitorRules) {
            Integer triggerLogic = monitorRule.getTriggerLogic();
            if (triggerLogic != null) {
                monitorRule.setTriggerLogicString(TriggerLogic.valueOfCode(triggerLogic).getLabel());
            }
            QueryVo subQueryVo = new QueryVo();
            subQueryVo.filters.put("rule_id", monitorRule.getId());
            List<MonitorCondition> conditions = monitorConditionService.index(subQueryVo);
            monitorRule.setConditions(conditions);
            // monitorRule.setActivePeriods(activePeriods);
        }
        return monitorRules;
    }

    @Override
    @UserDataIsolation(tableAlias = "tbl_monitor_rule")
    public List<Map<String, Object>> enumerate(QueryVo queryVo) {
        return monitorRuleMapper.enumerate(queryVo);
    }

    @Override
    @UserDataIsolation(tableAlias = "tbl_monitor_rule")
    public MonitorRule retrieve(QueryVo queryVo) {
        MonitorRule monitorRule = monitorRuleMapper.retrieve(queryVo);
        if (monitorRule != null) {
            Integer triggerLogic = monitorRule.getTriggerLogic();
            if (triggerLogic != null) {
                monitorRule.setTriggerLogicString(TriggerLogic.valueOfCode(triggerLogic).getLabel());
            }
            QueryVo subQueryVo = new QueryVo();
            subQueryVo.filters.put("rule_id", monitorRule.getId());
            List<MonitorCondition> conditions = monitorConditionService.index(subQueryVo);
            monitorRule.setConditions(conditions);
            // monitorRule.setActivePeriods(activePeriods);
        }
        return monitorRule;
    }

    @Override
    @UserDataIsolation(tableAlias = "tbl_monitor_rule")
    public Boolean update(MonitorRule monitorRule) {
        Boolean done = false;
        try {
            Integer affected = monitorRuleMapper.update(monitorRule);
            if (affected > 0) {
                done = true;
            }
        } catch (Exception e) {
            log.info("",e);
            StackTraceElement[] traces = e.getStackTrace();
            for (StackTraceElement trace : traces) {
                log.debug(trace.toString());
            }
        }
        return done;
    }

    @Override
    @UserDataIsolation(tableAlias = "tbl_monitor_rule")
    @Transactional(rollbackFor = Exception.class)
    public Boolean delete(QueryVo queryVo) {
        Boolean done = false;
        try {
            Integer affected = monitorRuleMapper.delete(queryVo);
            if (affected > 0) {
                QueryVo subQueryVo = new QueryVo();
                subQueryVo.filters.put("rule_id", queryVo.filters.get("id"));
                monitorConditionService.delete(subQueryVo);
                done = true;
            }
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            log.info("",e);
            StackTraceElement[] traces = e.getStackTrace();
            for (StackTraceElement trace : traces) {
                log.debug(trace.toString());
            }
        }
        return done;
    }
}
