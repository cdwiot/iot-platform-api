package com.ruoyi.system.service;

import com.ruoyi.system.domain.PointCheck;
import com.ruoyi.system.domain.PointCheckTask;
import com.ruoyi.system.domain.vo.CreateTaskGatherVO;
import com.ruoyi.system.domain.vo.DeviceHistoryDto;
import com.ruoyi.system.domain.vo.QueryVo;

import java.util.List;
import java.util.Map;

public interface IPointCheckTaskService {

    /**
     * 获取任务列表
     *
     * @param pointCheckTask
     * @return
     */
    List<PointCheckTask> queryList(PointCheckTask pointCheckTask);

    /**
     * app进行中列表
     *
     * @param pointCheckTask
     * @return
     */
    List<PointCheckTask> queryListProgress(PointCheckTask pointCheckTask);

    /**
     * 获取详情
     *
     * @param pointCheckTask
     * @return
     */
    PointCheckTask getOne(PointCheckTask pointCheckTask);

    /**
     * 添加
     *
     * @param pointCheckTask
     * @return
     */
    Boolean add(PointCheckTask pointCheckTask);
    Boolean addV2(PointCheckTask pointCheckTask);


    /**
     * 编辑
     *
     * @param pointCheckTask
     * @return
     */
    Boolean update(PointCheckTask pointCheckTask);

    void checkExecutorStatus(PointCheckTask pointCheckTask);

    /**
     * 定时更新点检任务超时状态
     */
    void updatePointCheckTaskStatusCs();


    /**
     * 根据计划任务Id获取计划汇总
     *
     * @param id
     * @return
     */
    CreateTaskGatherVO getTaskGather(QueryVo queryVo);

    /**
     * 获取产品点检异常历史
     *
     * @param queryVo
     * @return
     */
    List<DeviceHistoryDto> getDeviceHistory(QueryVo queryVo);

    /**
     * 根据项目id或项目分组id查询产品信息枚举
     *
     * @param queryVo
     * @return
     */
    List<Map<String,Object>> getDeviceEnum(QueryVo queryVo);
}
