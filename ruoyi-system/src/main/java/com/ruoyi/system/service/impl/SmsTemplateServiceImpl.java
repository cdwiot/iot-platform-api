package com.ruoyi.system.service.impl;

import com.ruoyi.common.annotation.UserDataIsolation;
import com.ruoyi.common.exception.CustomException;
import com.ruoyi.system.domain.SmsTemplate;
import com.ruoyi.system.mapper.SmsTemplateMapper;
import com.ruoyi.system.service.ISmsTemplateService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Slf4j
@Service
public class SmsTemplateServiceImpl implements ISmsTemplateService {
    @Autowired
    private SmsTemplateMapper smsTemplateMapper;

    @Override
    @UserDataIsolation(tableAlias = "t1")
    public List<SmsTemplate> queryList(SmsTemplate smsTemplate) {
        return smsTemplateMapper.queryList(smsTemplate);
    }

    @Override
    @UserDataIsolation(tableAlias = "t1")
    public SmsTemplate getOne(SmsTemplate smsTemplate) {
        return smsTemplateMapper.getOne(smsTemplate);
    }

    @Override
    public Boolean addMessageTemplate(SmsTemplate smsTemplate) {
        return smsTemplateMapper.addMessageTemplate(smsTemplate) > 0;
    }

    @Override
    @UserDataIsolation
    public Boolean updateMessageTemplate(SmsTemplate smsTemplate) {
        return smsTemplateMapper.updateMessageTemplate(smsTemplate) > 0;
    }

    @Override
    @UserDataIsolation
    public Boolean deleteMessageTemplate(SmsTemplate smsTemplate) {
        return smsTemplateMapper.deleteMessageTemplate(smsTemplate) > 0;
    }

    @Override
    public void checkTemplateCodeExist(SmsTemplate smsTemplate) {
        SmsTemplate template = smsTemplateMapper.checkTemplateCodeExist(smsTemplate);
        if (template == null){
            return;
        }
        if (smsTemplate.getId() == null || template.getId().compareTo(smsTemplate.getId()) != 0){
            throw new CustomException("该模板code已存在！");
        }
    }
}
