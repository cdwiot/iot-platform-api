package com.ruoyi.system.service.impl;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.ruoyi.system.domain.vo.QueryVo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruoyi.system.mapper.ProductChartMapper;
import com.ruoyi.common.annotation.UserDataIsolation;
import com.ruoyi.system.domain.ProductChart;
import com.ruoyi.system.service.IProductChartService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ProductChartServiceImpl implements IProductChartService
{
    @Autowired
    private ProductChartMapper productChartMapper;

    @Override
    @UserDataIsolation(tableAlias = "tbl_product_chart")
    public Map<String, Object> findExistance(ProductChart productChart) {
        return productChartMapper.findExistance(productChart);
    }

    @Override
    public Integer create(ProductChart productChart) {
        Integer id = null;
        try {
            Integer affected = productChartMapper.create(productChart);
            if (affected > 0) {
                id = productChart.getId();
            }
        } catch (Exception e) {
            log.info("",e);
            StackTraceElement[] traces = e.getStackTrace();
            for (StackTraceElement trace : traces) {
                log.debug(trace.toString());
            }
        }
        return id;
    }

    @Override
    @UserDataIsolation(tableAlias = "tbl_product_chart")
    public List<ProductChart> index(QueryVo queryVo) {
        List<ProductChart> productCharts = productChartMapper.index(queryVo);
        for (ProductChart productChart : productCharts) {
            productChart.setIdentifiers(Arrays.asList(productChart.getIdentifiersString().split(",")));
        }
        return productCharts;
    }

    @Override
    @UserDataIsolation(tableAlias = "tbl_product_chart")
    public List<Map<String, Object>> enumerate(QueryVo queryVo) {
        return productChartMapper.enumerate(queryVo);
    }

    @Override
    @UserDataIsolation(tableAlias = "tbl_product_chart")
    public ProductChart retrieve(QueryVo queryVo) {
        ProductChart productChart = productChartMapper.retrieve(queryVo);
        if (productChart != null) {
            productChart.setIdentifiers(Arrays.asList(productChart.getIdentifiersString().split(",")));
        }
        return productChart;
    }

    @Override
    @UserDataIsolation(tableAlias = "tbl_product_chart")
    public Boolean update(ProductChart productChart) {
        Boolean done = false;
        try {
            Integer affected = productChartMapper.update(productChart);
            if (affected > 0) {
                done = true;
            }
        } catch (Exception e) {
            log.info("",e);
            StackTraceElement[] traces = e.getStackTrace();
            for (StackTraceElement trace : traces) {
                log.debug(trace.toString());
            }
        }
        return done;
    }

    @Override
    @UserDataIsolation(tableAlias = "tbl_product_chart")
    public Boolean delete(QueryVo queryVo) {
        Boolean done = false;
        try {
            Integer affected = productChartMapper.delete(queryVo);
            if (affected > 0) {
                done = true;
            }
        } catch (Exception e) {
            log.info("",e);
            StackTraceElement[] traces = e.getStackTrace();
            for (StackTraceElement trace : traces) {
                log.debug(trace.toString());
            }
        }
        return done;
    }
}
