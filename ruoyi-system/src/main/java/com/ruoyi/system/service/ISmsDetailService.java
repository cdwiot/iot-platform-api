package com.ruoyi.system.service;

import com.ruoyi.system.domain.SmsDetail;

import java.util.List;

public interface ISmsDetailService {

    /**
     * 短信明细的添加
     *
     * @param smsDetail
     * @return
     */
    Boolean addMessageDetail(SmsDetail smsDetail);

    /**
     * 短信明细的列表查询
     *
     * @param smsDetail
     * @return
     */
    List<SmsDetail> list(SmsDetail smsDetail);

    /**
     * 短信明细的详情
     *
     * @param smsDetail
     * @return
     */
    SmsDetail getOne(SmsDetail smsDetail);
}
