package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.common.annotation.UserDataIsolation;
import com.ruoyi.system.domain.AlarmDetail;
import com.ruoyi.system.domain.vo.QueryVo;
import com.ruoyi.system.mapper.AlarmDetailMapper;
import com.ruoyi.system.service.IMonitorConditionService;
import com.ruoyi.system.service.IAlarmDetailService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AlarmDetailServiceImpl implements IAlarmDetailService
{
    @Autowired
    private IMonitorConditionService monitorConditionService;

    @Autowired
    private AlarmDetailMapper alarmDetailMapper;

    @Override
    @UserDataIsolation(tableAlias = "d")
    public List<AlarmDetail> index(QueryVo queryVo) {
        List<AlarmDetail> alarmDetails = alarmDetailMapper.index(queryVo);
        for (AlarmDetail alarmDetail : alarmDetails) {
            QueryVo subQueryVo = new QueryVo();
            subQueryVo.filters.put("id", alarmDetail.getConditionId());
            alarmDetail.setCondition(monitorConditionService.retrieve(subQueryVo));
        }
        return alarmDetails;
    }
}
