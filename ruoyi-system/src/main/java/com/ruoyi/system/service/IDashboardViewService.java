package com.ruoyi.system.service;

import com.ruoyi.system.domain.DashboardView;
import com.ruoyi.system.domain.vo.QueryVo;

public interface IDashboardViewService {
    public DashboardView load(QueryVo queryVo);
    public Boolean save(DashboardView dashboardView);
}
