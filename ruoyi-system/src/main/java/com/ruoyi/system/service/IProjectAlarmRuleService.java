package com.ruoyi.system.service;

import java.util.List;
import java.util.Map;

import com.ruoyi.system.domain.ProjectAlarmRule;
import com.ruoyi.system.domain.vo.QueryVo;

public interface IProjectAlarmRuleService {
    public Map<String, Object> findExistance(ProjectAlarmRule projectAlarmRule);
    public Integer create(ProjectAlarmRule projectAlarmRule);
    public List<ProjectAlarmRule> index(QueryVo queryVo);
    public List<Map<String, Object>> available(QueryVo queryVo);
    public ProjectAlarmRule retrieve(QueryVo queryVo);
    public Boolean update(ProjectAlarmRule projectAlarmRule);
    public Boolean delete(QueryVo queryVo);
}
