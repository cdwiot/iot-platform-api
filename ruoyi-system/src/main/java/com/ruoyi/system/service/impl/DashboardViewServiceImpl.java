package com.ruoyi.system.service.impl;

import com.ruoyi.system.domain.DashboardView;
import com.ruoyi.system.domain.vo.QueryVo;
import com.ruoyi.system.mapper.DashboardViewMapper;
import com.ruoyi.system.service.IDashboardViewService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class DashboardViewServiceImpl implements IDashboardViewService
{
    @Autowired
    private DashboardViewMapper dashboardViewMapper;

    @Override
    public DashboardView load(QueryVo queryVo) {
        DashboardView dashboardView = dashboardViewMapper.retrieve(queryVo);
        if (dashboardView == null) {
            dashboardView = new DashboardView();
        }
        return dashboardView;
    }

    @Override
    public Boolean save(DashboardView dashboardView) {
        Boolean done = false;
        try {
            Integer affected = 0;
            QueryVo queryVo = new QueryVo();
            queryVo.filters.put("project_id", dashboardView.getProjectId());
            queryVo.filters.put("user_id", dashboardView.getUserId());
            queryVo.filters.put("company_id", dashboardView.getCompanyId());
            DashboardView target = dashboardViewMapper.retrieve(queryVo);
            if (target == null) {
                affected = dashboardViewMapper.create(dashboardView);
            } else {
                dashboardView.setId(target.getId());
                affected = dashboardViewMapper.update(dashboardView);
            }
            if (affected > 0) {
                done = true;
            }
        } catch (Exception e) {
            log.info("",e);
            StackTraceElement[] traces = e.getStackTrace();
            for (StackTraceElement trace : traces) {
                log.debug(trace.toString());
            }
        }
        return done;
    }
}
