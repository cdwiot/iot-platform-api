package com.ruoyi.system.service;

import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.system.domain.ReportDetail;
import com.ruoyi.system.domain.vo.DeviceStatusSummaryVO;
import com.ruoyi.system.domain.vo.QueryVo;
import com.ruoyi.system.domain.vo.SalesSummaryVO;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.List;
import java.util.Map;

public interface IReportDetailService {

    /**
     * 查询列表
     *
     * @param reportDetail
     * @return
     */
    List<ReportDetail> queryList(ReportDetail reportDetail);

    /**
     * 添加
     *
     * @param reportDetail
     * @return
     */
    Boolean add(ReportDetail reportDetail);

    /**
     * 导出手动报表
     *
     * @param reportDetail
     * @return
     */
    ReportDetail queryDetail(ReportDetail reportDetail, String tid, SysUser sysUser);

    /**
     * 定时生成报表
     */
    void createTimedReport();

    /**
     * 产品销售汇总
     *
     * @param queryVo
     * @return
     */
    List<SalesSummaryVO> salesSummary(QueryVo queryVo);

    /**
     * 产品状态汇总
     *
     * @param queryVo
     * @return
     */
    List<DeviceStatusSummaryVO> deviceStatusSummary(QueryVo queryVo);

    public List<Map<String, Object>> pointCheckStatusSummary(QueryVo queryVo, Map<String, Object> options);
    public List<Map<String, Object>> pointCheckAbnormalSummaryByMonth(QueryVo queryVo, Map<String, Object> options);

}
