package com.ruoyi.system.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.ruoyi.common.annotation.UserDataIsolation;
import com.ruoyi.system.domain.PointCheckTaskDeal;
import com.ruoyi.system.domain.vo.PointCheckTaskDealVO;
import com.ruoyi.system.mapper.PointCheckTaskDealMapper;
import com.ruoyi.system.service.IPointCheckTaskDealService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Slf4j
@Service
public class PointCheckTaskDealServiceImpl implements IPointCheckTaskDealService {

    @Autowired
    private PointCheckTaskDealMapper pointCheckTaskDealMapper;

    @Override
    @UserDataIsolation(tableAlias = "td")
    public List<PointCheckTaskDeal> queryList(PointCheckTaskDeal pointCheckTaskDeal) {
        List<PointCheckTaskDeal> pointCheckTaskDealList = pointCheckTaskDealMapper.queryList(pointCheckTaskDeal);
        if (CollUtil.isNotEmpty(pointCheckTaskDealList)){
            pointCheckTaskDealList.forEach(a ->{
                if (!StrUtil.equals(a.getWay(),"外部处理")){
                    a.setSalePhone(null);
                    a.setManufacturer(null);
                }
            });
        }
        return pointCheckTaskDealList;
    }

    @Override
    @UserDataIsolation(tableAlias = "td")
    public PointCheckTaskDeal getOne(PointCheckTaskDeal pointCheckTaskDeal) {
        PointCheckTaskDeal one = pointCheckTaskDealMapper.getOne(pointCheckTaskDeal);
        if (one != null && !StrUtil.equals(one.getWay(),"外部处理")){
            one.setSalePhone(null);
            one.setManufacturer(null);
        }
        return one;
    }

    @Override
    public Boolean add(PointCheckTaskDeal pointCheckTaskDeal) {
        PointCheckTaskDeal result = dealWithData(pointCheckTaskDeal);
        return pointCheckTaskDealMapper.add(result) > 0;
    }

    @Override
    @UserDataIsolation
    public Boolean update(PointCheckTaskDeal pointCheckTaskDeal) {
        PointCheckTaskDeal result = dealWithData(pointCheckTaskDeal);
        return pointCheckTaskDealMapper.update(result) > 0;
    }

    /**
     * 处理数据
     *
     * @param pointCheckTaskDeal
     */
    private PointCheckTaskDeal dealWithData(PointCheckTaskDeal pointCheckTaskDeal) {
        if (pointCheckTaskDeal == null){
            return null;
        }
        if (StrUtil.equals(pointCheckTaskDeal.getWay(),"数据保存")){
            PointCheckTaskDeal result = new PointCheckTaskDeal();
            result.setId(pointCheckTaskDeal.getId());
            result.setDetailId(pointCheckTaskDeal.getDetailId());
            result.setWay(pointCheckTaskDeal.getWay());
            result.setCompanyId(pointCheckTaskDeal.getCompanyId());
            result.setCreateBy(pointCheckTaskDeal.getCreateBy());
            result.setCreateTime(pointCheckTaskDeal.getCreateTime());
            return result;
        }
        if (StrUtil.equals(pointCheckTaskDeal.getType(),"继续使用")){
            PointCheckTaskDeal result = new PointCheckTaskDeal();
            result.setId(pointCheckTaskDeal.getId());
            result.setDetailId(pointCheckTaskDeal.getDetailId());
            result.setWay(pointCheckTaskDeal.getWay());
            result.setType(pointCheckTaskDeal.getType());
            result.setCompanyId(pointCheckTaskDeal.getCompanyId());
            result.setCreateBy(pointCheckTaskDeal.getCreateBy());
            result.setCreateTime(pointCheckTaskDeal.getCreateTime());
            return result;
        }
        if (StrUtil.equals(pointCheckTaskDeal.getType(),"产品更换") || StrUtil.equals(pointCheckTaskDeal.getType(),"报废与更换")){
            pointCheckTaskDeal.setRepairDept(null);
            pointCheckTaskDeal.setRepairWay(null);
            pointCheckTaskDeal.setTypeMsg(null);
            pointCheckTaskDeal.setPhotoRecord(null);
            return pointCheckTaskDeal;
        }
        if (StrUtil.equals(pointCheckTaskDeal.getType(),"修复")){
            pointCheckTaskDeal.setMakeUnit(null);
            pointCheckTaskDeal.setReplaceReason(null);
            pointCheckTaskDeal.setSn(null);
        }
        return pointCheckTaskDeal;
    }

    @Override
    @UserDataIsolation
    public Boolean delete(PointCheckTaskDeal pointCheckTaskDeal) {
        return pointCheckTaskDealMapper.delete(pointCheckTaskDeal) > 0;
    }

    @Override
    public List<PointCheckTaskDealVO> queryDeviceDealVO(Set<Integer> detailIds) {
        if (CollUtil.isEmpty(detailIds)){
            return new ArrayList<>();
        }
        List<PointCheckTaskDealVO> taskDealVOList = pointCheckTaskDealMapper.queryDeviceDealVO(detailIds);
        if (CollUtil.isNotEmpty(taskDealVOList)){
            taskDealVOList.forEach(a ->{
                    if (!StrUtil.equals(a.getWay(),"外部处理")){
                    a.setSalePhone(null);
                    a.setManufacturer(null);
                }
            });
        }
        return taskDealVOList;
    }
}
