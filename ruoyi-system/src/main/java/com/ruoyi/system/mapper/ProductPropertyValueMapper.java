package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.ProductPropertyValue;
import com.ruoyi.system.domain.vo.ProductPropertyValueVO;

import java.util.List;

public interface ProductPropertyValueMapper {

    /**
     * 根据产品查询属性值
     *
     * @param productPropertyValue
     * @return
     */
    List<ProductPropertyValue> queryProductValue(ProductPropertyValue productPropertyValue);

    /**
     * 添加产品属性值
     *
     * @param productPropertyValue
     * @return
     */
    int saveProductProperty(ProductPropertyValue productPropertyValue);

    /**
     * 产品属性值的修改
     *
     * @param productPropertyValue
     * @return
     */
    int updateProductProperty(ProductPropertyValue productPropertyValue);

    /**
     * 删除属性值
     *
     * @param productPropertyValue
     * @return
     */
    int deleteProductPropertyValue(ProductPropertyValue productPropertyValue);

    /**
     * 产品售后服务查询
     *
     * @param productPropertyValueVO
     * @return
     */
    List<ProductPropertyValueVO> getProductValue(ProductPropertyValueVO productPropertyValueVO);
}
