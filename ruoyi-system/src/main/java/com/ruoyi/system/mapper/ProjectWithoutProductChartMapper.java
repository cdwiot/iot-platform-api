package com.ruoyi.system.mapper;

import java.util.List;
import java.util.Map;

import com.ruoyi.system.domain.ProductChart;
import com.ruoyi.system.domain.vo.QueryVo;

import org.apache.ibatis.annotations.Param;

public interface ProjectWithoutProductChartMapper {
    public Integer clear(Integer id);
    public Integer block(@Param("productChartIdList") List<Integer> productChartIdList, @Param("id") Integer id);
    public List<ProductChart> get(QueryVo queryVo);
    public List<Map<String, Object>> availability(QueryVo queryVo);
}
