package com.ruoyi.system.mapper;

import java.util.List;
import java.util.Map;

import com.ruoyi.system.domain.DeviceTerminal;
import com.ruoyi.system.domain.vo.QueryVo;

public interface DeviceTerminalMapper {
    public Map<String, Object> findExistance(DeviceTerminal deviceTerminal);
    public Integer create(DeviceTerminal deviceTerminal);
    public List<DeviceTerminal> index(QueryVo queryVo);
    public List<Map<String, Object>> enumerate(QueryVo queryVo);
    public DeviceTerminal retrieve(QueryVo queryVo);
    public Integer update(DeviceTerminal deviceTerminal);
    public Integer delete(QueryVo queryVo);
}
