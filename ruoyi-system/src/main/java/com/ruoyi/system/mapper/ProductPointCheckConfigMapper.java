package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.ProductPointCheckConfig;
import com.ruoyi.system.domain.vo.QueryVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ProductPointCheckConfigMapper {

//    /**
//     * 产品型号点检配置分页查询
//     *
//     * @param queryVo
//     * @return
//     */
//    List<ProductPointCheckConfig> queryList(QueryVo queryVo);
//
//    /**
//     * 获取产品型号点检配置详情
//     *
//     * @param queryVo
//     * @return
//     */
//    ProductPointCheckConfig getOne(QueryVo queryVo);
//
//    /**
//     * 添加
//     *
//     * @param productPointCheckConfig
//     * @return
//     */
//    int add(ProductPointCheckConfig productPointCheckConfig);
//
//    /**
//     * 修改
//     *
//     * @param productPointCheckConfig
//     * @return
//     */
//    int update(ProductPointCheckConfig productPointCheckConfig);
//
//    /**
//     * 删除
//     *
//     * @param queryVo
//     * @return
//     */
//    int delete(QueryVo queryVo);
//
//    /**
//     * 批量删除
//     *
//     * @param ids
//     * @return
//     */
//    int deleteBatch(@Param("ids") List<String> ids);
//
//    /**
//     * 验证检查项是否重复
//     *
//     * @param productPointCheckConfig
//     * @return
//     */
//    ProductPointCheckConfig checkIsExist(ProductPointCheckConfig productPointCheckConfig);
//
//    /**
//     * 根据产品代码查询产品型号点检表单配置
//     *
//     * @param queryVo
//     * @return
//     */
//    List<ProductPointCheckConfig> queryPointCheckConfig(QueryVo queryVo);
}
