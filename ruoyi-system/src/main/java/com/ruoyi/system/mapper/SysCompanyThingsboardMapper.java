package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.SysCompanyThingsboard;

/**
 * 公司与thingsboard关联Mapper接口
 * 
 * @author ruoyi
 * @date 2021-08-02
 */
public interface SysCompanyThingsboardMapper 
{
    /**
     * 查询公司与thingsboard关联
     * 
     * @param id 公司与thingsboard关联ID
     * @return 公司与thingsboard关联
     */
    public SysCompanyThingsboard selectSysCompanyThingsboardById(Long id);

    /**
     * 查询公司与thingsboard关联列表
     * 
     * @param sysCompanyThingsboard 公司与thingsboard关联
     * @return 公司与thingsboard关联集合
     */
    public List<SysCompanyThingsboard> selectSysCompanyThingsboardList(SysCompanyThingsboard sysCompanyThingsboard);

    /**
     * 新增公司与thingsboard关联
     * 
     * @param sysCompanyThingsboard 公司与thingsboard关联
     * @return 结果
     */
    public int insertSysCompanyThingsboard(SysCompanyThingsboard sysCompanyThingsboard);

    /**
     * 修改公司与thingsboard关联
     * 
     * @param sysCompanyThingsboard 公司与thingsboard关联
     * @return 结果
     */
    public int updateSysCompanyThingsboard(SysCompanyThingsboard sysCompanyThingsboard);

    /**
     * 删除公司与thingsboard关联
     * 
     * @param id 公司与thingsboard关联ID
     * @return 结果
     */
    public int deleteSysCompanyThingsboardById(Long id);

    /**
     * 批量删除公司与thingsboard关联
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteSysCompanyThingsboardByIds(Long[] ids);
}
