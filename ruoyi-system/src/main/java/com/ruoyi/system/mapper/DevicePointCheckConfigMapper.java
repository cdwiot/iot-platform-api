package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.DevicePointCheckConfig;
import com.ruoyi.system.domain.vo.QueryVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface DevicePointCheckConfigMapper {

    /**
     * 产品点检配置分页查询
     *
     * @param queryVo
     * @return
     */
    List<DevicePointCheckConfig> queryList(QueryVo queryVo);

    List<DevicePointCheckConfig> getEnum(QueryVo queryVo);

    /**
     * 获取产品点检配置详情
     *
     * @param queryVo
     * @return
     */
    DevicePointCheckConfig getOne(QueryVo queryVo);

    /**
     * 添加
     *
     * @param devicePointCheckConfig
     * @return
     */
    int add(DevicePointCheckConfig devicePointCheckConfig);

    /**
     * 批量添加
     *
     * @param devicePointCheckConfigs
     * @return
     */
    int addBatch(@Param("configs") List<DevicePointCheckConfig> devicePointCheckConfigs);

    /**
     * 修改
     *
     * @param devicePointCheckConfig
     * @return
     */
    int update(DevicePointCheckConfig devicePointCheckConfig);
    int updateSys(QueryVo queryVo);
    int updateItem(QueryVo queryVo);
    int updateType(QueryVo queryVo);

    /**
     * 删除
     *
     * @param queryVo
     * @return
     */
    int delete(QueryVo queryVo);
    int deleteSys(QueryVo queryVo);

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    int deleteBatch(@Param("ids") List<String> ids);

    /**
     * 验证检查项是否重复
     *
     * @param devicePointCheckConfig
     * @return
     */
    DevicePointCheckConfig checkIsExist(DevicePointCheckConfig devicePointCheckConfig);
    List<DevicePointCheckConfig> checkIsType(DevicePointCheckConfig devicePointCheckConfig);


    /**
     * 验证检查内容是否重复
     *
     * @param devicePointCheckConfig
     * @return
     */
    List<DevicePointCheckConfig> checkIsItem(DevicePointCheckConfig devicePointCheckConfig);

    /**
     * 查询检查类别和检查项
     * @param queryVo
     * @return
     */
    List<DevicePointCheckConfig> typeItemDefaultEnum(QueryVo queryVo);

    /**
     * 删除产品的所有点检配置
     * @param queryVo
     * @return
     */
    int deleteDeviceId(Integer deviceId);
}
