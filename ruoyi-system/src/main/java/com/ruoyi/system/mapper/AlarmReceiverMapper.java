package com.ruoyi.system.mapper;

import java.util.List;
import java.util.Map;

import com.ruoyi.system.domain.AlarmReceiver;
import com.ruoyi.system.domain.vo.QueryVo;

public interface AlarmReceiverMapper {
    public void clear(QueryVo queryVo);
    public Integer set(List<AlarmReceiver> receivers);
    public List<Map<String, Object>> get(QueryVo queryVo);
}
