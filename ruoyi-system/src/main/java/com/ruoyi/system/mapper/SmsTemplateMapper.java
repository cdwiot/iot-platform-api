package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.SmsTemplate;

import java.util.List;

public interface SmsTemplateMapper {

    /**
     * 查询模板配置列表
     *
     * @param smsTemplate
     * @return
     */
    List<SmsTemplate> queryList(SmsTemplate smsTemplate);

    /**
     * 查询单个模板
     *
     * @param smsTemplate
     * @return
     */
    SmsTemplate getOne(SmsTemplate smsTemplate);

    /**
     * 添加模板
     *
     * @param smsTemplate
     * @return
     */
    int addMessageTemplate(SmsTemplate smsTemplate);

    /**
     * 修改模板
     *
     * @param smsTemplate
     * @return
     */
    int updateMessageTemplate(SmsTemplate smsTemplate);

    /**
     * 删除模板
     *
     * @param smsTemplate
     * @return
     */
    int deleteMessageTemplate(SmsTemplate smsTemplate);

    /**
     * 验证模板code是否重复
     *
     * @param smsTemplate
     * @return
     */
    SmsTemplate checkTemplateCodeExist(SmsTemplate smsTemplate);

}
