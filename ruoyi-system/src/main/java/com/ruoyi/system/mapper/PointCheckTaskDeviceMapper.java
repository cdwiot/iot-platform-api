package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.PointCheckDeviceDetail;
import com.ruoyi.system.domain.PointCheckTask;
import com.ruoyi.system.domain.PointCheckTaskDevice;
import com.ruoyi.system.domain.vo.PointCheckDeviceDetailDTO;
import com.ruoyi.system.domain.vo.QueryVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface PointCheckTaskDeviceMapper {
    /**
     * 任务产品关系的列表查询
     *
     * @param pointCheckTaskDevice
     * @return
     */
    List<PointCheckTaskDevice> queryList(PointCheckTaskDevice pointCheckTaskDevice);
    List<PointCheckTaskDevice> queryDeviceList(PointCheckTaskDevice pointCheckTaskDevice);

    /**
     * 获取详情
     *
     * @param pointCheckTaskDevice
     * @return
     */
    PointCheckTaskDevice getOne(PointCheckTaskDevice pointCheckTaskDevice);

    /**
     * 添加
     *
     * @param pointCheckTaskDevice
     * @return
     */
    int add(PointCheckTaskDevice pointCheckTaskDevice);

    /**
     * 修改
     *
     * @param pointCheckTaskDevice
     * @return
     */
    int update(PointCheckTaskDevice pointCheckTaskDevice);

    /**
     * 删除
     *
     * @param pointCheckTaskDevice
     * @return
     */
    int delete(PointCheckTaskDevice pointCheckTaskDevice);

    /**
     * 验证产品是否重复
     *
     * @param pointCheckTaskDevice
     * @return
     */
    PointCheckTaskDevice checkIsExist(PointCheckTaskDevice pointCheckTaskDevice);


    /**
     * 根据任务产品关系Id查询任务信息
     *
     * @param pointCheckTaskDevice
     * @return
     */
    PointCheckTask queryTask(PointCheckTaskDevice pointCheckTaskDevice);


    /**
     * 查询所有的任务产品
     *
     * @param queryVo
     * @return
     */
    List<PointCheckTaskDevice> queryAllTaskDevice(QueryVo queryVo);

    /**
     * 查询所有异常检查项数据
     *
     * @return
     */
    List<Map<String, Object>> queryAllErrorItem(QueryVo queryVo);

    /**
     * APP根据产品代码获取产品跳转信息
     *
     * @param queryVo
     * @return
     */
    List<PointCheckDeviceDetailDTO> getDeviceDetail(QueryVo queryVo);

    /**
     * 根据产品代码查询自己当天关联该产品的点检任务
     * @param queryVo
     * @return
     */
    List<PointCheckTaskDevice> queryTaskDevice(QueryVo queryVo);
}
