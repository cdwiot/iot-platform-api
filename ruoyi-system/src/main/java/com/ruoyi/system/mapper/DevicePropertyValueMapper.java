package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.DevicePropertyValue;

import java.util.List;

public interface DevicePropertyValueMapper {

    /**
     * 根据产品查询属性值
     *
     * @param devicePropertyValue
     * @return
     */
    List<DevicePropertyValue> queryDeviceValue(DevicePropertyValue devicePropertyValue);

    /**
     * 产品属性的添加
     *
     * @param devicePropertyValue
     * @return
     */
    int saveDeviceProperty(DevicePropertyValue devicePropertyValue);

    /**
     * 产品属性的修改
     *
     * @param devicePropertyValue
     * @return
     */
    int updateDeviceProperty(DevicePropertyValue devicePropertyValue);

    /**
     * 产品属性值的删除
     *
     * @param devicePropertyValue
     * @return
     */
    int deleteDevicePropertyValue(DevicePropertyValue devicePropertyValue);
}
