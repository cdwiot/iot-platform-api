package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.OfficialAccountTemplate;

import java.util.List;

public interface OfficialAccountTemplateMapper {

    /**
     * 查询公众号模板列表
     *
     * @param officialAccountTemplate
     * @return
     */
    List<OfficialAccountTemplate> queryList(OfficialAccountTemplate officialAccountTemplate);

    /**
     * 获取模板详情
     *
     * @param officialAccountTemplate
     * @return
     */
    OfficialAccountTemplate getOne(OfficialAccountTemplate officialAccountTemplate);

    /**
     * 添加模板
     *
     * @param officialAccountTemplate
     * @return
     */
    int add(OfficialAccountTemplate officialAccountTemplate);

    /**
     * 修改模板
     *
     * @param officialAccountTemplate
     * @return
     */
    int update(OfficialAccountTemplate officialAccountTemplate);

    /**
     * 删除模板
     *
     * @param officialAccountTemplate
     * @return
     */
    int delete(OfficialAccountTemplate officialAccountTemplate);

    /**
     * 验证模板编码是否重复
     *
     * @param officialAccountTemplate
     * @return
     */
    OfficialAccountTemplate checkTemplateCodeExist(OfficialAccountTemplate officialAccountTemplate);
}
