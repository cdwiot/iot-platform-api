package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.DashboardView;
import com.ruoyi.system.domain.vo.QueryVo;

public interface DashboardViewMapper {
    public Integer create(DashboardView dashboardView);
    public DashboardView retrieve(QueryVo queryVo);
    public Integer update(DashboardView dashboardView);
}
