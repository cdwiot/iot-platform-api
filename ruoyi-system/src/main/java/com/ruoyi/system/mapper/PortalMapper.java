package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.Portal;
import com.ruoyi.system.domain.vo.QueryVo;

public interface PortalMapper {
    public Integer create(Portal portal);
    public Portal retrieve(QueryVo queryVo);
    public Integer update(Portal portal);
}
