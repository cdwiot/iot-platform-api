package com.ruoyi.system.mapper;

import java.util.List;
import java.util.Map;

import com.ruoyi.system.domain.Alarm;
import com.ruoyi.system.domain.vo.QueryVo;

public interface AlarmMapper {
    public List<Alarm> index(QueryVo queryVo);
    public Alarm retrieve(QueryVo queryVo);
    public Integer update(Alarm monitorEvent);
    public Map<String, Object> summary(QueryVo queryVo);
}
