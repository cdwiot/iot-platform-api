package com.ruoyi.system.mapper;

import java.util.List;
import java.util.Map;

import com.ruoyi.system.domain.Terminal;
import com.ruoyi.system.domain.vo.QueryVo;

public interface TerminalMapper 
{
    public Map<String, Object> findExistance(Terminal terminal);
    public Integer create(Terminal terminal);
    public List<Terminal> index(QueryVo queryVo);
    public List<Map<String, Object>> enumerate(QueryVo queryVo);
    public Terminal retrieve(QueryVo queryVo);
    public Integer update(Terminal terminal);
    public Integer delete(QueryVo queryVo);
}
