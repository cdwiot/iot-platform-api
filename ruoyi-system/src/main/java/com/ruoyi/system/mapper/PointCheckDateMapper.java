package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.PointCheckDate;

import java.util.List;

public interface PointCheckDateMapper {

    /**
     * 点检计划时间列表
     *
     * @param pointCheckDate
     * @return
     */
    List<PointCheckDate> queryList(PointCheckDate pointCheckDate);

    /**
     * 添加
     *
     * @param pointCheckDate
     * @return
     */
    int add(PointCheckDate pointCheckDate);

    /**
     * 修改
     *
     * @param pointCheckDate
     * @return
     */
    int update(PointCheckDate pointCheckDate);

    /**
     * 删除
     *
     * @param pointCheckDate
     * @return
     */
    int delete(PointCheckDate pointCheckDate);
}
