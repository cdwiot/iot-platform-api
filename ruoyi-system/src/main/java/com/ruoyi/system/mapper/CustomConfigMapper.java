package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.CustomConfig;
import com.ruoyi.system.domain.vo.QueryVo;

public interface CustomConfigMapper {
    public Integer create(CustomConfig customConfig);
    public CustomConfig retrieve(QueryVo queryVo);
    public Integer update(CustomConfig customConfig);
}
