package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.PointCheckConfig;
import com.ruoyi.system.domain.vo.QueryVo;

import java.util.ArrayList;
import java.util.List;

public interface PointCheckConfigMapper {

    /**
     * 计划-点检配置查询
     *
     * @param pointChPointCheckConfigeck
     * @return
     */
    List<PointCheckConfig> queryList(PointCheckConfig pointCheckConfig);

    /**
     * 添加
     *
     * @param PointCheckConfig
     * @return
     */
    int add(PointCheckConfig pointCheckConfig);


    /**
     * 删除
     *
     * @param PointCheckConfig
     * @return
     */
    int deletePointId(Integer pointId);
    int deletePointIdDeviceId(QueryVo queryVo);




}
