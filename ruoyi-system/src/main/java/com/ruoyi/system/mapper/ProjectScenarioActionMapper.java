package com.ruoyi.system.mapper;

import java.util.List;
import java.util.Map;

import com.ruoyi.system.domain.ProjectScenarioAction;
import com.ruoyi.system.domain.vo.QueryVo;

public interface ProjectScenarioActionMapper {
    public Map<String, Object> findExistance(ProjectScenarioAction projectScenarioAction);
    public Integer create(ProjectScenarioAction projectScenarioAction);
    public List<ProjectScenarioAction> index(QueryVo queryVo);
    public ProjectScenarioAction retrieve(QueryVo queryVo);
    public Integer update(ProjectScenarioAction projectScenarioAction);
    public Integer delete(QueryVo queryVo);
}
