package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.DeviceProperty;

import java.util.List;

public interface DevicePropertyMapper {

    /**
     * 产品额外属性的查询
     *
     * @param deviceProperty
     * @return
     */
    List<DeviceProperty> queryPageList(DeviceProperty deviceProperty);

    /**
     * 产品额外属性的添加
     *
     * @param deviceProperty
     * @return
     */
    Integer addDeviceProperty(DeviceProperty deviceProperty);

    /**
     * 查询产品额外属性详情
     *
     * @param deviceProperty
     * @return
     */
    DeviceProperty queryById(DeviceProperty deviceProperty);

    /**
     * 产品额外属性的修改
     *
     * @param deviceProperty
     * @return
     */
    Integer updateDeviceProperty(DeviceProperty deviceProperty);

    /**
     * 产品额外属性的删除
     *
     * @param deviceProperty
     * @return
     */
    Integer deleteDeviceProperty(DeviceProperty deviceProperty);

    /**
     * 验证属性名称是否重复
     *
     * @param deviceProperty
     * @return
     */
    DeviceProperty checkPropertyName(DeviceProperty deviceProperty);
}
