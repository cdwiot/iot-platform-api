package com.ruoyi.system.mapper;

import java.util.List;
import java.util.Map;

import com.ruoyi.system.domain.ProjectAlarmRule;
import com.ruoyi.system.domain.vo.QueryVo;

public interface ProjectAlarmRuleMapper {
    public Map<String, Object> findExistance(ProjectAlarmRule projectAlarmRule);
    public Integer create(ProjectAlarmRule projectAlarmRule);
    public List<ProjectAlarmRule> index(QueryVo queryVo);
    public List<Map<String, Object>> available(QueryVo queryVo);
    public ProjectAlarmRule retrieve(QueryVo queryVo);
    public Integer update(ProjectAlarmRule projectAlarmRule);
    public Integer delete(QueryVo queryVo);
}
