package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.AsyncDetail;
import com.ruoyi.system.domain.vo.QueryVo;

public interface AsyncDetailMapper {

    /**
     * 创建tid
     *
     * @param asyncDetail
     * @return
     */
    int create(AsyncDetail asyncDetail);

    /**
     * 修改
     *
     * @param asyncDetail
     * @return
     */
    int update(AsyncDetail asyncDetail);

    public AsyncDetail retrieve(QueryVo queryVo);
}
