package com.ruoyi.system.mapper;

import java.util.List;
import java.util.Map;

import com.ruoyi.system.domain.ProductTelemetry;
import com.ruoyi.system.domain.vo.QueryVo;

@SuppressWarnings("AlibabaLowerCamelCaseVariableNaming")
public interface ProductTelemetryMapper {
    public Map<String, Object> findExistance(ProductTelemetry ProductTelemetry);
    public Integer create(ProductTelemetry ProductTelemetry);
    public List<ProductTelemetry> index(QueryVo queryVo);
    public List<Map<String, Object>> enumerate(QueryVo queryVo);
    public ProductTelemetry retrieve(QueryVo queryVo);
    public Integer update(ProductTelemetry ProductTelemetry);
    public Integer delete(QueryVo queryVo);
}
