package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.PointCheck;
import com.ruoyi.system.domain.vo.QueryVo;
import io.lettuce.core.dynamic.annotation.Param;

import java.util.List;
import java.util.Map;

public interface PointCheckMapper {
    /**
     * 点检计划分页查询
     *
     * @param pointCheck
     * @return
     */
    List<PointCheck> queryList(PointCheck pointCheck);
    List<PointCheck> queryGroupList(PointCheck pointCheck);

    /**
     * 点检计划详情
     *
     * @param pointCheck
     * @return
     */
    PointCheck getOne(PointCheck pointCheck);

    /**
     * 添加
     *
     * @param pointCheck
     * @return
     */
    int add(PointCheck pointCheck);

    /**
     * 编辑
     *
     * @param pointCheck
     * @return
     */
    int update(PointCheck pointCheck);

    /**
     * 删除
     *
     * @param pointCheck
     * @return
     */
    int delete(PointCheck pointCheck);

    /**
     * 根据名称和系统字段检查是否重复
     *
     * @param pointCheck
     * @return
     */
    PointCheck checkIsExist(PointCheck pointCheck);

    /**
     * 获取该公司下所有用户信息
     * @param queryVo
     * @return
     */
    List<Map<String,Object>> queryUserInfo(QueryVo queryVo);

    /**
     *
     * @param queryVo
     * @return
     */
    List<Map<String,String>> queryAuthorizeEnum(QueryVo queryVo);
}
