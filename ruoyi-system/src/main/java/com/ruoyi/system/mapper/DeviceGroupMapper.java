package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.DeviceGroup;
import com.ruoyi.system.domain.ProjectDeviceGroup;

/**
 * 产品分组和产品关联Mapper接口
 *
 * @author ruoyi
 * @date 2021-08-10
 */
@SuppressWarnings("AlibabaLowerCamelCaseVariableNaming")
public interface DeviceGroupMapper
{
    /**
     * 查询产品分组和产品关联
     *
     * @param groupId 产品分组和产品关联ID
     * @return 产品分组和产品关联
     */
    public DeviceGroup selectTblDeviceGroupById(Long groupId);

    /**
     * 查询产品分组和产品关联列表
     *
     * @param deviceGroup 产品分组和产品关联
     * @return 产品分组和产品关联集合
     */
    public List<DeviceGroup> selectTblDeviceGroupList(DeviceGroup deviceGroup);

    /**
     * 新增产品分组和产品关联
     *
     * @param deviceGroup 产品分组和产品关联
     * @return 结果
     */
    public int insertTblDeviceGroup(DeviceGroup deviceGroup);

    /**
     * 修改产品分组和产品关联
     *
     * @param deviceGroup 产品分组和产品关联
     * @return 结果
     */
    public int updateTblDeviceGroup(DeviceGroup deviceGroup);

    /**
     * 删除产品分组和产品关联
     *
     * @param groupId 产品分组和产品关联ID
     * @return 结果
     */
    public int deleteTblDeviceGroupById(Integer groupId);

    /**
     * 批量删除产品分组和产品关联
     *
     * @param groupIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteTblDeviceGroupByIds(Long[] groupIds);
    /**
     * 删除产品分组和产品关联
     *
     * @param groupId需要删除的数据ID
     * @return 结果
     */
    public int deleteTblDeviceGroupByGId(Long id);
}