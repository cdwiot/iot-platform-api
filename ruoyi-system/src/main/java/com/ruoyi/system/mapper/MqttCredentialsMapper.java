package com.ruoyi.system.mapper;

import java.util.Map;

import com.ruoyi.system.domain.MqttCredentials;
import com.ruoyi.system.domain.vo.QueryVo;

public interface MqttCredentialsMapper 
{
    public Map<String, Object> findExistance(MqttCredentials mqttCredentials);
    public Integer create(MqttCredentials mqttCredentials);
    public MqttCredentials retrieve(QueryVo queryVo);
    public Integer update(MqttCredentials mqttCredentials);
    public Integer delete(QueryVo queryVo);
}
