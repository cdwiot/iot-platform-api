package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.SmsAccount;

import java.util.List;

public interface SmsAccountMapper {

    /**
     * 账户的查询
     *
     * @param smsAccount
     * @return
     */
    List<SmsAccount> queryList(SmsAccount smsAccount);

    /**
     * 查询公司账户信息
     *
     * @param smsAccount
     * @return
     */
    SmsAccount getOne(SmsAccount smsAccount);

    /**
     * 根据数据权限获取账户信息
     *
     * @param smsAccount
     * @return
     */
    SmsAccount getMessageAccount(SmsAccount smsAccount);

    /**
     * 默认查询大账户信息
     *
     * @param smsAccount
     * @return
     */
    SmsAccount getCompanyOne(SmsAccount smsAccount);

    /**
     * 添加账户
     *
     * @param smsAccount
     * @return
     */
    int addMessageAccount(SmsAccount smsAccount);

    /**
     * 修改账户
     *
     * @param smsAccount
     * @return
     */
    int updateMessageAccount(SmsAccount smsAccount);

    /**
     * 删除账户
     *
     * @param smsAccount
     * @return
     */
    int deleteMessageAccount(SmsAccount smsAccount);
}
