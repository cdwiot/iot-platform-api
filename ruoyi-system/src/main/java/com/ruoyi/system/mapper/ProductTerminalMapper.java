package com.ruoyi.system.mapper;

import java.util.List;
import java.util.Map;

import com.ruoyi.system.domain.ProductTerminal;
import com.ruoyi.system.domain.vo.QueryVo;

public interface ProductTerminalMapper 
{
    public Map<String, Object> findExistance(ProductTerminal productTerminal);
    public Integer create(ProductTerminal productTerminal);
    public List<ProductTerminal> index(QueryVo queryVo);
    public List<Map<String, Object>> enumerate(QueryVo queryVo);
    public ProductTerminal retrieve(QueryVo queryVo);
    public Integer update(ProductTerminal productTerminal);
    public Integer delete(QueryVo queryVo);
}
