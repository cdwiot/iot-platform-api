package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.EmailDetail;

import java.util.List;

public interface EmailDetailMapper {

    /**
     * 添加
     *
     * @param emailDetail
     * @return
     */
    int add(EmailDetail emailDetail);

    /**
     * 列表查询
     *
     * @param emailDetail
     * @return
     */
    List<EmailDetail> list(EmailDetail emailDetail);

    /**
     * 邮件详情
     *
     * @param emailDetail
     * @return
     */
    EmailDetail getOne(EmailDetail emailDetail);
}
