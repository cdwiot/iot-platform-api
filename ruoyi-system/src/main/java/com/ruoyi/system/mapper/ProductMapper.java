package com.ruoyi.system.mapper;

import java.util.List;
import java.util.Map;

import com.ruoyi.system.domain.Product;
import com.ruoyi.system.domain.vo.QueryVo;

public interface ProductMapper 
{
    public Map<String, Object> findExistance(Product product);
    public Integer create(Product product);
    public List<Product> index(QueryVo queryVo);
    public List<Map<String, Object>> enumerate(QueryVo queryVo);
    public Product retrieve(QueryVo queryVo);
    public Product lookup(QueryVo queryVo);
    public Integer update(Product product);
    public Integer delete(QueryVo queryVo);
    public List<Product> projectIndex(QueryVo queryVo);
    public Map<String, Object> summary(QueryVo queryVo);
}
