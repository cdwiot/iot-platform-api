package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.ReportDetail;
import com.ruoyi.system.domain.vo.DeviceStatusSummaryVO;
import com.ruoyi.system.domain.vo.QueryVo;
import com.ruoyi.system.domain.vo.SalesSummaryVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface ReportDetailMapper {

    /**
     * 查询列表
     *
     * @param reportDetail
     * @return
     */
    List<ReportDetail> queryList(ReportDetail reportDetail);


    /**
     * 添加
     *
     * @param reportDetail
     * @return
     */
    int add(ReportDetail reportDetail);

    /**
     * 修改
     *
     * @param reportDetail
     * @return
     */
    int update(ReportDetail reportDetail);

    /**
     * 膨胀节销售汇总
     *
     * @param queryVo
     * @return
     */
    List<SalesSummaryVO> salesSummary(QueryVo queryVo);

    /**
     * 查询产品异常汇总位置时间
     * @param sn
     * @return
     */
    List<Map<String,Object>> queryErrorAddress(QueryVo queryVo);

    /**
     * 查询产品最新点检信息
     *
     * @param queryVo
     * @return
     */
    List<DeviceStatusSummaryVO> queryDeviceStatusVO(QueryVo queryVo);

    /**
     * 查询产品处理次数
     *
     * @param queryVo
     * @return
     */
    List<Map<String,Object>> queryDeviceDealTimes(QueryVo queryVo);


}
