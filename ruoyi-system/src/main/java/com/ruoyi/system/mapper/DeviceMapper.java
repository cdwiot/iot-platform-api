package com.ruoyi.system.mapper;

import java.util.List;
import java.util.Map;

import com.ruoyi.system.domain.Device;
import com.ruoyi.system.domain.vo.QueryVo;

import org.apache.ibatis.annotations.Param;

public interface DeviceMapper {
    public Map<String, Object> findExistance(Device device);
    public Integer create(Device device);
    public List<Device> index(QueryVo queryVo);
    public List<Device> quertList();
    public List<Map<String, Object>> provinceSummary(QueryVo queryVo);
    public List<Map<String, Object>> enumerate(QueryVo queryVo);
    public List<Map<Integer, String>> enumerateAvailable(QueryVo queryVo);
    public Device retrieve(QueryVo queryVo);
    public Integer update(Device device);
    public Integer discard(QueryVo queryVo);
    public Integer delete(QueryVo queryVo);
    public Integer assignToProject(@Param("ids") List<Integer> ids, @Param("projectId") Integer projectId);
    public Map<String, Object> summary(QueryVo queryVo);
    public List<Map<String, Object>> deviceAndTerminalCountByMonth(QueryVo queryVo);
}
