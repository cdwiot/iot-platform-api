package com.ruoyi.system.mapper;

import java.util.List;
import java.util.Map;

import com.ruoyi.system.domain.ProjectScenarioRule;
import com.ruoyi.system.domain.vo.QueryVo;

public interface ProjectScenarioRuleMapper {
    public Map<String, Object> findExistance(ProjectScenarioRule projectScenarioRule);
    public Integer create(ProjectScenarioRule projectScenarioRule);
    public List<ProjectScenarioRule> index(QueryVo queryVo);
    public List<Map<String, Object>> available(QueryVo queryVo);
    public ProjectScenarioRule retrieve(QueryVo queryVo);
    public Integer update(ProjectScenarioRule projectScenarioRule);
    public Integer delete(QueryVo queryVo);
}
