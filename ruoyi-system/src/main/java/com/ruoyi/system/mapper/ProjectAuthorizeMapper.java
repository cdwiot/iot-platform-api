package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.ProjectAuthorize;

import java.util.List;
import java.util.Set;

public interface ProjectAuthorizeMapper {

    /**
     * 获取所有的用户信息
     *
     * @param projectAuthorize
     * @return
     */
    List<ProjectAuthorize> queryAllUserInfo(ProjectAuthorize projectAuthorize);

    /**
     * 查询当前公司非客户和巡检工角色用户
     *
     * @param projectAuthorize
     * @return
     */
    Set<ProjectAuthorize> queryCompanyUser(ProjectAuthorize projectAuthorize);

    /**
     * 获取已绑定的用户信息
     *
     * @param projectAuthorize
     * @return
     */
    List<ProjectAuthorize> queryAuthorizeUserInfo(ProjectAuthorize projectAuthorize);

    /**
     * 新增
     *
     * @param projectAuthorize
     * @return
     */
    int add(ProjectAuthorize projectAuthorize);

    /**
     * 删除
     *
     * @param projectAuthorize
     * @return
     */
    int delete(ProjectAuthorize projectAuthorize);

    /**
     * 验证该用户是否在该项目的授权用户中
     *
     * @param projectAuthorize
     * @return
     */
    List<ProjectAuthorize> checkProjectIsOwnerByUserId(ProjectAuthorize projectAuthorize);

    /**
     * 授权详细
     *
     * @param projectAuthorize
     * @return
     */
    ProjectAuthorize getOne(ProjectAuthorize projectAuthorize);
}
