package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.PointCheckTaskDeal;
import com.ruoyi.system.domain.vo.PointCheckTaskDealVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Set;

public interface PointCheckTaskDealMapper {

    /**
     * 处理方式的列表查询
     *
     * @param pointCheckTaskDeal
     * @return
     */
    List<PointCheckTaskDeal> queryList(PointCheckTaskDeal pointCheckTaskDeal);

    /**
     * 处理方式的详情
     *
     * @param pointCheckTaskDeal
     * @return
     */
    PointCheckTaskDeal getOne(PointCheckTaskDeal pointCheckTaskDeal);

    /**
     * 添加
     *
     * @param pointCheckTaskDeal
     * @return
     */
    int add(PointCheckTaskDeal pointCheckTaskDeal);

    /**
     * 修改
     *
     * @param pointCheckTaskDeal
     * @return
     */
    int update(PointCheckTaskDeal pointCheckTaskDeal);

    /**
     * 删除
     *
     * @param pointCheckTaskDeal
     * @return
     */
    int delete(PointCheckTaskDeal pointCheckTaskDeal);

    /**
     * 根据产品检查Id查询处理方式
     *
     * @param detailIds
     * @return
     */
    List<PointCheckTaskDealVO> queryDeviceDealVO(@Param("detailIds") Set<Integer> detailIds);
}
