package com.ruoyi.system.mapper;

import java.util.List;
import java.util.Map;

import com.ruoyi.system.domain.Project;
import com.ruoyi.system.domain.vo.QueryVo;

public interface ProjectMapper {
    public Map<String, Object> findExistance(Project project);
    public Integer create(Project project);
    public List<Project> index(QueryVo queryVo);
    public Project retrieve(QueryVo queryVo);
    public Integer update(Project project);
    public Map<String, Object> summary(QueryVo queryVo);
    public List<Map<String, Object>> deviceCount(QueryVo queryVo);
    public List<Map<String, Object>> configuredTerminalCount(QueryVo queryVo);
    public List<Map<String, Object>> valueByMonth(QueryVo queryVo);
    public List<Map<String, Object>> statusByMonth(QueryVo queryVo);

}
