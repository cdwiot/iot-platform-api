package com.ruoyi.system.mapper;

import java.util.Map;

import com.ruoyi.system.domain.CoapCredentials;
import com.ruoyi.system.domain.vo.QueryVo;

public interface CoapCredentialsMapper 
{
    public Map<String, Object> findExistance(CoapCredentials coapCredentials);
    public Integer create(CoapCredentials coapCredentials);
    public CoapCredentials retrieve(QueryVo queryVo);
    public Integer update(CoapCredentials coapCredentials);
}
