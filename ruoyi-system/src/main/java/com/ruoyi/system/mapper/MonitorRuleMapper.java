package com.ruoyi.system.mapper;

import java.util.List;
import java.util.Map;

import com.ruoyi.system.domain.MonitorRule;
import com.ruoyi.system.domain.vo.QueryVo;

public interface MonitorRuleMapper 
{
    public Map<String, Object> findExistance(MonitorRule monitorRule);
    public Integer create(MonitorRule monitorRule);
    public List<MonitorRule> index(QueryVo queryVo);
    public List<Map<String, Object>> enumerate(QueryVo queryVo);
    public MonitorRule retrieve(QueryVo queryVo);
    public Integer update(MonitorRule monitorRule);
    public Integer delete(QueryVo queryVo);
}
