package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.RpcMsgLog;
import com.ruoyi.system.domain.vo.QueryVo;

import java.util.List;

/**
 * ws发送rpc消息记录 Mapper
 */
public interface RpcMsgLogMapper {

        List<RpcMsgLog> query4List(QueryVo vo);
        List<RpcMsgLog> queryList(QueryVo vo);

        void insertRpcMsgLog(RpcMsgLog rpcMsgLog);

        int updateNotNullByPrimaryKeySelective(RpcMsgLog bean);
}

