package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.EmailDetail;
import com.ruoyi.system.domain.OfficialDetail;

import java.util.List;

public interface OfficialDetailMapper {

    /**
     * 添加
     *
     * @param officialDetail
     * @return
     */
    int add(OfficialDetail officialDetail);


    /**
     * 列表查询
     *
     * @param officialDetail
     * @return
     */
    List<OfficialDetail> list(OfficialDetail officialDetail);

    /**
     * 邮件详情
     *
     * @param officialDetail
     * @return
     */
    OfficialDetail getOne(OfficialDetail officialDetail);
}
