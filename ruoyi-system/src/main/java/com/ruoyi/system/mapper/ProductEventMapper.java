package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.ProductEvent;

import java.util.List;

public interface ProductEventMapper {

    /**
     * 列表查询
     *
     * @param productEvent
     * @return
     */
    List<ProductEvent> queryList(ProductEvent productEvent);

    /**
     * 获取详情
     *
     * @param productEvent
     * @return
     */
    ProductEvent getOne(ProductEvent productEvent);

    /**
     * 添加
     *
     * @param productEvent
     * @return
     */
    int add(ProductEvent productEvent);

    /**
     * 修改
     *
     * @param productEvent
     * @return
     */
    int update(ProductEvent productEvent);

    /**
     * 删除
     *
     * @param productEvent
     * @return
     */
    int delete(ProductEvent productEvent);

    /**
     * 验证是否重复
     *
     * @param productEvent
     * @return
     */
    ProductEvent checkIsExit(ProductEvent productEvent);
}
