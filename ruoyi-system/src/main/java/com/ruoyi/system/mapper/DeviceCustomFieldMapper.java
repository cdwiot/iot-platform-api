package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.DeviceCustomField;
import com.ruoyi.system.domain.vo.QueryVo;

import java.util.List;

public interface DeviceCustomFieldMapper {

    /**
     * 分页查询
     *
     * @param queryVo
     * @return
     */
    List<DeviceCustomField> queryPageList(QueryVo queryVo);

    /**
     * 添加
     *
     * @param deviceCustomField
     * @return
     */
    Integer add(DeviceCustomField deviceCustomField);

    /**
     * 查询详情
     *
     * @param queryVo
     * @return
     */
    DeviceCustomField getOne(QueryVo queryVo);

    /**
     * 修改
     *
     * @param deviceCustomField
     * @return
     */
    Integer update(DeviceCustomField deviceCustomField);

    /**
     * 删除
     *
     * @param queryVo
     * @return
     */
    Integer delete(QueryVo queryVo);

    /**
     * 验证子定义名称是否重复
     *
     * @param deviceCustomField
     * @return
     */
    DeviceCustomField checkIsExist(DeviceCustomField deviceCustomField);
}
