package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.OfficialAccount;

import java.util.List;

public interface OfficialAccountMapper {

    /**
     * 账户的查询
     *
     * @param officialAccount
     * @return
     */
    List<OfficialAccount> queryList(OfficialAccount officialAccount);

    /**
     * 查询公司账户信息
     *
     * @param officialAccount
     * @return
     */
    OfficialAccount getOne(OfficialAccount officialAccount);

    /**
     * 根据数据权限获取账户信息
     *
     * @param officialAccount
     * @return
     */
    OfficialAccount getOfficialAccount(OfficialAccount officialAccount);

    /**
     * 默认查询大账户信息
     *
     * @param officialAccount
     * @return
     */
    OfficialAccount getCompanyOne(OfficialAccount officialAccount);

    /**
     * 添加账户
     *
     * @param officialAccount
     * @return
     */
    int addOfficialAccount(OfficialAccount officialAccount);

    /**
     * 修改账户
     *
     * @param officialAccount
     * @return
     */
    int updateOfficialAccount(OfficialAccount officialAccount);

    /**
     * 删除账户
     *
     * @param officialAccount
     * @return
     */
    int deleteOfficialAccount(OfficialAccount officialAccount);
}
