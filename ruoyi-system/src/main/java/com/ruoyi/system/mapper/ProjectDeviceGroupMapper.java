package com.ruoyi.system.mapper;

import java.util.List;

import com.ruoyi.system.domain.Device;
import com.ruoyi.system.domain.ProjectDeviceGroup;
import com.ruoyi.system.domain.vo.ProjectGroupDeviceVO;
import com.ruoyi.system.domain.vo.ProjectGroupParamVO;
import io.lettuce.core.dynamic.annotation.Param;

/**
 * 产品分组Mapper接口
 *
 * @author ruoyi
 * @date 2021-08-10
 */
public interface ProjectDeviceGroupMapper {
    /**
     * 查询产品分组
     *
     * @param projectDeviceGroup
     * @return 产品分组
     */
    public ProjectDeviceGroup selectTblProjectDeviceGroupById(ProjectDeviceGroup projectDeviceGroup);

    /**
     * 根据产品分组Id查询产品信息
     *
     * @param groupId
     * @return
     */
    List<Device> getDeviceByGroupId(@Param("groupId") Integer groupId);

    /**
     * 查询产品分组列表
     *
     * @param projectDeviceGroup 产品分组
     * @return 产品分组集合
     */
    public List<ProjectDeviceGroup> selectTblProjectDeviceGroupList(ProjectDeviceGroup projectDeviceGroup);

    /**
     * 新增产品分组
     *
     * @param projectDeviceGroup 产品分组
     * @return 结果
     */
    public int insertTblProjectDeviceGroup(ProjectDeviceGroup projectDeviceGroup);

    public int saveDeviceGroup();

    /**
     * 修改产品分组
     *
     * @param projectDeviceGroup 产品分组
     * @return 结果
     */
    public int updateTblProjectDeviceGroup(ProjectDeviceGroup projectDeviceGroup);

    /**
     * 删除产品分组
     *
     * @param id 产品分组ID
     * @return 结果
     */
    public int deleteTblProjectDeviceGroupById(Integer id);

    /**
     * 批量删除产品分组
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteTblProjectDeviceGroupByIds(Long[] ids);

    /**
     * 查询需要绑定的产品信息
     *
     * @param projectGroupParamVO
     * @return
     */
    public List<ProjectGroupDeviceVO> queryBindDevice(ProjectGroupParamVO projectGroupParamVO);
}