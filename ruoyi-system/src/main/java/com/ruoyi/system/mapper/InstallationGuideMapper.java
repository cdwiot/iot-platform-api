package com.ruoyi.system.mapper;

import java.util.List;
import java.util.Map;

import com.ruoyi.system.domain.InstallationGuide;
import com.ruoyi.system.domain.vo.QueryVo;

public interface InstallationGuideMapper {
    public Map<String, Object> findExistance(InstallationGuide installationGuide);
    public Integer create(InstallationGuide installationGuide);
    public List<InstallationGuide> index(QueryVo queryVo);
    public InstallationGuide retrieve(QueryVo queryVo);
    public Integer update(InstallationGuide installationGuide);
    public Integer delete(QueryVo queryVo);
}
