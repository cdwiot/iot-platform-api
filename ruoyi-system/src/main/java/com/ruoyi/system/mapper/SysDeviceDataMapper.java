package com.ruoyi.system.mapper;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import com.ruoyi.system.domain.SysDeviceData;

/**
 * 产品数据表 数据层
 * 
 * @author ruoyi
 */
public interface SysDeviceDataMapper
{
    /**
     * 
     * @param deviceId 产品id
     * @return 产品实时数据
     */
    public List<SysDeviceData> selectLatestData(@Param("deviceId") Long deviceId);

    /**
     * 
     * @param deviceId 产品id
     * @return 产品实时数据
     */
    public List<SysDeviceData> selectHistoryData(@Param("deviceId") Long deviceId, @Param("attrName") String attrName, @Param("startTime") String startTime, @Param("endTime") String endTime);
}
