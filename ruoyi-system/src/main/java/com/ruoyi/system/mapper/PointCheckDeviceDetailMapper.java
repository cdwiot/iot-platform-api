package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.PointCheckDeviceDetail;
import com.ruoyi.system.domain.PointCheckTask;
import com.ruoyi.system.domain.PointCheckTaskDevice;
import com.ruoyi.system.domain.vo.QueryVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface PointCheckDeviceDetailMapper {

    /**
     * 产品检查项的列表查询
     *
     * @param pointCheckDeviceDetail
     * @return
     */
    List<PointCheckDeviceDetail> queryList(PointCheckDeviceDetail pointCheckDeviceDetail);

    /**
     * 产品异常点检项
     *
     * @param pointCheckDeviceDetail
     * @return
     */
    List<PointCheckDeviceDetail> queryListAbnormalList(PointCheckTaskDevice pointCheckTaskDevice);

    /**
     * 产品检查项的详情
     *
     * @param pointCheckDeviceDetail
     * @return
     */
    PointCheckDeviceDetail getOne(PointCheckDeviceDetail pointCheckDeviceDetail);

    /**
     * 添加
     *
     * @param pointCheckDeviceDetail
     * @return
     */
    int add(PointCheckDeviceDetail pointCheckDeviceDetail);

    /**
     * 修改
     *
     * @param pointCheckDeviceDetail
     * @return
     */
    int update(PointCheckDeviceDetail pointCheckDeviceDetail);

    /**
     * 删除
     *
     * @param pointCheckDeviceDetail
     * @return
     */
    int delete(PointCheckDeviceDetail pointCheckDeviceDetail);

    /**
     * 验证是否重复
     *
     * @param pointCheckDeviceDetail
     * @return
     */
    PointCheckDeviceDetail checkIsExist(PointCheckDeviceDetail pointCheckDeviceDetail);

    List<Map<String, Object>> pointCheckAbnormalSummaryByMonth(QueryVo queryVo);
}
