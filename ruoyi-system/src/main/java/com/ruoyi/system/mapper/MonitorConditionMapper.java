package com.ruoyi.system.mapper;

import java.util.List;

import com.ruoyi.system.domain.MonitorCondition;
import com.ruoyi.system.domain.vo.QueryVo;

public interface MonitorConditionMapper 
{
    public Integer create(MonitorCondition monitorCondition);
    public List<MonitorCondition> index(QueryVo queryVo);
    public MonitorCondition retrieve(QueryVo queryVo);
    public Integer update(MonitorCondition monitorCondition);
    public Integer delete(QueryVo queryVo);
}
