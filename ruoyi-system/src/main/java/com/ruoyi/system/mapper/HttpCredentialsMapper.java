package com.ruoyi.system.mapper;

import java.util.Map;

import com.ruoyi.system.domain.HttpCredentials;
import com.ruoyi.system.domain.vo.QueryVo;

public interface HttpCredentialsMapper 
{
    public Map<String, Object> findExistance(HttpCredentials httpCredentials);
    public Integer create(HttpCredentials httpCredentials);
    public HttpCredentials retrieve(QueryVo queryVo);
    public Integer update(HttpCredentials httpCredentials);
}
