package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.PointCheckDescription;
import com.ruoyi.system.domain.vo.PointCheckDescriptionVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Set;

public interface PointCheckDescriptionMapper {
    /**
     * 文字描述分页查询
     *
     * @param pointCheckDescription
     * @return
     */
    List<PointCheckDescription> queryList(PointCheckDescription pointCheckDescription);

    /**
     * 获取详情
     *
     * @param pointCheckDescription
     * @return
     */
    PointCheckDescription getOne(PointCheckDescription pointCheckDescription);

    /**
     * 批量添加
     *
     * @param pointCheckDescriptions
     * @return
     */
    int addBatch(@Param("descriptions") List<PointCheckDescription> pointCheckDescriptions);

    /**
     * 修改
     *
     * @param pointCheckDescription
     * @return
     */
    int update(PointCheckDescription pointCheckDescription);

    /**
     * 删除
     *
     * @param pointCheckDescription
     * @return
     */
    int delete(PointCheckDescription pointCheckDescription);

    /**
     * 根据产品检查Id查询文字描述信息
     *
     * @param detailIds
     * @return
     */
    List<PointCheckDescriptionVO> queryDescriptionVO(@Param("detailIds") Set<Integer> detailIds);
}
