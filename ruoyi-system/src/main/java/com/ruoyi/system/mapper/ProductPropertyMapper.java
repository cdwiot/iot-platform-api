package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.ProductProperty;

import java.util.List;

public interface ProductPropertyMapper {

    /**
     * 产品额外属性的查询
     *
     * @param productProperty
     * @return
     */
    List<ProductProperty> queryPageList(ProductProperty productProperty);

    /**
     * 产品额外属性的添加
     *
     * @param productProperty
     * @return
     */
    Integer addProductProperty(ProductProperty productProperty);

    /**
     * 查询产品额外属性详情
     *
     * @param productProperty
     * @return
     */
    ProductProperty queryById(ProductProperty productProperty);

    /**
     * 产品额外属性的修改
     *
     * @param productProperty
     * @return
     */
    Integer updateProductProperty(ProductProperty productProperty);

    /**
     * 产品额外属性的删除
     *
     * @param productProperty
     * @return
     */
    Integer deleteProductProperty(ProductProperty productProperty);

    /**
     * 产品额外属性的验证
     *
     * @param productProperty
     * @return
     */
    ProductProperty checkPropertyName(ProductProperty productProperty);
}
