package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.TerminalDetail;
import com.ruoyi.system.domain.TerminalEnum;
import com.ruoyi.system.domain.vo.QueryVo;
import com.ruoyi.system.domain.vo.TerminalDetailVO;

import java.util.List;

public interface TerminalDetailMapper {

    /**
     * 获取终端参数枚举
     *
     * @param queryVo
     * @return
     */
    List<TerminalEnum> getTerminalParamEnum(QueryVo queryVo);

    /**
     * 查询终端信息
     *
     * @param queryVo
     * @return
     */
    List<TerminalDetailVO> queryTerminal(QueryVo queryVo);

    /**
     * 查询终端参数值信息
     *
     * @param queryVo
     * @return
     */
    List<TerminalDetail> queryTerminalDetail(QueryVo queryVo);

    /**
     * 添加
     *
     * @param terminalDetail
     * @return
     */
    int add(TerminalDetail terminalDetail);

    /**
     * 修改
     *
     * @param terminalDetail
     * @return
     */
    int update(TerminalDetail terminalDetail);

    /**
     * 验证添加的属性值是否存在
     *
     * @param terminalDetail
     * @return
     */
    TerminalDetail checkIsExit(TerminalDetail terminalDetail);
}
