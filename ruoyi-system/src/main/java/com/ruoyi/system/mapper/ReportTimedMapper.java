package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.ReportTimed;

import java.util.List;

public interface ReportTimedMapper {

    /**
     * 查询列表
     *
     * @param reportTimed
     * @return
     */
    List<ReportTimed> queryList(ReportTimed reportTimed);

    /**
     * 获取单个详情
     *
     * @param reportTimed
     * @return
     */
    ReportTimed getOne(ReportTimed reportTimed);

    /**
     * 添加
     *
     * @param reportTimed
     * @return
     */
    int add(ReportTimed reportTimed);

    /**
     * 修改
     *
     * @param reportTimed
     * @return
     */
    int update(ReportTimed reportTimed);

    /**
     * 删除
     *
     * @param reportTimed
     * @return
     */
    int delete(ReportTimed reportTimed);

    /**
     * 验证是否重复
     *
     * @param reportTimed
     * @return
     */
    ReportTimed checkIsExit(ReportTimed reportTimed);
}
