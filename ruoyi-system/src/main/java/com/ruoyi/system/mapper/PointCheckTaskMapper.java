package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.PointCheckTask;
import com.ruoyi.system.domain.vo.DeviceHistoryDto;
import com.ruoyi.system.domain.vo.QueryVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;
import java.util.Set;

@SuppressWarnings("AlibabaLowerCamelCaseVariableNaming")
public interface PointCheckTaskMapper {

    /**
     * 获取任务列表
     *
     * @param pointCheckTask
     * @return
     */
    List<PointCheckTask> queryList(PointCheckTask pointCheckTask);

    /**
     * 查询APP进行中列表
     *
     * @param pointCheckTask
     * @return
     */
    List<PointCheckTask> queryListProgress(PointCheckTask pointCheckTask);

    /**
     * 获取详情
     *
     * @param pointCheckTask
     * @return
     */
    PointCheckTask getOne(PointCheckTask pointCheckTask);

    /**
     * 添加
     *
     * @param pointCheckTask
     * @return
     */
    int add(PointCheckTask pointCheckTask);

    /**
     * 编辑
     *
     * @param pointCheckTask
     * @return
     */
    int update(PointCheckTask pointCheckTask);

    /**
     * 根据任务状态查询任务
     *
     * @return
     */
    List<PointCheckTask> queryListByStatus(PointCheckTask pointCheckTask);
    List<PointCheckTask> getDetail(Integer id);

    /**
     * 批量更新任务状态为已超时
     *
     * @param ids
     * @return
     */
    int updatePointCheckTaskStatus(@Param("ids") Set<Integer> ids);

    /**
     * 根据任务查询产品信息
     *
     * @param id
     * @return
     */
    List<Integer> queryCheckTaskDevice(Integer id);

    /**
     * 更新任务状态为已完成
     *
     * @param id
     * @return
     */
    int updatePointCheckTaskStatusWc(@Param("ids") Set<Integer> ids);

    /**
     * 获取已超时并且邦有产品的任务
     *
     * @return
     */
    List<PointCheckTask> getTaskCSDevice();

    /**
     * 根据产品代码和执行人查询任务信息
     *
     * @param companyId
     * @param sn
     * @param usernames
     * @return
     */
    List<DeviceHistoryDto> queryTaskDetailBySn(@Param("companyId") Long companyId,@Param("sn") String sn,@Param("usernames") Set<String> usernames);

    /**
     * 查询产品异常历史数据
     *
     * @param queryVo
     * @return
     */
    List<DeviceHistoryDto> queryDeviceHistory(QueryVo queryVo);

    /**
     * 根据项目id或项目分组id查询产品信息枚举
     *
     * @param queryVo
     * @return
     */
    List<Map<String,Object>> getDeviceEnum(QueryVo queryVo);
}
