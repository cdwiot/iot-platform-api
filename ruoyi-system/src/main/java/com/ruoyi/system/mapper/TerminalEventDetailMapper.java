package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.TerminalEventDetail;

import java.util.List;

public interface TerminalEventDetailMapper {

    /**
     * 事件详情的查询
     *
     * @param terminalEventDetail
     * @return
     */
    List<TerminalEventDetail> queryList(TerminalEventDetail terminalEventDetail);

    /**
     *
     * @param terminalEventDetail
     * @return
     */
    TerminalEventDetail getOne(TerminalEventDetail terminalEventDetail);
}
