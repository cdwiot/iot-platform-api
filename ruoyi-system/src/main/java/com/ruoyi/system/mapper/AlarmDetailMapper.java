package com.ruoyi.system.mapper;

import java.util.List;

import com.ruoyi.system.domain.AlarmDetail;
import com.ruoyi.system.domain.vo.QueryVo;

public interface AlarmDetailMapper {
    public List<AlarmDetail> index(QueryVo queryVo);
}
