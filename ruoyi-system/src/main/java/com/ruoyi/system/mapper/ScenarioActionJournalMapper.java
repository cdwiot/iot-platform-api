package com.ruoyi.system.mapper;

import java.util.List;

import com.ruoyi.system.domain.ScenarioActionJournal;
import com.ruoyi.system.domain.vo.QueryVo;

public interface ScenarioActionJournalMapper {
    public List<ScenarioActionJournal> index(QueryVo queryVo);
}
