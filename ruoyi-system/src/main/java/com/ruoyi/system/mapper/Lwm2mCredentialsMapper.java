package com.ruoyi.system.mapper;

import java.util.Map;

import com.ruoyi.system.domain.Lwm2mCredentials;
import com.ruoyi.system.domain.vo.QueryVo;

public interface Lwm2mCredentialsMapper 
{
    public Map<String, Object> findExistance(Lwm2mCredentials lwm2mCredentials);
    public Integer create(Lwm2mCredentials lwm2mCredentials);
    public Lwm2mCredentials retrieve(QueryVo queryVo);
    public Integer update(Lwm2mCredentials lwm2mCredentials);
}
