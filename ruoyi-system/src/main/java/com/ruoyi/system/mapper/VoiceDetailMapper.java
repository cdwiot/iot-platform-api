package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.VoiceDetail;

import java.util.List;

public interface VoiceDetailMapper {
    int create(VoiceDetail voiceDetail);
    List<VoiceDetail> index(VoiceDetail voiceDetail);
    VoiceDetail retrieve(VoiceDetail voiceDetail);
}
