package com.ruoyi.system.domain;

import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.system.domain.ValidationGroups.*;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

@Data
public class PointCheckDate extends BaseEntity {
    /**
     * 计划时间Id
     */
    @Null(message = "计划时间Id应该为null", groups = {Create.class})
    @NotNull(message = "计划时间Id不应该为null", groups = {Update.class})
    private Integer id;

    /**
     * 计划Id
     */
    @NotNull(message = "计划Id不应该为null", groups = {Create.class,Update.class})
    private Integer pointId;

    /**
     * 开始时间
     */
    @NotNull(message = "开始时间不应该为null", groups = {Create.class,Update.class})
    private String start;

    /**
     * 结束时间
     */
    @NotNull(message = "结束时间不应该为null", groups = {Create.class,Update.class})
    private String end;

    /**
     * 周期
     */
    @NotNull(message = "周期不应该为null", groups = {Create.class,Update.class})
    private String weekday;

    /**
     * 所属公司
     */
    private Long companyId;
}
