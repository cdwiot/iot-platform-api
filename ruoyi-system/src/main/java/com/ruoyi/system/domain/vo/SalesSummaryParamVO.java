package com.ruoyi.system.domain.vo;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 产品销售汇总查询参数
 */
@Data
public class SalesSummaryParamVO implements Serializable {

    /**
     * 合同金额最小值
     */
    private BigDecimal minAmount;

    /**
     * 合同金额最大值
     */
    private BigDecimal maxAmount;

    /**
     * 项目名称
     */
    private String projectName;

    /**
     * 合同号
     */
    private String projectCode;

    /**
     * 客户合同号
     */
    private String peerCode;

    /**
     * 产品代码
     */
    private String sn;

    /**
     * 客户公司id
     */
    private Integer deptId;

    /**
     * 产品点检状态（0正常1异常）
     */
    private String status;

    /**
     * 发生时间--开始
     */
    private Date begin;

    /**
     * 发生时间--结束
     */
    private Date end;

    /**
     * 统计时间开始
     */
    private Date inputTimeStart;

    /**
     * 统计时间结束
     */
    private Date inputTimeEnd;

    /**
     * 项目类型（1内部2外部3测试）
     */
    private Integer type;
}
