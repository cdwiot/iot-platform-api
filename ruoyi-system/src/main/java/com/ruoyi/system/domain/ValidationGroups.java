package com.ruoyi.system.domain;

public class ValidationGroups {
    public interface Create {}
    public interface Select {}
    public interface Update {}
    public interface Set {}
    public interface Get {}
}
