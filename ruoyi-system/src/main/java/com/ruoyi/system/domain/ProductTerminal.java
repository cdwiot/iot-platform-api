package com.ruoyi.system.domain;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.system.domain.ValidationGroups.Create;
import com.ruoyi.system.domain.ValidationGroups.Select;
import com.ruoyi.system.domain.ValidationGroups.Update;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class ProductTerminal extends BaseEntity {
    @Null(message = "id应该为null", groups = {Create.class, Select.class})
    @NotNull(message = "id不能为null", groups = {Update.class})
    private Integer id;

    @Null(message = "型号id应该为null", groups = {Create.class, Update.class})
    private Integer productId;

    @Null(message = "序号应该为null", groups = {Update.class})
    @NotNull(message = "序号不能为null", groups = {Create.class})
    @Range(min = 0, max = 100, message = "序号应该在{min}-{max}之间", groups = {Create.class})
    private Integer number;

    @NotBlank(message = "名称不能为空", groups = {Create.class, Update.class})
    @Length(min = 0, max = 32, message = "名称应该少于{max}字符", groups = {Create.class, Update.class, Select.class})
    private String name;

    @Null(message = "终端id应该为null", groups = {Update.class})
    @NotNull(message = "终端id不能为null", groups = {Create.class})
    private Integer terminalId;

    @Null(message = "不支持的字段", groups = {Create.class, Update.class, Select.class})
    private String protocol;

    @Null(message = "不支持的字段", groups = {Create.class, Update.class, Select.class})
    private String code;

    @Null(message = "不支持的字段", groups = {Create.class, Update.class, Select.class})
    private String manufacturer;

    @Null(message = "不支持的字段", groups = {Create.class, Update.class, Select.class})
    private String remark;

    @Null(message = "不支持的字段", groups = {Create.class, Update.class, Select.class})
    private Long companyId;

    @Null(message = "不支持的字段", groups = {Create.class, Update.class, Select.class})
    private String groupName;
}
