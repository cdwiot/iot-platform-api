package com.ruoyi.system.domain;

import java.util.List;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.common.enums.AggregationMethod;
import com.ruoyi.common.enums.ChartType;
import com.ruoyi.system.domain.ValidationGroups.Create;
import com.ruoyi.system.domain.ValidationGroups.Select;
import com.ruoyi.system.domain.ValidationGroups.Update;

import org.hibernate.validator.constraints.Length;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class ProductChart extends BaseEntity {
    @Null(message = "id应该为null", groups = {Create.class, Select.class})
    @NotNull(message = "id不能为null", groups = {Update.class})
    private Integer id;

    @Excel(name = "产品id")
    @NotNull(message = "产品id不能为null", groups = {Create.class, Update.class})
    private Integer productId;

    @Excel(name = "名称")
    @NotBlank(message = "名称不能为空", groups = {Create.class, Update.class})
    @Length(min = 0, max = 32, message = "名称应该少于{max}字符", groups = {Create.class, Update.class, Select.class})
    private String name;

    @Excel(name = "图表类型")
    private String type;
    @AssertTrue(message = "无效的图表类型", groups = {Create.class, Update.class})
    public boolean isValidType() {
        boolean passed = false;
        if (type != null && ChartType.valueOfLabel(type) != null) {
            passed = true;
        }
        return passed;
    }

    @Excel(name = "（用户侧）标识符")
    @NotEmpty(message = "标识符不能为空", groups = {Create.class, Update.class})
    private List<String> identifiers;
    @Null(message = "不支持的字段", groups = {Create.class, Update.class})
    private String identifiersString;

    @Excel(name = "描述")
    @NotNull(message = "描述不能为null", groups = {Create.class, Update.class})
    @Length(min = 0, max = 128, message = "描述应该少于{max}字符", groups = {Create.class, Update.class, Select.class})
    private String description;


    @Excel(name = "聚合方法")
    private String aggregation;
    @AssertTrue(message = "无效的聚合方法", groups = {Create.class, Update.class})
    public boolean isValidAggregation() {
        boolean passed = false;
        if (aggregation != null && AggregationMethod.valueOfCode(aggregation) != null) {
            passed = true;
        }
        return passed;
    }

    @Null(message = "不支持的字段", groups = {Create.class, Update.class, Select.class})
    private Long companyId;
}
