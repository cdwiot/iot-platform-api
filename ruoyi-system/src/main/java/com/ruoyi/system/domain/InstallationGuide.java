package com.ruoyi.system.domain;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.system.domain.ValidationGroups.Create;
import com.ruoyi.system.domain.ValidationGroups.Select;
import com.ruoyi.system.domain.ValidationGroups.Update;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class InstallationGuide extends BaseEntity {
    @Null(message = "文件id应该为null", groups = {Create.class, Select.class})
    @NotNull(message = "文件id不能为null", groups = {Update.class})
    private Integer id;

    @NotBlank(message = "名称不能为空", groups = {Create.class, Update.class})
    @Length(min = 0, max = 64, message = "名称应该少于{max}字符", groups = {Create.class, Update.class, Select.class})
    private String name;

    @NotNull(message = "描述不能为null", groups = {Create.class, Update.class})
    @Length(min = 0, max = 512, message = "名称应该少于{max}字符", groups = {Create.class, Update.class, Select.class})
    private String description;

    @NotNull(message = "视频路径不能为空", groups = {Create.class, Update.class})
    @Length(min = 0, max = 256, message = "视频路径应该少于{max}字符", groups = {Create.class, Update.class, Select.class})
    private String videoPath;

    @NotBlank(message = "文件路径不能为空", groups = {Create.class, Update.class})
    @Length(min = 0, max = 256, message = "文件路径应该少于{max}字符", groups = {Create.class, Update.class, Select.class})
    private String filePath;

    @Null(message = "不支持的字段", groups = {Create.class, Update.class})
    private String sjtz;

    @Null(message = "不支持的字段", groups = {Create.class, Update.class})
    @Length(min = 0, max = 32, message = "产品序列号应该少于{max}字符", groups = {Select.class})
    private String deviceSn;

    @Null(message = "产品id应该为null", groups = {Update.class})
    @NotNull(message = "产品id不能为null", groups = {Create.class})
    @Min(value = 0, message = "产品id大于0", groups = {Create.class})
    private Integer deviceId;

    @Null(message = "不支持的字段", groups = {Create.class, Update.class})
    @Length(min = 0, max = 32, message = "产品名称应该少于{max}字符", groups = {Select.class})
    private String deviceName;

    @Null(message = "不支持的字段", groups = {Create.class, Update.class})
    @Length(min = 0, max = 32, message = "产品代码应该少于{max}字符", groups = {Select.class})
    private String deviceCode;

    @Null(message = "不支持的字段", groups = {Create.class, Update.class})
    @Length(min = 0, max = 32, message = "项目名称应该少于{max}字符", groups = {Select.class})
    private String projectName;

    @Null(message = "不支持的字段", groups = {Create.class, Update.class})
    @Length(min = 0, max = 32, message = "合同号应该少于{max}字符", groups = {Select.class})
    private String projectCode;

    @Null(message = "不支持的字段", groups = {Create.class, Update.class})
    private Boolean general;

    @Null(message = "不支持的字段", groups = {Update.class})
    private Long companyId;


}
