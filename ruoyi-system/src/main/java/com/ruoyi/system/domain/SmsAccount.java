package com.ruoyi.system.domain;

import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.system.domain.ValidationGroups.Update;
import com.ruoyi.system.domain.ValidationGroups.Create;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.io.Serializable;

/**
 * 短信账户配置
 */
@Data
public class SmsAccount extends BaseEntity implements Serializable {

    /**
     * 账户Id
     */
    @Null(message = "账户Id应该为null", groups = {Create.class})
    @NotNull(message = "账户Id不应该为null", groups = {Update.class})
    private Integer id;

    /**
     * 账户key
     */
    @NotNull(message = "账户key不能为null", groups = {Update.class, Create.class})
    private String keyId;

    /**
     * 密钥
     */
    @NotNull(message = "密钥不能为null", groups = {Update.class, Create.class})
    private String secret;

    /**
     * 请求地址
     */
//    @NotNull(message = "请求地址不能为null", groups = {Update.class, Create.class})
    private String url;

    /**
     * 短信签名
     */
    @NotNull(message = "短信签名不能为null", groups = {Update.class, Create.class})
    private String signName;

    /**
     * 账号标志（0大账号1特有账号）
     */
    private String accessFlag;

    /**
     * 能否添加标志，0代表不能添加，1能添加
     */
    private String addFlag;

    /**
     * 所属公司
     */
    private Long companyId;
}
