package com.ruoyi.system.domain.vo;

import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.system.domain.TerminalDetail;
import lombok.Data;

import java.util.List;

@Data
public class TerminalDetailVO extends BaseEntity {

    /**
     * 终端id
     */
    private Integer terminalId;

    /**
     * 传感器名称
     */
    private String name;

    /**
     * 接入协议
     */
    private String protocol;

    /**
     * 传感器型号
     */
    private String code;

    /**
     * 终端信息
     */
    private List<TerminalDetail> terminalDetails;
}
