package com.ruoyi.system.domain;

import lombok.Data;

@Data
public class Period {
    private Integer start;  // seconds of week
    private Integer end;    // seconds of week
}
