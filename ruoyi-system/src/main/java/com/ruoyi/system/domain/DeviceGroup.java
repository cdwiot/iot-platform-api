package com.ruoyi.system.domain;

import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 产品分组和产品关联对象 tbl_device_group
 *
 * @author ruoyi
 * @date 2021-08-10
 */
@Data
public class DeviceGroup extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 分组id */
    @Excel(name = "分组id")
    private Integer groupId;

    /** 产品id */
    @Excel(name = "产品id")
    private Integer deviceId;

}