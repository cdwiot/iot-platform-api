package com.ruoyi.system.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * APP扫描产品代码跳转返回结果
 */
@Data
public class PointCheckDeviceDetailDTO implements Serializable {

    /**
     * 任务开始时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date startTime;

    /**
     * 任务结束时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endTime;

    /**
     * 任务Id
     */
    private Integer id;

    /**
     * 产品型号Id
     */
    private Integer productId;

    /**
     * 项目Id
     */
    private Integer projectId;

    /**
     * 产品代码
     */
    private String sn;

    /**
     * 任务产品对应关系id
     */
    private Integer taskDeviceId;
}
