package com.ruoyi.system.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.system.domain.ValidationGroups.*;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.util.Date;

@Data
public class TerminalEventDetail extends BaseEntity {

    /**
     * 详情Id
     */
    @Null(message = "详情Id应该为null", groups = {Create.class, Select.class})
    private Integer id;

    /**
     * 事件Id
     */
    @NotNull(message = "事件Id不能为null", groups = {Create.class})
    private Integer eventId;

    /**
     * 产品终端Id
     */
    @NotNull(message = "产品终端Id不能为null", groups = {Create.class})
    private Integer terminalId;

    /**
     * 输出参数
     */
    @NotNull(message = "输出参数不能为null", groups = {Create.class,Update.class})
    private String param;

    /**
     * 产品名称
     */
    private String deviceName;

    /**
     * 产品Id
     */
    @NotNull(message = "产品Id不能为null", groups = {Select.class})
    private Integer deviceId;

    /**
     * 事件名称
     */
    private String eventName;

    /**
     * 时间戳
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date timestamp;

    /**
     * 所属公司
     */
    private Long companyId;

    /**
     * 查询开始时间--创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTimeStart;

    /**
     * 查询结束时间--创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTimeEnd;

}
