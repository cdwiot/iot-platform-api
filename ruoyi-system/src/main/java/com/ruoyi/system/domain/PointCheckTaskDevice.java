package com.ruoyi.system.domain;

import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

import javax.validation.constraints.NotNull;
import com.ruoyi.system.domain.ValidationGroups.*;
import org.joda.time.DateTime;

import javax.validation.constraints.Null;
import java.util.Date;

/**
 * 任务产品关系
 */
@Data
public class PointCheckTaskDevice extends BaseEntity {

    /**
     * 任务产品关系Id
     */
    @Null(message = "任务产品关系Id应该为null", groups = {Create.class})
    @NotNull(message = "任务产品关系Id不应该为null", groups = {Update.class})
    private Integer id;

    @NotNull(message = "任务Id不应该为null", groups = {Update.class,Create.class})
    private Integer taskId;

    private String sn;

    /**
     * 产品Id
     */
    private Integer deviceId;

    /**
     * 产品售后电话
     */
    private String salePhone;

    /**
     * 产品描述
     */
    private String description;


    /**
     * 产品名称
     */
    private String name;

    /**
     * 产品Id
     */
    private Integer productId;

    /**
     * 项目Id
     */
    private Integer projectId;

    /**
     * 产品照片
     */
    private String photo;

    /**
     * 产品型号照片
     */
    private String productPhoto;

    /**
     * 型号规格
     */
    private String code;

    /**
     * 系统
     */
    private String system;

    /**
     * 厂商
     */
    private String manufacturer;

    /**
     * 产品位号
     */
    private String positionNumber;

    /**
     * 客户组织Id
     */
    private Long deptId;

    /**
     * 报告地址
     */
    private String report;

    /**
     * 报告生成标志0未生成1已生成
     */
    private Integer reportFlag;

    /**
     * 所属公司
     */
    private Long companyId;

    /**
     * 产品状态
     */
    private String status;

    /**
     * 是否处理（false未处理，true已处理）
     */
    private boolean deal;

    /**
     * 产品代码查询参数
     */
    private String snParam;

    /**
     * 最后一次更新时间
     */
    private Date lastUpdateTime;

    private Date startTimeBegin;
    private Date startTimeEnd;
    private String taskName;
    private DateTime startTime;
    private Integer category;
}
