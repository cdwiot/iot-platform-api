package com.ruoyi.system.domain;

import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.system.domain.ValidationGroups.*;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * 点检计划
 */
@Data
public class PointCheck extends BaseEntity {

    /**
     * 计划Id
     */
    @Null(message = "计划Id应该为null", groups = {Create.class})
    @NotNull(message = "计划Id不应该为null", groups = {Update.class})
    private Integer id;

    /**
     * 计划名称
     */
    @NotNull(message = "计划名称不应该为null", groups = {Create.class, Update.class})
    private String name;

    /**
     * 项目id
     */
    @NotNull(message = "项目id不应该为null", groups = {Create.class, Update.class})
    private Integer projectId;

    /**
     * 客户公司Id
     */
    private Long deptId;

    /**
     * 客户公司名称
     */
    private String deptName;

    /**
     * 项目名称
     */
    private String projectName;

    /**
     * 项目合同号
     */
    private String projectCode;

    /**
     * 客户合同号
     */
    private String peerCode;

    /**
     * 选中内容
     */
    @NotNull(message = "选中内容不应该为null", groups = {Create.class, Update.class})
    private String items;

    /**
     * 分组Id
     */
    @NotNull(message = "分组Id不应该为null", groups = {Create.class, Update.class})
    private Integer groupId;

    /**
     * 分组名称
     */
    private String groupName;

    /**
     * 执行人
     */
    @NotNull(message = "执行人不应该为null", groups = {Create.class, Update.class})
    private String executor;

    /**
     * 用户昵称
     */
    private String nickName;

    /**
     * 描述
     */
    private String description;

    /**
     * 启用状态(0未启用1已启用)
     */
    @NotNull(message = "启用状态不应该为null", groups = {Create.class, Update.class})
    private String status;

    /**
     * 点检计划时间
     */
    private List<PointCheckDate> pointCheckDates;

    /**
     * 所属公司
     */
    private Long companyId;
    /**
     * 选中的各个产品点检配置
     */
    private HashMap<String,ArrayList<Integer>> checkConfigList;
}
