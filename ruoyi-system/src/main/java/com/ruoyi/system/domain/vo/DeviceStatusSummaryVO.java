package com.ruoyi.system.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 膨胀节状态汇总
 */
@Data
public class DeviceStatusSummaryVO implements Serializable {


    /**
     * 客户名称
     */
    private String deptName;

    /**
     * 项目id
     */
    private Integer projectId;

    /**
     * 项目名称
     */
    private String projectName;

    /**
     * 合同号
     */
    private String projectCode;

    /**
     * 产品名称
     */
    private String deviceName;

    /**
     * 产品代码
     */
    private String sn;

    /**
     * 异常并处理次数
     */
    private Integer dealTimes;

    /**
     * 异常处理Id
     */
    private Integer dealId;

    /**
     * 异常处理明细
     */
    private String detail;

    /**
     * 检查类别
     */
    private String type;

    /**
     * 检查项
     */
    private String item;


    /**
     * 异常位置
     */
    private String errorAddress;

    /**
     * 点检时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date time;

    /**
     * 是否处理（false未处理true已处理）
     */
    private boolean isDeal;

//    /**
//     * 异常次数状态标志（两次异常次数以上）
//     * false不做处理，true特殊处理（显示紫色）
//     */
//    private boolean errorFlag;
}
