package com.ruoyi.system.domain.vo;

import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.system.domain.Device;
import com.ruoyi.system.domain.ValidationGroups.Create;
import com.ruoyi.system.domain.ValidationGroups.Select;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * 查询项目分组产品的参数VO
 */
@Data
public class ProjectGroupParamVO extends BaseEntity implements Serializable {

    /**
     * 项目id
     */
    @NotNull(message = "项目id不能为null", groups = {Select.class})
    private Integer projectId;

    /**
     * 分组Id
     */
    @NotNull(message = "分组Id不能为null", groups = {Select.class})
    private Integer groupId;

    /**
     * 产品Id
     */
    private Integer productId;

    /**
     * 产品名称
     */
    private String productName;

    /**
     * 产品名称
     */
    private String name;

}
