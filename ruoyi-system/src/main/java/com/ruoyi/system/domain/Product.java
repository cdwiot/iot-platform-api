package com.ruoyi.system.domain;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.validation.Valid;
import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

import lombok.Data;
import lombok.EqualsAndHashCode;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.system.domain.ValidationGroups.Create;
import com.ruoyi.system.domain.ValidationGroups.Select;
import com.ruoyi.system.domain.ValidationGroups.Update;

import org.hibernate.validator.constraints.Length;

@Data
@EqualsAndHashCode(callSuper=true)
public class Product extends BaseEntity {
    @Null(message = "产品id应该为null", groups = {Create.class, Select.class})
    @NotNull(message = "产品id不能为null", groups = {Update.class})
    private Integer id;

    @Excel(name = "产品名称")
    @NotBlank(message = "名称不能为空", groups = {Create.class, Update.class})
    @Length(min = 0, max = 32, message = "名称应该少于{max}字符", groups = {Create.class, Update.class, Select.class})
    private String name;

    @Excel(name = "产品型号")
    @NotBlank(message = "型号不能为空", groups = {Create.class, Update.class})
    @Length(min = 0, max = 32, message = "型号应该少于{max}字符", groups = {Create.class, Update.class, Select.class})
    private String code;

    @Excel(name = "厂商")
    @NotNull(message = "厂商不能为null", groups = {Create.class, Update.class})
    @Length(min = 0, max = 32, message = "厂商应该少于{max}字符", groups = {Create.class, Update.class, Select.class})
    private String manufacturer;

    @Excel(name = "描述")
    @NotNull(message = "描述不能为null", groups = {Create.class, Update.class})
    @Length(min = 0, max = 512, message = "描述应该少于{max}字符", groups = {Create.class, Update.class, Select.class})
    private String description;

    @Excel(name = "照片")
//    @NotNull(message = "照片不能为null", groups = {Create.class, Update.class})
    @Length(min = 0, max = 512, message = "照片应该少于{max}字符", groups = {Create.class, Update.class})
    private String photo;

    @Null(message = "不支持的字段", groups = {Create.class, Update.class, Select.class})
    private Long companyId;

    @Null(message = "不支持的字段", groups = {Update.class, Select.class})
    @NotNull(message = "终端列表不能为null", groups = {Create.class})
    private List<@Valid ProductTerminal> terminals;

    @AssertTrue(message = "终端名称不能重复", groups = {Create.class})
    private boolean isTerminalNamesUnique() {
        boolean passed = false;
        Set<String> names = new HashSet<>();
        for (ProductTerminal terminal : terminals) {
            String name = terminal.getName();
            names.add(name);
        }
        if (names.size() ==  terminals.size()) {
            passed = true;
        }
        return passed;
    }

    @AssertTrue(message = "终端序号不能重复", groups = {Create.class})
    private boolean isTerminalNumberUnique() {
        boolean passed = false;
        Set<Integer> numbers = new HashSet<>();
        for (ProductTerminal terminal : terminals) {
            Integer number = terminal.getNumber();
            numbers.add(number);
        }
        if (numbers.size() ==  terminals.size()) {
            passed = true;
        }
        return passed;
    }
}