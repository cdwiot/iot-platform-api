package com.ruoyi.system.domain;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.system.domain.ValidationGroups.*;

import org.hibernate.validator.constraints.*;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class ProjectAlarmRule extends BaseEntity {
    @Null(message = "id应该为null", groups = {Create.class, Select.class})
    @NotNull(message = "id不能为null", groups = {Update.class})
    private Integer id;

    @Null(message = "项目id应该为null", groups = {Update.class})
    @NotNull(message = "项目id不能为null", groups = {Create.class, Select.class})
    private Integer projectId;

    @Null(message = "监控规则id应该为null", groups = {Update.class})
    @NotNull(message = "监控规则id不能为null", groups = {Create.class})
    private Integer monitorRuleId;
    @Null(message = "不支持的字段", groups = {Create.class, Update.class})
    private String monitorRuleName;
    @Null(message = "不支持的字段", groups = {Create.class, Update.class})
    private MonitorRule monitorRule;

    @NotNull(message = "紧急程度不能为null", groups = {Create.class, Update.class})
    @Range(min = 0, max = 3, message = "紧急程度应该在{min}-{max}之间", groups = {Create.class, Update.class, Select.class})
    private Integer severity;
    @Null(message = "不支持的字段", groups = {Create.class, Update.class, Select.class})
    private String severityString;

    @Null(message = "静默时间应该为null", groups = {Select.class})
    @NotNull(message = "静默时间不能为null", groups = {Create.class, Update.class})
    @Range(min = 0, max = 86400, message = "静默时间应该小于{max}秒", groups = {Create.class, Update.class})
    private Integer cooldown; // seconds

    @NotNull(message = "短信接收人不能null", groups = {Create.class, Update.class})
    private List<Integer> smsReceivers;

    @NotNull(message = "邮件接收人不能null", groups = {Create.class, Update.class})
    private List<Integer> mailReceivers;

    @NotNull(message = "语音接收人不能null", groups = {Create.class, Update.class})
    private List<Integer> voiceReceivers;

    @NotNull(message = "微信接收人不能null", groups = {Create.class, Update.class})
    private List<Integer> wechatReceivers;

    // @Null(message = "触发条件应该为null", groups = {Create.class, Update.class, Select.class})
    private List<@Valid Period> activePeriods;

    @Null(message = "不支持的字段", groups = {Create.class, Update.class, Select.class})
    private Long companyId;
}
