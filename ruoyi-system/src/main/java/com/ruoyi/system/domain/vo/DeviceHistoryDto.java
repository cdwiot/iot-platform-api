package com.ruoyi.system.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class DeviceHistoryDto implements Serializable {

    /**
     * 产品名称
     */
    private String deviceName;

    /**
     * 产品代码
     */
    private String sn;

    /**
     * 计划名称
     */
    private String name;

    /**
     * 任务开始时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date startTime;

    /**
     * 任务结束时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endTime;

    /**
     * 执行人
     */
    private String nickName;

    /**
     * 任务产品Id
     */
    private String taskDeviceId;

    /**
     * 检查Id
     */
    private Integer detailId;

    /**
     * 检查类别
     */
    private String type;

    /**
     * 检查项
     */
    private String item;

    /**
     * 检查状态
     */
    private String status;

    /**
     * 点检时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date time;

    /**
     * 处理方式
     */
    private String way;

    /**
     * 处理结果
     */
    private String dealType;

    /**
     * 结果详情
     */
    private String dealTypeMsg;

    /**
     * 更换时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date replaceTime;

    /**
     * 制造单位
     */
    private String makeUnit;

    /**
     * 更换原因
     */
    private String replaceReason;

    /**
     * 更换设备代号
     */
    private String dealSn;

    /**
     * 修复部门
     */
    private String repairDept;

    /**
     * 修复方法
     */
    private String repairWay;

    /**
     * 拍照记录
     */
    private String photoRecord;
}
