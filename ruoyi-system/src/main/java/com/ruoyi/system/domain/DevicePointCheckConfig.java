package com.ruoyi.system.domain;

import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.system.domain.ValidationGroups.*;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

@Data
public class DevicePointCheckConfig extends BaseEntity {
    /**
     * 配置id
     */
    @Null(message = "配置id应该为null", groups = {Create.class})
    @NotNull(message = "配置id不能为null", groups = {Update.class})
    private Integer id;

    /**
     * 产品Id
     */
    @NotNull(message = "产品Id不能为null", groups = {Create.class,Update.class,Select.class})
    private Integer deviceId;

    /**
     * 检查类别
     */
    @NotNull(message = "点检类型不能为null", groups = {Create.class,Update.class})
    private Integer category;

    /**
     * 检查类别
     */
    @NotNull(message = "检查类别不能为null", groups = {Create.class,Update.class})
    private String type;

    /**
     * 检查项
     */
    @NotNull(message = "检查项不能为null", groups = {Create.class,Update.class})
    private String item;

    /**
     * 自定义字段
     */
    //@NotBlank(message = "自定义字段不能为空", groups = {Create.class,Update.class})
    private String field;

    /**
     * 数据类型
     */
//    @NotNull(message = "数据类型不能为空", groups = {Create.class,Update.class})
    private String dataType;

    /**
     * 下拉选项选择数据
     */
    private String  detail;

    /**
     * 排列序号
     */
    private Integer orderNum;

    /**
     * type排序
     */
    private Integer typeOrder;

    /**
     * item排序
     */
    private Integer itemOrder;

    /**
     * 所属公司
     */
    private Long companyId;
}
