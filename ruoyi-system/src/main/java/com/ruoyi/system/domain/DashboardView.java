package com.ruoyi.system.domain;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

import com.alibaba.fastjson.JSON;
import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.system.domain.ValidationGroups.Update;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class DashboardView extends BaseEntity {
    @Null(message = "不支持的字段", groups = {Update.class})
    private Integer id;

    @Null(message = "不支持的字段", groups = {Update.class})
    private Integer projectId;

    @NotNull(message = "配置不能为空", groups = {Update.class})
    private String configuration;

    @AssertTrue(message = "配置应该是json格式", groups = {Update.class})
    public boolean isConfigurationValidJson() {
        boolean yes = false;
        try {
            JSON.parse(configuration);
            yes = true;
        } catch (Exception e) {}
        return yes;
    }

    @Null(message = "不支持的字段", groups = {Update.class})
    private Integer userId;;

    @Null(message = "不支持的字段", groups = {Update.class})
    private Long companyId;
}
