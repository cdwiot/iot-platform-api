package com.ruoyi.system.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.system.domain.ValidationGroups.*;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.util.Date;

@Data
public class OfficialDetail extends BaseEntity {

    /**
     * 明细Id
     */
    @Null(message = "明细Id应该为null", groups = {Create.class})
    @NotNull(message = "明细Id不能为null", groups = {Update.class})
    private Integer id;

    /**
     * 通知类型
     */
    @NotNull(message = "通知类型不能为null", groups = {Create.class,Update.class})
    private String type;

    /**
     * 模板code
     */
    @NotNull(message = "模板code不能为null", groups = {Create.class,Update.class})
    private String code;

    /**
     * openId
     */
    @NotNull(message = "openId不能为null", groups = {Create.class,Update.class})
    private String openId;

    /**
     * appId
     */
    @NotNull(message = "appId不能为null", groups = {Create.class,Update.class})
    private String appId;

    /**
     * 参数
     */
    @NotNull(message = "参数不能为null", groups = {Create.class,Update.class})
    private String parameters;

    /**
     * 创建时间开始
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date startCreateTime;

    /**
     * 创建时间结束
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endCreateTime;

    /**
     * 成功状态
     */
    private String flag;

    /**
     * 异步处理Id
     */
    private String tid;
    /**
     * 发送状态0进行中1成功2失败
     */
    private String status;


    /**
     * 失败原因
     */
    private String errmsg;

    /**
     * 所属公司
     */
    private Long companyId;
}
