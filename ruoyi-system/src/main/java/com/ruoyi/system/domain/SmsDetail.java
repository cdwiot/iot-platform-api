package com.ruoyi.system.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.system.domain.ValidationGroups.*;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.util.Date;

/**
 * 短信发送明细
 */
@Data
public class SmsDetail extends BaseEntity {

    /**
     * 短信明细Id
     */
    @Null(message = "短信明细Id应该为null", groups = {Create.class})
    @NotNull(message = "短信明细Id不能为null", groups = {Update.class})
    private Integer id;

    /**
     * 通知类别
     */
    @NotNull(message = "通知类别不能为null", groups = {Create.class,Update.class})
    private String type;

    /**
     * 模板Code
     */
    @NotNull(message = "模板Code不能为null", groups = {Create.class,Update.class})
    private String templateCode;

    /**
     * 手机号
     */
    @NotNull(message = "手机号不能为null", groups = {Create.class,Update.class})
    private String phoneNumbers;

    /**
     * 模板参数
     */
    @NotNull(message = "模板参数不能为null", groups = {Create.class,Update.class})
    private String templateParam;

    /**
     * 发送回执ID
     */
    private String bizId;

    /**
     * 请求状态码
     */
    private String code;

    /**
     * 状态码的描述
     */
    private String message;

    /**
     * 请求ID
     */
    private String requestId;

    /**
     * 创建时间开始
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date startCreateTime;

    /**
     * 创建时间结束
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endCreateTime;

    /**
     * 异步处理Id
     */
    private String tid;

    /**
     * 发送状态0进行中1成功2失败
     */
    private Integer status;

    /**
     * 所属公司
     */
    private Long companyId;
}
