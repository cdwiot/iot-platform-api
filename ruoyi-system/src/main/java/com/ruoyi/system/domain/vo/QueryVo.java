package com.ruoyi.system.domain.vo;

import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.core.domain.BaseEntity;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class QueryVo extends BaseEntity {
    public JSONObject filters;

    public QueryVo() {
        filters = new JSONObject();
    }
}
