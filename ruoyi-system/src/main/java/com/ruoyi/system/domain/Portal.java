package com.ruoyi.system.domain;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

import com.alibaba.fastjson.JSON;
import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.system.domain.ValidationGroups.Update;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class Portal extends BaseEntity {
    @Null(message = "不支持的字段", groups = {Update.class})
    private Integer id;

    @Null(message = "不支持的字段", groups = {Update.class})
    private String code;

    @NotNull(message = "标题不能为空", groups = {Update.class})
    private String title;

    @NotNull(message = "logo不能为空", groups = {Update.class})
    private String logo;

    @NotNull(message = "背景图不能为空", groups = {Update.class})
    private String image;

    @Null(message = "不支持的字段", groups = {Update.class})
    private Long companyId;
}
