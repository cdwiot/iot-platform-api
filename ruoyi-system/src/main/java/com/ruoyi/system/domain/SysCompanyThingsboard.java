package com.ruoyi.system.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

@Data
@EqualsAndHashCode(callSuper=true)
public class SysCompanyThingsboard extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键，自增 */
    private Long id;

    /** 公司id, sys_dept.dept_id */
    @Excel(name = "公司id, sys_dept.dept_id")
    private Long companyId;

    /** 公司名称，sys_dept.dept_name */
    @Excel(name = "公司名称，sys_dept.dept_name")
    private String companyName;

    /** Thingsboard租户id,UUID类型 */
    @Excel(name = "Thingsboard租户id,UUID类型")
    private String tbTenantId;

    /** 租户名称 */
    @Excel(name = "租户名称")
    private String tbTenantTitle;

    /** Thingsboard租户用户id,UUID类型 */
    @Excel(name = "Thingsboard租户用户id,UUID类型")
    private String tbUserId;

    /** Thingsboard租户用户账号 */
    @Excel(name = "Thingsboard租户用户账号")
    private String tbUsername;

    /** Thingsboard租户用户密码 */
    @Excel(name = "Thingsboard租户用户密码")
    private String tbPassword;

    private String tbMqttDeviceProfileId;
    private String tbHttpDeviceProfileId;
    private String tbCoapDeviceProfileId;
    private String tbLwm2mDeviceProfileId;
}
