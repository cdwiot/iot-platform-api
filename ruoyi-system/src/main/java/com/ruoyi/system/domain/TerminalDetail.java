package com.ruoyi.system.domain;

import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

@Data
public class TerminalDetail extends BaseEntity {
    /**
     * 参数值id
     */
//    @Null(message = "参数值id应该为null", groups = {ValidationGroups.Create.class, ValidationGroups.Select.class})
//    @NotNull(message = "参数值id不能为null", groups = {ValidationGroups.Update.class})
    private Integer id;

    /**
     * 终端id
     */
    @NotNull(message = "终端id不能为null", groups = {ValidationGroups.Update.class, ValidationGroups.Create.class})
    private Integer terminalId;

    /**
     * 参数枚举id
     */
    @NotNull(message = "终端id不能为null", groups = {ValidationGroups.Update.class, ValidationGroups.Create.class})
    private Integer enumId;

    /**
     * 参数类别
     */
    private String type;

    /**
     * 参数项
     */
    private String item;

    /**
     * 数据类型
     */
    private String dataType;

    /**
     * 参数值
     */
    private String value;

    /**
     * 所属公司
     */
    private Long companyId;
}
