package com.ruoyi.system.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 膨胀节销售汇总
 */
@Data
public class SalesSummaryVO implements Serializable {

    /**
     * 客户名称
     */
    private String deptName;


    /**
     * 项目id
     */
    private Integer projectId;

    /**
     * 项目名称
     */
    private String projectName;

    /**
     * 合同号
     */
    private String projectCode;

    /**
     * 合同金额
     */
    private BigDecimal amount;

    /**
     * 产品名称
     */
    private String deviceName;

    /**
     * 客户合同号
     */
    private String peerCode;

    /**
     * 统计时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date inputTime;

    /**
     * 责任人
     */
    private String salesman;

    /**
     * 产品代码
     */
    private String sn;


    /**
     * 产品数量
     */
    private Integer deviceNum;

    /**
     * 项目类型（1内部2外部3测试）
     */
    private Integer type;


}
