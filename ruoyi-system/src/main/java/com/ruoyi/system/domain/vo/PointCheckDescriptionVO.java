package com.ruoyi.system.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.system.domain.PointCheckDescription;
import lombok.Data;

import java.util.Date;

/**
 * 点检任务-文字描述VO
 */
@Data
public class PointCheckDescriptionVO extends PointCheckDescription {

    /**
     * 检查类别
     */
    private String type;

    /**
     * 检查项
     */
    private String item;


    /**
     * 任务开始时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date startTime;

    /**
     * 任务结束时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endTime;

    /**
     * 执行人
     */
    private String nickName;



}
