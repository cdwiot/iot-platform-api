package com.ruoyi.system.domain;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.common.enums.TriggerSource;
import com.ruoyi.system.domain.ValidationGroups.Create;
import com.ruoyi.system.domain.ValidationGroups.Select;
import com.ruoyi.system.domain.ValidationGroups.Update;

import org.hibernate.validator.constraints.Range;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class MonitorCondition extends BaseEntity {
    @Null(message = "触发条件id应该为null", groups= {Create.class, Select.class})
    @NotNull(message = "触发条件id不能为null", groups= {Update.class})
    private Integer id;

    @Null(message = "触发条件的规则id应该为null", groups= {Update.class})
    @NotNull(message = "触发条件的规则id不能为null", groups= {Create.class, Select.class})
    private Integer ruleId;

    @NotNull(message = "触发来源不能为null", groups = {Create.class})
    @Null(message = "触发来源应该为null", groups= {Update.class})
    @Range(min = 0, max = 2, message = "触发来源应该在{min}-{max}之间", groups = {Create.class, Select.class})
    private Integer triggerSource;
    @Null(message = "不支持的字段", groups = {Create.class, Update.class})
    private String triggerSourceString;

    @NotNull(message = "产品id不能为null", groups = {Create.class})
    @Null(message = "产品id应该为null", groups= {Update.class})
    private Integer productId;
    @Null(message = "不支持的字段", groups = {Create.class, Update.class})
    private String productName;

    @NotNull(message = "产品id不能为null", groups = {Create.class})
    @Null(message = "产品id应该为null", groups= {Update.class})
    private Integer deviceId;
    @Null(message = "不支持的字段", groups = {Create.class, Update.class})
    private String deviceName;

    // telemetry
    // @fieldsMatchWithTriggerSource
    private Integer telemetryId;
    @Null(message = "不支持的字段", groups = {Create.class, Update.class, Select.class})
    private ProductTelemetry telemetry;

    // @fieldsMatchWithTriggerSource
    @Range(min = 0, max = 5, message = "比较运算符应该在{min}-{max}之间", groups = {Create.class, Update.class})
    private Integer comparisonOperator;
    @Null(message = "不支持的字段", groups = {Create.class, Update.class, Select.class})
    private String comparisonOperatorString;

    // @fieldsMatchWithTriggerSource
    private String threshold;

    // --
    // event
    // @fieldsMatchWithTriggerSource
    private Integer eventId;
    @Null(message = "不支持的字段", groups = {Create.class, Update.class, Select.class})
    private ProductEvent event;

    // --
    // connectivity
    // @fieldsMatchWithTriggerSource
    @Range(min = 0, max = 1, message = "在线状态应该在{min}-{max}之间", groups = {Create.class, Update.class})
    private Integer connectivity;
    @Null(message = "不支持的字段", groups = {Create.class, Update.class, Select.class})
    private String connectivityString;
    // --

    @AssertTrue(message = "字段和触发来源不匹配", groups = {Create.class})
    public boolean isFieldsMatchWithTriggerSource() {
        if (triggerSource.equals(TriggerSource.TELEMETRY.getCode())) {
            if (telemetryId == null) {
                return false;
            }
            if (comparisonOperator == null) {
                return false;
            }
            if (threshold == null) {
                return false;
            }
        } else if (triggerSource.equals(TriggerSource.EVENT.getCode())) {
            if (eventId == null) {
                return false;
            }
        } else if (triggerSource.equals(TriggerSource.CONNECTIVITY.getCode())) {
            if (connectivity == null) {
                return false;
            }
        }
        return true;
    }

    @Null(message = "不支持的字段", groups = {Create.class, Update.class})
    private Integer triggerType;
    @Null(message = "不支持的字段", groups = {Create.class, Update.class})
    private String triggerTypeString;

    // @fieldsMatchWithTriggerType
    @Min(value = 1, message = "不能小于{value}")
    private Integer repeatCount;

    // @fieldsMatchWithTriggerType
    @Min(value = 1, message = "不能小于{value}")
    private Integer duration;

    @AssertTrue(message = "字段和触发类型不匹配", groups = {Create.class})
    public boolean isFieldsMatchWithTriggerType() {
        if (triggerSource.equals(TriggerSource.TELEMETRY.getCode())) {
            if (repeatCount == null) {
                if (duration == null) {
                    return false;
                }
            } else {
                if (duration != null) {
                    return false;
                }
            }
        }
        return true;
    }

    @Null(message = "不支持的字段", groups = {Create.class, Update.class})
    private Long companyId;
}