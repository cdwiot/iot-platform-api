package com.ruoyi.system.domain;

import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

@Data
public class TerminalEnum extends BaseEntity {

    /**
     * 主键
     */
    private Integer id;

    /**
     * 数据类别
     */
    private String type;

    /**
     * 数据项
     */
    private String item;

    /**
     * 数据类型
     */
    private String dataType;
}
