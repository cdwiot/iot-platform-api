package com.ruoyi.system.domain;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.system.domain.ValidationGroups.Select;
import com.ruoyi.system.domain.ValidationGroups.Update;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class Alarm extends BaseEntity {
    @Null(message = "id应该为null", groups = {Select.class})
    @NotNull(message = "id不能为null", groups = {Update.class})
    private Integer id;

    @Excel(name = "项目id")
    @Null(message = "项目id应该为null", groups = {Update.class})
    private Integer projectId;

    @Null(message = "不支持的字段", groups = {Update.class})
    @Length(min = 0, max = 32, message = "项目名称应该少于{max}字符", groups = {Select.class})
    private String projectName;

    @Excel(name = "监控规则id")
    @Null(message = "监控规则id应该为null", groups = {Update.class})
    private Integer ruleId;

    @Null(message = "不支持的字段", groups = {Update.class})
    @Length(min = 0, max = 32, message = "规则名称应该少于{max}字符", groups = {Select.class})
    private String ruleName;

    @Null(message = "不支持的字段", groups = {Update.class})
    @Range(min = 0, max = 3, message = "紧急程度应该在{min}-{max}之间", groups = {Select.class})
    private Integer severity;
    @Null(message = "不支持的字段", groups = {Update.class, Select.class})
    private String severityString;

    @Null(message = "不支持的字段", groups = {Update.class})
    @Length(min = 0, max = 512, message = "描述应该少于{max}字符", groups = {Select.class})
    private String description;

    @Null(message = "不支持的字段", groups = {Update.class})
    @Range(min = 0, max = 1, message = "触发逻辑应该在{min}-{max}之间", groups = {Select.class})
    private Integer triggerLogic;
    @Null(message = "不支持的字段", groups = {Update.class})
    private String triggerLogicString;

    @Excel(name = "开始时间")
    @Null(message = "开始时间应该为null", groups = {Update.class})
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date start;

    @Excel(name = "结束时间")
    @Null(message = "结束时间应该为null", groups = {Update.class})
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date end;

    @Null(message = "不支持的字段", groups = {Update.class})
    private Boolean recovered;

    @NotBlank(message = "处理说明不能为空", groups = {Update.class})
    private String comment;

    @Null(message = "不支持的字段", groups = {Update.class})
    private String handledBy;

    @Null(message = "不支持的字段", groups = {Update.class})
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date handledAt;

    @Null(message = "不支持的字段", groups = {Update.class})
    private Boolean handled;

    @Null(message = "不支持的字段", groups = {Update.class, Select.class})
    private List<AlarmDetail> details;

    @NotBlank(message = "产品代码", groups = {Update.class})
    private String sn;
}
