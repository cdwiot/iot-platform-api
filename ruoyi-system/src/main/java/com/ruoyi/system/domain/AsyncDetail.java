package com.ruoyi.system.domain;

import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

@Data
public class AsyncDetail extends BaseEntity {

    /**
     * 主键
     */
    private Integer id;

    /**
     * 异步数据Id
     */
    private String tid;

    /**
     * 业务功能名称
     */
    private String business;

    /**
     * 状态0进行中1成功2失败
     */
    private Integer status;

    /**
     * 返回数据
     */
    private String data;

    /**
     * 所属公司
     */
    private Long companyId;
}
