package com.ruoyi.system.domain;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.system.domain.ValidationGroups.Create;
import com.ruoyi.system.domain.ValidationGroups.Update;

import org.hibernate.validator.constraints.Length;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class MqttCredentials extends BaseEntity {
    @Null(message = "id应该为null", groups = {Create.class})
    @NotNull(message = "id不能为null", groups = {Update.class})
    private Integer id;

    @Null(message = "产品终端id应该为null", groups = {Update.class})
    @NotNull(message = "产品终端id不能为null", groups = {Create.class})
    private Integer deviceTerminalId;

    @NotBlank(message = "客户端id不能为空", groups = {Create.class, Update.class})
    @Length(min = 1, max = 32, message = "客户端id应该少于{max}字符", groups = {Create.class, Update.class})
    private String clientId;

    @NotBlank(message = "用户名不能为空", groups = {Create.class, Update.class})
    @Length(min = 1, max = 32, message = "用户名应该少于{max}字符", groups = {Create.class, Update.class})
    private String username;

    @NotBlank(message = "密码不能为空", groups = {Create.class, Update.class})
    @Length(min = 1, max = 32, message = "密码应该少于{max}字符", groups = {Create.class, Update.class})
    private String password;

    @Null(message = "不支持的字段", groups = {Create.class, Update.class})
    private String tbDeviceId;

    @Null(message = "不支持的字段", groups = {Create.class})
    private Long companyId;
}
