package com.ruoyi.system.domain;

import com.alibaba.fastjson.JSONArray;
import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.system.domain.ValidationGroups.*;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 定时报表
 */
@Data
public class ReportTimed extends BaseEntity {

    /**
     * 定时报表Id
     */
    @Null(message = "定时报表Id应该为null", groups = {Create.class})
    @NotNull(message = "定时报表Id不应该为null", groups = {Update.class})
    private Integer id;

    /**
     * 报表名称
     */
    @NotNull(message = "报表名称不应该为null", groups = {Create.class,Update.class})
    private String name;

    /**
     * 周期
     */
    @NotNull(message = "周期不应该为null", groups = {Create.class,Update.class})
    private String periodic;

    /**
     * 格式
     */
    @NotNull(message = "格式不应该为null", groups = {Create.class,Update.class})
    private String form;

    /**
     * 统计数据
     */
    private JSONArray choseData;

    /**
     * 数据
     */
    private String data;

    /**
     * 所属公司
     */
    private Long companyId;

    /**
     * 用户名称集合
     */
    private Set<String> usernames;
}
