package com.ruoyi.system.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 生成计划任务汇总表
 */
@SuppressWarnings("AlibabaLowerCamelCaseVariableNaming")
@Data
public class CreateTaskGatherVO implements Serializable {

    /**
     * 单位/部门
     */
    private String useUnit;

    /**
     * 计划开始时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date startTime;

    /**
     * 计划结束时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endTime;

    /**
     * 项目名称
     */
    private String projectName;

    /**
     * 责任人
     */
    private String nickName;

    /**
     * 总数量
     */
    private Integer sumNum;

    /**
     * 异常数量
     */
    private Integer abnormalNum;

    /**
     * 产品详细
     */
    private List<DeviceGatherVO> deviceGatherVOS;
}
