package com.ruoyi.system.domain.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 产品额外属性返回结果VO
 */
@SuppressWarnings("AlibabaLowerCamelCaseVariableNaming")
@Data
public class ProductPropertyValueResultVO implements Serializable {

    /**
     * 属性组
     */
    private String propertyGroup;

    /**
     * 属性值
     */
    private List<ProductPropertyValueVO> productPropertyValueVOS;
}
