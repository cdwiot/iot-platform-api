package com.ruoyi.system.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.system.domain.DeviceCustomField;
import com.ruoyi.system.domain.PointCheckDeviceDetail;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

@SuppressWarnings("AlibabaLowerCamelCaseVariableNaming")
@Data
public class PointCheckCreatReportVO implements Serializable {

    /**
     * 产品名称
     */
    private String name;

    /**
     * 型号规格
     */
    private String code;

    /**
     * 产品编号
     */
    private String sn;

    /**
     * 产品描述
     */
    private String description;

    /**
     * 使用单位
     */
    private String useUnit;

    /**
     * 任务开始时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date startTime;

    /**
     * 任务结束时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endTime;

    /**
     * 执行人
     */
    private String executor;

    /**
     * 执行人昵称
     */
    private String nickName;

    /**
     * 任务状态
     */
    private String status;

    /**
     * 产品检查项
     */
    private Map<String, List<PointCheckDeviceDetail>> checkItemMap;

    /**
     * 产品检查文字描述
     */
    private List<Map<String, Object>> descriptionVOS;

    /**
     * 产品检查处理信息
     */
    private Map<String, List<PointCheckTaskDealVO>> taskDealVOS;

    /**
     * 产品异常信息
     */
    private Map<String, Object> deviceAbnormal;

    /**
     * 产品自定义字段
     */
    private List<DeviceCustomField> customFields;
}
