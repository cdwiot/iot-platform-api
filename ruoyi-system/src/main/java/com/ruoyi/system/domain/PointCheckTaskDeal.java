package com.ruoyi.system.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.system.domain.ValidationGroups.*;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.util.Date;

/**
 * 点检任务-处理方式
 */
@Data
public class PointCheckTaskDeal extends BaseEntity {

    /**
     * 处理方式Id
     */
    private Integer id;

    /**
     * 产品检查Id
     */
    @NotNull(message = "产品检查Id不应该为null", groups = {Update.class,Create.class})
    private Integer detailId;

    /**
     * 处理方式
     */
    @NotNull(message = "产品检查Id不应该为null", groups = {Update.class,Create.class})
    private String way;

    /**
     * 处理结果
     */
    private String type;

    /**
     * 结果详情
     */
    private String typeMsg;

    /**
     * 更换时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date replaceTime;

    /**
     * 制造单位
     */
    private String makeUnit;

    /**
     * 更换原因
     */
    private String replaceReason;

    /**
     * 产品代号
     */
    private String sn;

    /**
     * 修复部门
     */
    private String repairDept;

    /**
     * 修复方法
     */
    private String repairWay;

    /**
     * 拍照记录
     */
    private String photoRecord;

    /**
     * 售后电话
     */
    private String salePhone;

    /**
     * 厂商
     */
    private String manufacturer;

    /**
     * 所属公司
     */
    private Long companyId;
}
