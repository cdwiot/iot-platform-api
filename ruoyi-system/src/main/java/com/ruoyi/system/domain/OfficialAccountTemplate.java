package com.ruoyi.system.domain;

import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.system.domain.ValidationGroups.*;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

/**
 * 微信公众号模板
 */
@Data
public class OfficialAccountTemplate extends BaseEntity {

    /**
     * 模板Id
     */
    @Null(message = "模板Id应该为null", groups = {Create.class})
    @NotNull(message = "模板Id不应该为null", groups = {Update.class})
    private Integer id;

    /**
     * 模板名称
     */
    @NotNull(message = "模板名称不能为null", groups = {Update.class, Create.class})
    //@Length(min = 0, max = 50, message = "模板名称应该少于{max}字符", groups = {Update.class, Create.class})
    private String name;

    /**
     * 模板编码
     */
    @NotNull(message = "模板编码不能为null", groups = {Update.class, Create.class})
    private String code;

    /**
     * 模板内容
     */
    @NotNull(message = "模板内容不能为null", groups = {Update.class, Create.class})
    //@Length(min = 0, max = 500, message = "模板内容应该少于{max}字符", groups = {ValidationGroups.Update.class, ValidationGroups.Create.class})
    private String content;


    /**
     * 所属公司
     */
    private Long companyId;
}
