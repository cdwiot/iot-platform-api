package com.ruoyi.system.domain;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.system.domain.ValidationGroups.Create;
import com.ruoyi.system.domain.ValidationGroups.Select;
import com.ruoyi.system.domain.ValidationGroups.Update;

import java.math.BigDecimal;
import java.util.Date;

@Data
@EqualsAndHashCode(callSuper=true)
public class Project extends BaseEntity
{
    @Null(message = "项目id应该为null", groups = {Create.class, Select.class})
    @NotNull(message = "项目id不能为null", groups = {Update.class})
    private Integer id;

    @Excel(name = "合同号")
//    @NotBlank(message = "合同号不能为空", groups = {Create.class, Update.class})
    @Length(min = 0, max = 32, message = "合同号应该少于{max}字符", groups = {Create.class, Update.class, Select.class})
    private String code;

    @Excel(name = "客户合同号")
    @NotBlank(message = "客户合同号不能为空", groups = {Create.class, Update.class})
    @Length(min = 0, max = 32, message = "客户合同号应该少于{max}字符", groups = {Create.class, Update.class, Select.class})
    private String peerCode;

    @Excel(name = "项目名称")
    @NotBlank(message = "项目名称不能为空", groups = {Create.class, Update.class})
    @Length(min = 0, max = 32, message = "项目名称应该少于{max}字符", groups = {Create.class, Update.class, Select.class})
    private String name;

    @Excel(name = "客户id")
    @NotNull(message = "客户不能为null", groups = {Create.class, Update.class})
    private Integer deptId;

    @Excel(name = "客户名称")
    @Null(message = "不支持的字段", groups = {Create.class, Update.class, Select.class})
    private String deptName;

    @Excel(name = "合同金额")
    @NotNull(message = "合同金额不能为null", groups = {Create.class, Update.class})
    private Double amount;

    @Excel(name = "行政区")
    @NotNull(message = "行政区不能为null", groups = {Create.class, Update.class})
    private String areaCode;

    @Excel(name = "行政区字符串")
    @Null(message = "不支持的字段", groups = {Create.class, Update.class, Select.class})
    private String areaName;

    @Excel(name = "项目地址")
    @NotNull(message = "项目地址不能为null", groups = {Create.class, Update.class})
    @Length(min = 0, max = 128, message = "项目地址应该少于{max}字符", groups = {Create.class, Update.class, Select.class})
    private String address;

    @Excel(name = "经度")
    @NotNull(message = "经度不能为null", groups = {Create.class, Update.class})
    @Null(message = "经度应该为null", groups = {Select.class})
    private Double lng;

    @Excel(name = "纬度")
    @NotNull(message = "纬度不能为null", groups = {Create.class, Update.class})
    @Null(message = "纬度应该为null", groups = {Select.class})
    private Double lat;

    @Excel(name = "责任人")
    @NotBlank(message = "责任人不能为空", groups = {Create.class, Update.class})
    @Length(min = 0, max = 32, message = "责任人应该少于{max}字符", groups = {Create.class, Update.class, Select.class})
    private String salesman;

    @Excel(name = "是否启用1是0否")
    @NotNull(message = "是否启用不能为null", groups = {Create.class, Update.class})
    @Range(min = 0, max = 1, message = "在线状态应该在{min}-{max}之间", groups = {Create.class, Update.class, Select.class})
    private Integer isOn;

    @Excel(name = "1内部服务2外部服务3测试项目")
    @NotNull(message = "项目类型不能为null", groups = {Create.class, Update.class})
    private Integer type;

    @Excel(name = "统计时间")
    @NotNull(message = "统计时间不能为null", groups = {Create.class, Update.class})
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date inputTime;

    @Null(message = "不支持的字段", groups = {Create.class, Update.class})
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date inputTimeStart;

    @Null(message = "不支持的字段", groups = {Create.class, Update.class})
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date inputTimeEnd;

    @NotNull(message = "备注不能为null", groups = {Create.class, Update.class})
    @Length(min = 0, max = 64, message = "备注应该少于{max}字符", groups = {Create.class, Update.class, Select.class})
    private String remark;

    @Null(message = "不支持的字段", groups = {Create.class, Update.class, Select.class})
    private Integer deviceAmount;

    @Null(message = "不支持的字段", groups = {Create.class, Update.class, Select.class})
    @Excel(name = "所属公司Id")
    private Long companyId;
}
