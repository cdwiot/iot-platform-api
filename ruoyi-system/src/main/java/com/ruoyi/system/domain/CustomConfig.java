package com.ruoyi.system.domain;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.system.domain.ValidationGroups.Update;

import org.hibernate.validator.constraints.Length;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class CustomConfig extends BaseEntity {
    @Null(message = "不支持的字段", groups = {Update.class})
    private Integer id;

    @Null(message = "不支持的字段", groups = {Update.class})
    private String name;

    @NotNull(message = "配置项值不能为null", groups = {Update.class})
    @Length(min = 0, max = 256, message = "配置项值应该少于{max}字符", groups = {Update.class})
    private String value;

    @Null(message = "不支持的字段", groups = {Update.class})
    private Long companyId;
}
