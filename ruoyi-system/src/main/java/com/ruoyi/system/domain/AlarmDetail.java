package com.ruoyi.system.domain;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.system.domain.ValidationGroups.Select;
import com.ruoyi.system.domain.ValidationGroups.Update;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class AlarmDetail extends BaseEntity {
    @Null(message = "id应该为null", groups = {Select.class})
    @NotNull(message = "id不能为null", groups = {Update.class})
    private Integer id;

    @Excel(name = "事件id")
    @Null(message = "不支持的字段", groups = {Update.class})
    private Integer alarmId;

    @Null(message = "不支持的字段", groups = {Update.class})
    private Integer conditionId;
    @Null(message = "不支持的字段", groups = {Update.class})
    private MonitorCondition condition;

    @Null(message = "不支持的字段", groups = {Update.class})
    private Integer productId;
    @Null(message = "不支持的字段", groups = {Update.class})
    private Integer deviceId;
    @Null(message = "不支持的字段", groups = {Update.class})
    private String deviceName;
    @Null(message = "不支持的字段", groups = {Update.class})
    private String deviceCode;
    @Null(message = "不支持的字段", groups = {Update.class})
    private String system;
    @Null(message = "不支持的字段", groups = {Update.class})
    private String positionNumber;
    @Null(message = "不支持的字段", groups = {Update.class})
    private String address;
    @Null(message = "不支持的字段", groups = {Update.class})
    private String lng;
    @Null(message = "不支持的字段", groups = {Update.class})
    private String lat;

    @Excel(name = "值")
    @Null(message = "不支持的字段", groups = {Update.class})
    private String value;

    @Excel(name = "开始时间")
    @Null(message = "开始时间应该为null", groups = {Update.class})
    private Date start;

    @Excel(name = "结束时间")
    @Null(message = "结束时间应该为null", groups = {Update.class})
    private Date end;

    @Null(message = "不支持的字段", groups = {Update.class})
    private Boolean recovered;
}
