package com.ruoyi.system.domain;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.system.domain.ValidationGroups.*;

import org.hibernate.validator.constraints.Length;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class ProjectScenarioRule extends BaseEntity {
    @Null(message = "id应该为null", groups = {Create.class, Select.class})
    @NotNull(message = "id不能为null", groups = {Update.class})
    private Integer id;

    @NotBlank(message = "场景联动规则名称不能为空", groups = {Create.class, Update.class})
    @Length(min = 0, max = 32, message = "场景联动规则名称应该少于{max}字符", groups = {Create.class, Update.class, Select.class})
    private String name;

    @Null(message = "项目id应该为null", groups = {Update.class})
    @NotNull(message = "项目id不能为null", groups = {Create.class, Select.class})
    private Integer projectId;

    @Null(message = "监控规则id应该为null", groups = {Update.class})
    @NotNull(message = "监控规则id不能为null", groups = {Create.class})
    private Integer monitorRuleId;
    @Null(message = "不支持的字段", groups = {Create.class, Update.class})
    private String monitorRuleName;
    @Null(message = "不支持的字段", groups = {Create.class, Update.class})
    private MonitorRule monitorRule;

    @Null(message = "不支持的字段", groups = {Create.class, Update.class})
    private List<@Valid ProjectScenarioAction> actions;

    // @Null(message = "触发条件应该为null", groups = {Create.class, Update.class, Select.class})
    private List<@Valid Period> activePeriods;

    @Null(message = "不支持的字段", groups = {Create.class, Update.class, Select.class})
    private Long companyId;
}
