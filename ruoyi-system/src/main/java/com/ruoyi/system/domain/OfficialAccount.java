package com.ruoyi.system.domain;

import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.system.domain.ValidationGroups.*;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

/**
 * 公众号账户配置信息
 */
@Data
public class OfficialAccount extends BaseEntity {
    /**
     * 账户Id
     */
    @Null(message = "账户Id应该为null", groups = {Create.class})
    @NotNull(message = "账户Id不应该为null", groups = {Update.class})
    private Integer id;

    /**
     * appId
     */
    @NotNull(message = "appId不能为null", groups = {Update.class, Create.class})
    private String appId;

    /**
     * 密钥
     */
    @NotNull(message = "密钥不能为null", groups = {Update.class, Create.class})
    private String secret;

    /**
     * 账号标志（0大账号1特有账号）
     */
    private String accessFlag;

    /**
     * 能否添加标志，0代表不能添加，1能添加
     */
    private String addFlag;

    /**
     * 所属公司
     */
    private Long companyId;
}
