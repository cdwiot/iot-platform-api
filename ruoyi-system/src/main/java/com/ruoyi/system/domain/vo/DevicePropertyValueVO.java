package com.ruoyi.system.domain.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * 产品额外属性值VO
 */
@Data
public class DevicePropertyValueVO implements Serializable {

    /**
     * 属性值Id
     */
    private Integer id;

    /**
     * 产品Id
     */
    private Integer deviceId;

    /**
     * 属性Id
     */
    private Integer propertyId;

    /**
     * 属性名称
     */
    private String propertyName;

    /**
     * 属性组
     */
    private String propertyGroup;

    /**
     * 属性类型（文本string、时间date、附件file）
     */
    private String dataType;

    /**
     * 属性值
     */
    private String value;

    /**
     * 排列顺序
     */
    private Integer orderNum;
}
