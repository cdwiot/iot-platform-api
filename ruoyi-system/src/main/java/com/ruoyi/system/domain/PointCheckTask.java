package com.ruoyi.system.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.system.domain.ValidationGroups.*;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.util.*;
import java.util.Set;

@Data
public class PointCheckTask extends BaseEntity {
    /**
     * 任务Id
     */
    @Null(message = "任务Id应该为null", groups = {Create.class})
    @NotNull(message = "任务Id不应该为null", groups = {Update.class})
    private Integer id;

    /**
     * 任务类型
     */
    private Integer category;

    /**
     * 项目id
     */
    @NotNull(message = "项目id不应该为null", groups = {Create.class,Update.class})
    private Integer projectId;

    /**
     * 分组id
     */
    private Integer groupId;

    /**
     * 产品数量
     */
    private Integer deviceNum;

    /**
     * 项目名称
     */
    private String projectName;

    /**
     * 项目合同号
     */
    private String projectCode;

    /**
     * 客户合同号
     */
    private String peerCode;

    /**
     * 客户名称
     */
    private String deptName;

    /**
     * 客户部门Id
     */
    private Long deptId;


    /**
     * 开始时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @NotNull(message = "开始时间不应该为null", groups = {Create.class,Update.class})
    private Date startTime;

    /**
     * 查询开始时间-开始时间
     */
    private Date startTimeBegin;

    /**
     * 查询结束时间-开始时间
     */
    private Date startTimeEnd;

    /**
     * 结束时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @NotNull(message = "结束时间不应该为null", groups = {Create.class,Update.class})
    private Date endTime;

    /**
     * 查询开始时间-结束时间
     */
    private Date endTimeBegin;

    /**
     * 查询结束时间-结束时间
     */
    private Date endTimeEnd;

    /**
     * 执行人
     */
    @NotNull(message = "执行人不应该为null", groups = {Create.class,Update.class})
    private String executor;

    /**
     * 用户昵称
     */
    private String nickName;

    /**
     * 状态（0未开始1进行中2已超时3已完成）
     */
    private String status;

    /**
     * 状态集合
     */
    private List<String> statusList;

    /**
     * 任务id集合
     */
    private Set<Integer> taskIds;

    /**
     * 备注
     */
    private String remark;

    /**
     * 所属公司
     */
    private Long companyId;

    /**
     * 任务名称
     */
    @NotNull(message = "任务名称不应该为null", groups = {Create.class,Update.class})
    private String name;


    /**
     * 搜索字段
     */
    private String keyValue;

    /**
     * 产品Id（多个用，分割）
     */
    @NotNull(message = "所选产品不应该为null", groups = {Create.class,Update.class})
    private String deviceIds;

    /**
     * 产品Id（多个用，分割）
     */
    @NotNull(message = "所选产品不应该为null", groups = {Create.class,Update.class})
    private HashMap<String,ArrayList<Integer>> checkConfigList;


}
