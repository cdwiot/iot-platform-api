package com.ruoyi.system.domain.vo;

import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class ProjectGroupDeviceResultVO extends BaseEntity implements Serializable {

    /**
     * 已经绑定的产品集合
     */
    private List<ProjectGroupDeviceVO> bindDevices;

    /**
     * 未绑定的产品集合
     */
    private List<ProjectGroupDeviceVO> notBindDevices;
}
