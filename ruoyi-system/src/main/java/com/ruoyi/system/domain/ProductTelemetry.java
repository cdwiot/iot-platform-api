package com.ruoyi.system.domain;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.common.enums.DataType;
import com.ruoyi.system.domain.ValidationGroups.Create;
import com.ruoyi.system.domain.ValidationGroups.Select;
import com.ruoyi.system.domain.ValidationGroups.Update;

import org.hibernate.validator.constraints.Length;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class ProductTelemetry extends BaseEntity {
    @Null(message = "id应该为null", groups = {Create.class, Select.class})
    @NotNull(message = "id不能为null", groups = {Update.class})
    private Integer id;

    @Excel(name = "产品id")
    @NotNull(message = "产品id不能为null", groups = {Create.class, Update.class})
    private Integer productId;

    @Excel(name = "终端序号")
    private Integer terminalNumber;
//    @AssertTrue(message = "无效的终端序号", groups = {Create.class, Update.class})
//    public boolean isValidTerminalNumber() {
//        boolean passed = false;
//        try {
//            if (isComputed) {
//                passed = (terminalNumber != null);
//            } else {
//                passed = (terminalNumber != null);
//            }
//        } catch(Exception e) {}
//        return passed;
//    }

    @Excel(name = "名称")
    @NotBlank(message = "名称不能为空", groups = {Create.class, Update.class})
    @Length(min = 0, max = 32, message = "名称应该少于{max}字符", groups = {Create.class, Update.class, Select.class})
    private String name;

    @Excel(name = "终端侧标识符")
    private String terminalIdentifier;
    @AssertTrue(message = "无效的终端侧标识符", groups = {Create.class, Update.class})
    public boolean isValidTerminalIdentifier() {
        boolean passed = false;
        try {
            if (isComputed) {
                passed = (terminalIdentifier == null);
            } else {
                passed = (terminalIdentifier != null && terminalIdentifier.length() < 16);
            }
        } catch(Exception e) {}
        return passed;
    }

    @Excel(name = "用户侧标识符")
    @NotBlank(message = "用户侧标识符不能为空", groups = {Create.class, Update.class})
    @Length(min = 0, max = 16, message = "用户侧标识符应该少于{max}字符", groups = {Create.class, Update.class, Select.class})
    private String userIdentifier;

    @Excel(name = "数据类型")
    private String dataType;
    @AssertTrue(message = "数据类型不存在", groups = {Create.class, Update.class})
    public boolean isValidDataType() {
        return DataType.labels().contains(dataType);
    }

    @Excel(name = "单位")
    @NotNull(message = "单位不能为null", groups = {Create.class, Update.class})
    private String unit;

    @Excel(name = "最小值")
    @NotNull(message = "最小值不能为null", groups = {Create.class, Update.class})
    private String min;
    @AssertTrue(message = "无效的最小值", groups = {Create.class, Update.class})
    public boolean isValidMin() {
        boolean passed = false;
        try {
            if (min.isEmpty()) {} else {
                Double.parseDouble(min);
            }
            passed = true;
        } catch(Exception e) {}
        return passed;
    }

    @Excel(name = "最大值")
    @NotNull(message = "最大值不能为null", groups = {Create.class, Update.class})
    private String max;
    @AssertTrue(message = "无效的最大值", groups = {Create.class, Update.class})
    public boolean isValidMax() {
        boolean passed = false;
        try {
            if (max.isEmpty()) {} else {
                Double.parseDouble(max);
            }
            passed = true;
        } catch(Exception e) {}
        return passed;
    }

    @Excel(name = "步长")
    @NotNull(message = "步长不能为null", groups = {Create.class, Update.class})
    private String step;
    @AssertTrue(message = "无效的步长值", groups = {Create.class, Update.class})
    public boolean isValidStep() {
        boolean passed = false;
        try {
            if (step.isEmpty()) {} else {
                Double.parseDouble(step);
            }
            passed = true;
        } catch(Exception e) {}
        return passed;
    }

    @Excel(name = "是否计算值")
    @NotNull(message = "是否计算值不能为null", groups = {Create.class})
    Boolean isComputed;

    @Excel(name = "latex")
    @NotNull(message = "latex不能为null", groups = {Create.class, Update.class})
    String latex;

    @Excel(name = "公式")
    @NotNull(message = "公式不能为null", groups = {Create.class, Update.class})
    String formula;

    @Excel(name = "公式图片")
    @NotNull(message = "公式图片不能为null", groups = {Create.class, Update.class})
    String formulaImage;

    @AssertTrue(message = "计算值参数不正确", groups = {Create.class, Update.class})
    public boolean isValidComputed() {
        boolean passed = false;
        if (isComputed) {
            passed = (!latex.isEmpty() && !formula.isEmpty() && !formulaImage.isEmpty());
        } else {
            passed = ("".equals(latex) && "".equals(formula) && "".equals(formulaImage));
        }
        return passed;
    }

    @Null(message = "不支持的字段", groups = {Create.class, Update.class, Select.class})
    private Long companyId;

    @NotNull(message = "是否隐藏不能为null", groups = {Create.class})
    Boolean isHidden;

    @Excel(name = "分组名称")
    @NotNull(message = "分组名称不能为null", groups = {Create.class})
    String groupName;
}
