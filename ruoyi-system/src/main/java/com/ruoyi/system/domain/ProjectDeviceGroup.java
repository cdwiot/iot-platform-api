package com.ruoyi.system.domain;

import lombok.Data;
import lombok.experimental.Accessors;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.hibernate.validator.constraints.Length;
import com.ruoyi.system.domain.ValidationGroups.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.util.List;

/**
 * 产品分组对象 tbl_project_device_group
 *
 * @author ruoyi
 * @date 2021-08-10
 */
@SuppressWarnings("AlibabaLowerCamelCaseVariableNaming")
@Data
public class ProjectDeviceGroup extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * $column.columnComment
     */
    @Null(message = "分组id应该为null", groups = {Create.class, Select.class})
    @NotNull(message = "分组id不能为null", groups = {Update.class})
    private Integer id;

    /**
     * 项目id
     */
    @Excel(name = "项目id")
    @NotNull(message = "项目id不能为null", groups = {Create.class, Update.class})
    private Integer projectId;

    @Excel(name = "项目名称")
    private String projectName;

    /**
     * 分组名称
     */
    @Excel(name = "分组名称")
    @NotBlank(message = "名称不能为空", groups = {Create.class, Update.class})
    @Length(min = 1, max = 32, message = "名称长度应该在{min}-{max}之间", groups = {Create.class, Update.class, Select.class})
    private String name;


    /**
     * 描述
     */
    @Excel(name = "描述")
    private String description;

    /**
     * 产品Id集合，多个用,分割
     */
    //@Null(message = "产品id应该为null", groups = {Create.class, Update.class})
    private List<Integer> Ids;
    private List<Integer> oldIds;

    @Null(message = "产品list", groups = {Create.class, Update.class})
    private List<Device> deviceList;

    /**
     * 所属公司Id
     */
    @Excel(name = "所属公司Id")
    private Long companyId;

}