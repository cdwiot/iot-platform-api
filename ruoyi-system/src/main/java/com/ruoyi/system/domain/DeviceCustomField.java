package com.ruoyi.system.domain;

import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.system.domain.ValidationGroups.Create;
import com.ruoyi.system.domain.ValidationGroups.Update;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.io.Serializable;

/**
 * 产品额外属性表
 */
@Data
public class DeviceCustomField extends BaseEntity implements Serializable {
    /**
     * 字段Id
     */
    @Null(message = "字段Id应该为null", groups = {Create.class})
    @NotNull(message = "字段Id不能为null", groups = {Update.class})
    private Integer id;

    /**
     * 字段名称
     */
    @NotNull(message = "名称不能为null", groups = {Update.class,Create.class})
    private String name;

    /**
     * 产品Id
     */
    @NotNull(message = "产品Id不能为null", groups = {Update.class,Create.class})
    private Integer deviceId;

    /**
     * 字段值
     */
//    @NotNull(message = "属性名称不能为null", groups = {Update.class,Create.class})
    private String value;

    /**
     * 所属公司
     */
    private Long companyId;
}
