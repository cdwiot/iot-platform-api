package com.ruoyi.system.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 任务产品汇总信息
 */
@Data
public class DeviceGatherVO implements Serializable {

    /**
     * 产品名称
     */
    private String name;

    /**
     * 产品状态
     */
    private String status;

    /**
     * 产品代号
     */
    private String code;

    /**
     * 最后检查时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date lastTime;

    /**
     * 异常位置
     */
    private List<Map<String,Object>> errorData;

    /**
     * 系统
     */
    private String system;
    /**
     * 产品位号
     */
    private String positionNumber;

    /**
     * 厂家
     */
    private String factory;
}
