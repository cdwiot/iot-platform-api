package com.ruoyi.system.domain.vo;

import lombok.Data;

import java.io.Serializable;

@SuppressWarnings("AlibabaLowerCamelCaseVariableNaming")
@Data
public class WxCallBackParamVO implements Serializable {

    /**
     * 开发者微信号
     */
    private String ToUserName;

    /**
     * 发送方帐号（一个OpenID）
     */
    private String FromUserName;

    /**
     * 消息创建时间 （整型）
     */
    private Long CreateTime;

    /**
     * 消息类型，event
     */
    private String MsgType;

    /**
     * 事件类型，subscribe
     */
    private String Event;

    /**
     * 事件KEY值，qrscene_为前缀，后面为二维码的参数值
     */
    private String EventKey;

    /**
     * 二维码的ticket，可用来换取二维码图片
     */
    private String Ticket;

}
