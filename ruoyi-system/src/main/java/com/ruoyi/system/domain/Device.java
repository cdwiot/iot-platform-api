package com.ruoyi.system.domain;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.system.domain.ValidationGroups.Create;
import com.ruoyi.system.domain.ValidationGroups.Select;
import com.ruoyi.system.domain.ValidationGroups.Update;

import org.hibernate.validator.constraints.Length;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class Device extends BaseEntity {
    @Null(message = "设备id应该为null", groups = {Create.class, Select.class})
    @NotNull(message = "设备id不能为null", groups = {Update.class})
    private Integer id;

    @Excel(name = "项目id")
    @Null(message = "项目id应该为null", groups = {Create.class, Update.class})
    private Integer projectId;

    @Excel(name = "产品id")
    @Null(message = "产品id应该为null", groups = {Update.class})
    @NotNull(message = "产品id不能为null", groups = {Create.class})
    private Integer productId;

    @Excel(name = "产品名称")
    @Null(message = "不支持的字段", groups = {Create.class, Update.class})
    @Length(min = 0, max = 32, message = "产品名称应该少于{max}字符", groups = {Select.class})
    private String productName;

    @Null(message = "不支持的字段", groups = {Create.class, Update.class})
    private Boolean alarm;


    @Excel(name = "设备名称")
    @NotBlank(message = "名称不能为空", groups = {Create.class, Update.class})
    @Length(min = 0, max = 32, message = "名称应该少于{max}字符", groups = {Create.class, Update.class, Select.class})
    private String name;

    @Excel(name = "产品代码")
    @NotBlank(message = "产品代码不能为空", groups = {Create.class, Update.class})
    @Length(min = 0, max = 32, message = "产品代码应该少于{max}字符", groups = {Create.class, Update.class, Select.class})
    private String sn;

    @Null(message = "不支持的字段", groups = {Create.class, Update.class})
    @Length(min = 0, max = 32, message = "终端序列号应该少于{max}字符", groups = {Select.class})
    private String terminalSn;

    @Excel(name = "系统")
    @NotNull(message = "系统不能为null", groups = {Create.class, Update.class})
    @Length(min = 0, max = 64, message = "系统应该少于{max}字符", groups = {Create.class, Update.class, Select.class})
    private String system;

    @Excel(name = "位号")
    @NotNull(message = "位号不能为null", groups = {Create.class, Update.class})
    @Length(min = 0, max = 32, message = "系统应该少于{max}字符", groups = {Create.class, Update.class, Select.class})
    private String positionNumber;

    @Excel(name = "描述")
    @NotNull(message = "描述不能为null", groups = {Create.class, Update.class})
    @Length(min = 0, max = 512, message = "描述应该少于{max}字符", groups = {Create.class, Update.class, Select.class})
    private String description;

    @Excel(name = "照片")
    @NotNull(message = "照片不能为null", groups = {Create.class, Update.class})
    @Length(min = 0, max = 512, message = "照片应该少于{max}字符", groups = {Create.class, Update.class})
    private String photo;

    /**
     * 售后电话
     */
    @Excel(name = "售后电话")
    private String salePhone;

    @Excel(name = "地址")
    @NotNull(message = "地址不能为null", groups = {Create.class, Update.class})
    @Length(min = 0, max = 64, message = "地址应该少于{max}字符", groups = {Create.class, Update.class, Select.class})
    private String address;

    @Excel(name = "经度")
    // @NotNull(message = "经度不能为null", groups = {Create.class, Update.class})
    @Null(message = "经度应该为null", groups = {Select.class})
    private Double lng;

    @Excel(name = "纬度")
    // @NotNull(message = "纬度不能为null", groups = {Create.class, Update.class})
    @Null(message = "纬度应该为null", groups = {Select.class})
    private Double lat;

    @Null(message = "已报废应该为null", groups = {Create.class, Update.class})
    private Boolean discarded;

    @Null(message = "不支持的字段", groups = {Create.class, Update.class, Select.class})
    private String tbDeviceId;

    @Null(message = "不支持的字段", groups = {Create.class, Update.class, Select.class})
    private String tbDeviceName;

    @Null(message = "不支持的字段", groups = {Create.class, Update.class, Select.class})
    private List<@Valid DeviceTerminal> terminals;

    @Null(message = "不支持的字段", groups = {Create.class, Update.class, Select.class})
    private Long companyId;

    /**
     * 项目合同号
     */
    private String projectCode;

    /**
     * 客户合同号
     */
    private String peerCode;

    /**
     * 项目名称
     */
    private String projectName;
}
