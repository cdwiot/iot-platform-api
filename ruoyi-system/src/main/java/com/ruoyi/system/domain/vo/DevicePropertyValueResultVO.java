package com.ruoyi.system.domain.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@SuppressWarnings("AlibabaLowerCamelCaseVariableNaming")
@Data
public class DevicePropertyValueResultVO implements Serializable {


    /**
     * 属性组
     */
    private String propertyGroup;

    /**
     * 属性值
     */
    private List<DevicePropertyValueVO> devicePropertyValueVOS;
}
