package com.ruoyi.system.domain.vo;

import com.ruoyi.system.domain.DeviceCustomField;
import com.ruoyi.system.domain.PointCheckDeviceDetail;
import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * PC端点检报告
 */
@SuppressWarnings("AlibabaLowerCamelCaseVariableNaming")
@Data
public class PointCheckPCReportVO implements Serializable {
    /**
     * 产品名称
     */
    private String name;

    /**
     * 型号规格
     */
    private String code;

    /**
     * 产品编号
     */
    private String sn;

    /**
     * 产品描述
     */
    private String description;

    /**
     * 使用单位
     */
    private String useUnit;

    /**
     * 任务检查信息
     */
    private List<PointCheckPCDeviceDetailVO> pcDeviceDetailVOList;

    /**
     * 产品检查文字描述
     */
    private List<Map<String,Object>> descriptionVOS;

    /**
     * 产品检查处理信息
     */
    private Map<String,List<PointCheckTaskDealVO>> taskDealVOS;

    /**
     * 产品自定义字段
     */
    private List<DeviceCustomField> customFields;

    /**
     * 产品自定义字段
     */
    private List<PointCheckDeviceDetail> deviceCustomFields;
    /**
     * 产品异常信息
     */
    private Map<String, Object> deviceAbnormal;

}
