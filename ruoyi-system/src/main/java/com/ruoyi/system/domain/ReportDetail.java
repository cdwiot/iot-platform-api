package com.ruoyi.system.domain;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.system.domain.ValidationGroups.*;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 实时报表实例
 */
@Data
public class ReportDetail extends BaseEntity {

    /**
     * 报表详情Id
     */
    @Null(message = "报表详情Id应该为null", groups = {Create.class})
    private Integer id;

    /**
     * 报表Id
     */
    private Integer reportId;

    /**
     * 生成方式0手动1自动
     */
    private String type;

    /**
     * 报表名称
     */
    @NotNull(message = "报表名称不应该为null", groups = {Create.class})
    private String name;

    /**
     * 报表格式
     */
    @NotNull(message = "报表格式不应该为null", groups = {Create.class})
    private String form;

    /**
     * 周期
     */
    private String periodic;

    /**
     * 统计数据
     */
    private JSONArray choseData;

    /**
     * 数据
     */
    private String data;


    /**
     * 报表
     */
    private String url;

    /**
     * 异步数据Id
     */
    private String tid;

    /**
     * 异步状态0进行中1成功2失败
     */
    private Integer status;

    /**
     * 所属公司
     */
    private Long companyId;

    /**
     * 搜索-开始时间
     */
    private Date begin;

    /**
     * 搜索-结束时间
     */
    private Date end;


    /**
     * 用户名称集合
     */
    private Set<String> usernames;
}
