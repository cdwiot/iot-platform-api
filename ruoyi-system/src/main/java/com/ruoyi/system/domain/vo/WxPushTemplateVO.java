package com.ruoyi.system.domain.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.Map;

@SuppressWarnings("AlibabaLowerCamelCaseVariableNaming")
@Data
public class WxPushTemplateVO implements Serializable {

    /**
     * 接收者openid
     */
    private String touser;

    /**
     * 模板ID
     */
    private String template_id;

    /**
     * 模板跳转链接（海外帐号没有跳转能力）
     */
    private String url;

    /**
     * 跳小程序所需数据，不需跳小程序可不用传该数据
     */
    private String miniprogram;

    /**
     * 所需跳转到的小程序appid（该小程序appid必须与发模板消息的公众号是绑定关联关系，暂不支持小游戏）
     */
    private String appid;

    /**
     * 所需跳转到小程序的具体页面路径，支持带参数,（示例index?foo=bar），要求该小程序已发布，暂不支持小游戏
     */
    private String pagepath;

    /**
     * 模板数据
     * "data":{
     *            "first": {
     *                "value":"恭喜你购买成功！",
     *                "color":"#173177"
     *             },
     *            "keyword1":{
     *                 "value":"巧克力",
     *                 "color":"#173177"
     *             }
     *        }
     */
    private Map<String,Map<String,Object>>  data;
}
