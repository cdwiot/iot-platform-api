package com.ruoyi.system.domain;

import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.system.domain.ValidationGroups.Create;
import com.ruoyi.system.domain.ValidationGroups.Update;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.io.Serializable;

/**
 * 产品额外属性值
 */
@Data
public class ProductPropertyValue extends BaseEntity implements Serializable {

    /**
     * 属性值Id
     */
    @Null(message = "属性值Id应该为null", groups = {Create.class})
    @NotNull(message = "属性值Id不能为null", groups = {Update.class})
    private Integer id;
    /**
     * 产品Id
     */
    @NotNull(message = "产品Id不能为null", groups = {Update.class,Create.class})
    private Integer productId;

    /**
     * 属性Id
     */
    @NotNull(message = "属性Id不能为null", groups = {Update.class,Create.class})
    private Integer propertyId;

    /**
     * 属性组
     */
    private String propertyGroup;
    /**
     * 属性值
     */
    private String value;

    /**
     * 所属公司
     */
    private Long companyId;


}
