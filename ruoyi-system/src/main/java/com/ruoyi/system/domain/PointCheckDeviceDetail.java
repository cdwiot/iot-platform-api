package com.ruoyi.system.domain;

import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.system.domain.ValidationGroups.*;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

/**
 * 产品检查详情
 */
@Data
public class PointCheckDeviceDetail extends BaseEntity {

    /**
     * 产品检查Id
     */
    private Integer id;

    @NotNull(message = "任务产品关系Id不应该为null", groups = {Update.class, Create.class})
    private Integer taskDeviceId;

    /**
     * 检查类别
     */
//    @NotNull(message = "点检类型不能为null", groups = {Create.class,Update.class})
    private Integer category;

    @NotNull(message = "检查类别不应该为null", groups = {Update.class, Create.class})
    private String type;

    @NotNull(message = "检查项Id不应该为null", groups = {Update.class, Create.class})
    private String item;

    @NotNull(message = "状态不应该为null", groups = {Update.class, Create.class})
    private String status;

    /**
     * 异常详情
     */
    private String detail;

    /**
     * 所属公司
     */
    private Long companyId;

    /**
     * 处理id
     */
    private Integer dealId;

    /**
     * 排序
     */
    private Integer orderNum;

    /**
     * type排序
     */
    private Integer typeOrder;

    /**
     * item排序
     */
    private Integer itemOrder;

    /**
     * 是否处理（false未处理true已处理）
     */
    private boolean isDeal;
}
