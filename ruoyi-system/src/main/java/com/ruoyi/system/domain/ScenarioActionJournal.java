package com.ruoyi.system.domain;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.system.domain.ValidationGroups.Select;
import com.ruoyi.system.domain.ValidationGroups.Update;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class ScenarioActionJournal extends BaseEntity {
    @Null(message = "id应该为null", groups = {Select.class})
    private Integer id;

    private Integer deviceId;
    @Length(min = 0, max = 32, message = "产品名称应该少于{max}字符", groups = {Select.class})
    private String deviceName;

    private Integer projectId;
    @Length(min = 0, max = 32, message = "项目名称应该少于{max}字符", groups = {Select.class})
    private String projectName;

    private Integer scenarioRuleId;
    @Length(min = 0, max = 32, message = "场景联动规则名称应该少于{max}字符", groups = {Select.class})
    private String scenarioRuleName;

    private Integer scenarioActionId;

    @Range(min = 0, max = 1, message = "执行状态应该在{min}-{max}之间", groups = {Select.class})
    private Integer state;
    @Null(message = "不支持的字段", groups = {Select.class})
    private String stateString;

    @Null(message = "不支持的字段", groups = {Select.class})
    private String data;
    @Null(message = "不支持的字段", groups = {Select.class})
    private String result;

    @Null(message = "不支持的字段", groups = {Select.class})
    private Date timestamp;

    private Date start;
    private Date end;
}
