package com.ruoyi.system.domain.vo;

import com.ruoyi.system.domain.ValidationGroups;
import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * ws发送rpc消息记录
 */
@ToString
@Data
public class RpcMsgLogVO implements Serializable {
    private static final long serialVersionUID = -1L;

    @NotNull(message = "设备id不能为null", groups = {ValidationGroups.Update.class})
    private Long id;

    private String deviceId;

    private String requestBody;

    private String tbUsername;

    private String tbPassword;

    //@Excel(name = "公司id, sys_dept.dept_id")
    private Long companyId;

    //失败次数
    private Integer errorTimes;

    //状态，0：成功，1：失败
    private Integer status;

    //更新时间
    private Date updatedTime;

    private List<Long> ids;

    //传感器sn
    private String sn;
    //产品名称
    private String productName;
    //产品sn
    private String productSn;

}
