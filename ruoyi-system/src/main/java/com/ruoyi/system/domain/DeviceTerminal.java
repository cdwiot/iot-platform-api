package com.ruoyi.system.domain;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.system.domain.ValidationGroups.Create;
import com.ruoyi.system.domain.ValidationGroups.Select;
import com.ruoyi.system.domain.ValidationGroups.Update;

import org.hibernate.validator.constraints.Length;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class DeviceTerminal extends BaseEntity {
    @Null(message = "终端id应该为null", groups = {Create.class, Select.class})
    @NotNull(message = "终端id不能为null", groups = {Update.class})
    private Integer id;

    @Excel(name = "产品id")
    @Null(message = "产品id应该为null", groups = {Update.class})
    @NotNull(message = "产品id不能为null", groups = {Create.class})
    private Integer deviceId;

    @Excel(name = "终端序列号")
    @NotNull(message = "序列号不能为null", groups = {Update.class})
    @Length(min = 1, max = 32, message = "序列号长度应该在{min}-{max}之间", groups = {Update.class, Select.class})
    private String sn;

    @Excel(name = "产品终端id")
    @Null(message = "产品终端id应该为null", groups = {Update.class})
    @NotNull(message = "产品终端id不能为null", groups = {Create.class})
    private Integer productTerminalId;

    @Excel(name = "产品终端名称")
    @Null(message = "不支持的字段", groups = {Create.class, Update.class})
    @Length(min = 0, max = 32, message = "产品终端名称应该少于{max}字符", groups = {Select.class})
    private String productTerminalName;

    @Excel(name = "产品终端序号")
    @Null(message = "不支持的字段", groups = {Create.class, Update.class})
    private Integer productTerminalNumber;

    @Excel(name = "产品终端协议")
    @Null(message = "不支持的字段", groups = {Create.class, Update.class})
    private String productTerminalProtocol;

    @Excel(name = "tb产品id")
    @Null(message = "不支持的字段", groups = {Create.class, Update.class, Select.class})
    private String tbDeviceId;

    @Excel(name = "tb产品名称")
    @Null(message = "不支持的字段", groups = {Create.class, Update.class, Select.class})
    private String tbDeviceName;

    @Excel(name = "所属公司Id")
    private Long companyId;

    @Null(message = "不支持的字段", groups = {Create.class, Update.class, Select.class})
    private Integer mqttCredentialsId;

    @Null(message = "不支持的字段", groups = {Create.class, Update.class, Select.class})
    private Integer httpCredentialsId;

    @Null(message = "不支持的字段", groups = {Create.class, Update.class, Select.class})
    private Integer coapCredentialsId;

    @Null(message = "不支持的字段", groups = {Create.class, Update.class, Select.class})
    private Integer lwm2mCredentialsId;
}
