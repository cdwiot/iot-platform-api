package com.ruoyi.system.domain;

import com.ruoyi.common.core.domain.BaseEntity;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class AlarmReceiver extends BaseEntity {
    private Integer id;
    private Integer ruleId;
    private Integer method;
    private Integer userId;
    private Long companyId;
}
