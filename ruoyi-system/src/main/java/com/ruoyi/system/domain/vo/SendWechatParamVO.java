package com.ruoyi.system.domain.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.Map;

@Data
public class SendWechatParamVO implements Serializable {

    /**
     * 用户openId
     */
    private String openId;

    /**
     * 模板参数
     */
    private Map<String,Object> params;
}
