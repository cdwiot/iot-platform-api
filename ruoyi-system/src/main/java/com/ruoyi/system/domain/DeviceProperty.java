package com.ruoyi.system.domain;

import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.system.domain.ValidationGroups.Create;
import com.ruoyi.system.domain.ValidationGroups.Update;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.io.Serializable;

/**
 * 产品额外属性表
 */
@Data
public class DeviceProperty extends BaseEntity implements Serializable {
    /**
     * 属性Id
     */
    @Null(message = "属性Id应该为null", groups = {Create.class})
    @NotNull(message = "属性Id不能为null", groups = {Update.class})
    private Integer id;

    /**
     * 属性组
     */
    @NotNull(message = "属性名称不能为null", groups = {Update.class,Create.class})
    private String propertyGroup;

    /**
     * 属性名称
     */
    @NotNull(message = "属性名称不能为null", groups = {Update.class,Create.class})
    private String propertyName;
    /**
     * 属性数据类型
     */
    @NotNull(message = "属性名称不能为null", groups = {Update.class,Create.class})
    private String dataType;

    /**
     * 排列顺序
     */
    private Integer orderNum;

    /**
     * 锁定状态（0未锁定（可以修改）1锁定（不能修改））
     */
    private Integer lock;

    /**
     * 所属公司
     */
    private Long companyId;
}
