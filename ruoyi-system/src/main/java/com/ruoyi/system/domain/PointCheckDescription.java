package com.ruoyi.system.domain;

import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

import com.ruoyi.system.domain.ValidationGroups.*;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

/**
 * 点检任务-文字描述
 */
@Data
public class PointCheckDescription extends BaseEntity {
    /**
     * 描述Id
     */
    private Integer id;

    @NotNull(message = "产品检查Id不应该为null", groups = {Update.class, Create.class})
    private Integer detailId;

    /**
     * 自定义字段
     */
    private String field;

    /**
     * 数据类型
     */
    private String dataType;

    /**
     * 值
     */
    private String value;

    /**
     * 下拉选项值
     */
    private String detail;

    /**
     * 排序
     */
    private Integer orderNum;

    /**
     * 所属公司
     */
    private Long companyId;
}
