package com.ruoyi.system.domain.vo;

import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.system.domain.Device;
import lombok.Data;

import java.io.Serializable;

@Data
public class ProjectGroupDeviceVO extends Device implements Serializable {

    /**
     * 产品分组Id
     */
    private Integer deviceGroupId;

}
