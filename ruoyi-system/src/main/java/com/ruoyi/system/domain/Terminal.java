package com.ruoyi.system.domain;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.system.domain.ValidationGroups.Create;
import com.ruoyi.system.domain.ValidationGroups.Select;
import com.ruoyi.system.domain.ValidationGroups.Update;

import org.hibernate.validator.constraints.Length;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class Terminal extends BaseEntity {
    @Null(message = "终端id应该为null", groups = {Create.class, Select.class})
    @NotNull(message = "终端id不能为null", groups = {Update.class})
    private Integer id;

    @Excel(name = "名称")
    @NotBlank(message = "名称不能为空", groups = {Create.class, Update.class})
    @Length(min = 0, max = 32, message = "名称应该少于{max}字符", groups = {Create.class, Update.class, Select.class})
    private String name;

    @Excel(name = "接入协议")
    @Null(message = "接入协议应该为null", groups = {Update.class})
    @NotBlank(message = "接入协议不能为空", groups = {Create.class})
    @Length(min = 0, max = 16, message = "接入协议应该少于{max}字符", groups = {Create.class, Select.class})
    private String protocol;

    @Excel(name = "终端型号")
    @NotNull(message = "终端型号不能为null", groups = {Create.class, Update.class})
    @Length(min = 0, max = 32, message = "终端型号应该少于{max}字符", groups = {Create.class, Update.class, Select.class})
    private String code;

    @Excel(name = "厂商")
    @NotNull(message = "厂商不能为null", groups = {Create.class, Update.class})
    @Length(min = 0, max = 32, message = "厂商应该少于{max}字符", groups = {Create.class, Update.class, Select.class})
    private String manufacturer;

    @Excel(name = "备注")
    @NotNull(message = "备注不能为null", groups = {Create.class, Update.class})
    @Length(min = 0, max = 64, message = "备注应该少于{max}字符", groups = {Create.class, Update.class, Select.class})
    private String remark;

    @Excel(name = "照片")
    @NotNull(message = "照片不能为null", groups = {Create.class, Update.class})
    @Length(min = 0, max = 512, message = "照片应该少于{max}字符", groups = {Create.class, Update.class})
    private String photo;

    @Excel(name = "数据处理")
    @NotNull(message = "数据处理不能为null", groups = {Create.class, Update.class})
    @Length(min = 0, max = 8192, message = "数据处理应该少于{max}字符", groups = {Create.class, Update.class})
    private String convertor;

    @Null(message = "不支持的字段", groups = {Create.class, Update.class, Select.class})
    private Long companyId;

    @Excel(name = "分组名称")
    private String groupName;
}
