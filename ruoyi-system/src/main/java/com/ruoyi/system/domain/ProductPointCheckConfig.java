package com.ruoyi.system.domain;

import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.system.domain.ValidationGroups.*;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

@Data
public class ProductPointCheckConfig extends BaseEntity {
    /**
     * 配置id
     */
    @Null(message = "配置id应该为null", groups = {Create.class})
    @NotNull(message = "配置id不能为null", groups = {Update.class})
    private Integer id;

    /**
     * 项目id
     */
    @NotNull(message = "项目id不能为null", groups = {Create.class,Update.class,Select.class})
    private Integer projectId;

    /**
     * 产品型号id
     */
    @NotNull(message = "产品型号id不能为null", groups = {Create.class,Update.class,Select.class})
    private Integer productId;

    /**
     * 检查类别
     */
    @NotNull(message = "检查类别不能为null", groups = {Create.class,Update.class})
    private String type;

    /**
     * 检查项
     */
    @NotNull(message = "检查项不能为null", groups = {Create.class,Update.class})
    private String item;

    /**
     * 数据类型
     */
    @NotNull(message = "数据类型不能为null", groups = {Create.class,Update.class})
    private String dataType;

    /**
     * 排列序号
     */
    private Integer orderNum;

    /**
     * 所属公司
     */
    private Long companyId;
}
