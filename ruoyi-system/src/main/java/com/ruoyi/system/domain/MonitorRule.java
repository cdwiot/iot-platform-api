package com.ruoyi.system.domain;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.system.domain.ValidationGroups.*;

import org.hibernate.validator.constraints.*;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class MonitorRule extends BaseEntity {
    @Null(message = "监控规则id应该为null", groups = {Create.class, Select.class})
    @NotNull(message = "监控规则id不能为null", groups = {Update.class})
    private Integer id;

    @NotBlank(message = "名称不能为空", groups = {Create.class})
    @Length(min = 0, max = 32, message = "名称应该少于{max}字符", groups = {Create.class, Update.class, Select.class})
    private String name;

    @NotNull(message = "描述不能为null", groups = {Create.class})
    @Length(min = 0, max = 512, message = "描述应该少于{max}字符", groups = {Create.class, Update.class, Select.class})
    private String description;

    @Null(message = "规则类型应该为null", groups = {Update.class})
    @NotNull(message = "规则类型不能为null", groups = {Create.class})
    @Range(min = 0, max = 1, message = "规则类型应该在{min}-{max}之间", groups = {Create.class, Select.class})
    private Integer type;

    @Null(message = "触发逻辑应该为null", groups = {Create.class, Select.class})
    @Range(min = 0, max = 1, message = "触发逻辑应该在{min}-{max}之间", groups = {Update.class})
    private Integer triggerLogic;
    @Null(message = "不支持的字段", groups = {Create.class, Update.class, Select.class})
    private String triggerLogicString;

    @Null(message = "不支持的字段", groups = {Create.class, Update.class, Select.class})
    private List<MonitorCondition> conditions;

    @Null(message = "不支持的字段", groups = {Create.class, Update.class, Select.class})
    private Long companyId;
}
