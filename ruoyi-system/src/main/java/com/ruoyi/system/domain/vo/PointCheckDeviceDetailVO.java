package com.ruoyi.system.domain.vo;

import com.ruoyi.system.domain.PointCheck;
import com.ruoyi.system.domain.PointCheckDeviceDetail;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 产品检查项VO
 */
@Data
public class PointCheckDeviceDetailVO implements Serializable {

    /**
     * 正常数量
     */
    private Integer normalNum;

    /**
     * 异常数量
     */
    private Integer abnormalNum;

    /**
     * 待检测数量
     */
    private Integer needCheckNum;

    /**
     * 产品检查项
     */
    private List<PointCheckDeviceDetail> pointCheckDeviceDetails;


}
