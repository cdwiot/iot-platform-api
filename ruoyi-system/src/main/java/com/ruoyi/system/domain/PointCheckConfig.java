package com.ruoyi.system.domain;

import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.system.domain.ValidationGroups.*;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
/**
 * 计划-产品-点检配置关联表
 */
@Data
public class PointCheckConfig extends BaseEntity {
    /**
     * 计划-产品-点检配置关联Id
     */
    @Null(message = "关联Id应该为null", groups = {Create.class})
    @NotNull(message = "关联Id不应该为null", groups = {Update.class})
    private Integer id;

    /**
     * 计划id
     */
    @NotNull(message = "计划id不应该为null", groups = {Create.class,Update.class})
    private Integer pointId;

    /**
     * 项目id
     */
    @NotNull(message = "项目id不应该为null", groups = {Create.class,Update.class})
    private Integer projectId;

    /**
     * 产品id
     */
    @NotNull(message = "产品id不应该为null", groups = {Create.class,Update.class})
    private Integer deviceId;

    /**
     * 点检配置id
     */
    @NotNull(message = "点检配置id不应该为null", groups = {Create.class,Update.class})
    private Integer checkConfigId;

    /**
     * 所属公司
     */
    private Long companyId;
}
