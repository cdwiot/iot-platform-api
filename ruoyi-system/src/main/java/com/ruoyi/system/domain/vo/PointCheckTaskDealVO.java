package com.ruoyi.system.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.system.domain.PointCheckTaskDeal;
import lombok.Data;

import java.util.Date;

@Data
public class PointCheckTaskDealVO extends PointCheckTaskDeal {

    /**
     * 检查类别
     */
    private String detailType;

    /**
     * 检查项
     */
    private String item;

    /**
     * 图片地址
     */
    private String file;


    /**
     * 任务开始时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date startTime;

    /**
     * 任务结束时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endTime;

    /**
     * 执行人
     */
    private String nickName;

    /**
     * 数据类型（图片、视频、文本）
     */
    private String dataType;
}
