package com.ruoyi.system.domain;

import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.system.domain.ValidationGroups.*;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

@Data
public class ProjectAuthorize extends BaseEntity {
    /**
     * 授权Id
     */
    @Null(message = "授权Id应该为null", groups = {Create.class})
    @NotNull(message = "授权Id不应该为null", groups = {Update.class})
    private Integer id;

    /**
     * 项目Id
     */
    @NotNull(message = "项目Id不能为null", groups = {Update.class, Create.class, Select.class,})
    private Integer projectId;

    /**
     * 用户Id
     */
    @NotNull(message = "用户Id不能为null", groups = {Update.class, Create.class})
    private Integer userId;

    /**
     * 项目所属者0不是1是
     */
    private Integer isOwner;

    /**
     * 用户角色Id
     */
    private Integer roleId;

    /**
     * 用户账号
     */
    private String userName;

    /**
     * 用户昵称
     */
    private String nickName;

    /**
     * 用户手机号
     */
    private String phonenumber;

    /**
     * 用户邮箱
     */
    private String email;

    /**
     * 用户openId
     */
    private String openId;

    /**
     * 所属公司
     */
    private Long companyId;

    /**
     * 部门id
     */
    private Integer deptId;
}
