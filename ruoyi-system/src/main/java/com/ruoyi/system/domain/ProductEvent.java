package com.ruoyi.system.domain;

import cn.hutool.json.JSONArray;
import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.system.domain.ValidationGroups.*;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.util.List;
import java.util.Map;

@Data
public class ProductEvent extends BaseEntity {

    /**
     * 事件id
     */
    @Null(message = "事件Id应该为null", groups = {Create.class, Select.class})
    @NotNull(message = "事件Id不能为null", groups = {Update.class})
    private Integer id;

    /**
     * 产品Id
     */
    @NotNull(message = "产品Id不能为null", groups = {Select.class,Create.class,Update.class})
    private Integer productId;

    /**
     * 产品名称
     */
    private String productName;

    /**
     * 终端序号
     */
    @NotNull(message = "终端序号不能为null", groups = {Create.class,Update.class})
    private Integer terminalNumber;

    /**
     * 终端名称
     */
    private String terminalName;

    /**
     * 事件名称
     */
    @NotNull(message = "事件名称不能为null", groups = {Create.class,Update.class})
    private String name;

    /**
     * 终端侧标识符
     */
    @NotNull(message = "终端侧标识符不能为null", groups = {Create.class,Update.class})
    private String terminalIdentifier;

    /**
     * 用户侧标识符
     */
    @NotNull(message = "用户侧标识符不能为null", groups = {Create.class,Update.class})
    private String userIdentifier;
    /**
     * 输出参数
     */
    @NotNull(message = "输出参数不能为null", groups = {Create.class,Update.class})
    private JSONArray paramList;

    /**
     * 输出参数
     */
    private String param;
    /**
     * 描述
     */
    private String description;

    /**
     * 所属公司
     */
    private Long companyId;

}
