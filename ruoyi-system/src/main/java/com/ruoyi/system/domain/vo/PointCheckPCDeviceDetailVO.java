package com.ruoyi.system.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.system.domain.PointCheckDeviceDetail;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Data
public class PointCheckPCDeviceDetailVO implements Serializable {

    /**
     * 任务开始时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date startTime;

    /**
     * 任务结束时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endTime;

    /**
     * 执行人
     */
    private String executor;

    /**
     * 执行人昵称
     */
    private String nickName;

    /**
     * 任务状态
     */
    private String status;

    /**
     * 产品检查项
     */
    private Map<String, List<PointCheckDeviceDetail>> checkItemMap;

}
