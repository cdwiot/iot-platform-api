package com.ruoyi.system.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.system.domain.ValidationGroups.*;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.util.Date;

/**
 * 邮件发送明细
 */
@Data
public class EmailDetail extends BaseEntity {
    /**
     * 邮件明细Id
     */
    @Null(message = "邮件明细Id应该为null", groups = {Create.class})
    @NotNull(message = "邮件明细Id不能为null", groups = {Update.class})
    private Integer id;

    /**
     * 发送编号
     */
    @NotNull(message = "发送编号不能为null", groups = {Create.class,Update.class})
    private String tid;

    /**
     * 通知类别
     */
    @NotNull(message = "通知类别不能为null", groups = {Create.class,Update.class})
    private String type;

    /**
     * 发件人
     */
    @NotNull(message = "发件人不能为null", groups = {Create.class,Update.class})
    private String from;

    /**
     * 收件人
     */
    @NotNull(message = "收件人不能为null", groups = {Create.class,Update.class})
    private String to;

    /**
     * 模板文件
     */
    @NotNull(message = "模板文件不能为null", groups = {Create.class,Update.class})
    private String html;

    /**
     * 主题
     */
    @NotNull(message = "主题不能为null", groups = {Create.class,Update.class})
    private String subject;

    /**
     * 模板名称
     */
    @NotNull(message = "模板名称不能为null", groups = {Create.class,Update.class})
    private String template;

    /**
     * 模板内容
     */
    @NotNull(message = "模板内容不能为null", groups = {Create.class,Update.class})
    private String parameters;

    /**
     * 创建时间开始
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date startCreateTime;

    /**
     * 创建时间结束
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endCreateTime;

    /**
     * 附件
     */
    private String attachments;

    /**
     * 发送状态0失败1成功
     */
    private Integer flag;

    /**
     * 所属公司
     */
    private Long companyId;

    /**
     * 发送状态0进行中1成功2失败
     */
    private String status;
}
