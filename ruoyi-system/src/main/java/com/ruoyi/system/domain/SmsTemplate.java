package com.ruoyi.system.domain;

import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.system.domain.ValidationGroups.*;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

@Data
public class SmsTemplate extends BaseEntity {
    /**
     * 模板Id
     */
    @Null(message = "模板Id应该为null", groups = {Create.class})
    @NotNull(message = "模板Id应该为null", groups = {Update.class})
    private Integer id;

    /**
     * 模板名称
     */
    @NotNull(message = "模板名称不能为null", groups = {Update.class, Create.class})
    @Length(min = 0, max = 50, message = "模板名称应该少于{max}字符", groups = {Update.class, Create.class})
    private String name;

    /**
     * 模板code
     */
    @NotNull(message = "模板code不能为null", groups = {Update.class, Create.class})
    private String code;

    /**
     * 模板内容
     */
    @NotNull(message = "模板内容不能为null", groups = {Update.class, Create.class})
    @Length(min = 0, max = 500, message = "模板内容应该少于{max}字符", groups = {Update.class, Create.class})
    private String content;

//    /**
//     * 模板参数
//     */
//    @NotNull(message = "模板参数不能为null", groups = {Update.class, Create.class})
    private String param;

    /**
     * 所属公司
     */
    private Long companyId;
}
