package com.ruoyi.system.domain;

import java.net.URL;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.system.domain.ValidationGroups.*;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class ProjectScenarioAction extends BaseEntity {
    @Null(message = "id应该为null", groups = {Create.class, Select.class})
    @NotNull(message = "id不能为null", groups = {Update.class})
    private Integer id;

    @Null(message = "场景联动规则id应该为null", groups = {Update.class})
    @NotNull(message = "场景联动规则id不能为null", groups = {Create.class, Select.class})
    private Integer ruleId;
    @Null(message = "不支持的字段", groups = {Create.class, Update.class})
    private String ruleName;

    @NotNull(message = "动作类型不能为null", groups = {Create.class, Update.class})
    @Range(min = 0, max = 0, message = "动作类型应该在{min}-{max}之间", groups = {Create.class, Update.class})
    private Integer type;
    @Null(message = "不支持的字段", groups = {Create.class, Update.class})
    private String typeString;

    private String sfCode;
    // @AssertTrue(message = "无效的定制功能", groups = {Create.class, Update.class})
    // private boolean isSfCodeValid() {
    //     boolean valid = false;
    //     if (type == 0) {
    //         try {
    //             new URL(apiUrl);
    //             valid = true;
    //         } catch (Exception e) {}
    //     } else {
    //         valid = (apiUrl.isEmpty());
    //     }
    //     return valid;
    // }

    private String sfParameters;
    // @AssertTrue(message = "无效的api请求参数", groups = {Create.class, Update.class})
    // public boolean isSfParametersValid() {
    //     boolean valid = false;
    //     if (type == 0) {
    //         try {
    //             new URL(apiUrl);
    //             valid = true;
    //         } catch (Exception e) {}
    //     } else {
    //         valid = (apiUrl.isEmpty());
    //     }
    //     return valid;
    // }

    @Length(min = 0, max = 32, message = "安全令牌应该少于{max}字符", groups = {Create.class, Update.class})
    private String sfToken;
    // public boolean isSfTokenValid() {
    //     boolean valid = false;
    //     if (type == 0) {
    //     }
    //     return valid;
    // }

    private String rpcMethod;
    private String rpcParameters;

    @Null(message = "不支持的字段", groups = {Create.class, Update.class, Select.class})
    private Long companyId;
}
