package com.ruoyi.system.domain.vo;

import com.ruoyi.system.domain.ProductPointCheckConfig;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class ProductPointCheckConfigVO implements Serializable {

    /**
     * 检查项
     */
    private String type;

    /**
     * 产品型号点检配置集合
     */
    private List<ProductPointCheckConfig> pointCheckConfigList;
}
