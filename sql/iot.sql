drop table if exists tbl_project;
create table tbl_project (
  id INT NOT NULL AUTO_INCREMENT,
  code VARCHAR(32) NOT NULL COMMENT '项目编号',
  name VARCHAR(32) NOT NULL COMMENT '项目名称',
  address VARCHAR(32) NOT NULL COMMENT '项目地址',
  is_on TINYINT NOT NULL DEFAULT 1 COMMENT '是否启用1是0否',
  remark VARCHAR(64) NOT NULL DEFAULT '' COMMENT '备注',
  primary key (id),
  UNIQUE KEY `name` (`name`)
) CHARSET=utf8 comment = '项目表';

drop table if exists tbl_product;
create table tbl_product (
  id INT NOT NULL AUTO_INCREMENT,
  name VARCHAR(32) NOT NULL COMMENT '产品名称',
  code VARCHAR(32) NOT NULL COMMENT '产品型号',
  manufacturer VARCHAR(32) NOT NULL DEFAULT '' COMMENT '厂商',
  description VARCHAR(64) NOT NULL DEFAULT '' COMMENT '描述',
  primary key (id),
  UNIQUE KEY `name` (`name`)
) CHARSET=utf8 comment = '产品表';

drop table if exists tbl_product_terminal;
create table tbl_product_terminal (
  product_id INT NOT NULL COMMENT '产品id',
  number VARCHAR(64) NOT NULL COMMENT '序号',
  name VARCHAR(32) NOT NULL COMMENT '名称',
  protocol VARCHAR(100) NOT NULL COMMENT '接入协议',
  code VARCHAR(32) DEFAULT '' COMMENT '终端型号',
  manufacturer VARCHAR(32) DEFAULT '' COMMENT '厂商',
  remark VARCHAR(64) DEFAULT '' COMMENT '备注'
) CHARSET=utf8 comment = '产品终端';

drop table if exists tbl_product_info;
create table tbl_product_info (
  id INT NOT NULL AUTO_INCREMENT,
  product_id INT NOT NULL UNIQUE COMMENT '产品id',
  image VARCHAR(256) NOT NULL DEFAULT '' COMMENT '图片',
  product_video VARCHAR(256) NOT NULL DEFAULT '' COMMENT '产品视频url',
  instructions VARCHAR(256) NOT NULL DEFAULT '' COMMENT '说明书url',
  guide_book VARCHAR(256) NOT NULL DEFAULT '' COMMENT '指导手册url',
  guide_video VARCHAR(256) NOT NULL DEFAULT '' COMMENT '指导视频url',
  attachments VARCHAR(256) NOT NULL DEFAULT '' COMMENT '附件url',
  primary key (id)
) CHARSET=utf8 comment = '产品基本信息表';

drop table if exists tbl_product_nameplate;
create table tbl_product_nameplate (
  product_id INT NOT NULL COMMENT '产品id',
  param VARCHAR(32) NOT NULL COMMENT '参数',
  value VARCHAR(32) NOT NULL COMMENT '值',
  unit VARCHAR(32) NOT NULL COMMENT '单位'
) CHARSET=utf8 comment = '产品铭牌';

drop table if exists tbl_product_model;
create table tbl_product_model (
  id INT NOT NULL AUTO_INCREMENT,
  product_id INT NOT NULL COMMENT '产品id',
  terminal_number VARCHAR(64) NOT NULL DEFAULT '' COMMENT '序号',
  name VARCHAR(32) NOT NULL COMMENT '功能名称',
  identifier VARCHAR(256) NOT NULL DEFAULT '' COMMENT '标识符',
  user_identifier VARCHAR(256) NOT NULL COMMENT '用户侧标识符',
  is_computed TINYINT(1) NOT NULL DEFAULT 0 COMMENT '是否为计算属性',
  formula VARCHAR(256) NOT NULL DEFAULT '' COMMENT '计算公式',
  formula_img TEXT COMMENT '标识符图片',
  formula_raw VARCHAR(256) DEFAULT '' COMMENT '原始标识符(计算属性的latex)',
  max VARCHAR(128) COMMENT '最大值',
  min VARCHAR(128) COMMENT '最小值',
  step VARCHAR(128) COMMENT '步长',
  data_type VARCHAR(32) NOT NULL COMMENT '数据类型',
  unit VARCHAR(32) NOT NULL COMMENT '单位',
  primary key (id),
  UNIQUE KEY `product_user_identifier` (`product_id`, `user_identifier`)
) CHARSET=utf8 comment = '产品物模型表';

drop table if exists tbl_product_view_config;
create table tbl_product_view_config (
  id INT NOT NULL AUTO_INCREMENT,
  product_id INT NOT NULL COMMENT '产品id',
  name VARCHAR(32) NOT NULL COMMENT '视图名称',
  view_type VARCHAR(32) NOT NULL COMMENT '视图类型',
  aggregation VARCHAR(32) NOT NULL COMMENT '聚合函数',
  product_model_ids VARCHAR(1024) NOT NULL COMMENT '点位',
  description VARCHAR(128) NOT NULL DEFAULT '' COMMENT '描述',
  primary key (id)
) CHARSET=utf8 comment = '视图配置表';

drop table if exists tbl_product_view_config_setting;
create table tbl_product_view_config_setting (
  project_id INT NOT NULL COMMENT '项目id',
  product_id INT NOT NULL COMMENT '产品id',
  disabled_view_config_ids VARCHAR(1024) NOT NULL COMMENT '不显示产品视图id',
  UNIQUE key `project_product` (project_id, product_id)
) CHARSET=utf8 comment = '项目视图配置禁用表';


------------------------------------------------
drop table if exists tbl_device;
create table tbl_device (
  id INT NOT NULL AUTO_INCREMENT,
  project_id INT COMMENT '项目id',
  product_id INT NOT NULL COMMENT '产品id',
  name VARCHAR(32) NOT NULL COMMENT '产品名称',
  sn VARCHAR(32) NOT NULL COMMENT '序列号',
  description VARCHAR(128) NOT NULL DEFAULT '' COMMENT '描述',
  address VARCHAR(64) COMMENT '地址',
  lng DECIMAL(10, 6) COMMENT '经度',
  lat DECIMAL(10, 6) COMMENT '纬度',
  primary key (id),
  UNIQUE KEY `name` (`name`)
) CHARSET=utf8 comment = '产品表';

drop table if exists tbl_device_terminal;
create table tbl_device_terminal (
  id INT NOT NULL AUTO_INCREMENT,
  device_id INT NOT NULL COMMENT '产品id',
  sn VARCHAR(32) NOT NULL COMMENT '产品终端序列号',
  product_terminal_number VARCHAR(64) NOT NULL COMMENT '产品终端序号序号',
  tb_device_id VARCHAR(64) NOT NULL COMMENT 'tb产品id',
  primary key (id)
) CHARSET=utf8 comment = '产品终端表';

drop table if exists tbl_device_terminal_mqtt;
create table tbl_device_terminal_mqtt (
  device_terminal_id INT NOT NULL UNIQUE COMMENT '产品终端id',
  device_id INT NOT NULL COMMENT '产品id',
  client_id VARCHAR(32) NOT NULL UNIQUE COMMENT 'clientId',
  primary key (device_terminal_id)
) CHARSET=utf8 comment = '产品MQTT协议ClientId';