alter table tbl_product add (
company_id bigint(20) comment '所属公司id，',
create_by varchar(64) default '' comment '创建者',
create_time datetime comment '创建时间'
);

alter table tbl_product_info add (
company_id bigint(20) comment '所属公司id，',
create_by varchar(64) default '' comment '创建者',
create_time datetime comment '创建时间'
);

alter table tbl_product_model add (
company_id bigint(20) comment '所属公司id，',
create_by varchar(64) default '' comment '创建者',
create_time datetime comment '创建时间'
);

alter table tbl_product_nameplate add (
company_id bigint(20) comment '所属公司id，',
create_by varchar(64) default '' comment '创建者',
create_time datetime comment '创建时间'
);

alter table tbl_product_terminal add (
company_id bigint(20) comment '所属公司id，',
create_by varchar(64) default '' comment '创建者',
create_time datetime comment '创建时间'
);

alter table tbl_product_view_config add (
company_id bigint(20) comment '所属公司id，',
create_by varchar(64) default '' comment '创建者',
create_time datetime comment '创建时间'
);

alter table tbl_product_view_config_setting add (
company_id bigint(20) comment '所属公司id，',
create_by varchar(64) default '' comment '创建者',
create_time datetime comment '创建时间'
);

alter table tbl_project add (
company_id bigint(20) comment '所属公司id，',
create_by varchar(64) default '' comment '创建者',
create_time datetime comment '创建时间'
);

alter table tbl_device add (
company_id bigint(20) comment '所属公司id，',
create_by varchar(64) default '' comment '创建者',
create_time datetime comment '创建时间'
);

alter table tbl_device_terminal add (
company_id bigint(20) comment '所属公司id，',
create_by varchar(64) default '' comment '创建者',
create_time datetime comment '创建时间'
);

-- 2021-08-10
alter table tbl_product_model modify identifier VARCHAR(256) NOT NULL DEFAULT '' COMMENT '标识符';
alter table tbl_product_model modify terminal_number VARCHAR(64) NOT NULL DEFAULT '' COMMENT '序号';
alter table tbl_product_model add (
    formula VARCHAR(256) NOT NULL DEFAULT '' COMMENT '计算公式',
    formula_img TEXT COMMENT '标识符图片',
    formula_raw VARCHAR(256) DEFAULT '' COMMENT '原始标识符(计算属性的latex)'
);
update tbl_product_model set `formula`=`identifier` where is_computed=1;
update tbl_product_model set `identifier`='' where is_computed=1;
update tbl_product_model set `terminal_number`='' where is_computed=1;
update tbl_product_model set `formula_img`=`identifier_img`;
update tbl_product_model set `formula_raw`=`raw_identifier`;
alter table tbl_product_model drop identifier_img, drop raw_identifier;