package com.ruoyi.common.enums;

import java.util.HashMap;
import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Getter;

@SuppressWarnings("ALL")
@Getter
@AllArgsConstructor
public enum TriggerSource
{
    TELEMETRY(0, "数据"),
    EVENT(1, "事件"),
    CONNECTIVITY(2, "在线状态");

    private static final Map<Integer, TriggerSource> BY_CODE = new HashMap<>();
    private static final Map<String, TriggerSource> BY_LABEL = new HashMap<>();
    static {
        for (TriggerSource co : values()) {
            BY_CODE.put(co.code, co);
            BY_LABEL.put(co.label, co);
        }
    }

    private Integer code;
    private String label;

    public static TriggerSource valueOfCode(Integer code) {
        return BY_CODE.get(code);
    }

    public static TriggerSource valueOfLabel(String label) {
        return BY_LABEL.get(label);
    }
}
