package com.ruoyi.common.enums;

import java.util.HashMap;
import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Getter;

@SuppressWarnings("ALL")
@Getter
@AllArgsConstructor
public enum TriggerType
{
    SIMPLE(0, "直接触发"),
    DURATION(1, "持续时间"),
    REPEAT(2, "重复次数");

    private static final Map<Integer, TriggerType> BY_CODE = new HashMap<>();
    private static final Map<String, TriggerType> BY_LABEL = new HashMap<>();
    static {
        for (TriggerType co : values()) {
            BY_CODE.put(co.code, co);
            BY_LABEL.put(co.label, co);
        }
    }

    private Integer code;
    private String label;

    public static TriggerType valueOfCode(Integer code) {
        return BY_CODE.get(code);
    }

    public static TriggerType valueOfLabel(String label) {
        return BY_LABEL.get(label);
    }
}
