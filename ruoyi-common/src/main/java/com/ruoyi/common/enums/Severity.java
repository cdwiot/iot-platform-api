package com.ruoyi.common.enums;

import java.util.HashMap;
import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Getter;

@SuppressWarnings("ALL")
@Getter
@AllArgsConstructor
public enum Severity
{
    EMERGENT(0, "紧急"),
    IMPORTANT(1, "重要"),
    MINOR(2, "次要"),
    NOTIFY(3, "提示");

    private static final Map<Integer, Severity> BY_CODE = new HashMap<>();
    private static final Map<String, Severity> BY_LABEL = new HashMap<>();
    static {
        for (Severity co : values()) {
            BY_CODE.put(co.code, co);
            BY_LABEL.put(co.label, co);
        }
    }

    private Integer code;
    private String label;

    public static Severity valueOfCode(Integer code) {
        return BY_CODE.get(code);
    }

    public static Severity valueOfLabel(String label) {
        return BY_LABEL.get(label);
    }
}
