package com.ruoyi.common.enums;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.thingsboard.server.common.data.kv.Aggregation;

import lombok.AllArgsConstructor;
import lombok.Getter;

@SuppressWarnings("ALL")
@Getter
@AllArgsConstructor
public enum AggregationMethod
{
    NONE("NONE", "无", Aggregation.NONE), 
    AVG("AVG", "平均值", Aggregation.AVG), 
    MAX("MAX", "最大值", Aggregation.MAX),
    MIN("MIN", "最小值", Aggregation.MIN),
    SUM("SUM", "求和", Aggregation.SUM),
    COUNT("COUNT", "计数", Aggregation.COUNT);

    private final String code;
    private final String label;
    private final Aggregation aggregation;

    private static final Map<String, AggregationMethod> BY_CODE = new HashMap<>();
    private static final Map<String, AggregationMethod> BY_LABEL = new HashMap<>();
    private static final List<String> LABELS = new ArrayList<>();
    static {
        for (AggregationMethod co : values()) {
            BY_CODE.put(co.code, co);
            BY_LABEL.put(co.label, co);
            LABELS.add(co.label);
        }
    }

    public static AggregationMethod valueOfCode(String code) {
        return BY_CODE.get(code);
    }

    public static AggregationMethod valueOfLabel(String label) {
        return BY_LABEL.get(label);
    }

    public static List<String> labels() {
        return LABELS;
    }

    public static List<Map<String, Object>> toList() {
        List<Map<String, Object>> list = new ArrayList<>();
        for (AggregationMethod item : AggregationMethod.values()) {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("code", item.getCode());
            map.put("name", item.getLabel());
            list.add(map);
        }
        return list;
    }
}

