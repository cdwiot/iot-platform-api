package com.ruoyi.common.enums;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Getter;

@SuppressWarnings("ALL")
@Getter
@AllArgsConstructor
public enum MonitorRuleType
{
    PRODUCT(0, "产品规则"),
    DEVICE(1, "产品规则");

    private static final Map<Integer, MonitorRuleType> BY_CODE = new HashMap<>();
    private static final Map<String, MonitorRuleType> BY_LABEL = new HashMap<>();
    private static final List<String> LABELS = new ArrayList<>();
    static {
        for (MonitorRuleType co : values()) {
            BY_CODE.put(co.code, co);
            BY_LABEL.put(co.label, co);
            LABELS.add(co.label);
        }
    }

    private Integer code;
    private String label;

    public static MonitorRuleType valueOfCode(Integer code) {
        return BY_CODE.get(code);
    }

    public static MonitorRuleType valueOfLabel(String label) {
        return BY_LABEL.get(label);
    }

    public static List<String> labels() {
        return LABELS;
    }

    public static List<Map<String, Object>> toList() {
        List<Map<String, Object>> list = new ArrayList<>();
        for (MonitorRuleType item : MonitorRuleType.values()) {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("code", item.getCode());
            map.put("name", item.getLabel());
            list.add(map);
        }
        return list;
    }
}
