package com.ruoyi.common.enums;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Getter;

@SuppressWarnings("ALL")
@Getter
@AllArgsConstructor
public enum ActionType
{
    SF(0, "SF", "平台定制功能"),
    API(1, "API", "调用API"),
    RPC(2, "RPC", "产品指令");

    private static final Map<Integer, ActionType> BY_CODE = new HashMap<>();
    private static final Map<String, ActionType> BY_LABEL = new HashMap<>();
    private static final List<String> LABELS = new ArrayList<>();
    static {
        for (ActionType co : values()) {
            BY_CODE.put(co.code, co);
            BY_LABEL.put(co.label, co);
            LABELS.add(co.label);
        }
    }

    private Integer code;
    private String label;
    private String description;

    public static ActionType valueOfCode(Integer code) {
        return BY_CODE.get(code);
    }

    public static ActionType valueOfLabel(String label) {
        return BY_LABEL.get(label);
    }

    public static List<String> labels() {
        return LABELS;
    }

    public static List<Map<String, Object>> toList() {
        List<Map<String, Object>> list = new ArrayList<>();
        for (ActionType item : ActionType.values()) {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("code", item.getCode());
            map.put("name", item.getLabel());
            list.add(map);
        }
        return list;
    }
}
