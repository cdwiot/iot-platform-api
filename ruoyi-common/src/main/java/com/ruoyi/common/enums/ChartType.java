package com.ruoyi.common.enums;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Getter;

@SuppressWarnings("ALL")
@Getter
@AllArgsConstructor
public enum ChartType
{
    LINE(1, "LINE", "折线图"),
    BAR(2, "BAR", "柱状图"),
    STACKED_LINE(3, "STACKED_LINE", "堆叠折线图"),
    STACKED_BAR(4, "STACKED_BAR", "堆叠柱状图");

    private static final Map<Integer, ChartType> BY_CODE = new HashMap<>();
    private static final Map<String, ChartType> BY_LABEL = new HashMap<>();
    private static final List<String> LABELS = new ArrayList<>();
    static {
        for (ChartType co : values()) {
            BY_CODE.put(co.code, co);
            BY_LABEL.put(co.label, co);
            LABELS.add(co.label);
        }
    }

    private Integer code;
    private String label;
    private String description;

    public static ChartType valueOfCode(Integer code) {
        return BY_CODE.get(code);
    }

    public static ChartType valueOfLabel(String label) {
        return BY_LABEL.get(label);
    }

    public static List<String> labels() {
        return LABELS;
    }

    public static List<Map<String, Object>> toList() {
        List<Map<String, Object>> list = new ArrayList<>();
        for (ChartType item : ChartType.values()) {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("code", item.getCode());
            map.put("name", item.getLabel());
            list.add(map);
        }
        return list;
    }
}
