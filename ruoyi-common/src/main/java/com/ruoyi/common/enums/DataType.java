package com.ruoyi.common.enums;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Getter;

@SuppressWarnings("ALL")
@Getter
@AllArgsConstructor
public enum DataType
{
    INT32(0, "INT32", "整型"),
    INT64(1, "INT64", "长整型"),
    FLOAT(2, "FLOAT", "单精度浮点型"),
    DOUBLE(3, "DOUBLE", "双精度浮点型"),
    BOOL(4, "BOOL", "布尔型"),
    STRING(5, "STRING", "字符串");

    private static final Map<Integer, DataType> BY_CODE = new HashMap<>();
    private static final Map<String, DataType> BY_LABEL = new HashMap<>();
    private static final List<String> LABELS = new ArrayList<>();
    static {
        for (DataType co : values()) {
            BY_CODE.put(co.code, co);
            BY_LABEL.put(co.label, co);
            LABELS.add(co.label);
        }
    }

    private Integer code;
    private String label;
    private String description;

    public static DataType valueOfCode(Integer code) {
        return BY_CODE.get(code);
    }

    public static DataType valueOfLabel(String label) {
        return BY_LABEL.get(label);
    }

    public static List<String> labels() {
        return LABELS;
    }
}
