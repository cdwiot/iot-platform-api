package com.ruoyi.common.enums;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Getter;

@SuppressWarnings("ALL")
@Getter
@AllArgsConstructor
public enum TimeGranularity {
    ONEMIN("1m", "1分钟", 60000L),
    TWOMIN("2m", "2分钟", 120000L),  
    FIVEMIN("5m", "5分钟", 300000L), 
    TENMIN("10m", "10分钟", 600000L), 
    FIFTEENMIN("15m", "15分钟", 900000L),
    THIRTYMIN("30m", "30分钟", 1800000L),
    ONEHOUR("1h", "1小时", 3600000L),
    TWOHOUR("2h", "2小时", 7200000L),
    FOURHOUR("4h", "4小时", 14400000L),
    EIGHTHOUR("8h", "8小时", 28800000L),
    TWELVEHOUR("12h", "12小时", 43200000L),
    ONEDAY("1d", "1天", 86400000L);

    private final String code;
    private final String label;
    private final Long interval;

    private static final Map<String, TimeGranularity> BY_CODE = new HashMap<>();
    private static final Map<String, TimeGranularity> BY_LABEL = new HashMap<>();
    private static final List<String> LABELS = new ArrayList<>();
    static {
        for (TimeGranularity co : values()) {
            BY_CODE.put(co.code, co);
            BY_LABEL.put(co.label, co);
            LABELS.add(co.label);
        }
    }

    public static TimeGranularity valueOfCode(String code) {
        return BY_CODE.get(code);
    }

    public static TimeGranularity valueOfLabel(String label) {
        return BY_LABEL.get(label);
    }

    public static List<String> labels() {
        return LABELS;
    }

    public static List<Map<String, Object>> toList() {
        List<Map<String, Object>> list = new ArrayList<>();
        for (TimeGranularity item : TimeGranularity.values()) {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("code", item.getCode());
            map.put("name", item.getLabel());
            list.add(map);
        }
        return list;
    }

    public static Long getSuitable(Long start, Long end) {
        Long found = TimeGranularity.ONEDAY.getInterval();
        Long duration = end - start;
        for (TimeGranularity item : TimeGranularity.values()) {
            if (duration / item.getInterval() <= 100) {
                found = item.getInterval();
                break;
            }
        }
        return found;
    }
}