package com.ruoyi.common.enums;

import java.util.HashMap;
import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Getter;

@SuppressWarnings("ALL")
@Getter
@AllArgsConstructor
public enum TriggerLogic
{
    AND(0, "同时满足所有条件"),
    OR(1, "满足任意一个条件");

    private static final Map<Integer, TriggerLogic> BY_CODE = new HashMap<>();
    private static final Map<String, TriggerLogic> BY_LABEL = new HashMap<>();
    static {
        for (TriggerLogic co : values()) {
            BY_CODE.put(co.code, co);
            BY_LABEL.put(co.label, co);
        }
    }

    private Integer code;
    private String label;

    public static TriggerLogic valueOfCode(Integer code) {
        return BY_CODE.get(code);
    }

    public static TriggerLogic valueOfLabel(String label) {
        return BY_LABEL.get(label);
    }
}
