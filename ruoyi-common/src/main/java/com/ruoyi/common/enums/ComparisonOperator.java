package com.ruoyi.common.enums;

import java.util.HashMap;
import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Getter;

@SuppressWarnings("ALL")
@Getter
@AllArgsConstructor
public enum ComparisonOperator
{
    LESS_THAN(0, "<"),
    LESS_EQUAL(1, "<="),
    EQUAL(2, "="),
    NOT_EQUAL(3, "!="),
    GREATER_EQUAL(4, ">="),
    GREARER_THAN(5, ">");

    private static final Map<Integer, ComparisonOperator> BY_CODE = new HashMap<>();
    private static final Map<String, ComparisonOperator> BY_LABEL = new HashMap<>();
    static {
        for (ComparisonOperator co : values()) {
            BY_CODE.put(co.code, co);
            BY_LABEL.put(co.label, co);
        }
    }

    private Integer code;
    private String label;

    public static ComparisonOperator valueOfCode(Integer code) {
        return BY_CODE.get(code);
    }

    public static ComparisonOperator valueOfLabel(String label) {
        return BY_LABEL.get(label);
    }
}
