package com.ruoyi.common.enums;

import java.util.HashMap;
import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Getter;

@SuppressWarnings("ALL")
@Getter
@AllArgsConstructor
public enum NotificationMethod
{
    SMS(0, "短信"),
    EMAIL(1, "邮件"),
    VOICE(2, "语音"),
    WECHAT(3, "微信");

    private static final Map<Integer, NotificationMethod> BY_CODE = new HashMap<>();
    private static final Map<String, NotificationMethod> BY_LABEL = new HashMap<>();
    static {
        for (NotificationMethod co : values()) {
            BY_CODE.put(co.code, co);
            BY_LABEL.put(co.label, co);
        }
    }

    private Integer code;
    private String label;

    public static NotificationMethod valueOfCode(Integer code) {
        return BY_CODE.get(code);
    }

    public static NotificationMethod valueOfLabel(String label) {
        return BY_LABEL.get(label);
    }
}
