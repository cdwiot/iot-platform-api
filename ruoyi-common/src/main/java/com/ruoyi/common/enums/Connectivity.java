package com.ruoyi.common.enums;

import java.util.HashMap;
import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Getter;

@SuppressWarnings("ALL")
@Getter
@AllArgsConstructor
public enum Connectivity
{
    ONLINE(0, "在线"),
    OFFLINE(1, "离线");

    private static final Map<Integer, Connectivity> BY_CODE = new HashMap<>();
    private static final Map<String, Connectivity> BY_LABEL = new HashMap<>();
    static {
        for (Connectivity co : values()) {
            BY_CODE.put(co.code, co);
            BY_LABEL.put(co.label, co);
        }
    }

    private Integer code;
    private String label;

    public static Connectivity valueOfCode(Integer code) {
        return BY_CODE.get(code);
    }

    public static Connectivity valueOfLabel(String label) {
        return BY_LABEL.get(label);
    }
}
