//package com.ruoyi.common.utils;
//
//import cn.hutool.core.convert.Convert;
//import cn.hutool.core.date.DateUtil;
//import org.jfree.chart.ChartFactory;
//import org.jfree.chart.ChartPanel;
//import org.jfree.chart.ChartUtilities;
//import org.jfree.chart.JFreeChart;
//import org.jfree.chart.axis.DateAxis;
//import org.jfree.chart.block.BlockBorder;
//import org.jfree.chart.plot.XYPlot;
//import org.jfree.chart.title.TextTitle;
//import org.jfree.data.time.Minute;
//import org.jfree.data.time.TimeSeries;
//import org.jfree.data.time.TimeSeriesCollection;
//import org.jfree.data.xy.XYDataset;
//
//import javax.swing.*;
//import java.awt.*;
//import java.io.File;
//import java.util.Map;
//
//public class JFreeChartUtil extends JFrame {
//
//    /**
//     * @param startTime 统计开始时间
//     * @param endTime   统计结束时间
//     * @param data      数据
//     * @param path      生成图片地址
//     * @param name      产品名称和点位信息
//     */
//    public void initUI(String startTime, String endTime, java.util.List<Map<String, Object>> data, String path, String name) {
//        XYDataset dataset = createDataset(data);
//        JFreeChart chart = createChart(dataset, path, name, startTime, endTime);
//        ChartPanel chartPanel = new ChartPanel(chart);
//        chartPanel.setBorder(BorderFactory.createEmptyBorder(15, 15, 15, 15));
//        chartPanel.setBackground(Color.white);
//        add(chartPanel);
//
//        pack();
//        setTitle("Line chart");
//        setLocationRelativeTo(null);
//        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//        System.out.println(true);
//    }
//
//    //获取数据
//    private static XYDataset createDataset(java.util.List<Map<String, Object>> data) {
//        TimeSeriesCollection dataset = new TimeSeriesCollection();
//        //TODO 中文不显示
//        for (Map<String, Object> map : data) {
//            String name = (String) map.get("name");
//            java.util.List<Map<String, String>> dataValue = (java.util.List<Map<String, String>>) map.get("data");
//            TimeSeries series = new TimeSeries(name, Minute.class);
//            for (Map<String, String> mapData : dataValue) {
//                series.addOrUpdate(new Minute(DateUtil.parse(mapData.get("time"), "yyyy-MM-dd HH:mm:ss")), Convert.toInt(mapData.get("value")));
//            }
//            dataset.addSeries(series);
//        }
//        return dataset;
//    }
//
//    private static JFreeChart createChart(XYDataset dataset, String path, String name, String startTime, String endTime) {
//
//        JFreeChart chart = ChartFactory.createTimeSeriesChart(
//                name,
//                startTime + "-" + endTime,
//                "value",
//                dataset,
//                true,
//                true,
//                false
//        );
//
//        XYPlot plot = chart.getXYPlot();
//        //设置横纵坐标最大值，最小值
//        DateAxis domainAxis = (DateAxis) plot.getDomainAxis();
//        domainAxis.setRange(DateUtil.parse(startTime, "yyyy-MM-dd HH:mm:ss"), DateUtil.parse(endTime, "yyyy-MM-dd HH:mm:ss"));
//        plot.setBackgroundPaint(Color.white);
//
//        plot.setRangeGridlinesVisible(true);
//        plot.setRangeGridlinePaint(Color.BLACK);
//
//        plot.setDomainGridlinesVisible(true);
//        plot.setDomainGridlinePaint(Color.BLACK);
//
//        chart.getLegend().setFrame(BlockBorder.NONE);
//
//        chart.setTitle(new TextTitle(name,
//                        new Font("Serif", java.awt.Font.BOLD, 18)
//                )
//        );
//        try {
//            //E:\excel\line_chart.png
//            ChartUtilities.saveChartAsPNG(new File(path), chart, 500, 300);
//        } catch (Exception e) {
//            e.getMessage();
//        }
//        return chart;
//
//    }
//}
