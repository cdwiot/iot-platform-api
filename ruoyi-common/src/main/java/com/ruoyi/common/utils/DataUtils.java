package com.ruoyi.common.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections4.map.LinkedMap;

import lombok.extern.slf4j.Slf4j;

@SuppressWarnings("AlibabaLowerCamelCaseVariableNaming")
@Slf4j
public class DataUtils
{
    public static List<Map<String, Object>> KVtoList(Map<String, Object> data, Map<String, Object> options) {
        List<Map<String, Object>> inflated = new ArrayList<>();
        if (options == null) {
            options = new HashMap<>();
        }
        String keyName = (String)options.getOrDefault("keyName", "label");
        String valueName = (String)options.getOrDefault("valueName", "value");
        Set<String> keys = data.keySet();
        for (String key : keys) {
            Map<String, Object> item = new LinkedMap<>();
            item.put(keyName, key);
            item.put(valueName, data.get(key));
            inflated.add(item);
        }
        return inflated;
    }

    public static Map<String, Object> KVconvertKey(Map<String, Object> data, Map<String, Object> options) {
        Map<String, Object> converted = new HashMap<>();
        Set<String> keys = data.keySet();
        for (String key : keys) {
            String newKey = null;
            if (options.containsKey(key)) {
                newKey = (String)options.get(key);
            }
            if (newKey == null) {
                newKey = key;
            }
            converted.put(newKey, data.get(key));
        }
        return converted;
    }
}